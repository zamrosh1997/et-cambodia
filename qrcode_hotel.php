<?php
//include("includes/connect_db.php");
include('phpqrcode/qrlib.php');

/*Dara Airport Hotel
4
2015-2017
Ratana Plaza Building, Russian Boulevard, Sangkat Tek Tla, Khan Sen Sok, Phnom Penh
----------
Goldiana Angkor Hotel
3
2014-2016
National Road 6A, Sala Konsaeng Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap*/

$getkey = $_GET['key'];

$profiles = array(	
					'dara'=>array('name'=>'Dara Airport Hotel','star'=>'Four Star','validity'=>'2015-2017','address'=>'Ratana Plaza Building, Russian Boulevard, Sangkat Tek Tla, Khan Sen Sok, Phnom Penh'),
					'goldiana'=>array('name'=>'Goldiana Angkor Hotel','star'=>'Three Star','validity'=>'2014-2016','address'=>'National Road 6A, Sala Konsaeng Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'independence1'=>array('name'=>'Independence Hotel','star'=>'Four Star','validity'=>'2015-2017','address'=>'Street 2 Thnou, Phum 01, Sangkat 3, Sihanouk Ville, Kingdom of Cambodia'),
					'amansara1'=>array('name'=>'Amansara Princiere Resort','star'=>'Four Star','validity'=>'2015-2017','address'=>'Phum Boeng Daun Pa, Sangkat Slorkram, Siem Reap City, Siem Reap Province'),
					'miracle1'=>array('name'=>'Angkor Miracle Resort & Spa','star'=>'Five Star','validity'=>'2014-2016','address'=>'Phum Kasekam, Sangkat Srongae, Siem Reap City, Siem Reap Province'),
					'tara'=>array('name'=>'Tara Angkor Hotel','star'=>'Four Star','validity'=>'2015-2017','address'=>'Phum Mondol 3, Sangkat Slorkram, Siem Reap City, Siem Reap Province'),					
					'green'=>array('name'=>'Green Palace Hotel','star'=>'Three Star','validity'=>'2015-2017','address'=>'#61, St. 111 corner St. 232, Sangkat Boeung Prolit, Khan 7 Makara, Phnom Penh'),					
					'sokhalay'=>array('name'=>'Sokhalay Angkor Resort & Spa Hotel','star'=>'Five Star','validity'=>'2015-2017','address'=>'Street 6, Phum Kasekam, Sangkat Srongae, Siem Reap City, Siem Reap Province'),
					'almond'=>array('name'=>'ALMOND HOTEL','star'=>'Three Star','validity'=>'2015-2017','address'=>'#128 F 7-8-9-10, st. 3, Sangkat Tonle Bassac, Khan Chamkarmon, Phnom Penh'),					
					'lotus'=>array('name'=>'Lotus Blanc Resort Hotel','star'=>'Five Star','validity'=>'2015-2017','address'=>'Kruos Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
										
					'seventh'=>array('name'=>'Seventh Heaven Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'#209-211-213, St. 134, Sangkat Mittapheap, Khan 7 Makara, Phnom Penh'),					
					'howard'=>array('name'=>'Angkor Howard Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Street 6, Phum Kasekam, Sangkat Srongae, Siem Reap City, Siem Reap'),					
					'elephant'=>array('name'=>'Elephant Hill Resort','star'=>'Two Star','validity'=>'2016-2018','address'=>'#168, Monireth Blvd, Sangkat Tumnup Teuk, Khan Chamkarmon, Phnom Penh'),	
									
					'casa'=>array('name'=>'Casa Hotel','star'=>'Two Star','validity'=>'2016-2018','address'=>'#39, St. 47, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh'),
					
					'glorious'=>array('name'=>'Glorious Hotel & Spa','star'=>'Four Star','validity'=>'2016-2018','address'=>'National Road 6, Phum Balang Khang Lech, Sangkat Domrey Cheon Kla, Krong Steung Saen, Kampong Thom'),
					
					'era'=>array('name'=>'Angkor Era Hotel','star'=>'Five Star','validity'=>'2015-2017','address'=>'National Road 6, Khnar Village, Sangkat Chreav, Siem Reap City, Siem Reap Province'),
					
					'viroths'=>array('name'=>'Viroth’s Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'Taksen Tboung Village, Sangkat Kok Chok, Siem Reap City, Siem Reap Province'),
					
					'pursat'=>array('name'=>'Pursat Century Hotel','star'=>'Two Star','validity'=>'2016-2018','address'=>'National Road 5, Peal Nhek 1 Village, Sangkat Phteah Prey, Pursat City, Pursat Province'),
					'almond_bassac'=>array('name'=>'Almond Bassac River Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'#2, St. Sorbet, Sangkat Tonle Bassac, Khan Chamkarmon, Phnom Penh'),
					'thansuor'=>array('name'=>'Than Suor Thmei Hotel','star'=>'One Star','validity'=>'2016-2018','address'=>'St. 2, Peal Nhek 1 Village, Sangkat Phteah Prey, Pursat City, Pursat Province'),
					
					'raffles'=>array('name'=>'Raffles Hotel Le Royal','star'=>'Five Star','validity'=>'2016-2018','address'=>'#92, Rukhak Vithei Daun Penh, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh'),
					
					'riviera'=>array('name'=>'Angkor Riviera Hotel','star'=>'Four Star','validity'=>'2015-2017','address'=>'#828, St. Porkombor, Phum Mondul 1, Sangkat Svaydangkom, Siem Reap Province'),
					
					
					'khemara'=>array('name'=>'Khemara Angkor Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6, Sala Konsaeng 3 Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'city'=>array('name'=>'City Angkor','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6, Kruos Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'sokha_angkor'=>array('name'=>'Sokha Angkor Resort Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Road 6, Taphul Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'km'=>array('name'=>'K M Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'road 101, Peal Nhek 1 Village, Sangkat Phteah Prey, Pursat City, Pursat Province'),
					'majestic'=>array('name'=>'Majestic Oriental Hotel','star'=>'Four Star','validity'=>'2015-2017','address'=>'Kruos Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'dragon'=>array('name'=>'Dragon Royal Hotel','star'=>'Four Star','validity'=>'2015-2017','address'=>'Kruos Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					
					'naga'=>array('name'=>'Naga World Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Naga World Building, Samdech Techo Hun Sen Park, Phnom Penh'),
					'paradise'=>array('name'=>'Paradise Angkor Villa Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'St. 60, Chong Kao Sou Village, Sangkat Slorkram, Siem Reap City, Siem Reap Province'),
					'damrie'=>array('name'=>'Damrei Angkor Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'St. 20, Wat Bo Village, Sangkat Salakomroek, Siem Reap City, Siem Reap Province'),
					
					'starry'=>array('name'=>'Starry Angkor Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'#G4, Road 6, Banteay Chas Village, Sangkat Slorkram, Siem Reap City, Siem Reap Province'),
					'princess'=>array('name'=>'Princess Angkor Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6, Kruos Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'borei'=>array('name'=>'Borei Angkor Resort & Spa','star'=>'Five Star','validity'=>'2016-2018','address'=>'#0371, Road 6, Banteay Chas Village, Sangkat Slorkram, Siem Reap City, Siem Reap Province'),
					'empire'=>array('name'=>'Royal Empire Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Road 6, Kruos Village, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					
					
					'classy'=>array('name'=>'Classy Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'St. 207, Romjek Village, Sangkat Rattanak, Battambang'),
					'stung'=>array('name'=>'Stung Sangke Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'National Road 5, Prekmorhatep Village, Sangkat Svaypor, Battambang City, Battambang'),
					'khemara1'=>array('name'=>'Khemara Battambang 1 Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'#611, St. 515, Sangkat Slaket, Battambang City, Battambang'),
					'kingfy'=>array('name'=>'King Fy Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'#306, St. 155, Romjek 5 Village, Sangkat Rattanak, Battambang City, Battambang'),
					'emeraldbb'=>array('name'=>'Emerald BB Battambang Hotel','star'=>'Two Star','validity'=>'2016-2018','address'=>'St. 207, Romjek 4 Village, Sangkat Rattanak, Battambang City, Battambang'),
					
					'meridien'=>array('name'=>'Le Meridien Angkor Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Phum Tropang Ses, Sangkat Kokchak, Siem Reap City, Siem Reap Province'),
					'dangkor'=>array('name'=>'Prince D’Angkor Hotel & Spa','star'=>'Five Star','validity'=>'2016-2018','address'=>'Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'sokha_sr'=>array('name'=>'Sokha Siem Reap Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'St. 60, Phum Trang, Sangkat Slorkram, Siem Reap City, Siem Reap Province'),
					
					'holiday'=>array('name'=>'Holiday Villa Nataya Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'#502, Phoum 4, Sangkat 4, Sihanoukville'),
					'grand'=>array('name'=>'Grand Seagull Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'St. Lomhae Phumin, Phoum 4, Sangkat 4, Sihanoukville'),
					'punleu'=>array('name'=>'Punleu Reah Thmey Hotel','star'=>'Two Star','validity'=>'2016-2018','address'=>'St. 23 Tola, Phoum 4, Sangkat 4, Sihanoukville'),					
					'thalias'=>array('name'=>'Thalias "TOPAZ" Restaurant','star'=>'Five Star','validity'=>'2016-2018','address'=>'#162, Norodom Blvd, Sangkat Tonle Bassac, Khan Chamkarmorn, Phnom Penh'),
					'chhne'=>array('name'=>'Chhne Meas Restaurant','star'=>'Three Star','validity'=>'2016-2018','address'=>'Plov Krung, Phoum 3, Sangkat 3, Sihanoukville'),
					'treasure'=>array('name'=>'Treasure Island Restaurant','star'=>'Three Star','validity'=>'2016-2018','address'=>'St. 2 Thnou, Phoum 1, Sangkat 3, Sihanoukville'),
					'norkorsamreth'=>array('name'=>'Norkorsamreth Ochheuteal Karaoke','star'=>'2 Kesorkoul Flower','validity'=>'2016-2018','address'=>'St. 23 Tola, Phoum 4, Sangkat 4, Sihanoukville'),
					'garden'=>array('name'=>'Garden City Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Krol Ko village, Sombuo Meas Commune, Muk Kompol district, Kandal Province'),
					
					'blue'=>array('name'=>'Blue River Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6A, Phum Kean Klang, Sangkat Prek Leab, Khan Chroy Changvar, Phnom Penh'),
					
					'ffc'=>array('name'=>'FCC Angkor Hotel','star'=>'Three Star','validity'=>'2016-2018','address'=>'St. Porkombor, Phum Mondul 2, Sangkat Svay Dongkum,  Siem Reap City, Siem Reap'),
					'lin'=>array('name'=>'Lin Ratanak Angkor Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6, Phum Bonteayjas, Sangkat Slorkram, Siem Reap City, Siem Reap'),
					'saem'=>array('name'=>'Saem Siem Reap Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Phum Sala Konsaeng, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'pphotel'=>array('name'=>'Phnom Penh Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'#53, Monivong Blvd., Khan Daun Penh, Phnom Penh'),
					
					'pacific'=>array('name'=>'Pacific Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'Road 6, Phum Kaksekam, Siem Reap City, Siem Reap'),
					'reangsey'=>array('name'=>'Dara Reang Sey Angkor Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'Road 6, Phum chungkao, Sangkat Slorkram, Siem Reap City, Siem Reap'),
					'md'=>array('name'=>'MD Boutique Hotel','star'=>'Three Star','validity'=>'2017-2019','address'=>'Phum Boeng Daun Pa, Sangkat Slorkram, Siem Reap City, Siem Reap'),					
					'sofitel_sr'=>array('name'=>'Sofitel Angkor Phokeethra Golf & Spa Resort','star'=>'Five Star','validity'=>'2016-2018','address'=>'St. Charles de Gaulle, Phum Mondol 3, Sangkat Slorkram, Siem Reap City, Siem Reap'),		
								
					'momo'=>array('name'=>'Elite Food (Mo Mo Paradise)','star'=>'Four Star','validity'=>'2017-2019','address'=>'219 Pasteur St. (51), Sangkat Boeung Keng Kang 1, Khan Chamkarmon, Phnom Penh'),		
								
					'shinta'=>array('name'=>'Shinta Mani Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'St. Umkhun, Phum Mondol 1, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'parklane'=>array('name'=>'Parklane Hotel','star'=>'Two Star','validity'=>'2016-2018','address'=>'Phum Ta Phul, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'smiling'=>array('name'=>'Smiling Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6, Phum chungkaosu, Sangkat Slorkram, Siem Reap City, Siem Reap'),
					'angkor1'=>array('name'=>'Angkor Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'Road 6, Phum Sala Konsaeng, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'miracle2'=>array('name'=>'Angkor Miracle Resort & Spa','star'=>'Five Star','validity'=>'2016-2018','address'=>'Road 6, Phum Kasekam, Sangkat Srongae, Siem Reap City, Siem Reap'),
					'royal'=>array('name'=>'Royal Angkor Resort Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Road 6, Phum Kasekam, Sangkat Srongae, Siem Reap City, Siem Reap'),
					
					'mont'=>array('name'=>'Auberge Mont Royal Hotel','star'=>'Two Star','validity'=>'2017-2019','address'=>'St. Taphul, Phum Taphul, Svay Dongkum, Siem Reap City, Siem Reap'),
					'tara2'=>array('name'=>'Tara Angkor Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'Charles De Gaulle, Slorkram, Siem Reap City, Siem Reap'),	
					'vina'=>array('name'=>'Hotel Vina Golf Angkor','star'=>'Four Star','validity'=>'2016-2018','address'=>'St. 6, Kruos, Svay Dongkum, Siem Reap City, Siem Reap'),
					'belmond'=>array('name'=>'Belmond La Residence D`angkor Hotel','star'=>'Five Star','validity'=>'2017-2019','address'=>'Phum Wat Bo, Salakomroek, Siem Reap City, Siem Reap'),
					'angkor_paradise'=>array('name'=>'Angkor Paradise Hotel','star'=>'Five Star','validity'=>'2017-2019','address'=>'St. 6, Phum Taphul, Svay Dongkum, Siem Reap City, Siem Reap'),
					'bokor'=>array('name'=>'Thansur Bokor Hotel','star'=>'Five Star','validity'=>'2017-2019','address'=>'Bokor Mountain, Teuk Chhou District, Kampot'),
					
					'regency'=>array('name'=>'Regency Angkor Hotel','star'=>'Five Star','validity'=>'2016-2018','address'=>'Charles De Gaulle, Phum Mondol 3, Slorkram, Siem Reap City, Siem Reap'),
					
					'independence2'=>array('name'=>'Independence Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'Phum 1, Sangkat 3, Sihanouk Ville'),
					'angkor2'=>array('name'=>'Angkor Hotel','star'=>'Four Star','validity'=>'2016-2018','address'=>'Road 6, Phum Sala Konsaeng, Sangkat Svay Dongkum, Siem Reap City, Siem Reap'),
					'nataya'=>array('name'=>'Hotel Nataya Resort','star'=>'Four Star','validity'=>'2017-2019','address'=>'Prek Ampil, Koh Tauch , Toeuk Chhou, Kampot'),
					
					'embassy'=>array('name'=>'Embassy Place Hotel Apartment','star'=>'Three Star','validity'=>'2017-2019','address'=>'St. preah norodom, boeung keng kang 1, chamkarmon, Phnom Penh'),
					'angkor_palace'=>array('name'=>'Angkor Palace Resort & Spa Hotel','star'=>'Five Star','validity'=>'2017-2019','address'=>'Road 6, Phum Kruos, Siem Reap City, Siem Reap'),
					'amansara2'=>array('name'=>'Amansara Princiere Resort Hotel','star'=>'Four Star','validity'=>'2017-2019','address'=>'Phum Boeng Daun Pa, Sangkat Slorkram, Siem Reap City, Siem Reap Province')
					
				);

//var_dump($profiles['viroth']);

	$txt = "Hotel Name: ".$profiles[$getkey]['name'].PHP_EOL.
			"Standard of: ".$profiles[$getkey]['star'].PHP_EOL.
			"Validity: ".$profiles[$getkey]['validity'].PHP_EOL.
			"Address: ".$profiles[$getkey]['address'];	
			
			//echo $txt;
	QRcode::png($txt,
				$outfile = false,
				$level = QR_ECLEVEL_L,
				$size = 4,
				$margin = 0,
				$saveandprint = false 
				);	

?>