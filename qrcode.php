<?php
include("includes/connect_db.php");
include('phpqrcode/qrlib.php');

$cmd = $_GET['cmd'];
$id = decodeString($_GET['id'],$encryptKey);

if($cmd=='refreshment'){
	$regData = qry_arr("user_id,course_id","tblcourseregister","id=$id LIMIT 1");
	$courseData = qry_arr("area_id,register_date,course_end_date,total_hours","tblcourseschedule","id=".$regData['course_id']." LIMIT 1");
	$schoolData = qry_arr("code,firstName,lastName,firstNameEn,lastNameEn","tblusers","area_id='".$courseData['area_id']."' and type=(select id from user_role where code='school' limit 1) LIMIT 1");
	$userData = qry_arr("id,code,firstName,lastName,firstNameEn,lastNameEn","tblusers","id='".$regData['user_id']."' LIMIT 1");
	
	$fullnameKh = $userData['lastName'].' '.$userData['firstName'];	
	$fullnameEn = $userData['lastNameEn'].' '.$userData['firstNameEn'];	
	$profile_url = 'http://et.cambodia-touristguide.com/l.php?id='.$userData['id'].'&ref=qrcode';
	
	$reg_region = singleCell_qry("displayTitle","tblsubcategory","id=".$courseData['area_id']." limit 1");
	
	$course_start_kh = khmerDate($courseData['register_date']);
	$course_end_kh = khmerDate($courseData['course_end_date']);
	
	$score = avgScore($regData['course_id'],$id);
	
	$txt = 'Name: '.$fullnameEn.PHP_EOL.
			'Last Refreshment Course: '.date("d-F-Y",strtotime($courseData['register_date'])).PHP_EOL.
			'School: Phom Penh Hotel and Tourism School'.PHP_EOL.
			'Profile: '.$profile_url;	
	QRcode::png($txt,
				$outfile = false,
				$level = QR_ECLEVEL_L,
				$size = 3,
				$margin = 0,
				$saveandprint = false 
				);			
}elseif($cmd=='training'){
	$guideData = qry_arr("fname_en,lname_en,gender,language_id,exam_id,score_avg,score_grade","ng_profile","id=$id LIMIT 1");
	$fullname_en = $guideData['lname_en'].' '.$guideData['fname_en'];
	if($guideData['gender']=='m'){$gender='Male';}else{$gender='Female';}
	$language = singleCell_qry("name_en","tbllanguages","id=".$guideData['language_id']." LIMIT 1");
	$examData = qry_arr("exam_date,school_id","ng_exam_result","id=".$guideData['exam_id']." LIMIT 1");
	$schoolname = singleCell_qry("name_en","ng_school","id=".$examData['school_id']." LIMIT 1");
	$Profile_url = 'http://et.cambodia-touristguide.com/profile/training/'.encodeString($id,$encryptKey).'?ref=qr';
	$txt = 'Name: '.$fullname_en.PHP_EOL.
			'Gender: '.$gender.PHP_EOL.
			'Language: '.$language.PHP_EOL.
			/*'Mark: '.$guideData['score_avg'].PHP_EOL.
			'Grade: '.$guideData['score_grade'].PHP_EOL.
			'Result Date: '.date("d/m/Y",strtotime($examData['exam_date'])).PHP_EOL.
			'School: '.$schoolname.PHP_EOL.*/
			'Profile: '.$Profile_url;
	QRcode::png($txt,
				$outfile = false,
				$level = QR_ECLEVEL_L,
				$size = 4,
				$margin = 0,
				$saveandprint = false 
				);		
}elseif($cmd=='translator'){
	$translatorData = qry_arr("course_id,fname_en,lname_en,gender,nationality_id,dob,passport,mobile","translator_profile","id=$id LIMIT 1");
	$fullname_en = $translatorData['lname_en'].' '.$translatorData['fname_en'];
	if($translatorData['gender']=='m'){$gender='M';}else{$gender='F';}
	$nationality = singleCell_qry("nationality","translator_nationality","id=".$translatorData['nationality_id']." LIMIT 1");
	$Profile_url = 'http://et.cambodia-touristguide.com/profile/translator/'.encodeString($id,$encryptKey).'?ref=qr';
	
	//course info
	$courseinfo = qry_arr("school_id,course_title,start_date,end_date","training_course","id=".$translatorData['course_id']." limit 1");
	$schoolname = singleCell_qry("name_en","ng_school","id=".$courseinfo['school_id']." LIMIT 1");
	
	/*$txt = 'Full Name: '.$fullname_en.PHP_EOL.
			'Sex: '.$gender.PHP_EOL.
			'Date of Birth: '.date("d/M/Y",strtotime($translatorData['dob'])).PHP_EOL.
			'Passport: '.$translatorData['passport'].PHP_EOL.
			'Training: '.$courseinfo['course_title'].PHP_EOL.
			'Date of Training: '.date("d/M/Y",strtotime($courseinfo['start_date'])).' - '.date("d/M/Y",strtotime($courseinfo['end_date'])).PHP_EOL.
			'School: '.$schoolname.PHP_EOL.
			'Profile: '.$Profile_url;*/
			
	$txt = 'Name: '.$fullname_en.PHP_EOL.
			'Sex: '.$gender.PHP_EOL.
			/*'DOB: '.date("d/m/Y",strtotime($translatorData['dob'])).PHP_EOL.*/
			'Profile: '.$Profile_url;
					
	QRcode::png($txt,
				$outfile = false,
				$level = QR_ECLEVEL_L,
				$size = 3,
				$margin = 0,
				$saveandprint = false 
				);		
}

?>