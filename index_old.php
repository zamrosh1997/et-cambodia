<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$pageName='Homepage | Tourist Guide Refreshment Course';
$pageCode='Homepage';
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<?php include("includes/intro_progress.php"); ?>
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-lg-8 col-md-8 col-sm-8">
		<div class="well">
        	<?php
			
			$systemInfo = singleCell_qry("description","tblsubcategory"," title='system_description' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
			echo $systemInfo;
			
			?>
		</div>
        <div class="widget">
                <h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> មុខវិជ្ជានៃវគ្គវិក្រិត្យការ មគ្គុទេ្ទសក៍ទេសចរណ៍</h4>
				<hr />
                  <div class="widget-content">

                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>មុខវិជ្ជា</th>
                          <th style="width:120px;"></th>
                        </tr>
                      </thead>
                      <tbody>
                      
						
                        <?php
						$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY publishedDate DESC LIMIT 10");
						while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
							echo '<tr>
								  <td class="tableCellLeftCenter"><a href="/lesson/'.encodeString($lesson_row['id'],$encryptKey).'">'.$lesson_row['title'].'</a></td>
								  <td class="tableCellCenter">
								  	<a href="/lesson/'.encodeString($lesson_row['id'],$encryptKey).'"><span class="label label-info" title="ចុចដើម្បីចូលអាច">ទាញយក</span></a>
								  	<a href="/exam/'.encodeString($lesson_row['id'],$encryptKey).'"><span class="label label-info" title="ចុចដើម្បីប្រលង">ធ្វើតេស្ត</span></a></td>
								</tr>';	
						}
						
						?>                                                          

                      </tbody>
                    </table>
					
                    
                  </div>

        </div>
        
        <div class="well">
        	<?php
			$schoolmap = qry_arr("displayTitle,description","tblsubcategory"," title='school_map' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
			?>
        	<h5><?=$schoolmap['displayTitle']?></h5>
            <?=$schoolmap['description']?>
		</div>
        
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

</body>
</html>