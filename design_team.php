<?php 

	include("includes/connect_db.php");
	// include("includes/checkSession.php");


	session_start(); //Start the session
	ob_start();
	//if(!isset($_SESSION['userid'])){ //If session not registered
	  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
	//}

	if(!isset($_SESSION['userid'])){
	  $userid = 0;
	  //echo json_encode(array(false));
	  //exit;
	}else{$userid = $_SESSION['userid'];}

	$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

	// ========= End Session ============ //

	$pageName='Homepage | Tourist Guide Refreshment Course';
	$pageCode='Homepage';
	?>
	<?php include("includes/header.php");?>
	<?php include("includes/css_script.php");?>
	<?php include("includes/header_menu.php");?>


	<style>
		.container{
			/*margin-top: 40px;*/
			margin-bottom: 20px;
		}
		p{
			font-size: 15px;
			line-height: 25px;
		}
		h3>b{
			font-size: 20px;
		}
		h5{
			font-size: 18px;
		}
		p>b{
			font-size:18px;
		}
	</style>
	<!-- Content -->
	<div class="container">
		<div class="row">
			<div class="col-md-10">

			<center>
				<h3><b>ក្រុមការងាររៀបចំ</b></h3><br>

					<h5>ដឹកនាំ និង គ្រប់គ្រងការងារ</h5>
					<p><b>ឯកឧត្តម ណេប សាមុត </b> អគ្គនាយកគ្រប់គ្រងឧស្សាហកម្មទេសចរណ៍</p>
			</center>
				<br /><br /><br />
				<h3><b>១. ផ្នែកខ្លឹមសារ ៖</b></h3>
				<br /><br />

				<p>១. <b>ឯកឧត្តម ច្រឡឹង សុមេធា</b> 	អគ្គលេខាធិការរងគ.ជ.វ.ទ និង អគ្គនាយករងគ្រប់គ្រងឧស្សាហកម្ម 
					ទេសចរណ៍
				</p>
				<p>២ .  លោក <b>វ៉ាត រ៉ាន់វីរៈ</b> អនុប្រធាននាយកដ្ឋានគ្រប់គ្រងសេវាកម្មកម្សាន្ត និងកីឡាទេសចរណ៍</p>					
				<p>៣.  លោកស្រី <b>ហៀង លីហួរ</b>មន្ត្រីនាយកដ្ឋានសេវាគាំពារទីក្រុងស្អាត និងជំរុញការប្រឡងប្រណាំង</p>
				<p>៤. លោក​<b> ផេងស៊ីថា </b>(ព្រឹទ្ធបុរសមហាវិទ្យាល័យបុរាណវិទ្យា)</p>	
       			<p>៥.   លោកស្រី <b>តែ ថារី</b>	មន្ត្រីជំនួយការ</p>	
      			<p>៦.   កញ្ញា <b>ហុកសេង គឹមយ៉េក</b>	មន្ត្រីជំនួយការ</p>				
				<p>៧.   លោក <b>អ៊ួក គង់	</b>មន្ត្រីជំនួយការ</p>
				<p>៨.  លោក  <b>សាវី អារីណា</b> មន្ត្រីជំនួយការ</p>	
				<p>៩.   កញ្ញា 	<b>អ៊ូ រ៉ានី</b> មន្ត្រីជំនួយការ</p>
				 	
				<br /><br />
				

				<h3><b>២. ផ្នែកបច្ចេកទេសព័ត៍មានវិទ្យា ៖	</b></h3>
				<br /><br />
				<p>១.  ឯកឧត្តម <b>ឆាយ ឃុនឡុង</b> ទីប្រឹក្សា និង អគ្គនាយករងគ្រប់គ្រងឧស្សាហកម្មទេសចរណ៍</p>
				<p>២. លោក <b>នៅ បញ្ញា</b> ជំនួយការអគ្គនាយកគ្រប់គ្រងឧស្សាហកម្មទេសចរណ៍</p>
				<p>៣. លោក <b>យ៉ែម បណ្តូល</b>	 អ្នកជំនាញបច្ចេកវិទ្យាក្រុមហ៊ុន GIGB</p>
				<p>៤. លោក <b>សោ សំណាង</b>	អ្នកជំនាញបច្ចេកវិទ្យាក្រុមហ៊ុន GIGB</p>
				<p>៥. លោក <b>សុខ បញ្ញាត្តិ</b> មន្ត្រីជំនួយការ</p>
				<p>៦. លោក <b>អាត អូនស្រស់</b>	 មន្ត្រីជំនួយការ</p>

			</div>
		</div>
		

	</div>


	<!-- CTA Ends -->
	<?php include("includes/subscription.php"); ?>
	<?php include("includes/footer.php"); ?>
	<?php include("includes/script.php"); ?>



	

