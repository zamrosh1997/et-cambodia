<?php
include("includes/connect_db.php");

$reg_id = decodeString($_GET['reg_id'],$encryptKey);
$regData = qry_arr("user_id,course_id","tblcourseregister","id=$reg_id LIMIT 1");
$courseData = qry_arr("area_id,register_date,course_end_date,total_hours","tblcourseschedule","id=".$regData['course_id']." LIMIT 1");
$schoolData = qry_arr("code,firstName,lastName,firstNameEn,lastNameEn","tblusers","area_id='".$courseData['area_id']."' and type=(select id from user_role where code='school' limit 1) LIMIT 1");
$userData = qry_arr("code,firstName,lastName,firstNameEn,lastNameEn,language_id,gender,dob","tblusers","id='".$regData['user_id']."' LIMIT 1");

$fullnameKh = $userData['lastName'].' '.$userData['firstName'];	
$fullnameEn = $userData['lastNameEn'].' '.$userData['firstNameEn'];	

$reg_region = qry_arr("displayTitle,title","tblsubcategory","id=".$courseData['area_id']." limit 1");
$language = qry_arr("name_kh,name_en","tbllanguages","id=".($userData['language_id']==NULL?0:$userData['language_id'])." limit 1");


$score = avgScore($regData['course_id'],$reg_id);

?>
<!-- Page heading starts -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title><?=$fullnameKh?> | វិញ្ញាបនបត្រ វិក្រឹត្យការបគ្គុទ្ទេសក៍ទេសចរណ៍</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content=""><link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Favicon -->
  <link rel="shortcut icon" href="/img/favicon/favicon.png">
  <link href="/style/style.css" rel="stylesheet">
  
   <!-- jQuery -->
</head>
<body style="border:0; margin:0; padding:0;">  
<!--
            
    <span id="cert_fullname_kh" class="cert_txt">អ៊ុង សិរីវុធ</span>
    <span id="cert_fullname_en" class="cert_txt">Ung Sereyvuth</span>
    <span id="cert_gender" class="cert_txt">ប្រុស</span>
    <span id="cert_dob" class="cert_txt">១២ មករា ១៩៨៩</span>
    <span id="cert_pob_vil" class="cert_txt">រមន់</span>
    <span id="cert_pob_com" class="cert_txt">សំរោង</span>
    <span id="cert_pob_dis" class="cert_txt">សំរោង</span>
    <span id="cert_pob_pro" class="cert_txt">តាកែវ</span> -->
<!--<button id="btn_print">Print</button>-->
<div id="print_block" style="left:0; right:0; margin:auto;width:800px; height:1133px;">
    <div style="width:800px; height:1133px; background:url(http://et.cambodia-touristguide.com/documents/certificate_V14_1446259999.jpg) no-repeat center center; background-size:contain;">   
         
    <div id="cert_fullname_kh" class="cert_txt cert_font_kh"><span><?=$fullnameKh?></span></div>  
    <div id="cert_sex_kh" class="cert_txt cert_font_kh"><span><?=$userData['gender']=='m'?'ប្រុស':'ស្រី'?></span></div>
    <div id="cert_dob_d_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($userData['dob'],'d')?></span></div>
    <div id="cert_dob_m_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($userData['dob'],'m')?></span></div>
    <div id="cert_dob_y_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($userData['dob'],'y')?></span></div>
    <div id="cert_language_kh" class="cert_txt cert_font_kh"><span><?=$language['name_kh']?></span></div>
    <div id="cert_area_kh" class="cert_txt cert_font_kh"><span><?=$reg_region['displayTitle']?></span></div>    
    <div id="cert_hours_kh" class="cert_txt cert_font_kh"><span><?=enNum_khNum($courseData['total_hours'])?></span></div>
    <div id="cert_from_d_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($courseData['register_date'],'d')?></span></div>
    <div id="cert_from_m_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($courseData['register_date'],'m')?></span></div>
    <div id="cert_to_d_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($courseData['course_end_date'],'d')?></span></div>
    <div id="cert_to_m_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($courseData['course_end_date'],'m')?></span></div>
    <div id="cert_to_y_kh" class="cert_txt cert_font_kh"><span><?=khmerDate($courseData['course_end_date'],'y')?></span></div>
    <!--<div id="cert_grade_kh" class="cert_txt cert_font_En"><span><?=$score['grade']?></span></div>-->
    
    <div id="cert_fullname_en" class="cert_txt cert_font_En"><span><?=ucfirst($fullnameEn)?></span></div>  
    <div id="cert_sex_en" class="cert_txt cert_font_En"><span><?=strtoupper($userData['gender'])?></span></div>
    <div id="cert_dob_en" class="cert_txt cert_font_En"><span><?=date("d-F-Y",strtotime($userData['dob']))?></span></div>
    <div id="cert_language_en" class="cert_txt cert_font_En"><span><?=$language['name_en']?></span></div>
    <div id="cert_area_en" class="cert_txt cert_font_En"><span><?=ucwords($reg_region['title'])?></span></div>    
    <div id="cert_hours_en" class="cert_txt cert_font_En"><span><?=$courseData['total_hours']?></span></div>
    <div id="cert_from_en" class="cert_txt cert_font_En"><span><?=date("d-F-Y",strtotime($courseData['register_date']))?></span></div>
    <div id="cert_to_en" class="cert_txt cert_font_En"><span><?=date("d-F-Y",strtotime($courseData['course_end_date']))?></span></div>
    <!--<div id="cert_grade_en" class="cert_txt cert_font_En"><span><?=$score['grade']?></span></div>-->
    
    <div id="cert_qrcode" class="cert_txt"><span><img src="http://et.cambodia-touristguide.com/qrcode/refreshment/<?=encodeString($reg_id,$encryptKey)?>" width="100" height="100" /></span></div>
    
    </div>
</div>
<script src="/js/jquery.js"></script>
<script src="/js/functions.js"></script>
<script>

$(document).ready(function(e) {
	$("#btn_print").click(function(){PrintElemCert("#print_block","Print Test");});	
});


</script>
</body>
</html>