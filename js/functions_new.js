// JavaScript Document
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/*function switchMode(mode){
	if(mode=='mobile'){$.cookie('toMobile', true);}else{$.cookie('toMobile', false);}	
	location.reload();
}*/

function findBootstrapEnvironment() {
    var envs = ["xs", "sm", "md", "lg"],    
        doc = window.document,
        temp = doc.createElement("div");
    doc.body.appendChild(temp);
    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];
        temp.className = "hidden-" + env;
        if (temp.offsetParent === null) {
            doc.body.removeChild(temp);
            return env;
        }
    }
    return "";
}
function hideSaveMsg(id){
	$("#"+id).animate({'opacity':0},500,function(){
		$("#"+id).animate({'height':0,'margin-bottom':0,'padding':0},500,function(){
			$("#"+id).removeClass('alert');
			$("#"+id).removeClass('alert-warning');
			$("#"+id).removeClass('alert-success');	
			$("#"+id).removeClass('alert-danger');
			$("#"+id).html('');	
		});
	});	
}

function iniDisplaySaveMsg(id){	
	$("#pageLoadingStatus").removeClass('isHiden');	

	var eleName = $("#"+id+"_msg");
	eleName.removeClass('alert-warning');
	eleName.removeClass('alert-success');	
	eleName.removeClass('alert-danger');
	
	eleName.css({'height':57,'margin-bottom':20,'padding':'15px 35px 15px 15px'});
	eleName.animate({'opacity':1},500);
	eleName.addClass('alert alert-info');
	eleName.html('<span><img src="/admin/images/loading.gif" width="25" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
}

function displaySaveMsg(id,msgType,text){
	$("#pageLoadingStatus").addClass('isHiden');	

	var eleName = $("#"+id+"_msg");
	var msgImg = '';
	if(msgType=='info' || msgType=='warning'){msgImg = 'notice-info.png';}else if(msgType=='success'){msgImg = 'notice-success.png';}else if(msgType=='danger'){msgImg = 'notice-error.png';}
	eleName.removeClass('alert-info');
	eleName.addClass('alert-'+msgType+' alert-dismissable');
	eleName.html('<button type="button" class="close" onclick="hideSaveMsg(\''+id+'_msg\');">&times;</button><span><img src="/admin/images/'+msgImg+'" width="25" />&nbsp;&nbsp;&nbsp;'+text+'</span>');
}

function showHideDiv(boxName){
	var box_btn = $("#"+boxName+"_btn"),box_body = $("#"+boxName+"_body");
	if(box_body.is(":visible")){
		box_btn.attr("data-gh",box_body.height());
		box_body.animate({'height':0,opacity:0},200,function(){box_body.css({'display':'none'});});
		//box_body.css({'display':'none'});
		if($("#"+boxName+"_btn img").length){$("#"+boxName+"_btn img").css({'display':'inline-block'});}
	}else{
		box_body.css({'display':'block'});
		box_body.animate({'height':box_btn.data('gh'),opacity:1},200);
		//box_body.css({'display':'block'});
		if($("#"+boxName+"_btn img").length){$("#"+boxName+"_btn img").css({'display':'none'});}
	}
}

function PrintElem(elem,title){Popup($(elem).html(),title);}

function Popup(data,title) {
        var mywindow = window.open('', title, 'height=400,width=600');
        mywindow.document.write('<html><head><title>'+title+'</title>');
        /*optional stylesheet*/ 
		mywindow.document.write('<link href="/style/bootstrap.css" rel="stylesheet"> <link rel="stylesheet" href="/style/font-awesome.css"> <link href="/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> <link href="/style/style.css" rel="stylesheet"> <link href="/style/blue.css" rel="stylesheet">');
        mywindow.document.write('</head><body >');
        mywindow.document.write('<h4>'+title+'</h4><br />'+data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
}

function PrintElemCert(elem,title){PopupCert($(elem).html(),title);}

function PopupCert(data,title) {
        var mywindow = window.open('', title, 'height=400,width=600');
        mywindow.document.write('<html><head><title>'+title+'</title>');
        /*optional stylesheet*/ 
		mywindow.document.write('<link href="/style/style.css" rel="stylesheet">');
        mywindow.document.write('</head><body style="border:0; margin:0; padding:0;">');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
}

var confirmDialog_timeout = 0,dialog_wait=0;
function confirmDialog(modal,headerTxt,bodyTxt,mainBtnName,data,funcName){
	clearTimeout(confirmDialog_timeout);	
	confirmDialog_timeout = setTimeout(function(){
		$("#"+modal+"_modalLabel").html(headerTxt);
		$("#"+modal+"_modalLabelBodyText").html(bodyTxt);
		$("#"+modal+"_actionBtn").html(mainBtnName);
		$("#"+modal+"_confirmData").val(data);	
		$( "#"+modal+"_btn" ).trigger( "click" );
	},dialog_wait);
	
	$("#"+modal+"_actionBtn").unbind('click');
	$("#"+modal+"_actionBtn").click(function(){window[funcName](data);});
}

function popupMsg(modal,headerTxt,bodyTxt){
	$("#"+modal+"_modalLabel").html(headerTxt);
	$("#"+modal+"_modalLabelBodyText").html(bodyTxt);
	$( "#"+modal+"_btn" ).trigger( "click" );
}

function popupSearchguide(){
	$( "#searchguide_btn" ).trigger( "click" );
}

function checkLogin(){ 
	//$("#login_status_msg_cover").html('<div id="msg_loading"><div>Authenticating...</div></div>');
	$("#login_btn").html('<img src="/img/loading.gif" width="16" /> ចូល');
	$.post("/login/check",{username: $("#username").val(),password:$("#password").val()} ,function(data){
		//var data = JSON.parse(data);
		if(data==1){			
			var gotoNext = getParameterByName('next');
			//$("#login_status_msg_cover").html('<div id="msg_success"><div>Login Success</div></div>');
			//$("#login_status_msg_cover").html('<div id="msg_success"><div>Loging in...</div></div>');
			if(gotoNext==''){window.location.href="/index_after_login/111"}else{window.location.href=gotoNext;}
			$("#login_btn").html('<i class="fa fa-key fa-fw"></i> ចូល');
		}else{
			//$("#login_status_msg_cover").html('<div id="msg_error"><div>Invalid Username or Password</div></div>');
			//alert('invalid');
			popupMsg('yesno','<i class="fa fa-user fa-fw"></i> គណនី','<span style="color:red;"><i class="fa fa-times fa-fw"></i>ឈ្មោះគណនី ឬពាក្យសម្ងាត់មិនត្រឹមត្រូវ!</span>');
			$("#login_btn").html('<i class="fa fa-key fa-fw"></i> ចូល');
		}
	});
}

function exportData(listName,sql){
	$("#btn_print_excel").html('<img src="/admin/images/loading.gif" height="15" />');
	$.post("/phpExcel/standardReport.php",{listName:listName,sql:sql} ,function(data){
		window.location.href=data;
		$("#btn_print_excel").html('<i class="fa fa-file-excel-o fa-fw"></i');
	});
			
}

function register(){
	//$("#login_status_msg_cover").html('<div id="msg_loading"><div>Authenticating...</div></div>');
	//$("#register_msg").html('<img src="/img/loading.gif" width="16" /> កំពុងដំណើរការ...');
	var formName = 'register';
	iniDisplaySaveMsg(formName);
	var getData = {
						inputFName:$.trim($("#inputFName").val()),
						inputLName:$.trim($("#inputLName").val()),						
						inputFNameEn:$.trim($("#inputFName_en").val()),
						inputLNameEn:$.trim($("#inputLName_en").val()),
						inputDOB:$.trim($("#inputDOB").val()),
						inputGender:$.trim($("#inputGender").val()),
						// Language id & License Type id
						inputLanguageid:$.trim($("#inputLanguageid").val()),
						inputLicenseType:$.trim($("#inputLicenseType").val()),

						//inputMarital:$.trim($("#inputMarital").val()),
						//inputNationality:$.trim($("#inputNationality").val()),
						inputIDCard:$.trim($("#inputIDCard").val()),
						//inputObtainLicenseDate:$.trim($("#inputObtainLicenseDate").val()),
						inpuExpirytLicenseDate:$.trim($("#inpuExpirytLicenseDate").val()),
						//inputPOB:$.trim($("#inputPOB").val()),
						//inputCurrentAddress:$.trim($("#inputCurrentAddress").val()),
						inputMobile:$.trim($("#inputMobile").val()),
						//inputEmergencyMobile:$.trim($("#inputEmergencyMobile").val()),
						//inputEducation:$.trim($("#inputEducation").val()),
						//inputLanguage:$.trim($("#inputLanguage").val()),
						//inputExperience:$.trim($("#inputExperience").val())==''?'N/A':$.trim($("#inputExperience").val()),
						
						inputArea:$.trim($("#inputArea").val()),
						inputCourse:$.trim($("#inputCourse").val()),
						
						inputCode:$.trim($("#inputCode").val()),
						inputEmail:$.trim($("#inputEmail").val()),
						inputPassword:$.trim($("#inputPassword").val()),
						inputConfirmPassword:$.trim($("#inputConfirmPassword").val()),
						inputAgreePolicy:$.trim($("#inputAgreePolicy").prop('checked'))
		
				};
	$.post("/client/request",{cmd:'register',getData:getData} ,function(fdata){ 
		var fdata = JSON.parse(fdata);
		var data = fdata.data;
		$("#register_msg").html('');
			
		if(data.length==0){
			//go to next step
			/*$("#register_msg").html('<span style="color:blue;"><img src="/img/loading.gif" width="16" /> អ្នកបានចុះឈ្មោះដោយជោគជ័យ!</span>');
			setTimeout(function(){window.location.href = "/login";},2000);*/
			
			//$("#register_msg").html('<span style="color:blue;"><img src="/img/loading.gif" width="16" /> បន្តទៅទំព័របង់ប្រាក់...</span>');
			displaySaveMsg(formName,'success','<img src="/img/loading.gif" width="16" /> បន្តទៅទំព័របង់ប្រាក់...');
			setTimeout(function(){window.location.href = "/payment/summary/"+fdata.codeData;},2000);	
						
		}else{
			$.each(data,function(key,value){
				var eachMsg = value;
				$("#register_msg").removeClass('alert alert-info');
				$("#register_msg").append('<div style="color:red;"><i class="fa fa-times fa-fw"></i> '+eachMsg.msg+'</div>');				
			});
		}
	});
}

function reg_course(){
	var formName = 'regCourse';
	iniDisplaySaveMsg(formName);
	//$("#course_id").html('<option value="">--- កំពុងដំណើរការ ---</option>');
	$.post("/client/request",{cmd:'reg_course',courseid:$("#inputCourse").val()} ,function(data){
		var data = JSON.parse(data);	
		if(data.result){
			displaySaveMsg(formName,'success',data.msg);
			setTimeout(function(){window.location.href="/";},3000);
		}else{
			displaySaveMsg(formName,'danger',data.msg);
		}
	});
}

function lessonList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#lessonList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'lessonList',keyword:searchKeyword,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#lessonList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function readList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#readList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'readList',keyword:searchKeyword,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#readList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function testList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#testList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'testList',keyword:searchKeyword,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#testList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function announcementList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#announcementList_search_txt").val(),typeid=$("#typeid").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'announcementList',keyword:searchKeyword,typeid:typeid,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#announcementList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function examList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#examList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'examList',keyword:searchKeyword,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#examList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function candidateList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#candidateList_search_txt").val(),course_num=$("#course_num").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'candidateList',keyword:searchKeyword,course_num:course_num,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#candidateList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function courseList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#courseList_search_txt").val(),course_year=$("#course_year").val(),course_area=$("#course_area").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'courseList',keyword:searchKeyword,course_year:course_year,course_area:course_area,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
				var data = JSON.parse(data);
				$("#courseList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function resultList(navAction){
		$("#nav_info").html('<span><img src="img/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#resultList_search_txt").val(),course_num=$("#course_num").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/client/request",{cmd:'resultList',keyword:searchKeyword,course_num:course_num,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){ 
				var data = JSON.parse(data);
				$("#resultList_tbl tbody").html(data.list);
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
		});

}

function updateProfile(){
	//$("#login_status_msg_cover").html('<div id="msg_loading"><div>Authenticating...</div></div>');
	//$("#account_msg").html('<img src="/img/loading.gif" width="16" /> កំពុងដំណើរការ...');
	var formName = 'profile';
	iniDisplaySaveMsg(formName);
	var getData = {
						//inputCode:$.trim($("#inputCode").val()),
						inputFName:$.trim($("#inputFName").val()),
						inputLName:$.trim($("#inputLName").val()),						
						inputFNameEn:$.trim($("#inputFName_en").val()),
						inputLNameEn:$.trim($("#inputLName_en").val()),
						inputDOB:$.trim($("#inputDOB").val()),
						inputGender:$.trim($("#inputGender").val()),
						inputIDCard:$.trim($("#inputIDCard").val()),
						inpuExpirytLicenseDate:$.trim($("#inpuExpirytLicenseDate").val()),
						inputMobile:$.trim($("#inputMobile").val()),	
						inputEmail:$.trim($("#inputEmail").val())		
				}
	$.post("/client/request",{cmd:'updateProfile',getData:getData} ,function(data){
		var data = JSON.parse(data);
		$("#account_msg").html('');
		if(data.length==0){
			displaySaveMsg(formName,'success','ទិន្នន័យបានកែប្រែ');
			$("#currentPassword,#newPassword,#confirmNewPassword").val('');
		}else{
			$.each(data,function(key,value){
				var eachMsg = value;
				displaySaveMsg(formName,'danger',eachMsg.msg);				
			});
		}
	});
}

function updateAccount(){
	//$("#login_status_msg_cover").html('<div id="msg_loading"><div>Authenticating...</div></div>');
	//$("#account_msg").html('<img src="/img/loading.gif" width="16" /> កំពុងដំណើរការ...');
	var formName = 'account';
	iniDisplaySaveMsg(formName);
	var getData = {
						currentPassword:$.trim($("#currentPassword").val()),
						newPassword:$.trim($("#newPassword").val()),
						confirmNewPassword:$.trim($("#confirmNewPassword").val())		
				}
	$.post("/client/request",{cmd:'updateAccount',getData:getData} ,function(data){
		var data = JSON.parse(data);
		$("#account_msg").html('');
		if(data.length==0){
			displaySaveMsg(formName,'success','ទិន្នន័យបានកែប្រែ');
			//$("#account_msg").append('<div style="color:green;"><i class="fa fa-check fa-fw"></i> ទិន្នន័យបានកែប្រែ</div>');
			$("#currentPassword,#newPassword,#confirmNewPassword").val('');
		}else{
			$.each(data,function(key,value){
				var eachMsg = value;
				displaySaveMsg(formName,'danger',eachMsg.msg);
				//$("#account_msg").append('<div style="color:red;"><i class="fa fa-times fa-fw"></i> '+eachMsg.msg+'</div>');					
			});
		}
	});
}

function subscription(){
	//$("#login_status_msg_cover").html('<div id="msg_loading"><div>Authenticating...</div></div>');
	$("#subscribe_msg").html('  <img src="/img/loading.gif" width="16" />');
	var getData = {
						subscribe_name:$.trim($("#subscribe_name").val()),
						subscribe_email:$.trim($("#subscribe_email").val())		
				}
	$.post("/client/request",{cmd:'subscription',getData:getData} ,function(data){
		var data = JSON.parse(data);
		$("#subscribe_msg").html('');
		if(data.length==0){
			popupMsg("yesno",'<i class="fa fa-rss fa-fw"></i>តាមដានការប្រកាសថ្មីៗ','<div style="color:green;"><i class="fa fa-check fa-fw"></i>អ៊ីម៉េលរបស់អ្នកត្រូវបានរក្សាទុកដោយជោគជ័យ។</div>');
		}else{
			var err_msg = '';
			$.each(data,function(key,value){
				var eachMsg = value;
				err_msg += (eachMsg.result=='valid'?'<div style="color:green;"><i class="fa fa-check fa-fw"></i>':'<div style="color:red;"><i class="fa fa-times fa-fw"></i>')+eachMsg.msg+'</div>';					
			});
			popupMsg("yesno",'<i class="fa fa-rss fa-fw"></i>តាមដានការប្រកាសថ្មីៗ',err_msg);
		}
	});
}

function retrieveGuideInfo(licenseid){		
		if(licenseid.length==10){
			$("#license_msg").html('<img src="/img/loading.gif" width="16" />');
			$.ajax({
				url:"http://cambodia-touristguide.com/otgbs/public/api/guide/"+licenseid,
				dataType: 'jsonp',
				type : 'GET',
				crossDomain: true,
				data:{},
				success:function(data, textStatus, jqXHR){		
					console.log(data);	
					$("#license_msg").html('<img src="/img/green_check.png" width="16" />');
					var userInfo = data.user,nationality=data.nationality,languages=data.languages; 
					$("#inputLName").val(userInfo.last_name_khm);
					$("#inputFName").val(userInfo.first_name_khm);
					$("#inputLName_en").val(userInfo.last_name);
					$("#inputFName_en").val(userInfo.first_name);
					$("#inputDOB").val(data.dob);
					$(".dtpicker").datetimepicker('update');
					$("#inputGender").val(userInfo.gender.toLowerCase());
					//$("#inputNationality").val(nationality.name);
					$("#inputIDCard").val(data.id_card);
					//$("#inputObtainLicenseDate").val(data.issued_date);
					//$(".dtpicker").datetimepicker('update');
					$("#inpuExpirytLicenseDate").val(data.expired_date);
					$(".dtpicker").datetimepicker('update');
					//$("#inputCurrentAddress").val(userInfo.address);
					$("#inputMobile").val(userInfo.telephone);
					$("#inputEmail").val(userInfo.email);
					//$("#inputLanguage").val(languages[0].language_key);
					
					
					
					
				},
				error:function(){
					$("#license_msg").html('<img src="/img/red_cross.png" width="16" />');
				}
			});	
		}else{
			$("#license_msg").html('<img src="/img/red_cross.png" width="16" />');
		}
	
}

function getCourse(){
	//var formName = 'resetPassword';
	//iniDisplaySaveMsg(formName);
	$("#inputCourse").html('<option value="">--- កំពុងដំណើរការ ---</option>');
	$.post("/client/request",{cmd:'getCourse',area:$("#inputArea").val()} ,function(data){
		var data = JSON.parse(data);		
		if(data.result==1){
			$("#inputCourse").html('<option value="">--- ជ្រើសរើស ---</option>');
			$("#inputCourse").append(data.data);
		}else{
			if($("#inputArea").val()==''){
				$("#inputCourse").html('<option value="">--- សូមជ្រើសរើសតំបន់ ---</option>');
			}else{
				$("#inputCourse").html('<option value="">--- គ្មានទិន្នន័យ ---</option>');
			}
		}
	});
}

function courseSelectList(){
	//var formName = 'resetPassword';
	//iniDisplaySaveMsg(formName);
	$("#course_num").html('<option value="0">--- កំពុងដំណើរការ ---</option>');
	$.post("/client/request",{cmd:'courseSelectList',year:$("#course_year").val()} ,function(data){
		var data = JSON.parse(data);		
		if(data.result==1){
			$("#course_num").html('<option value="0">--- ជ្រើសរើស ---</option>');
			$("#course_num").append(data.data);
		}else{
			if($("#course_num").val()==''){
				$("#course_num").html('<option value="0">--- សូមជ្រើសរើសឆ្នាំ ---</option>');
			}else{
				$("#course_num").html('<option value="0">--- គ្មានទិន្នន័យ ---</option>');
			}
		}
		candidateList('');
	});
}

function courseResultList(){
	//var formName = 'resetPassword';
	//iniDisplaySaveMsg(formName);
	$("#course_num").html('<option value="0">--- កំពុងដំណើរការ ---</option>');
	$.post("/client/request",{cmd:'courseResultList',year:$("#course_year").val()} ,function(data){
		var data = JSON.parse(data);		
		if(data.result==1){
			$("#course_num").html('<option value="0">--- ជ្រើសរើស ---</option>');
			$("#course_num").append(data.data);
		}else{
			if($("#course_num").val()==''){
				$("#course_num").html('<option value="0">--- សូមជ្រើសរើសឆ្នាំ ---</option>');
			}else{
				$("#course_num").html('<option value="0">--- គ្មានទិន្នន័យ ---</option>');
			}
		}
		candidateList('');
	});
}

function registerExam(){
	//$("#login_status_msg_cover").html('<div id="msg_loading"><div>Authenticating...</div></div>');
	$("#btn_joinExam").html('<img src="/img/loading.gif" width="16" /> កំពុងដំណើរការ');
	$.post("/client/request",{cmd:'registerExam',id:$("#btn_joinExam").data('exam')} ,function(data){
		var data = JSON.parse(data);
		if(data.msg==''){
			$("#btn_joinExam").html('<i class="fa fa-check fa-fw"></i> បានចុះឈ្មោះ');
			$("#btn_joinExam").attr("disabled",true);
			$("#btn_joinExam").css("background","green"); 
		}else if(data.msg=='notlogin'){
			confirmDialog("confirm",'<i class="fa fa-lock fa-fw"></i> ចូលគណនី','សូមចូលក្នុងគណនីរបស់អ្នកជាមុនសិន','OK','','goto_page')
			$("#btn_joinExam").html('<i class="fa fa-plus fa-fw"></i> ចុះឈ្មោះប្រលង');
		}else{ //when data='invalid' and others
			$("#btn_joinExam").html('<i class="fa fa-times fa-fw"></i> មានបញ្ហា');
			//$("#btn_joinExam").attr("disabled",true);
			$("#btn_joinExam").css("background","red"); 		
		}
		/*popupMsg("yesno",'<i class="fa fa-rss fa-fw"></i>តាមដានការប្រកាសថ្មីៗ','<div style="color:green;"><i class="fa fa-check fa-fw"></i>អ៊ីម៉េលរបស់អ្នកត្រូវបានរក្សាទុកដោយជោគជ័យ។</div>');*/
	});
}

function goto_page(){window.location.href="/login?next="+window.location.pathname;}

function resetPassword(e){
	var formName = 'resetPassword';
	iniDisplaySaveMsg(formName);
	$.post("/client/request",{cmd:'resetPassword',username:$("#getusername").val()} ,function(data){
		var data = JSON.parse(data);
		
		if(data.result=='success'){
			var newContent = '<div><div><img src="/admin/images/notice-success.png" /> សូមពិនិត្យប្រអប់ Inbox និង Spam នៃអ៊ីម៉េលរបស់អ្នក។ អរគុណ!</div><div style="font-size:50px; padding-top:20px;"><i class="fa fa-envelope fa-fw"></i></div></div>';
			$("#resetPassword_div").addClass('green_gradient').html(newContent);
		}else{
			displaySaveMsg(formName,'danger',data.msg);
		}
	});
}

function searchguide(){
	$("#searchguide_tbl tbody").html('<tr><td colspan="6" style="text-align:center; color:#c0434d;"><span><img src="/admin/images/loading.gif" width="15" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span></td></tr>');
	var search_txt = $("#search_txt").val();
	if($.trim(search_txt)==''){return false;}
	//iniDisplaySaveMsg(formName);
	$.post("/client/request",{cmd:'searchguide',search_txt:search_txt} ,function(data){
		//var data = JSON.parse(data);
		
		$("#searchguide_tbl tbody").html(data);
	});
}