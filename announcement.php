<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");

session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false));
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// ========= End Session ============ //

$pageName='Lessons Area | Tourist Guide Refreshment Course';
$pageCode='lessons';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h3><i class="fa fa-th-list fa-fw"></i> បញ្ជីសេចក្តីប្រកាស</h3>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6">
                    
                    <div class="col-lg-5 col-md-5 col-sm-5">
                    <select class="form-control input-sm" id="typeid">
                    	<option value="0">--- ទាំងអស់ ---</option>
    					<?php
						$type_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle,item.title title FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='announcement' AND item.active=1 ORDER BY item.priority ASC");
                                                        while($type_row = mysqli_fetch_assoc($type_qry)){
                                                                echo '<option value="'.$type_row['id'].'" data-code="'.$type_row['title'].'">'.$type_row['displayTitle'].'</option>';
                                                        }	
						?>
                    </select>
                    </div>
                    <div class="input-group custom-search-form col-lg-6 col-md-6 col-sm-6">
                        <input type="text" id="announcementList_search_txt" class="form-control" placeholder="វាយពាក្យ">
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="announcementList_search_btn" type="button">
                            <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
            </div>
        </div>		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                 	<div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                	<option value="5">៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                    <div style="clear:both; padding:2px 0;"></div>
                    
                    				<table class="table table-striped table-bordered table-hover" id="announcementList_tbl">
                                        <thead>
                                            <tr>
                                                <th>ចំណងជើង</th>
                                                <th style=" min-width:100px;width:100px;" class="tableCellCenter">ទស្សនា</th>
                                                <th style=" min-width:100px;width:100px;" class="tableCellCenter">ផ្សាយ</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                      
                                        </tbody>
                                    </table>
                                
                 	
                 </div>
                 
                 <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                  </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_lessons").addClass('active');
						//--- end set active menu	
														
						$("#announcementList_search_btn").click(function(){announcementList('');});	
						$("#typeid").change(function(){announcementList('');});	
												
						//--- start navigation btn
						$("#nav_first").click(function(e){announcementList('first');});
						$("#nav_prev").click(function(e){announcementList('prev');});
						$("#nav_next").click(function(e){announcementList('next');});
						$("#nav_last").click(function(e){announcementList('last');});
						$("#nav_rowsPerPage").change(function(e){announcementList('');});
						$("#nav_currentPage").change(function(e){announcementList('goto');});
						//--- end navigation btn
						
						
						announcementList('');
						
                    });
                </script>

</body>
</html>