<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");

session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false)); 
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// =============== End Session ============ //
if(isset($_SESSION['userid'])){header("location: /");}

$pageName='Login | Tourist Guide Refreshment Course';
$pageCode='login';
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-user fa-fw"></i> ចូលប្រព័ន្ធ</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                    <!-- Title -->                              
                                 		<div class="form">
                                      <form id="login_frm" role="form">
                                         <div class="form-group">
                                           <label for="username" class="col-lg-12 control-label">អ៊ីម៉េល ឬ License ID ឬ ទូរស័ព្ទ</label>
                                           <div class="col-lg-12">
                                             <input type="text" class="form-control" id="username" placeholder="អ៊ីម៉េល ឬ License ID" autofocus required>
                                           </div>
                                           <label for="password" class="col-lg-12 control-label">ពាក្យសង្ងាត់</label>
                                           <div class="col-lg-12">
                                             <input type="password" class="form-control" id="password" placeholder="ពាក្យសង្ងាត់" required>
                                           </div>
                                         </div>
                                         <div class="form-group">
                                           <div class="col-md-4 col-sm-4">
                                             <button type="submit" id="login_btn" class="btn btn-default"><i class="fa fa-key fa-fw"></i> ចូល</button>
                                             <div style="padding:5px 0 5px 0;"><a href="/resetPassword"><i class="fa fa-bolt fa-fw"></i> ភ្លេចពាក្យសម្ងាត់</a></div>
                                           </div>
                                           <div class="col-md-8 col-sm-8" style="text-align:right;">
                                             តើមានគណនីហើយឬនៅ? <a href="/register.php">ចុះឈ្មោះ</a>
                                           </div>
                                           <div class="clearfix"></div>                                          
                                         </div>
                                       </form>                                             
                                    </div> 
                </div>

         </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

</body>
</html>