<?php
include("includes/connect_db.php");
$data_get = unserialize(decodeString($_GET['dt'],$encryptKey));
extract($data_get);
$user_token = $data_get['token'];

//$inputCode = 'TG00220113';

//course data
$courseData = qry_arr("year,area_id,course_num,register_date,workshop_start_date,workshop_end_date","tblcourseschedule","id=$inputCourse LIMIT 1");		
$workshop_date = date("d",strtotime($courseData['workshop_start_date'])).'-'.date("d/m/Y",strtotime($courseData['workshop_end_date']));
if($courseData['workshop_start_date']==$courseData['workshop_end_date']){$workshop_date = date("d/m/Y",strtotime($courseData['workshop_end_date']));}

//get user data
$userData = qry_arr("id,firstName,lastName,email","tblusers","id='".$get_userid."' AND active=1 LIMIT 1");
$toName = $userData['lastName'].' '.$userData['firstName'];
$to = trim($userData['email']);
$userid = $userData['id'];

if($userid==''){$userid=NULL;}

$data = file_get_contents("php://input");
$json=json_decode($data,true);

$datetime = date("Y-m-d H:i:s");
// Check connection
if (mysqli_connect_errno()){
	return "Database connection failed: " . mysqli_connect_error();
}

	if($json['tstatus'] == '200'){
		$tid = $json['tid'];
		$account = $json['account'];
		$amount = $json['amount'];
		$token = $json['token'];		
		//save payment data for history record
		$sql_pay=exec_query_utf8("INSERT INTO tblpayment SET userid=$userid,course_id=$inputCourse,user_token='$user_token',tid='$tid',account='$account',amount='$amount',token='$token',datetime='$datetime',result=1,msg='success'");
		//update user course registration status
		$sql_state=exec_query_utf8("UPDATE tblcourseregister SET pending=0,payment_date='$datetime' WHERE user_id=$get_userid AND course_id=$inputCourse LIMIT 1");
		//update user account pending status
		//$sql_state=exec_query_utf8("UPDATE tblusers SET pending=0 WHERE id=$get_userid LIMIT 1");
		
		if($sql_pay and $sql_state){
			/*------ Start Sending to Email----------*/
			$loginLink = 'http://et.cambodia-touristguide.com/login';		
			$subject = "Receipt for Course Registraion | Cambodia Tourist Guide Refreshment Course";
			$message = '
				សួស្តី '.$toName.'
				<p>
				អ្នកបានបង់ប្រាក់សម្រាប់ការចុះឈ្មោះ វគ្គបណ្តុះបណ្តាលវិក្រិត្យការមគ្គុទ្ទេសក៍ទេសចរណ៍<br />								
				ព័ត៌មាននៃការបង់ប្រាក់៖<br />
				- Transaction ID: '.$tid.'<br />
				- លេខគណនី Wing: '.$account.'<br />
				- ទឹកប្រាក់: '.$amount.'<br />
				- កាលបរិច្ឆេទ: '.date("d/m/Y H:i A").'<br />
				
				ព័ត៌មាននៃវគ្គសិក្សា៖<br />
				- តំបន់៖ '.singleCell_qry("displayTitle","tblsubcategory","id=".$courseData['area_id']." LIMIT 1").'<br />
				- វគ្គសិក្សាទី '.$courseData['course_num'].' ប្រចាំឆ្នាំ '.$courseData['year'].'<br />
				- បើកចុះឈ្មោះ៖ '.date("d/m/Y",strtotime($courseData['register_date'])).'<br />
				- សិក្ខាសាលា៖ '.$workshop_date.'<br />
			'; 
			
			if(filter_var($to, FILTER_VALIDATE_EMAIL)){
				$sent = sentMail($subject,$to,$message);		
				if($sent){
					$result='success';$msg=$to;
				}else{
					$result='mail failed';$msg='mail failed';
				}
			}else{$result='success';$msg='mail not sent, invalid email';}
			/*------ End Sending to Email----------*/
			
			return $result;
		}else{return 'db failed';}

	}else if($json['tstatus'] == 'E0204'){
		$tid = $json['tid']; //will be NULL
		$account = $json['account'];
		$amount = $json['amount'];
		$token = $json['token'];
		$err_msg = $json['message'];
		//save payment data for history record, result is 0 in default
		exec_query_utf8("INSERT INTO tblpayment SET userid=$userid,account='$account',amount='$amount',token='$token',datetime='$datetime',msg='$err_msg'");
		return "failed";
	}else{
		$err_msg = 'Unknown Error';
		//save payment data for history record, result is 0 in default
		exec_query_utf8("INSERT INTO tblpayment SET userid=$userid,datetime='$datetime',msg='$err_msg'");
		return "failed";
	}
?>