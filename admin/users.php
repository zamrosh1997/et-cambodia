<?php 
$pageInfo = array('code'=>'users','name'=>'User Account List');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-user-md fa-fw"></i> គណនីគ្រប់គ្រង<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-7 col-md-7">                    	
                        <div class="panel panel-default" id="userList">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីគណនី</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="userList_frm">
                                	<div class="row">
                                    	<div class="form-group col-lg-4 col-md-4 col-sm-4">                        
                                                <select id="typeid" class="form-control">
                                                    <?php
													$userRole_qry = exec_query_utf8("SELECT * FROM user_role WHERE backend_user=1 and active=1 ORDER BY id ASC");
													while($userRole_row = mysqli_fetch_assoc($userRole_qry)){
															echo '<option value="'.$userRole_row['id'].'">'.$userRole_row['title'].'</option>';
													}												
													?> 
                                                 </select>
                                        </div>
                                        <div class="form-group col-lg-8 col-md-8 col-sm-8">                        
                                                <div class="input-group custom-search-form">
                                                    <input type="text" id="userList_search_txt" class="form-control" placeholder="វាយឈ្មោះ ឬឈ្មោះគណនី">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" id="userList_search_btn">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួនទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                    <option value="10">១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="userList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="userList_tbl">
                                        <thead>
                                            <tr>
                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                <th style="width:100px;">ឈ្មោះគណនី</th>
                                                <th>គោត្តនាម</th>
                                                <th>នាម</th>
                                                <th>បង្កើតដោយ</th>
                                                <th>កាលបរិច្ឆេទ​បង្កើត</th>
                                                <th style="width:100px;" class="tableCellCenter"></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-5 col-md-5"> 
                    	<div class="panel panel-default" id="newUser_cover">                        
                        	<div class="panel-heading">
                                <h3 class="panel-title" id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</h3>
                            </div>
                            <div class="panel-body">
                        		  <form role="form" id="newUser_frm" action="" method="post">	
                                	<input type="hidden" id="recordid" name="recordid" value="0" />
                                    <div class="form-group"> 
                                    	<label>ប្រភេទគណនី</label> <span class="redStar">*</span>                       
                                         <select id="newTypeid" name="newTypeid" class="form-control">
                                         	<option value="0">--- ជ្រើសរើស ---</option>
                                            <?php
											$userRole_qry = exec_query_utf8("SELECT * FROM user_role WHERE backend_user=1 and active=1 ORDER BY id ASC");
											while($userRole_row = mysqli_fetch_assoc($userRole_qry)){
													echo '<option value="'.$userRole_row['id'].'">'.$userRole_row['title'].'</option>';
											}												
											?>  
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>ឈ្មោះគណនី</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ប្រើសម្រាប់ចូលក្នុងប្រព័ន្ធ"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="text" id="newUsername" name="newUsername" class="form-control" placeholder="ឈ្មោះគណនី" required>
                                    </div> 
                                    <div class="form-group">
                                        <label>គោត្តនាម</label> <span class="redStar">*</span>
                                        <input type="text" id="newLName" name="newLName" class="form-control" placeholder="គោត្តនាម" required>
                                    </div>
                                    <div class="form-group">
                                        <label>នាម</label> <span class="redStar">*</span>
                                        <input type="text" id="newFName" name="newFName" class="form-control" placeholder="នាម" required>
                                    </div> 
                                    <div class="form-group">
                                        <label>ពាក្យសម្ងាត់</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ប្រើសម្រាប់ចូលក្នុងប្រព័ន្ធ"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="password" id="newPassword" name="newPassword" class="form-control" placeholder="ពាក្យសម្ងាត់" required>
                                    </div> 
                                    <div class="form-group">
                                        <label>បញ្ចាក់ពាក្យសម្ងាត់</label> <span class="redStar">*</span>
                                        <input type="password" id="confirmNewPassword" name="confirmNewPassword" class="form-control" placeholder="វាយពាក្យសម្ងាត់ម្តងទៀត" required>
                                    </div>  
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="active" name="active" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="active"></label>
                                                </div>
                                                <div class="switch_label"> ដាក់អោយដំណើរការពេលនេះ</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបិទ ឬបើក ដើម្បីដាក់អោយដំណើរការពេលនេះ ឬចាំដាក់ដំណើរការពេលក្រោយ"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                    </div>        
                                	<div class="form-group">
                                    	<input type="hidden" name="cmd" value="newUser" />
                                        <button type="submit" id="newUser_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                        <button type="button" id="cancelUser_btn" name="cancelUser_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                    </div>
                                </form>                                
                                <div id="newUser_msg" class=""></div>                   
                        	</div> 
                        </div>                   
                    </div>
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_users").addClass('active');
						//--- end set active menu															
												
						//--- start navigation btn
						$("#nav_first").click(function(e){userList('first');});
						$("#nav_prev").click(function(e){userList('prev');});
						$("#nav_next").click(function(e){userList('next');});
						$("#nav_last").click(function(e){userList('last');});
						$("#nav_rowsPerPage").change(function(e){userList('');});
						$("#nav_currentPage").change(function(e){userList('goto');});
						//--- end navigation btn
						
						$("#userList_frm").submit(function(e){userList(''); e.preventDefault();});
						$("#userList_search_btn").click(function(){userList('');});	
						$("#typeid").change(function(){userList('');});						
						
						userList('');
						
						$("#newUser_frm").on('submit',(function(e) {addData(e,'newUser','userList');}));		
						$("#cancelUser_btn").on('click',(function(e) {closeEditForm('newUser');}));	
						
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-body">
                                                        <form role="form" id="editLesson_frm" action="/service/request" method="post" enctype="multipart/form-data">	
                                                            <div class="form-group">
                                                                <label>ចំនងជើងមុខវិជ្ជា</label>
                                                                <input type="text" id="editTitle" name="editTitle" class="form-control" placeholder="ចំនងជើង" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ឯកសារ៖</label>
                                                                <div id="currentDoc">
                                                                	<ul>
                                                                    	<li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                    </ul>
                                                                	
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ផ្លាស់ប្តូរឯកសារ</label>
                                                                <input type="file" id="editDoc" name="editDoc" class="form-control">
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="switch">
                                                                        <div class="switch_input">
                                                                        <input id="editallow_quiz" name="editallow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                                        <label for="editallow_quiz"></label>
                                                                        </div>
                                                                        <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                                </div>
                                                             </div> 
                                                                       
                                                            <div class="form-group">
                                                                <input type="hidden" name="cmd" value="editLesson" />
                                                                <input type="hidden" id="edit_confirmData" name="edit_confirmData" value="" />
                                                                <button type="submit" id="editLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                                            </div>
                                                        </form>                                
                                                        <div id="editLesson_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>   
<?php include("includes/page_footer.php"); 
				
			?>