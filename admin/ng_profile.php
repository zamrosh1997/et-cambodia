<?php 
$pageInfo = array('code'=>'ng_profile','name'=>'New Guide Profile');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-qrcode fa-fw"></i> មគ្គុទ្ទេសក៍ទេសចរណ៍ថ្មី<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-8 col-md-12">                    	
                        <div class="panel panel-default" id="ng_guidelist">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីឈ្មោះ</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="ng_guidelist_frm">
                                	<div class="row">
                                    	<div class="form-group col-xs-12 col-sm-6 col-md-2">                        
                                                <select class="form-control input-sm" id="result_id">
                                                	<option value="0">--- ជ្រើសរើសសាលា ---</option>
                                                    <?php                                                    
													$select_qry = exec_query_utf8("select r.id,r.generation,reg.name_kh region,s.name_kh school from ng_exam_result r
																					left join ng_region reg on reg.id=r.region_id
																					left join ng_school s on s.id=r.school_id
																					order by r.id asc");
													while($select_row = mysqli_fetch_assoc($select_qry)){
														echo '<option value="'.$select_row['id'].'">'.$select_row['school'].' ('.$select_row['region'].' Batch'.$select_row['generation'].')</option>';
													}												
													?>
                                                </select>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-6 col-md-2">                        
                                                <select class="form-control input-sm" id="search_gender">
                                                	<option value="">--- ជ្រើសរើសភេទ ---</option>
                                                    <option value="m">ប្រុស</option>
                                                    <option value="f">ស្រី</option>
                                                </select>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-6 col-md-2">                        
                                                <select class="form-control input-sm" id="languageid">
                                                	<option value="0">--- ជ្រើសរើសភាសា ---</option>
                                                    <?php                                                    
													$select_qry = exec_query_utf8("select * from tbllanguages where active=1 order by id asc");
													while($select_row = mysqli_fetch_assoc($select_qry)){
														echo '<option value="'.$select_row['id'].'">'.$select_row['name_kh'].' ('.$select_row['name_en'].')</option>';
													}												
													?>
                                                </select>
                                        </div>
                                                                              	
                                        <div class="form-group form-group col-xs-12 col-sm-6 col-md-2">
                                            <div class="input-append input-group dtpicker">
                                                    <input data-format="yyyy-MM-dd" type="text" id="from_date" name="from_date" placeholder="ចាប់ពី" class="form-control">
                                                    <span class="input-group-addon add-on">
                                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar" class="fa fa-calendar"></i>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="form-group form-group col-xs-12 col-sm-6 col-md-2">
                                            <div class="input-append input-group dtpicker">
                                                    <input data-format="yyyy-MM-dd" type="text" id="to_date" name="to_date" placeholder="រហូតដល់" class="form-control">
                                                    <span class="input-group-addon add-on">
                                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar" class="fa fa-calendar"></i>
                                                    </span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-xs-12 col-sm-6 col-md-2">                        
                                                <div class="input-group custom-search-form">
                                                    <input type="text" id="ng_guidelist_search_txt" class="form-control" placeholder="វាយឈ្មោះ">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="submit">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួនទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                    <option value="10">១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="ng_guidelist_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="ng_guidelist_tbl">
                                        <thead>
                                        	<tr>
                                                <th rowspan="2" style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                <th colspan="2" class="tableCellCenter">ឈ្មោះ</th>
                                                <th rowspan="2" class="tableCellCenter">ភេទ</th>
                                                <th rowspan="2" class="tableCellCenter">ឯកទេស</th>
                                                <?php                                                    
												/*$select_qry = exec_query_utf8("select * from ng_exam_subject where active=1 order by id asc");
												while($select_row = mysqli_fetch_assoc($select_qry)){
													echo '<th rowspan="2" class="tableCellCenter">'.$select_row['title'].'</th>';
												}*/												
												?>
                                                <th rowspan="2" class="tableCellCenter">មធ្យមភាគ</th>
                                                <th rowspan="2" class="tableCellCenter">និទ្ទេស</th>
                                                <th rowspan="2" class="tableCellCenter">QR Code</th>
                                                <th rowspan="2" style="width:100px;" class="tableCellCenter"></th>
                                            </tr>
                                            <tr>
                                                <th class="tableCellCenter">ភាសាខ្មែរ</th>
                                                <th class="tableCellCenter">ភាសាអង់គ្លេស</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-4 col-md-5"> 
                    	<div class="panel panel-default" id="ng_profile_cover">                        
                        	<div class="panel-heading">
                                <h3 class="panel-title" id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</h3>
                            </div>
                            <div class="panel-body">
                        		  <form role="form" id="ng_profile_frm" action="" method="post">	
                                	<input type="hidden" id="recordid" name="recordid" value="0" />
                                    
                                   	<div class="form-group">
                                       <label for="lname_kh" class="control-label">គោត្តនាម</label> <span class="redStar">*</span>
                                       <div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                                <div class="input-group">        
                                                    <input type="text" class="form-control col-lg-6" id="lname_kh" name="lname_kh" placeholder="គោត្តនាម" required>                                    
                                                    <span class="input-group-addon">ខ្មែរ</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                                <div class="input-group">        
                                                    <input type="text" class="form-control col-lg-6" id="lname_en" name="lname_en" placeholder="Family Name" required>  
                                                    <span class="input-group-addon">ឡាតាំង</span>
                                                </div>
                                            </div>
                                         
                                         
                                       </div>
                                   </div> 
                                   <div class="form-group">
                                       <label for="fname_kh" class="control-label">នាម</label> <span class="redStar">*</span>
                                       <div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                                <div class="input-group">        
                                                    <input type="text" class="form-control" id="fname_kh" name="fname_kh" placeholder="នាម" required>   
                                                    <span class="input-group-addon">ខ្មែរ</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                                <div class="input-group">        
                                                    <input type="text" class="form-control" id="fname_en" name="fname_en" placeholder="First Name" required>
                                                    <span class="input-group-addon">ឡាតាំង</span>
                                                </div>
                                            </div>
                                       </div>
                                    </div> 
                                    
                                    <div class="form-group">
                                       <label for="guide_gender" class="control-label">ភេទ</label> <span class="redStar">*</span>
                                       <div>
                                         <select id="guide_gender" name="guide_gender" class="form-control" required>
                                            <option value="">--- ជ្រើសរើស ---</option>
                                            <option value="m">ប្រុស</option>
                                            <option value="f">ស្រី</option>
                                         </select>
                                       </div>
                                     </div>                                      
                                                                          
                                     <div class="form-group">
                                       <label for="language_id" class="control-label">ភាសា</label> <span class="redStar">*</span>
                                       <div>
                                         <select id="language_id" name="language_id" class="form-control" required>
                                            <option value="">--- ជ្រើសរើស ---</option>
                                            <?php                                                    
											$select_qry = exec_query_utf8("select * from tbllanguages where active=1 order by id asc");
											while($select_row = mysqli_fetch_assoc($select_qry)){
												echo '<option value="'.$select_row['id'].'">'.$select_row['name_kh'].' ('.$select_row['name_en'].')'.'</option>';
											}												
											?>
                                         </select>
                                       </div>
                                     </div>
                                     
                                     <div class="form-group">
                                       <label for="exam_id" class="control-label">ប្រលង</label> <span class="redStar">*</span>
                                       <div>
                                         <select id="exam_id" name="exam_id" class="form-control" required>
                                            <option value="">--- ជ្រើសរើស ---</option>
                                            <?php                                                    
											$select_qry = exec_query_utf8("select * from ng_exam_result where active=1 order by id asc");
											while($select_row = mysqli_fetch_assoc($select_qry)){
												$region = singleCell_qry("name_kh","ng_region","id=".$select_row['region_id']." LIMIT 1");
												echo '<option value="'.$select_row['id'].'">'.$region.' វគ្គ '.$select_row['generation'].' '.$select_row['exam_date'].'</option>';
											}												
											?>
                                         </select>
                                       </div>
                                     </div>
                                     
                                     <div class="form-group">
                                       <label for="score_avg" class="control-label">មធ្យមភាគ</label>
                                       <div>
                                         <input type="text" class="form-control" id="score_avg" name="score_avg" placeholder="Average">
                                       </div>
                                     </div> 
                                     
                                     <div class="form-group">
                                       <label for="score_grade" class="control-label">និទ្ទេស</label>
                                       <div>
                                         <input type="text" class="form-control" id="score_grade" name="score_grade" placeholder="Grade">
                                       </div>
                                     </div> 
                                     
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="active" name="active" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="active"></label>
                                                </div>
                                                <div class="switch_label"> បិទ/បើក Profile</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="បិទ/បើក Profile"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                    </div>        
                                	<div class="form-group">
                                    	<input type="hidden" name="cmd" value="ng_profile" />
                                        <button type="submit" id="ng_profile_btn" name="ng_profile_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                        <button type="button" id="cancelUser_btn" name="cancelUser_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                    </div>
                                </form>                                
                                <div id="ng_profile_msg" class=""></div>                   
                        	</div> 
                        </div>                   
                    </div>
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_ng_profile").addClass('active');
						//--- end set active menu															
												
						//--- start navigation btn
						$("#nav_first").click(function(e){newguideList('first');});
						$("#nav_prev").click(function(e){newguideList('prev');});
						$("#nav_next").click(function(e){newguideList('next');});
						$("#nav_last").click(function(e){newguideList('last');});
						$("#nav_rowsPerPage").change(function(e){newguideList('');});
						$("#nav_currentPage").change(function(e){newguideList('goto');});
						//--- end navigation btn
						
						$("#ng_guidelist_frm").submit(function(e){newguideList(''); e.preventDefault();});
						$("#languageid").change(function(e){newguideList(''); e.preventDefault();});
						$("#result_id").change(function(e){newguideList(''); e.preventDefault();});
						$("#search_gender").change(function(e){newguideList(''); e.preventDefault();});
						
						newguideList('');
						
						$("#ng_profile_frm").on('submit',(function(e) {addData(e,'ng_profile','newguideList');}));		
						$("#cancelUser_btn").on('click',(function(e) {closeEditForm('ng_profile');}));	
						
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-body">
                                                        <form role="form" id="editLesson_frm" action="/service/request" method="post" enctype="multipart/form-data">	
                                                            <div class="form-group">
                                                                <label>ចំនងជើងមុខវិជ្ជា</label>
                                                                <input type="text" id="editTitle" name="editTitle" class="form-control" placeholder="ចំនងជើង" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ឯកសារ៖</label>
                                                                <div id="currentDoc">
                                                                	<ul>
                                                                    	<li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                    </ul>
                                                                	
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ផ្លាស់ប្តូរឯកសារ</label>
                                                                <input type="file" id="editDoc" name="editDoc" class="form-control">
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="switch">
                                                                        <div class="switch_input">
                                                                        <input id="editallow_quiz" name="editallow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                                        <label for="editallow_quiz"></label>
                                                                        </div>
                                                                        <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                                </div>
                                                             </div> 
                                                                       
                                                            <div class="form-group">
                                                                <input type="hidden" name="cmd" value="editLesson" />
                                                                <input type="hidden" id="edit_confirmData" name="edit_confirmData" value="" />
                                                                <button type="submit" id="editLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                                            </div>
                                                        </form>                                
                                                        <div id="editLesson_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="popup_btn" data-toggle="modal" data-target="#popup_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="popup_modal" tabindex="-1" role="dialog" aria-labelledby="popup_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="popup_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="popup_modalLabelBodyText">
                                               
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/page_footer.php"); 
				
			?>