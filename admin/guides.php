<?php 
$pageInfo = array('code'=>'guides','name'=>'Guide Account');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");

$id_str = isset($_GET['id'])?get('id'):'';
$request_profile = false;
if($id_str<>''){
	$getid = decodeString($id_str,$encryptKey);
	if(is_numeric($getid)){$request_profile = true;}	
}
$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-user fa-fw"></i> គណនីមគ្គុទេសក៍ទេសចរណ៍<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-7 col-md-7">                    	
                        <div class="panel panel-default" id="guideList">
                            <div class="panel-heading">
                            	<div style="float:left;">
                                	<h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីឈ្មោះ</h3> 
                                </div>
                                <div style="float:right;">
                                	<button type="button" id="fullexport" data-listname="" data-sql="" class="btn btn-default"><img src="/img/excel_icon.png" /></button>
                                    <button type="button" id="partexport" data-listname="" data-sql="" class="btn btn-default"><img src="/img/excel_icon_1.png" /></button>
                                </div> 
                                <div class="clearfix"></div>                                                          
                            </div>                            
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="guideList_frm">
                                    <div class="form-group">                        
                                            <div class="input-group custom-search-form">
                                                <input type="text" id="guideList_search_txt" class="form-control" placeholder="វាយឈ្មោះ ឬ License ID ឬ លេខទូរស័ព្ទ">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" id="guideList_search_btn">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                    </div>
                                </form>
                                <div>
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួនទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                    <option value="5">០៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                    <option value="50">៥០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="guideList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="guideList_tbl">
                                        <thead>
                                            <tr>
                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                <th style="width:100px;">License ID</th>
                                                <th>ឈ្មោះ (ខ្មែរ)</th>
                                                <th>ឈ្មោះ (ឡាតាំង)</th>
                                                <th style="width:50px;">ភេទ</th>
                                                <th>DOB</th>
                                                <th>ភាសា</th>
                                                <th>Origin ID</th>
                                                <th style="width:130px;" class="tableCellCenter"></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-5 col-md-5"> 
                    	<div class="panel panel-default" id="guideProfile_cover">                        
                        	<div class="panel-body">
                        		<fieldset>
                                    <legend id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</legend>
                                    <form role="form" id="guideProfile_frm" method="post" enctype="multipart/form-data">
                                    	<input type="hidden" class="form-control" id="recordid" name="recordid">	                                        
                                        <div class="form-group">
                                           <div>
                                           		<div style="float:left;"><label for="guide_code" class="control-label">License ID: </label></div>
                                            	<div style="float:right;"><a href="#" id="popupSearchguide"><i class="fa fa-search"></i> ស្វែងរក License ID</a></div>
                                           </div>
                                           <div class="clearfix"></div>
                                           <div>                            
                                             <div class="input-group">        
                                               <input type="text" class="form-control" id="guide_code" name="guide_code" placeholder="License ID">
                                               <div class="input-group-btn">
                                               		<button class="btn btn-default" type="button" id="update_licenseid_btn"><i class="fa fa-floppy-o"></i></button>
                                               </div>
                                             </div>
                                           </div>
                                         </div>
                                         <div id="selectCourse_div">
                                             <div class="form-group">
                                               <label for="inputArea" class="control-label">តំបន់ <span class="redStar">*</span></label>
                                               <div>
                                                 <select id="inputArea" name="inputArea" class="form-control" required>
                                                    <option value="">--- ជ្រើសរើស ---</option>
                                                    <?php
                                                    
                                                    $select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
                                                    while($select_row = mysqli_fetch_assoc($select_qry)){
                                                        echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
                                                    }	
                                                    
                                                    ?>
                                                 </select>
                                               </div>
                                             </div> 
                                             <div class="form-group">
                                               <label for="inputCourse" class="control-label">វគ្គសិក្សា <span class="redStar">*</span></label>
                                               <div>
                                                 <select id="inputCourse" name="inputCourse" class="form-control" required>
                                                    <option value="">--- សូមជ្រើសរើសតំបន់ ---</option>
                                                 </select>
                                               </div>
                                             </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_lastName" class="control-label">គោត្តនាម</label> <span class="redStar">*</span>
                                           <div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                                    <div class="input-group">        
                                                        <input type="text" class="form-control col-lg-6" id="guide_lastName" name="guide_lastName" placeholder="គោត្តនាម" required>                                    
                                                        <span class="input-group-addon">ខ្មែរ</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                                    <div class="input-group">        
                                                        <input type="text" class="form-control col-lg-6" id="guide_lastNameEn" name="guide_lastNameEn" placeholder="Family Name" required>  
                                                        <span class="input-group-addon">ឡាតាំង</span>
                                                    </div>
                                                </div>
                                             
                                             
                                           </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_firstName" class="control-label">នាម</label> <span class="redStar">*</span>
                                           <div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                                    <div class="input-group">        
                                                        <input type="text" class="form-control" id="guide_firstName" name="guide_firstName" placeholder="នាម" required>   
                                                        <span class="input-group-addon">ខ្មែរ</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                                    <div class="input-group">        
                                                        <input type="text" class="form-control" id="guide_firstNameEn" name="guide_firstNameEn" placeholder="First Name" required>
                                                        <span class="input-group-addon">ឡាតាំង</span>
                                                    </div>
                                                </div>
                                           </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_dob" class="control-label">ថ្ងៃខែឆ្នាំកំណើត</label>
                                           <div>
                                             <div class="input-append input-group dtpicker">
                                                <input data-format="yyyy-MM-dd" type="text" id="guide_dob" name="guide_dob" class="form-control">
                                                <span class="input-group-addon add-on">
                                                    <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                           </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_gender" class="control-label">ភេទ</label> <span class="redStar">*</span>
                                           <div>
                                             <select id="guide_gender" name="guide_gender" class="form-control" required>
                                                <option value="">--- ជ្រើសរើស ---</option>
                                                <option value="m">ប្រុស</option>
                                                <option value="f">ស្រី</option>
                                             </select>
                                           </div>
                                         </div>   
                                         <div class="form-group">
                                           <label for="guide_language_id" class="control-label">ភាសា</label> <span class="redStar">*</span>
                                           <div>
                                             <select id="guide_language_id" name="guide_language_id" class="form-control" required>
                                                <option value="">--- ជ្រើសរើស ---</option>
                                                <?php                                                    
                                                $select_qry = exec_query_utf8("select * from tbllanguages where active=1 order by id asc");
                                                while($select_row = mysqli_fetch_assoc($select_qry)){
                                                    echo '<option value="'.$select_row['id'].'">'.$select_row['name_kh'].' ('.$select_row['name_en'].')'.'</option>';
                                                }												
                                                ?>
                                             </select>
                                           </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_idCard" class="control-label">អត្តសញ្ញាណប័ណ្ណសញ្ជាតិខ្មែរ / លេខលិខិតឆ្លងដែន</label> <span class="redStar">*</span>
                                           <div>
                                             <input type="text" class="form-control" id="guide_idCard" name="guide_idCard" placeholder="ID/Passport" required>
                                           </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_licenseExpiredDate" class="control-label">ថ្ងៃខែផុតសុពលភាពអាជ្ញាបណ្ណ មគ្គុទ្ទេសក៍ទេសចរណ៍ៈ</label>
                                           <div>
                                             <div class="input-append input-group dtpicker">
                                                <input data-format="yyyy-MM-dd" type="text" id="guide_licenseExpiredDate" name="guide_licenseExpiredDate" class="form-control">
                                                <span class="input-group-addon add-on">
                                                    <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                           </div>
                                         </div> 
                                         <div class="form-group">
                                           <label for="guide_mobile" class="control-label">លេខទូរស័ព្ទផ្ទាល់ខ្លួន</label> <span class="redStar">*</span>
                                           <div>
                                             <input type="text" class="form-control" id="guide_mobile" name="guide_mobile" placeholder="លេខទូរស័ព្ទ" required>
                                           </div>
                                         </div>                                          
                                         <div class="form-group">
                                           <label for="guide_email" class="control-label">អ៊ីម៉ែល</label>
                                           <div>
                                             <input type="email" class="form-control" id="guide_email" name="guide_email" placeholder="អ៊ីម៉ែល">
                                           </div>
                                         </div>
                                         <div class="form-group">
                                           <label for="guide_origin_id" class="control-label">Original ID</label>
                                           <div>
                                             <input type="number" class="form-control" id="guide_origin_id" name="guide_origin_id" placeholder="Original ID">
                                           </div>
                                         </div>
                                         <div class="form-group"> 
                                         	<input type="hidden" name="cmd" value="newguide" />
                                         	<button type="submit" id="guideProfile_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                            <button type="button" id="cancelGuide_btn" name="cancelGuide_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                         </div>
                                         <div id="guideProfile_msg" class=""></div>
                               </form>                                
                            </fieldset>                            
                        </div> 
                        </div>                   
                    </div>
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_guides").addClass('active');
						//--- end set active menu															
												
						//--- start navigation btn
						$("#nav_first").click(function(e){guideList('first');});
						$("#nav_prev").click(function(e){guideList('prev');});
						$("#nav_next").click(function(e){guideList('next');});
						$("#nav_last").click(function(e){guideList('last');});
						$("#nav_rowsPerPage").change(function(e){guideList('');});
						$("#nav_currentPage").change(function(e){guideList('goto');});
						//--- end navigation btn
						
						$("#guideList_search_btn").click(function(){guideList('');});	
						$("#guideList_frm").submit(function(){guideList(''); return false;});
						$("#guideProfile_frm").submit(function(e){addData(e,'guideProfile','guideList');});						
						$("#update_licenseid_btn").click(updateGuide);
						
						guideList('');
						
						$("#popupSearchguide").click(function(e){popupSearchguide(); e.preventDefault();});		
						$("#inputArea").change(function(){reg_selectcourse(this.id,'inputCourse')});	
						$("#selectArea").change(function(){reg_selectcourse(this.id,'selectCourse')});	
						$("#cancelGuide_btn").on('click',(function(e) {closeEditForm('guideProfile');}));				
						
						
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!--<div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>-->
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>
                           
                           <div>                                
                                <!-- Button trigger modal -->
                                <div id="regCourse_btn" data-toggle="modal" data-target="#regCourse_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="regCourse_modal" tabindex="-1" role="dialog" aria-labelledby="regCourse_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="regCourse_modalLabel">
                                                	ជ្រើសវគ្គសិក្សា
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="regCourse_modalLabelBodyText1">
                                              	<form class="form-horizontal" role="form" id="regCourse_frm">
                                                     <div class="form-group" style="margin-right: 0;margin-left: 0;">
                                                           <label for="selectArea" class="control-label">តំបន់ <span class="redStar">*</span></label>
                                                           <div>
                                                             <select id="selectArea" name="selectArea" class="form-control" required>
                                                                <option value="">--- ជ្រើសរើស ---</option>
                                                                <?php
                                                                
                                                                $select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
                                                                while($select_row = mysqli_fetch_assoc($select_qry)){
                                                                    echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
                                                                }	
                                                                
                                                                ?>
                                                             </select>
                                                           </div>
                                                     </div> 
                                                     <div class="form-group" style="margin-right: 0;margin-left: 0;">
                                                           <label for="selectCourse" class="control-label">វគ្គសិក្សា <span class="redStar">*</span></label>
                                                           <div>
                                                             <select id="selectCourse" name="selectCourse" class="form-control" required>
                                                                <option value="">--- សូមជ្រើសរើសតំបន់ ---</option>
                                                             </select>
                                                           </div>
                                                     </div>
                                                     <div class="form-group" style="margin-right: 0;margin-left: 0;">
                                                           <div id="regCourse_msg"></div>
                                                     </div>
                                                 </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="regCourse_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="regCourse_actionBtn"></button>
                                                <input type="hidden" id="regCourse_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>
                           
                           <div>
								<!-- Button trigger modal -->
                                <div id="searchguide_btn" data-toggle="modal" data-target="#searchguide_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="searchguide_modal" tabindex="-1" role="dialog" aria-labelledby="searchguide_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="searchguide_modalLabel">
                                                	<i class="fa fa-search"></i> ស្វែងរក
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="searchguide_modalLabelBodyText">
                                               	 <form class="form-horizontal" role="form" id="searchguide_frm">
                                                 	<div class="form-group" style="margin-right: 0;margin-left: 0;">
                                                       <div class="input-group">        
                                                           <input type="text" class="form-control" id="search_txt" placeholder="ឈ្មោះពេញ (ខ្មែរឬឡាតាំង) ឬ ថ្ងៃកំណើត (1989-12-23) ឬ លេខទូរស័ព្ទ" required>
                                                           <span class="input-group-btn" >
                                                           		<button class="btn btn-default" id="searchguide_btn" type="submit">
                                                                	<i class="fa fa-search"></i>
                                                                </button>
                                                           </span>
                                                       </div>
                                                     </div>
                                                 </form>
                                                 <div>
                                                 	<table class="table table-striped table-bordered table-hover" id="searchguide_tbl">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                                <th>License ID</th>
                                                                <th>ឈ្មោះខ្មែរ</th>
                                                                <th>ឈ្មោះឡាតាំង</th>
                                                                <th>ថ្ងៃកំណើត</th>
                                                                <th>ទួរស័ព្ទ</th>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="searchguide_modalCloseBtn">បិទ</button>
                                                <button type="button" class="btn btn-primary" id="searchguide_actionBtn"></button>
                                                <input type="hidden" id="searchguide_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                        </div>
                        <script>
							$(document).ready(function(e) {
                                $("#searchguide_frm").submit(function(e){ searchguide(); e.preventDefault();});
								<?=$request_profile?"loadProfile($getid);":''?>
                            });							
                        </script>
<?php include("includes/page_footer.php"); 
				
			?>