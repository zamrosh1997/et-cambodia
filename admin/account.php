<?php 
$pageInfo = array('code'=>'myaccount','name'=>'My Account');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}

$username='';$fname='';$lname='';
$user_qry = exec_query_utf8("SELECT * FROM tblusers WHERE id=".$_SESSION['adminid']." LIMIT 1");
while($user_row = mysqli_fetch_assoc($user_qry)){
	$username=$user_row['code'];
	$fname=$user_row['firstName'];
	$lname=$user_row['lastName'];
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">
				<!-- Page Heading -->
                <div style="padding-top:20px;">
                    
                </div>
                <!-- /.row -->
                <div class="row">                     
                    <div class="col-lg-5 col-md-8"> 
                    	<div class="panel panel-default" id="newUser_cover">                        
                        	<div class="panel-heading">
                                <h3 class="panel-title" id="form_label"><i class="fa fa-user fa-fw"></i> ព័ត៌មានគណនី</h3>
                            </div>
                            <div class="panel-body">
                        		  <form role="form" id="myAccount_frm" action="" method="post">	
                                	<input type="hidden" id="recordid" name="recordid" value="0" />
                                    <div class="form-group">
                                        <label>ឈ្មោះគណនី</label>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ប្រើសម្រាប់ចូលក្នុងប្រព័ន្ធ"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="text" id="newUsername" name="newUsername" class="form-control" value="<?=$username?>" placeholder="ឈ្មោះគណនី" disabled>
                                    </div> 
                                    <div class="form-group">
                                        <label>គោត្តនាម</label>
                                        <input type="text" id="newLName" name="newLName" class="form-control" placeholder="គោត្តនាម" value="<?=$fname?>" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label>នាម</label>
                                        <input type="text" id="newFName" name="newFName" class="form-control" placeholder="នាម"​ value="<?=$lname?>" disabled>
                                    </div> 
                                    <div class="form-group">
                                        <label>ពាក្យសម្ងាត់បច្ចុប្បន្ន</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ពាក្យសម្ងាត់ដែលកំពុងប្រើ"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="password" id="currentPassword" name="currentPassword" class="form-control" placeholder="ពាក្យសម្ងាត់" required>
                                    </div>
                                    <div class="form-group">
                                        <label>ពាក្យសម្ងាត់ថ្មី</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ប្រើសម្រាប់ចូលក្នុងប្រព័ន្ធ"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="password" id="newPassword" name="newPassword" class="form-control" placeholder="ពាក្យសម្ងាត់" required>
                                    </div> 
                                    <div class="form-group">
                                        <label>បញ្ចាក់ពាក្យសម្ងាត់</label> <span class="redStar">*</span>
                                        <input type="password" id="confirmNewPassword" name="confirmNewPassword" class="form-control" placeholder="វាយពាក្យសម្ងាត់ម្តងទៀត" required>
                                    </div>        
                                	<div class="form-group">
                                    	<input type="hidden" name="cmd" value="updateAccount" />
                                        <button type="submit" id="myAccount_btn" name="myAccount_btn" class="btn btn-primary"><i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ</button>
                                    </div>
                                </form>                                
                                <div id="myAccount_msg" class=""></div>                   
                        	</div> 
                        </div>                   
                    </div>
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						$("#myAccount_frm").on('submit',(function(e) {addData(e,'myAccount','');}));
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-body">
                                                        <form role="form" id="editLesson_frm" action="/service/request" method="post" enctype="multipart/form-data">	
                                                            <div class="form-group">
                                                                <label>ចំនងជើងមុខវិជ្ជា</label>
                                                                <input type="text" id="editTitle" name="editTitle" class="form-control" placeholder="ចំនងជើង" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ឯកសារ៖</label>
                                                                <div id="currentDoc">
                                                                	<ul>
                                                                    	<li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                    </ul>
                                                                	
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ផ្លាស់ប្តូរឯកសារ</label>
                                                                <input type="file" id="editDoc" name="editDoc" class="form-control">
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="switch">
                                                                        <div class="switch_input">
                                                                        <input id="editallow_quiz" name="editallow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                                        <label for="editallow_quiz"></label>
                                                                        </div>
                                                                        <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                                </div>
                                                             </div> 
                                                                       
                                                            <div class="form-group">
                                                                <input type="hidden" name="cmd" value="editLesson" />
                                                                <input type="hidden" id="edit_confirmData" name="edit_confirmData" value="" />
                                                                <button type="submit" id="editLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                                            </div>
                                                        </form>                                
                                                        <div id="editLesson_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/page_footer.php"); 
				
			?>