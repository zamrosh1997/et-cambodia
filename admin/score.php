<?php 
$pageInfo = array('code'=>'score','name'=>'Score');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");
$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-dot-circle-o fa-fw"></i> គ្រប់គ្រងពិន្ទុ Online Test<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-9">                    	
                        <div class="panel panel-default" id="scoreList">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> តារាងពិន្ទុ</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            <div>                                
                                <div>
                                	<div class="form-group" style="display:inline-block">
                                        <label>ឈ្មោះមុខវិជ្ជា</label>
                                        <select class="form-control chosen-select input-sm" id="lessonid">
                                               <option value="0">--- ជ្រើសរើស ---</option>
                                               <?php
                                                        $lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY id DESC");
                                                        while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
                                                                echo '<option value="'.$lesson_row['id'].'">'.$lesson_row['title'].'</option>';
                                                        }												
                                                        ?>  
                                         </select>
                                    </div>   
                                    <div class="form-group" style="display:inline-block">
                                        <label>កំនត់ពិន្ទុ</label>
                                        <input type="text" id="txtScore" class="form-control" placeholder="ពិន្ទុ" disabled>
                                    </div> 
                                    <div class="form-group" style="display:inline-block">
                                        <button type="submit" id="btnSetScore" class="btn btn-primary" disabled><i class="fa fa-floppy-o fa-fw"></i> រក្សាទុក</button>
                                    </div> 
                                </div>
                                <div style="clear:both; padding:2px 0;  "></div>
                                
                                <div class="table-responsive" id="scoreList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="scoreList_tbl">
                                        <thead>
                                            <tr>
                                                <th>មុខវិជ្ចា</th>
                                                <th style="width:170px;">ចំនួនចេញប្រលង</th>
                                                <th  class="tableCellCenter">ពិន្ទុ</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        	</div>
                        </div>
                        
                    </div>
                    
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_score").addClass('active');
						//--- end set active menu	
														
						$("#lessonid").change(function(){scoreInput();scoreList();});	
						$("#difficultyid").change(function(){scoreInput();});	
						$("#btnSetScore").click(function(){setScore(); return false;});
						scoreList();
						
						function scoreInput(){
							if($("#lessonid").val()==0 || $("#difficultyid").val() == 0){
								$("#txtScore,#btnSetScore").prop('disabled',true);								
							}else{
								$("#txtScore,#btnSetScore").prop('disabled',false);
							}
						}
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!--<div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>-->
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-heading">
                                                        <a name="a_editCateItem" class="anchorLocation"><div></div></a>
                                                        <h3 class="panel-title"><i class="fa fa-plus-circle fa-fw"></i> Edit Category Item</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <form role="form" id="editCateItem_frm">		
                                                            <div class="form-group">
                                                                <label for="editCateTypeid">Category Type</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Select a main category type"><i class="fa fa-info-circle"></i></span></div>                                     
                                                        
                                                            </div>	
                                                            <div class="form-group">
                                                                <label>Category Item Name</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Item title or name for a specific category"><i class="fa fa-info-circle"></i></span></div>
                                                                <input type="text" id="editItemName" class="form-control editCateItem_input" data-inidata="" placeholder="Item Name" required>
                                                            </div>                                    
                                                            <div class="form-group">
                                                                <label>Icon</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="can be image with html tag or awesome font"><i class="fa fa-info-circle"></i></span></div>         
                                                                <input type="text" id="editIcon" class="form-control editCateItem_input" data-inidata="" placeholder="img tag or awesome font">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Item Description</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Say someting to explain this item"><i class="fa fa-info-circle"></i></span></div>         
                                                                <textarea class="form-control editCateItem_input" rows="5" data-inidata="" id="editItemDes"></textarea>
                                                            </div>
                                                        </form>                                
                                                        <div id="editCateItem_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/page_footer.php"); ?>