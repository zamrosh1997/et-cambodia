<?php 
$pageInfo = array('code'=>'announcement','name'=>'Announcement List');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");
$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}
?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-bullhorn fa-fw"></i> គ្រប់គ្រងសេចក្តីប្រកាស<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>               

                <!-- /.row -->

                <div class="row">                    

                    <div class="col-lg-8 col-md-12">                    	

                        <div class="panel panel-default" id="announcementList">

                            <div class="panel-heading">

                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីការប្រកាស</h3>                            

                            </div>

                            <!-- /.panel-heading -->

                            <div class="panel-body">

                            	

                                <div>

                                

                                <div class="row">

                                    <div class="form-group col-lg-6 col-md-6">

                                        <label>ប្រភេទ</label>

                                        <select class="form-control input-sm" id="typeid">

                                                  <option value="0">--- ជ្រើសរើស ---</option>

                                                  <?php
                                                        $type_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='announcement' AND item.active=1 ORDER BY item.priority ASC");
                                                        while($type_row = mysqli_fetch_assoc($type_qry)){
                                                                echo '<option value="'.$type_row['id'].'">'.$type_row['displayTitle'].'</option>';
                                                        }		
                                                        ?>  
                                         </select>

                                    </div> 

                                    <div class="form-group col-lg-6 col-md-6">

                                    	<label>ស្វែងរក</label>

                                        <form role="form" id="announcementList_frm">

                                            <div class="form-group">                        

                                                    <div class="input-group custom-search-form">

                                                        <input type="text" id="announcementList_search_txt" class="form-control" placeholder="ស្វែងរក">

                                                        <span class="input-group-btn">

                                                            <button class="btn btn-default" type="button" id="announcementList_search_btn">

                                                                <i class="fa fa-search"></i>

                                                            </button>

                                                        </span>

                                                    </div>

                                            </div>

                                        </form>

                                    </div> 

                                </div>

                                

                                <div>

                                	<div style="float:right;">

                                		<label>

                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>

                                            <div style="display:inline-block">

                                                <select class="form-control input-sm" id="nav_rowsPerPage">

                                                    <option value="5">០៥</option>
                                                    <option value="10" selected>១០</option>

                                                    <option value="20">២០</option>

                                                    <option value="30">៣០</option>

                                                </select>

                                            </div>

                                         </label>

                                    </div>

                                    <div style="float:left;">

                                		<label>

                                        	<span id="nav_info"></span>

                                         </label>

                                    </div>

                                </div>

                                <div style="clear:both; padding:2px 0;"></div>

                                

                                <div class="table-responsive" id="announcementList_tbl_cover">

                                    <table class="table table-striped table-bordered table-hover" id="announcementList_tbl">
                                        <thead>
                                            <tr>
                                                <th class="tableCellCenter">ល.រ.</th>
                                                <th>ចំនងជើង</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>            
                                        </tbody>
                                    </table>
                                </div>

                                <!-- /.table-responsive -->

                                <div class="form-group" style="width:100%; text-align:center;">

                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>

                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>

                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">

                                         <option value="1">1</option>

                                         <option value="2">2</option>

                                         <option value="3">3</option>

                                         <option value="4">4</option>

                                     </select>

                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>

                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>

                                </div>

                            </div>

                            <!-- /.panel-body -->

                        </div>

                        </div>

                        

                    </div>

                    

              <!--</div>

              <div class="row">-->

                    <div class="col-lg-4 col-md-8">
                        <div class="panel panel-default" id="newAnnouncement">
                            <div class="panel-heading">
                                <h3 class="panel-title" id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</h3>
                            </div>
                            <div class="panel-body">
                                <form role="form" id="newAnnouncement_frm">	 
                                	<input type="hidden" id="recordid" name="recordid" value="0" />
                                    <div class="form-group">
                                        <label>ប្រភេទ</label> <span class="redStar">*</span>
                                        <select class="form-control input-sm" id="newTypeid" name="newTypeid">
                                                  <option value="0">--- ជ្រើសរើស ---</option>
                                                  <?php
                                                        $type_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle,item.title title FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='announcement' AND item.active=1 ORDER BY item.priority ASC");
                                                        while($type_row = mysqli_fetch_assoc($type_qry)){
                                                                echo '<option value="'.$type_row['id'].'" data-code="'.$type_row['title'].'">'.$type_row['displayTitle'].'</option>';
                                                        }		
                                                        ?>  
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>ចំនងជើង</label> <span class="redStar">*</span>
                                        <input type="text" id="newTitle" name="newTitle" class="form-control" placeholder="ចំនងជើង" required>
                                    </div>
                                    <div class="form-group">
                                        <label>ខ្លឹមសារ</label> <span class="redStar">*</span>
                                        <textarea id="newDes" name="newDes"></textarea>
                                    </div> 
                                    <div class="form-group">
                                        <label>ភ្ទាប់ឯកសារ</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="សូមភ្ជាប់ឯកសារ ដែលមានប្រភេទជា jpg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, mp3, wav, mp4"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="file" id="file" name="file" class="form-control">
                                        <input type="hidden" id="allfiles" name="allfiles" />
                                        <div id="selectedFile" class="thumbnail" style="display:none; margin-top:10px;"></div>
                                    </div> 
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="publishNow" name="publishNow" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="publishNow"></label>
                                                </div>
                                                <div class="switch_label"> ដាក់បង្ហាញជាសាធារណៈ</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបើក ដើម្បីដាក់បង្ហាញជាសាធារណៈ"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                    </div>    
                                	<div class="form-group">
                                    	<input type="hidden" name="cmd" value="newAnnouncement" />
                                        <button type="submit" id="newAnnouncement_btn" name="newAnnouncement_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                        <button type="button" id="cancelAnnouncement_btn" name="cancelAnnouncement_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                    </div>
                                </form>       
                                <div id="newAnnouncement_msg" class=""></div>
                            </div>
                        </div>
                    </div>

                </div>                

                <!-- /.row -->

                <script>					

					// tooltip demo

					$('.tooltip-des').tooltip({

						selector: "[data-toggle=tooltip]",

						container: "body"

					})

					

					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_announcement").addClass('active');
						//--- end set active menu															

						$("#announcementList_search_btn").click(function(){announcementList('');});	
						$("#typeid").change(function(){announcementList('');});							

						//--- start navigation btn
						$("#nav_first").click(function(e){announcementList('first');});
						$("#nav_prev").click(function(e){announcementList('prev');});
						$("#nav_next").click(function(e){announcementList('next');});
						$("#nav_last").click(function(e){announcementList('last');});
						$("#nav_rowsPerPage").change(function(e){announcementList('');});
						$("#nav_currentPage").change(function(e){announcementList('goto');});
						//--- end navigation btn
					

						announcementList('');
							
						$("#newAnnouncement_frm").on('submit',(function(e) {addData(e,'newAnnouncement','announcementList');}));		
						$("#file").on('change',(function(e) {
							$("#newAnnouncement_btn").prop('disabled',true);
							uploadfile("newAnnouncement_btn",e,new FormData($("#newAnnouncement_frm")[0]));
						}));
						$("#cancelAnnouncement_btn").on('click',(function(e) {closeEditForm('newAnnouncement');}));	
						
                    });

                </script>



            </div>            

            

            <!-- /#page-wrapper -->

            				<div>

								<!-- Button trigger modal -->

                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>

                                <!-- Modal -->

                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">

                                    <div class="modal-dialog">

                                        <div class="modal-content">

                                            <!--<div class="modal-header">

                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                <h4 class="modal-title" id="edit_modalLabel">

                                                

                                                </h4>

                                            </div>-->

                                            <div class="modal-body" id="edit_modalLabelBodyText1">

                                                <div class="panel panel-default" id="editCateItem">

                                                    <div class="panel-heading">

                                                        <a name="a_editCateItem" class="anchorLocation"><div></div></a>

                                                        <h3 class="panel-title"><i class="fa fa-plus-circle fa-fw"></i> Edit Category Item</h3>

                                                    </div>

                                                    <div class="panel-body">

                                                        <form role="form" id="editCateItem_frm">		

                                                            <div class="form-group">

                                                                <label for="editCateTypeid">Category Type</label>

                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Select a main category type"><i class="fa fa-info-circle"></i></span></div>                                     

                                                        

                                                            </div>	

                                                            <div class="form-group">

                                                                <label>Category Item Name</label>

                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Item title or name for a specific category"><i class="fa fa-info-circle"></i></span></div>

                                                                <input type="text" id="editItemName" class="form-control editCateItem_input" data-inidata="" placeholder="Item Name" required>

                                                            </div>                                    

                                                            <div class="form-group">

                                                                <label>Icon</label>

                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="can be image with html tag or awesome font"><i class="fa fa-info-circle"></i></span></div>         

                                                                <input type="text" id="editIcon" class="form-control editCateItem_input" data-inidata="" placeholder="img tag or awesome font">

                                                            </div>

                                                            <div class="form-group">

                                                                <label>Item Description</label>

                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Say someting to explain this item"><i class="fa fa-info-circle"></i></span></div>         

                                                                <textarea class="form-control editCateItem_input" rows="5" data-inidata="" id="editItemDes"></textarea>

                                                            </div>

                                                        </form>                                

                                                        <div id="editCateItem_msg" class=""></div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="modal-footer">

                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>

                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>

                                                <input type="hidden" id="edit_confirmData" value="" />

                                            </div>

                                        </div>

                                        <!-- /.modal-content -->

                                    </div>

                                    <!-- /.modal-dialog -->

                                </div>

                                <!-- /.modal -->

                                

                                <!-- Button trigger modal -->

                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>

                                <!-- Modal -->

                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">

                                    <div class="modal-dialog">

                                        <div class="modal-content">

                                            <div class="modal-header">

                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                <h4 class="modal-title" id="yesno_modalLabel">

                                                

                                                </h4>

                                            </div>

                                            <div class="modal-body" id="yesno_modalLabelBodyText">

                                               

                                            </div>

                                            <div class="modal-footer">

                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>

                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>

                                                <input type="hidden" id="yesno_confirmData" value="" />

                                            </div>

                                        </div>

                                        <!-- /.modal-content -->

                                    </div>

                                    <!-- /.modal-dialog -->

                                </div>

                                <!-- /.modal -->

                           </div>  

<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<script>					

					tinymce.init({

						selector: "textarea",

						plugins: [

							"advlist autolink lists link image charmap print preview anchor",

							"searchreplace visualblocks code fullscreen"

						],

						toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"

					});

</script>   

<?php include("includes/page_footer.php"); ?>