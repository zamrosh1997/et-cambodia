<?php 
$pageInfo = array('code'=>'courses','name'=>'Training Courses');

include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-table fa-fw"></i> គ្រប់គ្រងវគ្គសិក្សា<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-7 col-md-7">                    	
                        <div class="panel panel-default" id="courseList">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីវគ្គសិក្សា</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="courseList_frm">
                                	<div class="row">
                                    	<div class="form-group col-lg-4 col-md-4 col-sm-4">                        
                                                <label>ឆ្នាំ</label>
                                                <select class="form-control input-sm" id="course_year">
                                                       <option value="0">--- ជ្រើសរើស ---</option>
                                                 <?php
                                                        $start_year = singleCell_qry("year","tblcourseschedule","active=1 ORDER BY year ASC LIMIT 1");
														
                                                        $thisYear = date("Y");
                                                        for($i=($start_year==''?$thisYear:$start_year);$i<=$thisYear;$i++){
                                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                                        }
                                                 ?>  
                                                 </select>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4 col-sm-4">                        
                                                <label>តំបន់</label>
                                                <select class="form-control input-sm" id="course_area">
                                                       <option value="0">--- ជ្រើសរើស ---</option>
                                                 <?php
                                                        $select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
                                                        while($select_row = mysqli_fetch_assoc($select_qry)){
                                                            echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
                                                        }	
                                                 ?>  
                                                 </select>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួនទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                    <option value="10">១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="courseList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="courseList_tbl">
                                        <thead>
                                            <tr>
                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                <th style="width:50px;">ឆ្នាំ</th>
                                                <th>តំបន់</th>
                                                <th>វគ្គ</th>
                                                <th>ចូលរៀន</th>
                                                <th>សិក្ខាសាលា</th>
                                                <th>សិក្ខាកាម</th>
                                                <th style="width:90px;" class="tableCellCenter"></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-5 col-md-5"> 
                    	<div class="panel panel-default" id="newCourse_cover">                        
                        	<div class="panel-heading">
                                <h3 class="panel-title" id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</h3>
                            </div>
                            <div class="panel-body">
                        		  <form role="form" id="newCourse_frm" action="" method="post">	
                                	<input type="hidden" id="recordid" name="recordid" value="0" />
                                    <div class="form-group"> 
                                    	<label for="newCourse_year">ប្រចាំឆ្នាំ</label> <span class="redStar">*</span>                       
                                        <select class="form-control input-sm" id="newCourse_year" name="newCourse_year">
                                              <option value="0">--- ជ្រើសរើស ---</option>
                                                 <?php
												 		$start_year = singleCell_qry("year","tblcourseschedule","active=1 ORDER BY year ASC LIMIT 1");
														
                                                        $thisYear = date("Y");
                                                        for($i=($start_year==''?$thisYear:$start_year);$i<=$thisYear+1;$i++){
                                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                                        }
                                                 ?>  
                                      </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="newCourse_area">ប្រចាំតំបន់</label> <span class="redStar">*</span>                       
                                        <select class="form-control input-sm" id="newCourse_area" name="newCourse_area">
                                                       <option value="0">--- ជ្រើសរើស ---</option>
                                                 <?php
                                                        $select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
                                                        while($select_row = mysqli_fetch_assoc($select_qry)){
                                                            echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
                                                        }	
                                                 ?>  
                                                 </select>
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label for="newCourse_num">វគ្គសិក្សាទី</label> <span class="redStar">*</span>
                                        <input type="text" id="newCourse_num" name="newCourse_num" class="form-control" placeholder="វគ្គសិក្សា" required>
                                    </div>
                                    <div class="form-group">                                        
                                        <div class="row">                                        	
                                        	<div class="form-group col-lg-6 col-md-6">
                                            	<label for="newRegister_date">ចូលរៀន</label> <span class="redStar">*</span>
                                                <div class="input-append input-group dtpicker">
                                                        <input data-format="yyyy-MM-dd" type="text" id="newRegister_date" name="newRegister_date" placeholder="ចាប់ពី" class="form-control" required>
                                                        <span class="input-group-addon add-on">
                                                            <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar" class="fa fa-calendar"></i>
                                                        </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6">
                                            	<label for="course_end_date">បញ្ចប់</label>
                                                <div class="input-append input-group dtpicker">
                                                        <input data-format="yyyy-MM-dd" type="text" id="course_end_date" name="course_end_date" placeholder="រហូតដល់" class="form-control">
                                                        <span class="input-group-addon add-on">
                                                            <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar" class="fa fa-calendar"></i>
                                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="total_hours">ចំនួនម៉ោង</label>
                                        <input type="text" id="total_hours" name="total_hours" class="form-control" placeholder="ចំនួន">
                                    </div>  
                                    <div class="form-group">
                                        <label for="newMax_participant">ចំនួនសិក្ខាកាម</label> <span class="redStar">*</span>
                                        <input type="text" id="newMax_participant" name="newMax_participant" class="form-control" placeholder="ចំនួន" required>
                                    </div>  
                                    <div class="form-group">     
                                        <label>មុខវិជ្ជា</label> <span class="redStar">*</span>
                                        <select class="form-control input-sm chosen-select" id="newSubject_id">
                                                <option value="0">--- ជ្រើសរើស ---</option>
                                                <?php
                                                        $select_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY publishedDate DESC");
                                                        while($select_row = mysqli_fetch_assoc($select_qry)){
                                                            echo '<option value="'.$select_row['id'].'">'.$select_row['title'].'</option>';
                                                        }	
                                                 ?>        
                                         </select>                                  
                                    </div> 
                                    <div class="form-group">
                                        <button type="button" id="addSubject_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                    </div>
                                    <div class="form-group">
                                    	<div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="subject_list">
                                                <thead>
                                                    <tr>
                                                        <th>មុខវិជ្ជា</th>
                                                        <th style="width:130px;" class="tableCellCenter">ប្រលងអនឡាញ</th>
                                                        <th style="width:100px;" class="tableCellCenter">ថែមពេល</th>
                                                        <th style="width:30px;" class="tableCellCenter"></th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                	<tr><td colspan="4" class="tableCellCenter">គ្មានមុខវិជ្ជា</td></tr> 
                                                </tbody>
                                            </table>
                                        </div>
                                        <input type="hidden" id="subjectData" name="subjectData" />
                                    </div>
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="start_online_exam" name="start_online_exam" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="start_online_exam"></label>
                                                </div>
                                                <div class="switch_label"> បើកការប្រលងអនឡាញ</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបិទ ឬបើក ដើម្បីចាបផ្តើមការប្រលងអនឡាញ"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="active" name="active" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="active"></label>
                                                </div>
                                                <div class="switch_label"> ដាក់អោយដំណើរការពេលនេះ</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបិទ ឬបើក ដើម្បីដាក់អោយដំណើរការពេលនេះ ឬចាំដាក់ដំណើរការពេលក្រោយ"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                    </div>        
                                	<div class="form-group">
                                    	<input type="hidden" name="cmd" value="newCourse" />
                                        <button type="submit" id="newCourse_btn" name="newCourse_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                        <button type="button" id="cancelCourse_btn" name="cancelCourse_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                    </div>
                                </form>                                
                                <div id="newCourse_msg" class=""></div>                   
                        	</div> 
                        </div>                   
                    </div>
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_courses").addClass('active');
						//--- end set active menu															
												
						//--- start navigation btn
						$("#nav_first").click(function(e){courseList('first');});
						$("#nav_prev").click(function(e){courseList('prev');});
						$("#nav_next").click(function(e){courseList('next');});
						$("#nav_last").click(function(e){courseList('last');});
						$("#nav_rowsPerPage").change(function(e){courseList('');});
						$("#nav_currentPage").change(function(e){courseList('goto');});
						//--- end navigation btn
						
						$("#courseList_frm").submit(function(e){courseList(''); e.preventDefault();});
						$("#course_year,#course_area").change(function(){courseList('');});						
						$("#addSubject_btn").click(addSubject_course);
						
						courseList('');
						
						$("#newCourse_year,#newCourse_area").change(nextCourseNumber);		
						$("#newCourse_frm").on('submit',(function(e) {addData(e,'newCourse','courseList');}));		
						$("#cancelCourse_btn").on('click',(function(e) {closeEditForm('newCourse');}));	
						
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-body">
                                                        <form role="form" id="editLesson_frm" action="/service/request" method="post" enctype="multipart/form-data">	
                                                            <div class="form-group">
                                                                <label>ចំនងជើងមុខវិជ្ជា</label>
                                                                <input type="text" id="editTitle" name="editTitle" class="form-control" placeholder="ចំនងជើង" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ឯកសារ៖</label>
                                                                <div id="currentDoc">
                                                                	<ul>
                                                                    	<li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                    </ul>
                                                                	
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ផ្លាស់ប្តូរឯកសារ</label>
                                                                <input type="file" id="editDoc" name="editDoc" class="form-control">
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="switch">
                                                                        <div class="switch_input">
                                                                        <input id="editallow_quiz" name="editallow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                                        <label for="editallow_quiz"></label>
                                                                        </div>
                                                                        <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                                </div>
                                                             </div> 
                                                                       
                                                            <div class="form-group">
                                                                <input type="hidden" name="cmd" value="editLesson" />
                                                                <input type="hidden" id="edit_confirmData" name="edit_confirmData" value="" />
                                                                <button type="submit" id="editLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                                            </div>
                                                        </form>                                
                                                        <div id="editLesson_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>    
<?php include("includes/page_footer.php"); 
				
			?>