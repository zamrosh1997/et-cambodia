<?php 
$pageInfo = array('code'=>'candidates','name'=>'Candidates');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");
$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}

?>

        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h3 class="page-header">
                        	<i class="fa fa-list fa-fw"></i> គ្រប់គ្រងឈ្មោះបេក្ខជនតាមវគ្គ<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>

                    </div>

                </div>

                

                <!-- /.row -->

                <div class="row">                    

                    <div class="col-lg-12 col-md-12">                    	

                        <div class="panel panel-default" id="candidateList">

                            <div class="panel-heading">                                
                                <div style="float:left;">
                            		<h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីឈ្មោះ</h3>   
                                </div>
                                <div style="float:right;">
                                    <button type="button" id="fullexport" data-listname="" data-sql="" class="btn btn-default"><img src="/img/excel_icon.png" /></button>
                                    <button type="button" id="partexport" data-listname="" data-sql="" class="btn btn-default"><img src="/img/excel_icon_1.png" /></button>
                                </div> 
                                <div class="clearfix"></div>                           

                            </div>

                            <!-- /.panel-heading -->

                            <div class="panel-body">

                            <div>                                
								<form role="form" id="candidateList_frm">
                                    <div>
                                        <div class="form-group" style="display:inline-block;">
                                            <label>ឆ្នាំ</label>
                                            <select class="form-control input-sm" id="course_year">
                                                   <option value="0">--- ជ្រើសរើស ---</option>
                                             <?php
                                                    $start_year = singleCell_qry("year","tblcourseschedule","active=1 ORDER BY year ASC LIMIT 1");
                                                    $thisYear = date("Y");
                                                    for($i=($start_year==''?$thisYear:$start_year);$i<=$thisYear;$i++){
                                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                                    }
                                             ?>  
                                             </select>
                                        </div> 
                                        <div class="form-group" style="display:inline-block;">
                                            <label>តំបន់</label>
                                            <select class="form-control input-sm" id="course_area" <?=($roleInfo['code']=='school' or $roleInfo['code']=='professor')?'disabled':''?>>
                                                   <option value="0">--- ជ្រើសរើស ---</option>
                                             <?php
                                                    $select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
                                                    while($select_row = mysqli_fetch_assoc($select_qry)){
                                                        if($roleInfo['code']=='school' or $roleInfo['code']=='professor'){
                                                            if($userInfo['area_id']==$select_row['id']){
                                                                echo '<option value="'.$select_row['id'].'" selected>'.$select_row['displayTitle'].'</option>';
                                                            }
                                                        }else{
                                                            echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
                                                        }
                                                        
                                                    }	
                                             ?>  
                                             </select>
                                        </div> 
                                        
                                        <div class="form-group" style="display:inline-block;">
                                            <label>វគ្គសិក្សា</label>
                                            <select class="form-control input-sm" id="course_id">
                                                   <option value="0">--- ជ្រើសរើស ---</option>
                                             </select>
                                        </div> 
                                        
                                        <div class="form-group" style="display:inline-block;">
                                            <label>បងប្រាក់</label>
                                            <select class="form-control input-sm" id="pending_status">
                                                   <option value="">--- ទាំងអស់ ---</option>
                                                   <option value="0">បងប្រាក់រួច</option>
                                                   <option value="1">មិនទានបង់ប្រាក់</option>
                                             </select>
                                        </div>
                                        
                                        <div class="form-group" style="display:inline-block;">
                                            <label>ស្ថានភាព</label>
                                            <select class="form-control input-sm" id="reg_status">
                                                   <option value="">--- ទាំងអស់ ---</option>
                                                   <option value="1">មានក្នុងបញ្ជីឈ្មោះ</option>
                                                   <option value="0">បានលុបចេញពីបញ្ជី</option>
                                             </select>
                                        </div>   
                                        <div class="form-group" style="display:inline-block;">
                                            <label>វិធីចុះឈ្មោះ</label>
                                            <select class="form-control input-sm" id="reg_via">
                                                   <option value="">--- ទាំងអស់ ---</option>
                                                   <option value="0">តាមរយៈសាលា</option>
                                                   <option value="1">ចុះឈ្មោះអនឡាញ</option>
                                             </select>
                                        </div>                                    
                                    </div>
                                    <div>
                                        <div class="form-group col-md-3" style="display:inline-block;padding-right: 0; padding-left: 0;">     
                                                <label>ឈ្មោះ ឬ License ID</label>                                		                       
                                                <div class="input-group custom-search-form">
                                                    <input type="text" id="candidateList_search_txt" class="form-control" placeholder="វាយឈ្មោះ ឬ License ID">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="submit">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                                <div style="clear:both; padding:2px 0;  "></div>
                                

                                <div>

                                	<div style="float:right;">

                                		<label>

                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>

                                            <div style="display:inline-block">

                                                <select class="form-control input-sm" id="nav_rowsPerPage">

                                                	<option value="5">៥</option>

                                                    <option value="10" selected>១០</option>

                                                    <option value="20">២០</option>

                                                    <option value="30">៣០</option>
                                                    
                                                    <option value="50">៥០</option>

                                                </select>

                                            </div>

                                         </label>

                                    </div>

                                    <div style="float:left;">

                                		<label>

                                        	<span id="nav_info"></span>

                                         </label>

                                    </div>

                                </div>

                    <div style="clear:both; padding:2px 0;"></div>

                    			<div>

                    				<table class="table table-striped table-bordered table-hover" id="candidateList_tbl">

                                        <thead>
                                            <tr>
                                                <th>ល.រ</th>
                                                <th>License ID</th>
                                                <th>គោត្តនាម និងនាម</th>
                                                <th>ភេទ</th>
                                                <th>ថ្ងៃកំណើត</th>
                                                <th>ទូរស័ព្ទ</th>
                                                <th>ថ្ងៃចុះឈ្មោះ</th>
                                                <th>មធ្យមភាគ</th>
                                                <th>និទ្ទេស</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>    
                                        </tbody>
                                    </table>

                                </div>

                                <div class="form-group" style="width:100%; text-align:center;">

                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>

                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>

                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">

                                         <option value="1">1</option>

                                         <option value="2">2</option>

                                         <option value="3">3</option>

                                         <option value="4">4</option>

                                     </select>

                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>

                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>

                  				</div>

                                

                                

                            </div>

                            <!-- /.panel-body -->

                        	</div>

                        </div>

                        

                    </div>

                    

                </div>                

                <!-- /.row -->

                <script>					

					// tooltip demo

					$('.tooltip-des').tooltip({

						selector: "[data-toggle=tooltip]",

						container: "body"

					})

					

					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_candidates").addClass('active');
						//--- end set active menu	
						//$("#lessonList_search_btn").click(function(){candidateList('');});
						$("#course_id,#pending_status,#reg_status,#reg_via").change(function(){candidateList('');});	
						$("#candidateList_frm").submit(function(e){candidateList(''); e.preventDefault();});
						//--- start navigation btn
						$("#nav_first").click(function(e){candidateList('first');});
						$("#nav_prev").click(function(e){candidateList('prev');});
						$("#nav_next").click(function(e){candidateList('next');});
						$("#nav_last").click(function(e){candidateList('last');});
						$("#nav_rowsPerPage").change(function(e){candidateList('');});
						$("#nav_currentPage").change(function(e){candidateList('goto');});
						//--- end navigation btn
						$("#course_year,#course_area").change(getCourse);
						candidateList('');
                    });

                </script>



            </div>            

            

            <!-- /#page-wrapper -->

            				<div>

								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!--<div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">     
                                                </h4>
                                            </div>-->
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-heading">
                                                        <a name="a_editCateItem" class="anchorLocation"><div></div></a>
                                                        <h3 class="panel-title"><i class="fa fa-plus-circle fa-fw"></i> Edit Category Item</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <form role="form" id="editCateItem_frm">	
                                                            <div class="form-group">
                                                                <label for="editCateTypeid">Category Type</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Select a main category type"><i class="fa fa-info-circle"></i></span></div>              
                                                            </div>	
                                                            <div class="form-group">
                                                                <label>Category Item Name</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Item title or name for a specific category"><i class="fa fa-info-circle"></i></span></div>
                                                                <input type="text" id="editItemName" class="form-control editCateItem_input" data-inidata="" placeholder="Item Name" required>
                                                            </div>                    
                                                            <div class="form-group">
                                                                <label>Icon</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="can be image with html tag or awesome font"><i class="fa fa-info-circle"></i></span></div>         
                                                                <input type="text" id="editIcon" class="form-control editCateItem_input" data-inidata="" placeholder="img tag or awesome font">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Item Description</label>
                                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="Say someting to explain this item"><i class="fa fa-info-circle"></i></span></div>         
                                                                <textarea class="form-control editCateItem_input" rows="5" data-inidata="" id="editItemDes"></textarea>
                                                            </div>
                                                        </form>                               
                                                        <div id="editCateItem_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->

                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">

                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="popup_btn" data-toggle="modal" data-target="#popup_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="popup_modal" tabindex="-1" role="dialog" aria-labelledby="popup_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width:800px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="popup_modalLabel">

                                                </h4>
                                            </div>
                                            <div class="modal-body" id="popup_modalLabelBodyText">
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="popup_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="popup_actionBtn"></button>
                                                <input type="hidden" id="popup_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                           </div>  
<?php include("includes/page_footer.php"); ?>