<?php 
$pageInfo = array('code'=>'payment','name'=>'Payment');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-usd fa-fw"></i>  គ្រប់គ្រងការបង់ប្រាក់<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-9 col-md-9">                    	
                        <div class="panel panel-default" id="paymentList">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីបង់ប្រាក់</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="paymentList_frm">
                                	<div class="row">
                                    	<div class="form-group col-lg-3 col-md-6 col-sm-6">  
                                            <div class="input-append input-group dtpicker">
                                                    <input data-format="yyyy-MM-dd" type="text" placeholder="From Date" id="payment_date_from" class="form-control">
                                                    <span class="input-group-addon add-on">
                                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                                    </span>
                                            </div>
                                        </div> 
                                        <div class="form-group col-lg-3 col-md-6 col-sm-6">  
                                            <div class="input-append input-group dtpicker">
                                                    <input data-format="yyyy-MM-dd" type="text" placeholder="To Date" id="payment_date_to" class="form-control">
                                                    <span class="input-group-addon add-on">
                                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-3 col-md-6 col-sm-6">  
                                            <select class="form-control input-sm" id="payment_result">
                                                  <option value="">ទាំងអស់</option> 
                                                  <option value="1">ជោគជ័យ</option> 
                                                  <option value="0">បរាជ័យ</option> 
                                         	</select>
                                        </div>   
                                        <div class="form-group col-lg-3 col-md-6 col-sm-6">                        
                                                <div class="input-group custom-search-form">
                                                    <input type="text" id="paymentList_search_txt" class="form-control" placeholder="វាយឈ្មោះ ឬ Transaction ID ឬ លេខគណនី">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" id="paymentList_search_btn">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួនទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                    <option value="10">១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="paymentList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="paymentList_tbl">
                                        <thead>
                                            <tr>
                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                <th style="width:100px;">License ID</th>
                                                <th>គោត្តនាម នាម</th>
                                                <th style="width:150px;">លេខគណនី Wing</th>
                                                <th style="width:150px;">Transaction ID</th>
                                                <th>ទឹកប្រាក់</th>
                                                <th style="width:150px;">កាលបរិច្ឆេទ​</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_payment").addClass('active');
						//--- end set active menu															
												
						//--- start navigation btn
						$("#nav_first").click(function(e){paymentList('first');});
						$("#nav_prev").click(function(e){paymentList('prev');});
						$("#nav_next").click(function(e){paymentList('next');});
						$("#nav_last").click(function(e){paymentList('last');});
						$("#nav_rowsPerPage").change(function(e){paymentList('');});
						$("#nav_currentPage").change(function(e){paymentList('goto');});
						//--- end navigation btn
						
						$("#paymentList_frm").submit(function(e){paymentList(''); e.preventDefault();});
						$("#paymentList_search_btn").click(function(){paymentList('');});
						$("#payment_date_from,#payment_date_to,#payment_result").change(function(){paymentList('');});
						
						paymentList('');
						
						
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-body">
                                                        <form role="form" id="editLesson_frm" action="/service/request" method="post" enctype="multipart/form-data">	
                                                            <div class="form-group">
                                                                <label>ចំនងជើងមុខវិជ្ជា</label>
                                                                <input type="text" id="editTitle" name="editTitle" class="form-control" placeholder="ចំនងជើង" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ឯកសារ៖</label>
                                                                <div id="currentDoc">
                                                                	<ul>
                                                                    	<li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                    </ul>
                                                                	
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ផ្លាស់ប្តូរឯកសារ</label>
                                                                <input type="file" id="editDoc" name="editDoc" class="form-control">
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="switch">
                                                                        <div class="switch_input">
                                                                        <input id="editallow_quiz" name="editallow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                                        <label for="editallow_quiz"></label>
                                                                        </div>
                                                                        <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                                </div>
                                                             </div> 
                                                                       
                                                            <div class="form-group">
                                                                <input type="hidden" name="cmd" value="editLesson" />
                                                                <input type="hidden" id="edit_confirmData" name="edit_confirmData" value="" />
                                                                <button type="submit" id="editLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                                            </div>
                                                        </form>                                
                                                        <div id="editLesson_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/page_footer.php"); 
				
			?>