<?php 
$pageInfo = array('code'=>'settings','name'=>'Advanced Settings');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c'); 
//if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}

$settingData = array();
$basicSetting_qry = exec_query_utf8("SELECT * FROM tblgeneralsetting WHERE categoryid IN (0) ORDER BY id ASC");
while($basicSetting_row = mysqli_fetch_assoc($basicSetting_qry)){
	$settingData[$basicSetting_row['settingName']] = array('value'=>$basicSetting_row['settingValue'],'des'=>$basicSetting_row['description']);
	
}
extract($settingData);

?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-cog fa-fw"></i> ការកំនត់ទូទៅ<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                
                <!-- /.row -->
                <div class="row"> 
                    <div class="col-lg-6 col-md-6">
                        	<div class="col-lg-12 col-md-12">
                            	<div class="panel panel-default settingBox" id="questionSettings">
                                    <div class="panel-heading box_title">
                                    	<span style="float:right; padding:0 3px;"><i class="fa fa-sort"></i></span>
                                        <h3 class="panel-title" ><i class="fa fa-file-o fa-fw"></i> កំនត់សំណួរ</h3>
                                    </div>
                                    <div class="panel-body box_body">
                                        <form role="form" id="questionSettings_frm">	                                    
                                            <div class="form-group">
                                                <label>ចំនួនសណួរសម្រាប់ការធ្វើតេស្ត</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $totalQuestion['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                <div class="switch">
                                                        <div class="switch_label">
                                                            <div class="input-group" style="width:180px;">
                                                                 <input type="text" id="totalQuestion" class="form-control" placeholder="ចំនួនសំណួរ" value="<?php echo $totalQuestion['value']; ?>" required>
                                                                 <span class="input-group-addon">សំណួរ</span>
                                                             </div> 
                                                        </div>
                                                </div>
                                                 
                                            </div>
                                            <div class="form-group">
                                                <label>អនុញ្ញាតអោយមាន ការប្រលងសាកល្បង (Test)</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $allowedQuiz['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                
                                                <div class="switch">
                                                        <div class="switch_input">
                                                        <input id="allowedQuiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" <?php echo $allowedQuiz['value']==1?'checked':''; ?>>
                                                        <label for="allowedQuiz"></label>
                                                        </div>
                                                        <div class="switch_label"> មិនអនុញ្ញាត / អនុញ្ញាត</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>បង្ហាញចម្លើយត្រឹមត្រូវ</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $showCorrectAnswer['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                
                                                <div class="switch">
                                                        <div class="switch_input">
                                                        <input id="showCorrectAnswer" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" <?php echo $showCorrectAnswer['value']==1?'checked':''; ?>>
                                                        <label for="showCorrectAnswer"></label>
                                                        </div>
                                                        <div class="switch_label"> មិនអនុញ្ញាត / អនុញ្ញាត</div>
                                                </div>
                                            </div>        
                                            <div class="form-group">
                                                <button type="submit" id="questionSettings_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> រក្សាទុក</button>
                                            </div>
                                        </form>                                
                                        <div id="questionSettings_msg" class=""></div>
                                    </div>
                                </div>  
                            </div>
                            
                            <div class="col-lg-12 col-md-12">
                            	<div class="panel panel-default settingBox" id="examSettings">
                                    <div class="panel-heading box_title">
                                    	<span style="float:right; padding:0 3px;"><i class="fa fa-sort"></i></span>
                                        <h3 class="panel-title" ><i class="fa fa-crosshairs fa-fw"></i> ការប្រលង</h3>
                                    </div>
                                    <div class="panel-body box_body">
                                        <form role="form" id="examSettings_frm">
                                        	<div class="form-group">
                                                <label>ចំនួនសណួរសម្រាប់ការប្រលងអនឡាញ</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $totalQuestion_OE['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                <div class="switch">
                                                        <div class="switch_label">
                                                            <div class="input-group" style="width:180px;">
                                                                 <input type="text" id="totalQuestion_OE" name="totalQuestion_OE" class="form-control" placeholder="ចំនួនសំណួរ" value="<?php echo $totalQuestion_OE['value']; ?>" required>
                                                                 <span class="input-group-addon">សំណួរ</span>
                                                             </div> 
                                                        </div>
                                                </div>
                                                 
                                            </div>                                    
                                            <div class="form-group">
                                                <label>កំឡូងពេលសម្រាប់ការប្រលង</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $examPeriod['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                <div class="switch">
                                                        <div class="switch_label">
                                                            <div class="input-group" style="width:180px;">
                                                                 <input type="text" id="examPeriod" name="examPeriod" class="form-control" placeholder="ចំនួនសំណួរ" value="<?php echo $examPeriod['value']; ?>" required>
                                                                 <span class="input-group-addon">ថ្ងៃ</span>
                                                             </div> 
                                                        </div>
                                                </div>
                                                 
                                            </div>
                                            <div class="form-group">
                                                <label>ចំនួនដងនៃការប្រលង</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $allowedExamTimes['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                <div class="switch">
                                                        <div class="switch_label">
                                                            <div class="input-group" style="width:180px;">
                                                                 <input type="text" id="allowedExamTimes" name="allowedExamTimes" class="form-control" placeholder="ចំនួនសំណួរ" value="<?php echo $allowedExamTimes['value']; ?>" required>
                                                                 <span class="input-group-addon">ដង</span>
                                                             </div> 
                                                        </div>
                                                </div>
                                                 
                                            </div>
                                            <div class="form-group">
                                                <label>រយៈពេលសម្រាប់ការប្រលងម្តងៗ</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $examSessionTime['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                <div class="switch">
                                                        <div class="switch_label">
                                                            <div class="input-group" style="width:180px;">
                                                                 <input type="text" id="examSessionTime" name="examSessionTime" class="form-control" placeholder="ចំនួនសំណួរ" value="<?php echo $examSessionTime['value']; ?>" required>
                                                                 <span class="input-group-addon">នាទី</span>
                                                             </div> 
                                                        </div>
                                                </div>
                                                 
                                            </div>
                                            <div class="form-group">
                                                <label>បង្ហាញចម្លើយត្រឹមត្រូវ</label>
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $showCorrectAnswer_OE['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                
                                                <div class="switch">
                                                        <div class="switch_input">
                                                        <input id="showCorrectAnswer_OE" name="showCorrectAnswer_OE" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" <?php echo $showCorrectAnswer_OE['value']==1?'checked':''; ?>>
                                                        <label for="showCorrectAnswer_OE"></label>
                                                        </div>
                                                        <div class="switch_label"> មិនអនុញ្ញាត / អនុញ្ញាត</div>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                            	<input type="hidden" name="cmd" value="examSettings" />
                                                <button type="submit" id="examSettings_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> រក្សាទុក</button>
                                            </div>
                                        </form>  
                                        <div id="examSettings_msg" class=""></div>   
                                    </div>
                                </div>  
                            </div>
                            
                            <div class="col-lg-12 col-md-12">
                            	<div class="panel panel-default settingBox" id="questionSettings">
                                    <div class="panel-heading box_title">
                                    	<span style="float:right; padding:0 3px;"><i class="fa fa-sort"></i></span>
                                        <h3 class="panel-title"><i class="fa fa-file-o fa-fw"></i> កំនត់តម្លៃចុះឈ្នោះ</h3>
                                    </div>
                                    <div class="panel-body box_body">
                                        <form role="form" id="registerFee_frm">	                                    
                                            <div class="form-group">
                                                <label>ឈ្មោះក្រុមហ៊ុន: </label>
                                                <input type="text" id="onlinePayCompanyName" name="onlinePayCompanyName" class="form-control" placeholder="ឈ្មោះក្រុមហ៊ុន" value="<?=$onlinePayCompanyName['value']?>" required>
                                            </div> 
                                            <div class="form-group">
                                                <label>ឈ្មោះទំនិញ: </label>
                                                <textarea id="onlinePayItemName_kh" name="onlinePayItemName_kh" required><?=$onlinePayItemName_kh['value']?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>ឈ្មោះទំនិញ (សម្រាប់ Wing): </label>
                                                <input type="text" id="onlinePayItemName" name="onlinePayItemName" class="form-control" placeholder="ឈ្មោះទំនិញ (ឡាតាំង)" value="<?=$onlinePayItemName['value']?>" required>
                                            </div>                                               
                                            <div class="form-group">
                                                <label>តម្លៃជាដុល្លា: </label>
                                                
                                                		<div class="switch">
                                                                        <div class="switch_label">
                                                                            <div class="input-group">
                                                                                 <input type="text" id="registrationFee" name="registrationFee" class="form-control" placeholder="Authentication ID" value="<?=$registrationFee['value']?>" required>
                                                                                 <span class="input-group-addon">$</span>
                                                                             </div> 
                                                                        </div>
                                                                </div>
                                            </div> 
                                            
                                             
                                            <div class="form-group">
                                            	<input type="hidden" name="cmd" value="registerFee" />
                                                <button type="submit" id="registerFee_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> រក្សាទុក</button>
                                            </div>
                                        </form>                                
                                        <div id="registerFee_msg" class=""></div>
                                    </div>
                                </div>  
                            </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                    	<div class="col-lg-12 col-md-12">
                        	<div class="panel panel-default settingBox" id="paymentSettings">
                                <div class="panel-heading box_title">
                                    	<span style="float:right; padding:0 3px;"><i class="fa fa-sort"></i></span>
                                    	<h3 class="panel-title"><img src="/admin/images/wing_logo.png" height="20" /> វីងបង់ប្រាក់អនឡាញ</h3>
                                </div>
                                <div class="panel-body box_body">
                                    <form role="form" id="wingOnlinePay_frm">	
                                    	<div class="form-group">
                                            <label>បង់ប្រាក់អនឡាញ</label>
                                            <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="<?php echo $allowedPayment['des']; ?>"><i class="fa fa-info-circle"></i></span></div>
                                                
                                            <div class="switch">
                                                 <div class="switch_input">
                                                 	<input id="allowedPayment" name="allowedPayment" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" <?php echo $allowedPayment['value']==1?'checked':''; ?>>
                                                 	<label for="allowedPayment"></label>
                                                 </div>
                                                 <div class="switch_label"> មិនអនុញ្ញាត / អនុញ្ញាត</div>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Authentication ID: </label>
                                            <input type="text" id="wingOnlinePayID" name="wingOnlinePayID" class="form-control" placeholder="Authentication ID" value="<?=$wingOnlinePayID['value']?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Authentication Code: </label>
                                            <input type="password" id="wingOnlinePayPassword" name="wingOnlinePayPassword" class="form-control" placeholder="Authentication Code" value="<?=$wingOnlinePayPassword['value']?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Authentication URL: </label>
                                            <input type="text" id="wingAuthenticateUrl" name="wingAuthenticateUrl" class="form-control" placeholder="Authentication URL" value="<?=$wingAuthenticateUrl['value']?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Redirect URL: </label>
                                            <input type="text" id="wingRedirectUrl" name="wingRedirectUrl" class="form-control" placeholder="Redirect URL" value="<?=$wingRedirectUrl['value']?>" required>
                                        </div>  
                                          
                                        <div class="form-group">
                                            <input type="hidden" name="cmd" value="updateWing" />
                                            <button type="submit" id="wingOnlinePay_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> រក្សាទុក</button>
                                        </div>
                                    </form>                                
                                    <div id="wingOnlinePay_msg" class=""></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                        	<div class="panel panel-default settingBox" id="features">
                                <div class="panel-heading box_title">
                                    	<span style="float:right; padding:0 3px;"><i class="fa fa-sort"></i></span>
                                    	<h3 class="panel-title"><i class="fa fa-info-circle fa-fw"></i> ព័ត៌មានអំពីវេបសាយ</h3>
                                </div>
                                <div class="panel-body box_body">
                                    <form role="form" id="features_frm">	                                    
                                        <div class="form-group">
                                            <label>លក្ខណៈនៃប្រព័ន្ធ </label>
                                            <textarea id="systemFeatures" name="systemFeatures" required><?=$systemFeatures['value']?></textarea>
                                        </div> 
                                          
                                        <div class="form-group">
                                            <input type="hidden" name="cmd" value="updateFeaturesTxt" />
                                            <button type="submit" id="features_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> រក្សាទុក</button>
                                        </div>
                                    </form>                                
                                    <div id="features_msg" class=""></div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu		
						$("#m_settings").addClass('active');
						//--- end set active menu	
						
						$("#questionSettings_btn").click(function(){updateSetting();return false;});
						
						$("#examSettings_frm").on('submit',(function(e) {addData(e,'examSettings','');}));	
						$("#registerFee_frm").on('submit',(function(e) {addData(e,'registerFee','');}));	
						$("#wingOnlinePay_frm").on('submit',(function(e) {addData(e,'wingOnlinePay','');}));
						$("#features_frm").on('submit',(function(e) {addData(e,'features','');}));	
						
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!--<div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>-->
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-heading">
                                                        <a name="a_editCateItem" class="anchorLocation"><div></div></a>
                                                        <h3 class="panel-title"><i class="fa fa-plus-circle fa-fw"></i> title</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/tinymce.php");?>
<script>					
		tinymce.init({			
			setup: function (ed) {
					ed.on('init', function(args) {
						//do sth when tinymce done loading
					});
				}
		});
		
		$(document).ready(function(e) {
			
			$(".box_title").click(function(){showHideDiv($(this))});
			$(".box_title").trigger('click')
		});
				
</script>   
<?php include("includes/page_footer.php"); 
				
			?>