<?php
include("../../includes/connect_db.php");
	
	$result=0;$msg = '';$filename='';$filesize='0KB';$filetype='';
	if(isset($_FILES["file"]["type"]))
	{
		$filepath = '../../documents/';
		$filepath_root = '/documents/';
		$validextensions = array("jpeg", "jpg", "png" , "pdf", "doc" ,"docx","xls","xlsx","ppt","pptx","mp3","wav","mp4");
		$validtype = array("image/png",
							"image/jpg",
							"image/jpeg",
							"application/pdf",
							"application/msword",
							"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
							"application/vnd.ms-excel",
							"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
							"application/vnd.ms-powerpoint",
							"application/vnd.openxmlformats-officedocument.presentationml.presentation",
							"audio/mp3",
							"audio/mpeg",
							"audio/wav",
							"video/mp4");
		$temporary = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		if($_FILES["file"]["size"] < (10*1000000)){
			if (in_array($_FILES["file"]["type"], $validtype) && in_array(strtolower($file_extension), $validextensions)) {
				if ($_FILES["file"]["error"] > 0)
				{
					//echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
					$result=0;$msg = $_FILES["file"]["error"];
				}
				else
				{
					$newfilename = str_replace(array(".".$file_extension," "),array("_".time().".".$file_extension,"_"),$_FILES['file']['name']);
					if (file_exists($filepath . $newfilename)) {
						//echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
						$result=0;$msg = 'exist';
					}
					else
					{
						$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
						$targetPath = $filepath.$newfilename; // Target path where file is to be stored
						move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
						
						/*echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
						echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
						echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
						echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
						echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";*/
						
						$result=1;$msg = 'uploaded';
					}
				}
			}
			else
			{
				//echo "<span id='invalid'>***Invalid file Size or Type***<span>";
				$result=0;$msg = 'invalid type';
			}
		}else{
			$result=0;$msg = 'invalid size';	
		}
		
		$filename=$_FILES["file"]["name"];
		$filesize=$_FILES["file"]["size"];
		$filetype=$_FILES["file"]["type"];
	}
	$data = array('result'=>$result,'msg'=>$msg,'filename'=>$filename,'newfilename'=>$newfilename,'filePath'=>$filepath_root,'filesize'=>format_filesize($filesize),'filetype'=>$filetype);
	echo json_encode($data);

?>