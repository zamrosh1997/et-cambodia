<?php include("../../includes/connect_db.php");

session_start(); //Start the session
//date_default_timezone_set('Europe/London'); 
if(!isset($_SESSION['adminid'])){
	echo 'no_authentication';
	exit;
}else{	
	$cmd = post("cmd");
	$sessonid = $_SESSION['adminid'];
}
if($cmd=='delFile'){
	$filename = post("filename");
	$filepath = '../../documents/';	
	unlink($filepath.$filename);	
}elseif($cmd=='lessonList'){
	$keyword = post("keyword");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");
	$sql_condition = 'WHERE';

	if($keyword<>''){if($sql_condition=='WHERE'){$sql_condition.=" title LIKE '%$keyword%'";}else{$sql_condition.=" AND title LIKE '%$keyword%'";}}
	if($sql_condition=='WHERE'){$sql_condition = '';}

	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tbllessons $sql_condition ORDER BY uploadedDate ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}

	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}

	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$lessonListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons $sql_condition ORDER BY uploadedDate ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($lesson_row = mysqli_fetch_assoc($lesson_qry)){	
		$ann_des = $lesson_row['description'];		
		$attachedFile = explode("|",$lesson_row['filename']);			
		$attachedFile = array_reverse($attachedFile);
		$attachedFile_list = '';$num=0;
		foreach($attachedFile as $value){
			if(strpos($ann_des,$value) === false){
				$num++;
				$totaldownload = singleCell_qry("download","tbldownload","filename='$value' and active=1 LIMIT 1");
				$attachedFile_list .= '<tr style="border-top:1px solid #dadada;">
											<td class="tableCellCenter">'.$num.'</td>
											<td style="padding:5px 0 5px 0;">'.$value.' <span style="color:blue; font-weight:bold;">(<i class="fa fa-download"></i> '.number_format($totaldownload).')</span></td>
										</tr>';		
				
			}
		}
		if($num==0){$attachedFile_list .= '<tr style="border-top:1px solid #dadada;"><td colspan="2" class="tableCellCenter" style="padding:5px 0 5px 0;">គ្មានឯកសារសម្រាប់ទាញយក</td></tr>';	}
		$totalDoc = count($attachedFile).' ឯកសារ (សម្រាប់ទាញយកៈ '.$num.') ';			
		$lessonListString .= '<tr>
									<td class="tableCellCenter" style="width:70px;">'.enNum_khNum($i).'</td>   
                                    <td>
										<h5 style="'.($lesson_row['active']==0?'color:red':'').'">'.$lesson_row['title'].'</h5>
										<span class="sub_text"><i class="fa fa-file-text-o fa-fw"></i> '.($lesson_row['filename']<>''?$totalDoc:'គ្មានឯកសារភ្ជាប់').'</span>
										<span class="sub_text"><i class="fa fa-clock-o fa-fw"></i> '.date('d/m/Y',strtotime($lesson_row['uploadedDate'])).'</span>
										<span class="sub_text"><i class="fa fa-crosshairs fa-fw"></i> '.$lesson_row['total_question'].' សំណួរ</span>
										<span class="sub_text"><i class="fa fa-share-square-o fa-fw"></i> '.date('d/m/Y',strtotime($lesson_row['publishedDate'])).'</span>
										<span class="sub_text"><i class="fa fa-eye fa-fw"></i> '.$lesson_row['views'].' ដង</span>
										<div style="background:#efefef; font-size:11px;">
											<table border="0" width="100%" cellspacing="0" cellpadding="5">
												<thead>
													<tr>
														<th style="width:50px;padding:5px 0 5px 0;" class="tableCellCenter">ល.រ.</td>
														<th>ឈ្មោះឯកសារ</td>
													</tr>
												</thead>
												<tbody>'.$attachedFile_list.'</tbody>
											</table>													
										</div>
									</td>
									<td class="tableCellCenter" style="width:50px;">
										<div class="btn_padding"><button class="btn btn-primary btn-xs editLesson_btn" data-recordid="'.$lesson_row['id'].'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
									</td>
                               </tr>';
		$i++;
	}
	if($lessonListString == ''){$lessonListString = '<tr><td colspan="3" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}

	$data = array('list'=>$lessonListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='newLesson'){
					$lessonid = trim($_POST['recordid']);
					$newTitle = trim($_POST['newTitle']);
					$filename = trim($_POST['allfiles']);
					$des = trim($_POST['newDes']);
					$newTotalQuestion = trim($_POST['newTotalQuestion']);					
					//$online_exam_date = trim($_POST['online_exam_date'])==''?"NULL":"'".trim($_POST['online_exam_date'])."'";
					$allow_quiz = isset($_POST['allow_quiz'])?1:0;
					$publishNow = isset($_POST['publishNow'])?1:0;

					$add_date = date("Y-m-d H:i:s");
					$result = 0; $msg='';
					if($newTitle<>'' and $filename<>'' and is_numeric($newTotalQuestion)){
						
						if($lessonid=='' or $lessonid==0){
							$inserted = exec_query_utf8("INSERT INTO tbllessons SET title='$newTitle',filename='$filename',description='".addslashes($des)."',allowQuiz=$allow_quiz,total_question=$newTotalQuestion,uploadedDate='$add_date'".($publishNow==1?",publishedDate='".$add_date."'":'').",active=$publishNow");
							if($inserted){$result = 1; $msg='inserted';}
						}else{
							if(is_numeric($lessonid)){
								$updated = exec_query_utf8("UPDATE tbllessons SET title='$newTitle',filename='$filename',description='".addslashes($des)."',allowQuiz=$allow_quiz,total_question=$newTotalQuestion,lastUpdateDate='$add_date',active=$publishNow WHERE id=$lessonid LIMIT 1");
								exec_query_utf8("UPDATE tbllessons SET publishedDate='$add_date' WHERE publishedDate IS NULL and id=$lessonid LIMIT 1");
								if($updated){$result = 1; $msg='updated';}
							}						
						}
					}
		$data = array('result'=>$result,'msg'=>$msg);
		echo json_encode($data);
}elseif($cmd=='questionList'){
	$keyword = post("keyword");
	$lessonid=post("lessonid");
	$examType=post("examType");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." title LIKE '%$keyword%'";}
	$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type='$examType'";	
	$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." lessonid = $lessonid";
	if($sql_condition=='WHERE'){$sql_condition = '';}

	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblquestions $sql_condition ORDER BY id DESC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}

	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}

	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$questionListString = '';$i=$startIndex+1;
	$question_qry = exec_query_utf8("SELECT * FROM tblquestions $sql_condition ORDER BY id ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($question_row = mysqli_fetch_assoc($question_qry)){	
		$options = explode('|',trim($question_row['options']));
		$respondsive_class = intval(12/count($options));
		$options_str ='';
		$orderNum = array('ក','ខ','គ','ឃ','ង','ច');
		$answer = $question_row['answer'] - 1;
		foreach($options as $key=>$value){
			$options_str .= '<div style=" text-align:center; font-size:12px;'.($key == $answer?'background:#387931;color:#fff;':'').'" class="col-lg-'.$respondsive_class.' col-md-'.$respondsive_class.'">'.($key == $answer?'<i class="fa fa-check fa-fw"></i> ':'').$orderNum[$key].'. ' .$value . '</div>';
		}
		$forecolor = $question_row['active']==0?'color:red':'';
		$color = $question_row['active']==0?'color:red;':'';
		$questionListString .= '<tr>
                                                <td style="overflow:hidden;">
                                                	<h5><span style="'.$color.'" class="iniEditQuestion" data-recordid="'.$question_row['id'].'">'.enNum_khNum($i).'. '.$question_row['title'].'</span></h5>
                                                    <div class="row">                    
                    										'.$options_str.'
                                                    </div>  
                                                </td>
                                            </tr>                ';
		$i++;
	}
	if($questionListString == ''){$questionListString = '<tr><td colspan="1" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}

	$data = array('list'=>$questionListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='newQuestion'){
	$getData = $_POST["getData"];
	extract($getData);
	$datetime = date('Y-m-d H:i:s');	
	
	$active = $active=='true'?1:0;
	
	$result = 0; $msg='';
	if($newTitle<>'' and $optionData<>'' and $newLessonid>0 and $newExamType>0 and $answer>0){						
		if($recordid=='' or $recordid==0){
			$inserted = exec_query_utf8("INSERT INTO tblquestions SET type=$newExamType,lessonid=$newLessonid,title='$newTitle',options='$optionData',answer=$answer,addedDate='$datetime',active='$active'");
			if($inserted){$result = 1; $msg='inserted';}
		}else{
			if(is_numeric($recordid)){
				$updated = exec_query_utf8("UPDATE tblquestions SET type=$newExamType,lessonid=$newLessonid,title='$newTitle',options='$optionData',answer=$answer,modifiedDate='$datetime',active='$active' WHERE id=$recordid LIMIT 1");
				if($updated){$result = 1; $msg='updated';}
			}						
		}
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
}elseif($cmd=='iniEditQuestion'){
	$lessonid=post("recordid");
	$data=array();
	$q_qry = exec_query_utf8("SELECT * FROM tblquestions WHERE id=$lessonid LIMIT 1");
	while($q_row = mysqli_fetch_assoc($q_qry)){
		$data = array('type'=>$q_row['type'],'title'=>$q_row['title'],'lessonid'=>$q_row['lessonid'],'q_options'=>$q_row['options'],'answer'=>$q_row['answer'],'active'=>$q_row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='scoreList'){
	$lessonid=post("lessonid");
	
	$sql_condition = 'WHERE';
	$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." lessonid = $lessonid";
	if($sql_condition=='WHERE'){$sql_condition = '';}

	$scoreListString ='';$totalScore=0;
	$score_qry = exec_query_utf8("SELECT * FROM tblscoresetting $sql_condition ORDER BY id ASC");
	while($score_row = mysqli_fetch_assoc($score_qry)){	
		$lessonid = $score_row['lessonid'];
		$score = $score_row['score'];

		//total question available
		$totalExistingQ = mysqli_num_rows(exec_query_utf8("SELECT id FROM tblquestions WHERE lessonid=$lessonid AND active=1"));


		//get total question allowed

		$totalQuestionAllowed = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion' LIMIT 1");

		$scoreListString .= '<tr>
                                            	<td>'.singleCell_qry("title","tbllessons","id=$lessonid LIMIT 1").'</td>
												<td>'.enNum_khNum($totalQuestionAllowed).'</td>
												<td  class="tableCellCenter">'.$score.'</td>
                                            </tr> ';
	}

	if($scoreListString == ''){$scoreListString = '<tr><td colspan="3" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}

	//$totalScore = '<tr style="background:#e1efaf; font-weight:bold;"><td colspan="2" style="text-align:right;">ពិន្ទុសរុប</td><td class="tableCellCenter">'.$totalScore.'</td></tr>';
	$totalScore = '';
	$data = array('list'=>$scoreListString,'score'=>$totalScore);
	echo json_encode($data);
}elseif($cmd=='setScore'){
	$getData = $_POST["getData"];
	extract($getData);
	$datetime = date('Y-m-d H:i:s');

	$actionType = '';	
	if(mysqli_num_rows(exec_query_utf8("SELECT id FROM tblscoresetting WHERE lessonid=$lessonid"))==0){
		exec_query_utf8("INSERT INTO tblscoresetting SET lessonid=$lessonid,score=$txtScore,datetime='$datetime'");
		$actionType = 'inserted';
	}else{
		exec_query_utf8("UPDATE tblscoresetting SET score=$txtScore,datetime='$datetime' WHERE lessonid=$lessonid");
		$actionType = 'updated';
	}
	echo $actionType;	
}elseif($cmd=='updateSetting'){
	$updateData = $_POST["updateData"];
	$datetime = date('Y-m-d H:i:s');
	foreach($updateData as $key=>$value){
		exec_query_utf8("UPDATE tblgeneralsetting SET settingValue='$value',datetime='$datetime' WHERE settingName='$key' LIMIT 1");	
	}
}elseif($cmd=='announcementList'){
	$keyword = post("keyword");
	$typeid=post("typeid");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." title LIKE '%$keyword%'";}
	$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type = $typeid";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblannouncement $sql_condition ORDER BY datetime DESC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$announcementListString = '';$i=$startIndex+1;
	$announcement_qry = exec_query_utf8("SELECT * FROM tblannouncement $sql_condition ORDER BY datetime DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($announcement_row = mysqli_fetch_assoc($announcement_qry)){	
		$totalDoc = count(explode("|",$announcement_row['filename']));
		$announcementListString .= '<tr>
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td>
													<h5 style="'.($announcement_row['active']==0?'color:red':'').'">'.$announcement_row['title'].'</h5>
													<span class="sub_text"><i class="fa fa-file-text-o fa-fw"></i> '.($announcement_row['filename']<>''?'មាន '.$totalDoc.' ឯកសារ':'គ្មានឯកសារភ្ជាប់').'</span>
													<span class="sub_text"><i class="fa fa-clock-o fa-fw"></i> '.date('d/m/Y',strtotime($announcement_row['datetime'])).'</span>
													<span class="sub_text"><i class="fa fa-eye fa-fw"></i> '.$announcement_row['view'].' ដង</span>
												</td>
												<td class="tableCellCenter" style="width:50px;">
													<div class="btn_padding"><button class="btn btn-primary btn-xs editAnnouncement_btn" data-recordid="'.$announcement_row['id'].'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
												</td>
                                            </tr>                ';
		$i++;
	}

	if($announcementListString == ''){$announcementListString = '<tr><td colspan="3" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$announcementListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='newAnnouncement'){
	//$getData = $_POST["getData"];
	//extract($getData);
	$recordid = $_POST["recordid"];
	$newTypeid = $_POST["newTypeid"];
	$examid = $_POST["examid"];
	$newTitle = $_POST["newTitle"];
	$newDes = $_POST["newDes"];
	$expiredDate = $_POST["expiredDate"];
	$allfiles = $_POST["allfiles"];	
	$publishNow = isset($_POST['publishNow'])?1:0;
	$datetime = date('Y-m-d H:i:s');

	
	
	$result = 0; $msg='';
	if($newTypeid>0 and $newTitle<>'' and ($allfiles<>'' or $newDes<>'')){						
		if($recordid=='' or $recordid==0){
			$inserted = exec_query_utf8("INSERT INTO tblannouncement SET type=$newTypeid,title='".addslashes($newTitle)."',filename='$allfiles',description='".addslashes($newDes)."',data='$examid',userid=$sessonid,expiredDate=".($expiredDate==''?"NULL":"'$expiredDate'").",datetime='$datetime',active=$publishNow");
			if($inserted){$result = 1; $msg='inserted';}
		}else{
			if(is_numeric($recordid)){
				$updated = exec_query_utf8("UPDATE tblannouncement SET type=$newTypeid,title='".addslashes($newTitle)."',filename='$allfiles',description='".addslashes($newDes)."',data='$examid',userid=$sessonid,expiredDate=".($expiredDate==''?"NULL":"'$expiredDate'").",lastUpdateDate='$datetime',active=$publishNow WHERE id=$recordid LIMIT 1");
				if($updated){$result = 1; $msg='updated';}
			}						
		}
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='iniEditAnnouncement'){
	$recordid=post("recordid");
	$data=array();
	$ann_qry = exec_query_utf8("SELECT * FROM tblannouncement WHERE id=$recordid LIMIT 1");
	while($ann_row = mysqli_fetch_assoc($ann_qry)){
		//$doc_str = '<li style="color:red;">គ្មានឯកសារភ្ជាប់</li>';
		$doc_path = '/documents/';
		$filenames = array();
		if($ann_row['filename']<>''){
			//$doc_str = '<li><a href="'.$doc_path.$lesson_row['filename'].'" target="_blank"><i class="fa fa-file-text-o fa-fw"></i> '.$lesson_row['filename'].'</a></li>';
			$filenames = explode("|",$ann_row['filename']);
		}
		$data = array('type'=>$ann_row['type'],'title'=>$ann_row['title'],'des'=>$ann_row['description'],'examid'=>$ann_row['data'],'expiredDate'=>$ann_row['expiredDate'],'filenames'=>$filenames,'filenames_str'=>$ann_row['filename'],'filePath'=>$doc_path,'active'=>$ann_row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='iniEditLesson'){
	$lessonid=post("recordid");
	$data=array();
	$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id=$lessonid LIMIT 1");
	while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
		//$doc_str = '<li style="color:red;">គ្មានឯកសារភ្ជាប់</li>';
		$doc_path = '/documents/';
		$filenames = array();
		if($lesson_row['filename']<>''){
			//$doc_str = '<li><a href="'.$doc_path.$lesson_row['filename'].'" target="_blank"><i class="fa fa-file-text-o fa-fw"></i> '.$lesson_row['filename'].'</a></li>';
			$filenames = explode("|",$lesson_row['filename']);
		}
		$data = array('title'=>$lesson_row['title'],'des'=>$lesson_row['description'],'filenames'=>$filenames,'filenames_str'=>$lesson_row['filename'],'filePath'=>$doc_path,'allowQuiz'=>$lesson_row['allowQuiz'],'totalQuestion'=>$lesson_row['total_question'],'online_exam_date'=>$lesson_row['online_exam_date'],'publishNow'=>$lesson_row['active']);
	}
	echo json_encode($data);
}/*elseif($cmd=='editLesson'){
					$lessonid = trim($_POST['edit_confirmData']);
					$editTitle = trim($_POST['editTitle']);
					$editallow_quiz = isset($_POST['editallow_quiz'])?1:0;

					$filename = $_FILES["editDoc"]["name"];
					$filetype = $_FILES["editDoc"]["type"];
					$filesize = $_FILES["editDoc"]["size"];	
					
					$add_date = date("Y-m-d H:i:s");
					$allowedType = array('application/pdf');

					if($editTitle<>''){
						if($filetype!=NULL){
							if (in_array($filetype, $allowedType)) {	
								$path = '../../documents/';
								if(file_exists($path.$filename))
								{
									echo "<script> alert('Error: File named ($filename) already exists.');window.location.href='/admin/lessons'</script>";	
								}else{		
									$currentFilename = singleCell_qry("filename","tbllessons","id=$lessonid LIMIT 1");
									if(trim($currentFilename)<>''){unlink($path.$currentFilename);}	
									exec_query_utf8("UPDATE tbllessons SET title='$editTitle',filename='$filename',allowQuiz=$editallow_quiz,lastUpdateDate='$add_date' WHERE id=$lessonid LIMIT 1");
									move_uploaded_file($_FILES["editDoc"]["tmp_name"], $path.$filename);	
									echo "<script> alert('Success: Lesson updated!'); window.location.href='/admin/lessons'</script>";
								}
							}else{
								echo "<script> alert('Error: Wrong file format! Only document type (pdf,doc,docx,xls,ppt) are allowed.'); window.location.href='/admin/lessons'</script>";
							}
						}else{
							exec_query_utf8("UPDATE tbllessons SET title='$editTitle',allowQuiz=$editallow_quiz,lastUpdateDate='$add_date' WHERE id=$lessonid LIMIT 1");
							echo "<script> alert('Success: Lesson updated!'); window.location.href='/admin/lessons'</script>";
						}
					}else{
						echo "<script> alert('Error: Incomplete Data.');window.location.href='/admin/lessons'</script>";
					}
}*/elseif($cmd=='guideList'){
	$keyword = post("keyword");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = '';
	if($keyword<>''){$sql_condition.= " AND (code LIKE '%$keyword%' OR firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%' OR firstNameEn LIKE '%$keyword%' OR lastNameEn LIKE '%$keyword%' OR mobile LIKE '%$keyword%')";}
	//work with total page
	$fullsql = "SELECT * FROM tblusers WHERE type=(select id from user_role where code='guide' limit 1) $sql_condition ORDER BY registerDate DESC";
	$navRow_qry = exec_query_utf8($fullsql);
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}	

	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}

	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$guideListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$partsql = "SELECT * FROM tblusers WHERE type=(select id from user_role where code='guide' limit 1) $sql_condition ORDER BY registerDate DESC LIMIT ".$startIndex.",$rowsPerPage";
	$guide_qry = exec_query_utf8($partsql);
	while($guide_row = mysqli_fetch_assoc($guide_qry)){	
		$isIncourse = isIncourse($guide_row['id']);
		$guide_langauge = singleCell_qry("name_kh","tbllanguages","id=".($guide_row['language_id']==NULL?0:$guide_row['language_id'])." LIMIT 1");
		$guideListString .= '<tr style="'.($guide_row['active']==0?'color:red;':($guide_row['pending']==1?'color:#9da209;':'')).'">
									<td class="tableCellCenter">'.enNum_khNum($i).'</td>    
                                    <td>'.$guide_row['code'].'</td>
									<td>'.$guide_row['lastName'].' '.$guide_row['firstName'].'</td>
									<td>'.$guide_row['lastNameEn'].' '.$guide_row['firstNameEn'].'</td>
									<td>'.($guide_row['gender']=='m'?'ប្រុស':'ស្រី').'</td>
									<td>'.date("d.m.Y",strtotime($guide_row['dob'])).'</td>
									<td>'.$guide_langauge.'</td>
									<td>'.$guide_row['origin_id'].'</td>
									<td class="tableCellLeftCenter">
										<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs editGuide_btn" data-recordid="'.$guide_row['id'].'"><i class="fa fa-pencil-square-o fa-fw"></i></button></div>
										<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs statusGuide_btn" data-recordid="'.$guide_row['id'].'" data-status="'.$guide_row['active'].'" data-licenseid="'.($guide_row['code']==''?$guide_row['firstName']:$guide_row['code']).'"><i class="fa fa-ban fa-fw"></i></button></div>
										'.($guide_row['pending']==1?
										'<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs pendingGuide_btn" data-recordid="'.$guide_row['id'].'" data-status="'.$guide_row['pending'].'" data-licenseid="'.($guide_row['code']==''?$guide_row['firstName']:$guide_row['code']).'"><i class="fa fa-exclamation-triangle"></i></button></div>
										':'').
										((!$isIncourse['result'] or !$isIncourse['active'])?
										'<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs addCourseGuide_btn" data-recordid="'.$guide_row['id'].'" data-licenseid="'.($guide_row['code']==''?$guide_row['firstName']:$guide_row['code']).'"><i class="fa fa-plus-circle"></i></button></div>
										':'').'									
									</td>
                               </tr>';
		$i++;
	}
	if($guideListString == ''){$guideListString = '<tr><td colspan="9" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}

	$data = array('listname'=>$cmd,'fullsql'=>encodeString($fullsql,$encryptKey),'partsql'=>encodeString($partsql,$encryptKey),'list'=>$guideListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='loadProfile'){
	$recordid=post("recordid");
	$data = mysqli_fetch_array(exec_query_utf8("SELECT * FROM tblusers WHERE type=(select id from user_role where code='guide' limit 1) AND id=$recordid"),MYSQLI_ASSOC);
	echo json_encode($data);
}elseif($cmd=='updateGuide'){
	$recordid=post("recordid");
	$guide_code=post("guide_code");
	exec_query_utf8("update tblusers set code='$guide_code' WHERE type=(select id from user_role where code='guide' limit 1) AND id=$recordid");
}elseif($cmd=='getCourse'){
	$year=$_POST['course_year'];
	$area=$_POST['course_area'];	
	
	$result=1;$data='';
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE area_id=$area AND year=$year AND active=1 ORDER BY course_num ASC");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$course_id  = $select_row['id'];
		$register_date  = $select_row['register_date'];
		$start_ws  = $select_row['workshop_start_date'];
		$end_ws  = $select_row['workshop_end_date'];
		
		$exam_date = singleCell_qry("exam_date","tblexamschedule","exam_date>'$end_ws' AND active=1 ORDER BY exam_date ASC LIMIT 1");
		
		$total_participant = mysqli_num_rows(exec_query_utf8("SELECT id FROM tblcourseregister WHERE course_id=$course_id AND active=1"));
		
		$data.= '<option value="'.$course_id.'" '.($total_participant>0?'style="color:blue;"':'').'>វគ្គទី '.$select_row['course_num'].' (ចូលរៀន '.date("d/m/Y",strtotime($register_date)).')</option>';
	}
	
	if(mysqli_num_rows($select_qry)==0){$result=0;}
	echo json_encode(array('result'=>$result,'data'=>$data));	
}elseif($cmd=='courseListMultiple'){
	$year=$_POST['course_year'];
	$area=$_POST['course_area'];	
	$selected=explode(',',trim($_POST['selected']));	
	
	$result=1;$data='';
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE area_id=$area AND year=$year AND active=1 ORDER BY course_num ASC");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$course_id  = $select_row['id'];	
		if(in_array($course_id,$selected)){
			$data.= '<option value="'.$course_id.'" selected>វគ្គទី '.$select_row['course_num'].'</option>';
		}else{
			$data.= '<option value="'.$course_id.'">វគ្គទី '.$select_row['course_num'].'</option>';
		}
	}	
	if(mysqli_num_rows($select_qry)==0){$result=0;}
	echo json_encode(array('result'=>$result,'data'=>$data));	
}elseif($cmd=='candidateList'){
	$keyword = post("keyword");
	$course_id = post("course_id");
	$pending_status = post("pending_status");
	$reg_status = post("reg_status");
	$reg_via = post("reg_via");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");	

	$sql_condition = 'AND';
	if($keyword<>''){$sql_condition.= ($sql_condition=='AND'?'':' AND')." user_id IN (select id from tblusers where code LIKE '%$keyword%' OR firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%' OR firstNameEn LIKE '%$keyword%' OR lastNameEn LIKE '%$keyword%')";}
	if($pending_status<>''){$sql_condition.= ($sql_condition=='AND'?'':' AND')." pending=$pending_status";}
	if($reg_status<>''){$sql_condition.= ($sql_condition=='AND'?'':' AND')." active=$reg_status";}
	if($reg_via<>''){$sql_condition.= ($sql_condition=='AND'?'':' AND')." online_reg=$reg_via";}
	if($sql_condition=='AND'){$sql_condition = '';}
	//work with total page
	$fullsql = "SELECT * FROM tblcourseregister WHERE course_id=$course_id $sql_condition ORDER BY register_date ASC";
	$navRow_qry = exec_query_utf8($fullsql);
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}

	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	//get subject by course
	$course_subject = json_decode(singleCell_qry("subject_id","tblcourseschedule","id=$course_id LIMIT 1"),true);
	$subject_id_arr = array_keys($course_subject);
	$subject_id_str = implode(",",$subject_id_arr);
	
	//subject score
	$subjectData = array();$scoreCol=array('Paper','Online','Total','Grade');
	$subjectName = $subjectScore = '';
	foreach($subject_id_arr as $key => $value){
		$getname = singleCell_qry("title","tbllessons","id=$value LIMIT 1");
		$subjectName .= '<th colspan="'.count($scoreCol).'" class="tableCellCenter">'.$getname.'</th>';
		$subjectScore .= '<th class="tableCellCenter">'.implode('</th><th class="tableCellCenter">',$scoreCol).'</th>';
	}
	$subjectTable = '<form role="form" id="update_score_frm">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>'.$subjectName.'</tr>
								<tr>'.$subjectScore.'</tr>
							</thead>
							<tbody><tr>[score_row]</tr></tbody>
						</table>
						<div class="modal-footer">
							<input type="hidden" name="reg_id" value="[reg_id]" />
							<input type="hidden" name="cmd" value="update_score" />
							<button type="button" class="btn btn-default" data-dismiss="modal" id="popup_modalCloseBtn">Cancel</button>
							<button type="submit" class="btn btn-primary" id="update_score">Save</button>
						</div>
						<div id="update_score_msg" class=""></div>
					</form><script>$("#update_score_frm").unbind("submit");$("#update_score_frm").submit(function(e){update_score(e);});</script>';

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$candidateListString = '';$i=$startIndex+1;
	$mainQry = "select * from tblcourseregister WHERE course_id=$course_id $sql_condition order by register_date DESC";	
	
	$partsql = $mainQry." LIMIT ".$startIndex.",$rowsPerPage";
	$candidate_qry = exec_query_utf8($partsql);
	while($candidate_row = mysqli_fetch_assoc($candidate_qry)){	
		$rowid = $candidate_row['id'];
		$candidate_data = qry_arr("code,firstName,lastName,gender,dob,mobile,active","tblusers","id='".$candidate_row['user_id']."' LIMIT 1");
		$fullname = $candidate_data['lastName'].' '.$candidate_data['firstName'];	
		
		//get score each subject
		$totalScore = 0;$paper_perc=0.8;$online_perc=0.2;$did_exam=false;
		$score_row = '';
		foreach($subject_id_arr as $key => $value){
			$examData = qry_arr("paper_exam_score,online_exam_score","exam_score","reg_id=$rowid and subject_id=$value and active=1 LIMIT 1");
			if(count($examData)>1){$did_exam=true;}
			$paper_exam_score = $examData['paper_exam_score'];
			$online_exam_score = $examData['online_exam_score'];
			$paper_score_calc = $paper_exam_score;			
			$totalScore_bySubject = $paper_score_calc + $online_exam_score;
			$totalScore += $totalScore_bySubject;
							
			$score_row .= '<td class="tableCellCenter"><input style="width:50px;" name="subject_'.$value.'" type="text" value="'.$paper_exam_score.'" /></td>
							<td class="tableCellCenter">'.$online_exam_score.'</td>
							<td class="tableCellCenter">'.$totalScore_bySubject.'</td>
							<td class="tableCellCenter">'.getGrade($totalScore_bySubject).'</td>';
		}		
		$subjectData[$rowid] = str_replace(array('[score_row]','[reg_id]'),array($score_row,encodeString($rowid,$encryptKey)),$subjectTable);		
		
		
		$total_subject = count($subject_id_arr);
		$agv_score = number_format($totalScore/$total_subject,2);
		
		
		//get total online exam score
		/*$totalOnlineScore = 0;
		$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY publishedDate DESC LIMIT 10");
		while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
			//get maximum online exam score
			$onlineMaxScore = singleCell_qry("score","tblrandomquestion","userid=".$candidate_row['user_id']." and lessonid=".$lesson_row['id']." and exam=1 and active=1 ORDER BY score DESC limit 1");
			if(is_numeric($onlineMaxScore)){$totalOnlineScore += $onlineMaxScore;}
		}
		$totalLesson = mysqli_num_rows($lesson_qry);
		$totalOnlineScore = number_format($totalOnlineScore/$totalLesson,2);*/
		
		//get score detail
		
		
		if(	$candidate_data['active']==0 or $candidate_row['active']==0){$active=false;}else{$active=true;}

		$candidateListString .= '<tr style="'.(!$active?'color:red;':($candidate_row['pending']==1?'color:#eca613;':'')).'">
									<td class="tableCellCenter">'.enNum_khNum($i).'</td>      
                                    <td>'.$candidate_data['code'].'</td>
									<td><a href="/admin/guides/'.encodeString($candidate_row['user_id'],$encryptKey).'">'.$fullname.'</a></td>
									<td>'.($candidate_data['gender']=='f'?'ស្រី':'ប្រុស').'</td>
									<td>'.($candidate_data['dob']==''?'N/A':date('d/m/Y',strtotime($candidate_data['dob']))).'</td>
									<td>'.$candidate_data['mobile'].'</td>
									<td>'.date('d/m/Y',strtotime($candidate_row['register_date'])).'</td>
									<td id="total_'.$rowid.'">'.($did_exam?$agv_score:'---').'</td>
									<td id="total_'.$rowid.'">'.($did_exam?getGrade($agv_score):'---').'</td>
									<td>'.($candidate_data['active']==1?'										
										<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs score_btn" data-recordid="'.$candidate_row['id'].'" data-guidename="'.$fullname.'"><i class="fa fa-crosshairs"></i></button></div>
										<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs courseRegStatus_btn" data-recordid="'.$candidate_row['id'].'" data-status="'.$candidate_row['active'].'" data-licenseid="'.$candidate_row['user_code'].' ('.$fullname.')" style="'.($candidate_row['active']==0?'background:#d93904;':'').'"><i class="fa fa-ban fa-fw"></i></button></div>
										<div class="btn_padding" style="display:inline-block"><button class="btn btn-primary btn-xs paidStatus_btn" data-recordid="'.$candidate_row['id'].'" data-status="'.$candidate_row['active'].'" data-licenseid="'.$candidate_row['user_code'].' ('.$fullname.')" '.($candidate_row['pending']==0?'disabled':'').' style="'.($candidate_row['active']==0?'background:#d93904;':'').'"><i class="fa fa-usd fa-fw"></i></button></div>
										<div class="btn_padding" style="display:inline-block"><a href="/certificate/'.encodeString($candidate_row['id'],$encryptKey).'" target="_new"><button class="btn btn-primary btn-xs certificate_btn"><i class="fa fa-graduation-cap"></i></button></a></div>
										':'').'
									</td>
                               </tr>';
		$i++;
	}

	if($candidateListString == ''){$candidateListString = '<tr><td colspan="10" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('subjectData'=>$subjectData,'listname'=>$cmd,'fullsql'=>encodeString($fullsql,$encryptKey),'partsql'=>encodeString($partsql,$encryptKey),'list'=>$candidateListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);	
}elseif($cmd=='update_score'){
	$datetime = date("Y-m-d H:i:s");
	$reg_id = decodeString($_POST['reg_id'],$encryptKey);
	$course_id = singleCell_qry("course_id","tblcourseregister","id=$reg_id LIMIT 1");
	//get subject by course
	$result=0;$msg='Error';
	$course_subject = json_decode(singleCell_qry("subject_id","tblcourseschedule","id=$course_id LIMIT 1"),true);
	$subject_id_arr = array_keys($course_subject);
	foreach($subject_id_arr as $key=>$value){
		$scoreBysubject = $_POST['subject_'.$value];
		if(is_numeric($scoreBysubject) and $scoreBysubject>=0){
			$exist = singleCell_qry("id","exam_score","reg_id=$reg_id and subject_id=$value and active=1 LIMIT 1");
			if($exist){
				exec_query_utf8("update exam_score set paper_exam_score=$scoreBysubject,datetime='$datetime' where reg_id=$reg_id and subject_id=$value and active=1 LIMIT 1");
			}else{
				exec_query_utf8("insert into exam_score set reg_id=$reg_id,subject_id=$value,paper_exam_score=$scoreBysubject,datetime='$datetime'");
			}
			$result=1;$msg='Score updated';
		}else{$msg='Invalid Score';}
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);	
}/*elseif($cmd=='updateScore'){
		$getData = $_POST['getData'];
		extract($getData);
		

		$totalScore=0;$success=0;
		foreach($cateid as $key=>$value){
			if(is_numeric($value)){
				if(mysqli_num_rows(exec_query_utf8("SELECT * FROM tblcandidatescore WHERE registration_id=$registration_id AND cateid=$key"))==0){
					$addNew = exec_query_utf8("INSERT INTO tblcandidatescore SET registration_id=$registration_id,cateid=$key,score=$value");
					if($addNew){$totalScore+=$value;$success=1;}
				}else{
					$update = exec_query_utf8("UPDATE tblcandidatescore SET score=$value WHERE registration_id=$registration_id AND cateid=$key LIMIT 1");
					if($update){$totalScore+=$value;$success=1;}
				}
			}else{$success=0;}
		}		
		echo json_encode(array('msg'=>$success,'data'=>$totalScore));
}*/elseif($cmd=='courseRegStatus'){
	$recordid=post("recordid");$datetime = date("Y-m-d H:i:s");
	exec_query_utf8("UPDATE tblcourseregister SET active = NOT active,register_date='$datetime' WHERE id=$recordid LIMIT 1");
	echo 'candidateList';
}elseif($cmd=='paidStatus'){
	$recordid=post("recordid");
	$datetime = date("Y-m-d H:i:s");
	exec_query_utf8("UPDATE tblcourseregister SET pending = NOT pending,approved_by=".$_SESSION['adminid'].",approved_date='$datetime' WHERE id=$recordid LIMIT 1");
	echo 'candidateList';
}elseif($cmd=='userList'){
	$keyword = post("keyword");
	$typeid=post("typeid");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (code LIKE '%$keyword%' OR firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%')";}
	$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type = $typeid";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblusers $sql_condition ORDER BY id DESC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$userListString = '';$i=$startIndex+1;
	$user_qry = exec_query_utf8("SELECT * FROM tblusers $sql_condition ORDER BY id DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($user_row = mysqli_fetch_assoc($user_qry)){	
		$registerBy = singleCell_qry("code","tblusers","id=".($user_row['registerBy']>0?$user_row['registerBy']:0)." LIMIT 1");
		$userListString .= '<tr style="'.($user_row['active']==0?'color:red':'').'">
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td>'.$user_row['code'].'</td>
												<td>'.$user_row['lastName'].'</td>
												<td>'.$user_row['firstName'].'</td>
												<td>'.$registerBy.'</td>
												<td>'.date("d-m-Y",strtotime($user_row['registerDate'])).'</td>
												<td class="tableCellCenter" style="width:50px;">
													<div class="btn_padding"><button class="btn btn-primary btn-xs editUser_btn" data-recordid="'.$user_row['id'].'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
												</td>
                                            </tr>                ';
		$i++;
	}

	if($userListString == ''){$userListString = '<tr><td colspan="7" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$userListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='newUser'){
	$recordid = $_POST["recordid"];
	$newTypeid = $_POST["newTypeid"];
	$newUsername = $_POST["newUsername"];
	$newLName = $_POST["newLName"];
	$newFName = $_POST["newFName"];
	$newPassword = $_POST["newPassword"];
	$confirmNewPassword = $_POST["confirmNewPassword"];
	$active = isset($_POST['active'])?1:0;
	$datetime = date('Y-m-d H:i:s');

	
	
	$result = 0; $msg='';
	if($newTypeid>0 and $newUsername<>'' and $newLName<>'' and $newFName<>'' and $newPassword<>'' and $confirmNewPassword<>''){			
			if($newPassword==$confirmNewPassword){
				$encrypt_pass = encodeString($newPassword,$encryptKey);
				if($recordid=='' or $recordid==0){
					$exist = singleCell_qry("id","tblusers","code='$newUsername' LIMIT 1");				
					if($exist=='' or $exist<=0){
						$inserted = exec_query_utf8("INSERT INTO tblusers SET type=$newTypeid,code='$newUsername',firstName='$newFName',lastName='$newLName',loginPassword='$encrypt_pass',registerDate='$datetime',registerBy=$sessonid,active=$active");
						if($inserted){$result = 1; $msg='inserted';}
					}else{
						$msg='exist';					
					}
				}else{
					if(is_numeric($recordid)){
						$exist = singleCell_qry("id","tblusers","code='$newUsername' AND id<>$recordid LIMIT 1");				
						if($exist=='' or $exist<=0){
							$updated = exec_query_utf8("UPDATE tblusers SET type=$newTypeid,code='$newUsername',firstName='$newFName',lastName='$newLName',loginPassword='$encrypt_pass',registerDate='$datetime',active=$active WHERE id=$recordid LIMIT 1");
							if($updated){$result = 1; $msg='updated';}
						}else{
							$msg='exist';		
						}
						
					}	
				}
			}else{
				$msg='not_match';	
			}		
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='iniEditUser'){
	$recordid=post("recordid");
	$data=array();
	$user_qry = exec_query_utf8("SELECT * FROM tblusers WHERE id=$recordid LIMIT 1");
	while($user_row = mysqli_fetch_assoc($user_qry)){
		$raw_pass = decodeString($user_row['loginPassword'],$encryptKey);
		$data = array('type'=>$user_row['type'],'username'=>$user_row['code'],'fname'=>$user_row['firstName'],'lname'=>$user_row['lastName'],'password'=>$raw_pass,'active'=>$user_row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='newguideList'){
	$keyword = post("keyword");	
	$language_id = post("language_id");	
	$from_date = post("from_date");	
	$to_date = post("to_date");	
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (fname_kh LIKE '%$keyword%' OR lname_kh LIKE '%$keyword%' OR fname_en LIKE '%$keyword%' OR lname_en LIKE '%$keyword%')";}
	if($language_id>0){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." language_id=$language_id";}
	if($from_date<>'' and $to_date<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (created_date between '$from_date' and '$to_date')";}
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM ng_profile $sql_condition ORDER BY id ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	//subject data
	$subject_qry = exec_query_utf8("select * from ng_exam_subject where active=1 order by id asc");
	$total_subject = mysqli_num_rows($subject_qry);

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$listString = '';$i=$startIndex+1;
	$row_qry = exec_query_utf8("SELECT * FROM ng_profile $sql_condition ORDER BY id ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($row = mysqli_fetch_assoc($row_qry)){	
		$rowid = $row['id'];
		$fullname_kh = $row['lname_kh'].' '.$row['fname_kh'];
		$fullname_en = $row['lname_en'].' '.$row['fname_en'];
		if($row['gender']=='m'){$gender='ប្រុស';}else{$gender='ស្រី';}
		$language = singleCell_qry("name_en","tbllanguages","id=".$row['language_id']." LIMIT 1");
		
		//get subject score by guides
		$score_str = '';
		$subject_qry = exec_query_utf8("select * from ng_exam_subject where active=1 order by id asc");
		while($subject_row = mysqli_fetch_assoc($subject_qry)){	
			$get_score = singleCell_qry("score","ng_exam_score","guide_id=".$rowid." and subject_id=".$subject_row['id']." LIMIT 1");
			if(!$get_score){$get_score=0;}
			$score_str .= '<td class="tableCellCenter"><div id="'.$rowid.'_'.$subject_row['id'].'" class="score_box row_'.$rowid.'" title="Edit here then click save" contenteditable>'.$get_score.'</div></td>';
		}
		
		$qrcodeImg = '/qrcode/training/'.encodeString($rowid,$encryptKey);
		$cert_url = '/certificate_ng/'.encodeString($rowid,$encryptKey);
		
		$listString .= '<tr style="'.($row['active']==0?'color:red':'').'">
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td class="tableCellLeftCenter">'.$fullname_kh.'</td>
												<td class="tableCellLeftCenter">'.$fullname_en.'</td>
												<td class="tableCellCenter">'.$gender.'</td>
												<td class="tableCellCenter">'.$language.'</td>
												'.$score_str.'
												<td class="tableCellCenter">'.$row['score_avg'].'</td>
												<td class="tableCellCenter">'.$row['score_grade'].'</td>
												<td class="tableCellCenter">
													<button class="btn btn-standard btn-xs qrcode_img" title="Click to view QR code" data-name="'.$fullname_kh.'" data-src="'.$qrcodeImg.'"><i class="fa fa-search-plus fa-fw"></i></button>
													<a target="_blank" href="'.$cert_url.'" class="btn btn-standard" title="Click to view Certificate"><i class="fa fa-graduation-cap"></i></a>
												</td>
												<td class="tableCellCenter" style="width:50px;">
													<div class="btn_padding"><button class="btn btn-primary btn-xs save_ng_score_btn" data-recordid="'.$rowid.'"><i class="fa fa-floppy-o fa-fw"></i> ពិន្ទុ</button></div>
													<div class="btn_padding"><button class="btn btn-primary btn-xs editRow_btn" data-recordid="'.$rowid.'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
												</td>
                                            </tr>                ';
		$i++;
	}

	if($listString == ''){$listString = '<tr><td colspan="'.($total_subject+8).'9" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$listString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='ng_profile'){
	$recordid = $_POST["recordid"];
	$fname_kh = $_POST["fname_kh"];
	$lname_kh = $_POST["lname_kh"];
	$fname_en = $_POST["fname_en"];
	$lname_en = $_POST["lname_en"];
	$guide_gender = $_POST["guide_gender"];
	$language_id = $_POST["language_id"];
	$exam_id = $_POST["exam_id"];
	$score_avg = $_POST["score_avg"];
	$score_grade = $_POST["score_grade"];
	$active = isset($_POST['active'])?1:0;
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if($language_id>0 and $exam_id>0 and $fname_kh<>'' and $lname_kh<>'' and $fname_en<>'' and $lname_en<>'' and $guide_gender<>''){		
		if($recordid=='' or $recordid==0){	
			$inserted = exec_query_utf8("INSERT INTO ng_profile SET fname_kh='$fname_kh',lname_kh='$lname_kh',fname_en='$fname_en',lname_en='$lname_en',gender='$guide_gender',language_id=$language_id,exam_id=$exam_id,score_avg='$score_avg',score_grade='$score_grade',created_date='$datetime',active=$active");
			if($inserted){$result = 1; $msg='inserted';}
		}else{
			if(is_numeric($recordid)){
				$updated = exec_query_utf8("UPDATE ng_profile SET fname_kh='$fname_kh',lname_kh='$lname_kh',fname_en='$fname_en',lname_en='$lname_en',gender='$guide_gender',language_id=$language_id,exam_id=$exam_id,score_avg=$score_avg,score_grade='$score_grade',last_update_date='$datetime',active=$active WHERE id=$recordid LIMIT 1");
				if($updated){$result = 1; $msg='updated';}				
			}	
		}	
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='iniEdit_ng_profile'){
	$recordid=post("recordid");
	$data=array();
	$row_qry = exec_query_utf8("SELECT * FROM ng_profile WHERE id=$recordid LIMIT 1");
	while($row = mysqli_fetch_assoc($row_qry)){
		$data = array('fname_kh'=>$row['fname_kh'],'lname_kh'=>$row['lname_kh'],'fname_en'=>$row['fname_en'],'lname_en'=>$row['lname_en'],'gender'=>$row['gender'],'language_id'=>$row['language_id'],'exam_id'=>$row['exam_id'],'score_avg'=>$row['score_avg'],'score_grade'=>$row['score_grade'],'active'=>$row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='translatorList'){
	$keyword = post("keyword");	
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (fname_kh LIKE '%$keyword%' OR lname_kh LIKE '%$keyword%' OR fname_en LIKE '%$keyword%' OR lname_en LIKE '%$keyword%')";}
	//$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type = $typeid";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM translator_profile $sql_condition ORDER BY id ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$filePath = '/documents/';
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$listString = '';$i=$startIndex+1;
	$row_qry = exec_query_utf8("SELECT * FROM translator_profile $sql_condition ORDER BY id ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($row = mysqli_fetch_assoc($row_qry)){	
		$rowid = $row['id'];
		$fullname_kh = $row['lname_kh'].' '.$row['fname_kh'];
		$fullname_en = $row['lname_en'].' '.$row['fname_en'];
		if($row['gender']=='m'){$gender='ប្រុស';}else{$gender='ស្រី';}
		$nationality = singleCell_qry("nationality","translator_nationality","id=".$row['nationality_id']." LIMIT 1");
		
		$qrcodeImg = '/qrcode/translator/'.encodeString($rowid,$encryptKey);
		$photo_url = $row['photo']<>''?$filePath.(end(explode("|",$row['photo']))):'';
		$cert_url = '/certificate_translator/'.encodeString($rowid,$encryptKey);
		
		$listString .= '<tr style="'.($row['active']==0?'color:red':'').'">
                                            	<td class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td class="tableCellLeftCenter"><img src="'.$photo_url.'" width="50" /></td>
												<td class="tableCellLeftCenter">'.$fullname_kh.'</td>
												<td class="tableCellLeftCenter">'.$fullname_en.'</td>
												<td class="tableCellCenter">'.$gender.'</td>
												<td class="tableCellCenter">'.$nationality.'</td>
												<td class="tableCellCenter">'.date("d/m/Y",strtotime($row['dob'])).'</td>
												<td class="tableCellCenter">'.$row['passport'].'</td>
												<td class="tableCellCenter">'.$row['mobile'].'</td>
												<td class="tableCellCenter">
													<button style="margin-top:5px;" class="btn btn-standard btn-xs qrcode_img" title="Click to view QR code" data-name="'.$fullname_kh.'" data-src="'.$qrcodeImg.'"><i class="fa fa-search-plus fa-fw"></i></button>
													<a target="_blank" href="'.$cert_url.'"><button style="margin-top:5px;" class="btn btn-standard btn-xs" title="Click to view Certificate"><i class="fa fa-graduation-cap"></i></button></a>
												</td>
												<td class="tableCellCenter" style="width:50px;">
													<div class="btn_padding"><button class="btn btn-primary btn-xs editRow_btn" data-recordid="'.$rowid.'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
												</td>
                                            </tr>                ';
		$i++;
	}

	if($listString == ''){$listString = '<tr><td colspan="10" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$listString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='translator_profile'){
	$recordid = $_POST["recordid"];
	$course_id = $_POST["course_id"];
	$fname_kh = $_POST["fname_kh"];
	$lname_kh = $_POST["lname_kh"];
	$fname_en = $_POST["fname_en"];
	$lname_en = $_POST["lname_en"];
	$gender = $_POST["gender"];
	$nationality_id = $_POST["nationality_id"];
	$dob = $_POST["dob"];
	$passport = $_POST["passport"];
	$mobile = $_POST["mobile"];
	$photo = $_POST["allfiles"];
	$active = isset($_POST['active'])?1:0;
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if($nationality_id>0 and $fname_kh<>'' and $lname_kh<>'' and $fname_en<>'' and $lname_en<>'' and $gender<>''){		
		if($recordid=='' or $recordid==0){	
			$inserted = exec_query_utf8("INSERT INTO translator_profile SET course_id=$course_id,fname_kh='$fname_kh',lname_kh='$lname_kh',fname_en='$fname_en',lname_en='$lname_en',gender='$gender',nationality_id=$nationality_id,dob='$dob',passport='$passport',mobile='$mobile',photo='$photo',created_date='$datetime',active=$active");
			if($inserted){$result = 1; $msg='inserted';}
		}else{
			if(is_numeric($recordid)){
				$updated = exec_query_utf8("UPDATE translator_profile SET course_id=$course_id,fname_kh='$fname_kh',lname_kh='$lname_kh',fname_en='$fname_en',lname_en='$lname_en',gender='$gender',nationality_id=$nationality_id,dob='$dob',passport='$passport',mobile='$mobile',photo='$photo',last_update_date='$datetime',active=$active WHERE id=$recordid LIMIT 1");
				if($updated){$result = 1; $msg='updated';}				
			}	
		}	
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='iniEdit_translator_profile'){
	$recordid=post("recordid");
	$doc_path = '/documents/';
	$data=array();
	$row_qry = exec_query_utf8("SELECT * FROM translator_profile WHERE id=$recordid LIMIT 1");
	while($row = mysqli_fetch_assoc($row_qry)){
		$data = array('course_id'=>$row['course_id'],'fname_kh'=>$row['fname_kh'],'lname_kh'=>$row['lname_kh'],'fname_en'=>$row['fname_en'],'lname_en'=>$row['lname_en'],'gender'=>$row['gender'],'nationality_id'=>$row['nationality_id'],'dob'=>$row['dob'],'passport'=>$row['passport'],'mobile'=>$row['mobile'],'photo'=>$row['photo'],'filePath'=>$doc_path,'active'=>$row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='save_ng_score'){
	$datetime = date('Y-m-d H:i:s');
	$scorebyrow=$_POST["scorebyrow"];
	$result=1;$msg='';
	foreach($scorebyrow as $key=>$value){
		$score_parts = explode("_",$value); //3 elements => 0:guideid,1:subjectid,2:score
		if(is_numeric($score_parts[0]) and is_numeric($score_parts[1]) and is_numeric($score_parts[2])){
			$exist = singleCell_qry("id","ng_exam_score","guide_id=".$score_parts[0]." and subject_id=".$score_parts[1]." LIMIT 1");
			if($exist){
				exec_query_utf8("update ng_exam_score set score=".$score_parts[2]." where id=$exist LIMIT 1");
			}else{
				exec_query_utf8("insert into ng_exam_score set guide_id=".$score_parts[0].",subject_id=".$score_parts[1].",score=".$score_parts[2].",created_date='$datetime'");
			}
		}else{$result=0;$msg='error';}		
	}
	$data=array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
}elseif($cmd=='guideStatus'){
	$recordid=post("recordid");
	exec_query_utf8("UPDATE tblusers SET active = NOT active WHERE id=$recordid LIMIT 1");
	echo 'guideList';
}elseif($cmd=='guidePending'){
	$recordid=post("recordid");
	exec_query_utf8("UPDATE tblusers SET pending = NOT pending WHERE id=$recordid LIMIT 1");
	echo 'guideList';
}elseif($cmd=='updateAccount'){
	$currentPassword = $_POST["currentPassword"];
	$newPassword = $_POST["newPassword"];
	$confirmNewPassword = $_POST["confirmNewPassword"];
	$datetime = date('Y-m-d H:i:s');

	
	
	$result = 0; $msg='';
	if($currentPassword<>'' and $newPassword<>'' and $confirmNewPassword<>''){	
			$getCurPass = singleCell_qry("loginPassword","tblusers","id=".$_SESSION['adminid']." LIMIT 1");	
			if($getCurPass==encodeString($currentPassword,$encryptKey)){
				if($newPassword==$confirmNewPassword){
					$encrypt_pass = encodeString($newPassword,$encryptKey);
					$updated = exec_query_utf8("UPDATE tblusers SET loginPassword='$encrypt_pass',lastUpdateDate='$datetime' WHERE id=".$_SESSION['adminid']." LIMIT 1");
					if($updated){$result = 1; $msg='updated';}
				}else{
					$msg='not_match';	
				}
			}else{
				$msg='wrong_password';	
			}
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='updateWing'){
	$wingSettings = array('allowedPayment'=>(isset($_POST["allowedPayment"])?'1':'0'),
							'wingOnlinePayID'=>$_POST["wingOnlinePayID"],
							'wingOnlinePayPassword' => $_POST["wingOnlinePayPassword"],
							'wingAuthenticateUrl'=>$_POST["wingAuthenticateUrl"],
							'wingRedirectUrl'=>$_POST["wingRedirectUrl"]);
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if(!in_array('',$wingSettings)){		
		foreach($wingSettings as $key=>$value){
			exec_query_utf8("UPDATE tblgeneralsetting SET settingValue='$value',userid='".$_SESSION['adminid']."',datetime='$datetime' WHERE settingName='$key' LIMIT 1");	
		}			
		$result = 1; $msg='updated';
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='registerFee'){
	$priceSettings = array('onlinePayCompanyName'=>$_POST["onlinePayCompanyName"],
							'onlinePayItemName_kh' => $_POST["onlinePayItemName_kh"],
							'onlinePayItemName' => $_POST["onlinePayItemName"],
							'registrationFee'=>$_POST["registrationFee"]);
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if(!in_array('',$priceSettings)){		
		foreach($priceSettings as $key=>$value){
			exec_query_utf8("UPDATE tblgeneralsetting SET settingValue='$value',userid='".$_SESSION['adminid']."',datetime='$datetime' WHERE settingName='$key' LIMIT 1");	
		}			
		$result = 1; $msg='updated';
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='examSettings'){
	$getSettings = array('totalQuestion_OE'=>$_POST["totalQuestion_OE"],
							'examPeriod'=>$_POST["examPeriod"],
							'allowedExamTimes' => $_POST["allowedExamTimes"],
							'examSessionTime' => $_POST["examSessionTime"],
							'showCorrectAnswer_OE' => (isset($_POST["showCorrectAnswer_OE"])?'1':'0'));
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if(!in_array('',$getSettings)){		
		foreach($getSettings as $key=>$value){
			exec_query_utf8("UPDATE tblgeneralsetting SET settingValue='$value',userid='".$_SESSION['adminid']."',datetime='$datetime' WHERE settingName='$key' LIMIT 1");	
		}			
		$result = 1; $msg='updated';
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='paymentList'){
	$keyword = post("keyword");
	$searchDate_from=post("searchDate_from");
	$searchDate_to=post("searchDate_to");
	$payment_result = post("payment_result");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (tid LIKE '%$keyword%' OR account LIKE '%$keyword%' OR userid IN (SELECT id FROM tblusers WHERE firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%' OR firstNameEn LIKE '%$keyword%' OR lastNameEn LIKE '%$keyword%' OR code LIKE '%$keyword%'))";}
	if($searchDate_from<>'' and $searchDate_to<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (datetime BETWEEN '$searchDate_from 00:00:00' AND '$searchDate_to 23:59:50')";}
	if($payment_result<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." result=$payment_result";}
	//$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type = $typeid";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblpayment $sql_condition ORDER BY datetime DESC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$paymentListString = '';$i=$startIndex+1;
	$payment_qry = exec_query_utf8("SELECT * FROM tblpayment $sql_condition ORDER BY datetime DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($payment_row = mysqli_fetch_assoc($payment_qry)){	
		$paymentName = qry_arr("code,firstName,lastName","tblusers","id=".$payment_row['userid']." LIMIT 1");
		$paymentListString .= '<tr title="'.$payment_row['msg'].'" style="'.($payment_row['result']==0?'color:red;':'').'">
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td>'.$paymentName['code'].'</td>
												<td>'.$paymentName['lastName'].' '.$paymentName['firstName'].'</td>
												<td>'.$payment_row['account'].'</td>
												<td>'.($payment_row['result']==0?'<i class="fa fa-exclamation-triangle fa-fw"></i> បរាជ័យ':$payment_row['tid']).'</td>
												<td>'.$payment_row['amount'].'</td>
												<td>'.date("d-m-Y H:i",strtotime($payment_row['datetime'])).'</td>
                                            </tr>                ';
		$i++;
	}

	if($paymentListString == ''){$paymentListString = '<tr><td colspan="7" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$paymentListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='activityList'){
	$keyword = post("keyword");
	$searchDate_from=post("searchDate_from");
	$searchDate_to=post("searchDate_to");
	$activity_type = post("activity_type");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." user_id IN (SELECT id FROM tblusers WHERE firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%' OR firstNameEn LIKE '%$keyword%' OR lastNameEn LIKE '%$keyword%' OR code LIKE '%$keyword%')";}
	if($searchDate_from<>'' and $searchDate_to<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." (datetime BETWEEN '$searchDate_from 00:00:00' AND '$searchDate_to 23:59:50')";}
	if($activity_type<>''){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type_id=$activity_type";}
	//$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." type = $typeid";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM user_activity $sql_condition ORDER BY datetime DESC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$activityListString = '';$i=$startIndex+1;
	$activity_qry = exec_query_utf8("SELECT * FROM user_activity $sql_condition ORDER BY datetime DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($activity_row = mysqli_fetch_assoc($activity_qry)){	
		$deviceInfo = json_decode($activity_row['browser_info'],true);
		$userData = qry_arr("code,firstName,lastName","tblusers","id=".$activity_row['user_id']." LIMIT 1");
		$activityTypeInfo = qry_arr("code,title,description","user_activity_type","id=".$activity_row['type_id']." LIMIT 1");
		$moreData = $activity_row['more_data'];
		
		if($activityTypeInfo['code']=='new_test' or $activityTypeInfo['code']=='finish_test' or $activityTypeInfo['code']=='renew_test'){$moreData = '<a target="new" href="/tests/'.encodeString($activity_row['more_data'],$encryptKey).'">'.$activity_row['more_data'].'</a>';}
		if($activityTypeInfo['code']=='new_exam' or $activityTypeInfo['code']=='finish_exam'){$moreData = '<a target="new" href="/examResult/'.encodeString($activity_row['more_data'],$encryptKey).'">'.$activity_row['more_data'].'</a>';}
		
		$userAgent_parts = explode(" ",$deviceInfo['userAgent']);
		$ipInfo = ipInfo($activity_row['ip']);
		$activityListString .= '<tr>
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td><a href="/admin/guides/'.encodeString($activity_row['user_id'],$encryptKey).'">'.$userData['lastName'].' '.$userData['firstName'].'</a></td>
												<td title="'.$activityTypeInfo['description'].'">'.$activityTypeInfo['title'].'</td>
												<td title="'.$deviceInfo['userAgent'].'">'.str_replace(array('(',';'),"",$userAgent_parts[1]).'</td>
												<td title="'.$activity_row['ip'].'">'.$ipInfo->org.'</td>												
												<td>'.date("d-m-Y H:i",strtotime($activity_row['datetime'])).'</td>
												<td>'.$moreData.'</td>
                                            </tr>                ';
		$i++;
	}

	if($activityListString == ''){$activityListString = '<tr><td colspan="7" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$activityListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='updateWing'){
	$wingSettings = array('wingOnlinePayID'=>$_POST["wingOnlinePayID"],
							'wingOnlinePayPassword' => $_POST["wingOnlinePayPassword"],
							'wingAuthenticateUrl'=>$_POST["wingAuthenticateUrl"],
							'wingRedirectUrl'=>$_POST["wingRedirectUrl"]);
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if(!in_array('',$wingSettings)){		
		foreach($wingSettings as $key=>$value){
			exec_query_utf8("UPDATE tblgeneralsetting SET settingValue='$value',userid='".$_SESSION['adminid']."',datetime='$datetime' WHERE settingName='$key' LIMIT 1");	
		}			
		$result = 1; $msg='updated';
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='updateFeaturesTxt'){
	$settings = array('systemFeatures'=>$_POST["systemFeatures"]);
	$datetime = date('Y-m-d H:i:s');
	
	$result = 0; $msg='';
	if(!in_array('',$settings)){		
		foreach($settings as $key=>$value){
			exec_query_utf8("UPDATE tblgeneralsetting SET settingValue='$value',userid='".$_SESSION['adminid']."',datetime='$datetime' WHERE settingName='$key' LIMIT 1");	
		}			
		$result = 1; $msg='updated';
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='courseList'){
	$course_year = post("course_year");
	$course_area=post("course_area");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($course_year>0){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." year=$course_year";}
	if($course_area>0){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." area_id=$course_area";}
	
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblcourseschedule $sql_condition ORDER BY year,area_id,course_num ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$courseListString = '';$i=$startIndex+1;
	$course_qry = exec_query_utf8("SELECT * FROM tblcourseschedule $sql_condition ORDER BY year,area_id,course_num ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($course_row = mysqli_fetch_assoc($course_qry)){	
		$areaName = singleCell_qry("displayTitle","tblsubcategory","id=".$course_row['area_id']." LIMIT 1");
		
		if($course_row['workshop_start_date']<>''){
		$workshop_date = date("d",strtotime($course_row['workshop_start_date'])).'-'.date("d/m/Y",strtotime($course_row['workshop_end_date']));
		if($course_row['workshop_start_date']==$course_row['workshop_end_date']){$workshop_date = date("d/m/Y",strtotime($course_row['workshop_end_date']));}
		}else{$workshop_date = 'N/A';}
				
		$courseListString .= '<tr style="'.($course_row['active']==0?'color:red':'').'">
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td>'.$course_row['year'].'</td>
												<td>'.$areaName.'</td>
												<td>វគ្គសិក្សាទី '.$course_row['course_num'].'</td>
												<td>'.date("d/m/Y",strtotime($course_row['register_date'])).'</td>
												<td>'.$workshop_date.'</td>
												<td>'.$course_row['max_participant'].'</td>
												<td class="tableCellCenter" style="width:50px;">
													<div class="btn_padding"><button class="btn btn-primary btn-xs editCourse_btn" data-recordid="'.$course_row['id'].'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
												</td>
                                            </tr>                ';
		$i++;
	}

	if($courseListString == ''){$courseListString = '<tr><td colspan="8" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$courseListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='newCourse'){
	$recordid = $_POST["recordid"];
	$newCourse_year = $_POST["newCourse_year"];
	$newCourse_area = $_POST["newCourse_area"];
	$newCourse_num = $_POST["newCourse_num"];
	$newRegister_date = $_POST["newRegister_date"];
	$course_end_date = $_POST["course_end_date"];
	$total_hours = $_POST["total_hours"];
	//$newWS_start_date = $_POST["newWS_start_date"]==''?'NULL':"'".$_POST["newWS_start_date"]."'";
	//$newWS_end_date = $_POST["newWS_end_date"]==''?'NULL':"'".$_POST["newWS_end_date"]."'";
	$newMax_participant = $_POST["newMax_participant"];
	$subjectData = $_POST["subjectData"];
	$start_online_exam = isset($_POST['start_online_exam'])?1:0;
	$active = isset($_POST['active'])?1:0;
	$datetime = date('Y-m-d H:i:s');

	
	
	$result = 0; $msg='';
	if($newCourse_year<>'' and $newCourse_area<>'' and $newCourse_num<>'' and $newRegister_date<>'' and $newMax_participant>0 and $subjectData<>''){			
				if($recordid=='' or $recordid==0){
					$exist = singleCell_qry("id","tblcourseschedule","year=$newCourse_year AND area_id=$newCourse_area AND course_num=$newCourse_num LIMIT 1");				
					if($exist=='' or $exist<=0){
						$inserted = exec_query_utf8("INSERT INTO tblcourseschedule SET year=$newCourse_year,area_id=$newCourse_area,course_num=$newCourse_num,register_date='$newRegister_date',course_end_date='$course_end_date',total_hours=$total_hours,max_participant=$newMax_participant,subject_id='$subjectData',created_date='$datetime',start_online_exam=$start_online_exam,active=$active");
						if($inserted){$result = 1; $msg='inserted';}
					}else{
						$msg='exist';					
					}
				}else{
					if(is_numeric($recordid)){
						$exist = singleCell_qry("id","tblcourseschedule","year=$newCourse_year AND area_id=$newCourse_area AND course_num=$newCourse_num AND id<>$recordid LIMIT 1");				
						if($exist=='' or $exist<=0){
							$updated = exec_query_utf8("UPDATE tblcourseschedule SET year=$newCourse_year,area_id=$newCourse_area,course_num=$newCourse_num,register_date='$newRegister_date',course_end_date='$course_end_date',total_hours=$total_hours,max_participant=$newMax_participant,subject_id='$subjectData',start_online_exam=$start_online_exam,active=$active WHERE id=$recordid LIMIT 1");
							if($updated){$result = 1; $msg='updated';}
						}else{
							$msg='exist';		
						}
						
					}	
				}	
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='iniEditCourse'){
	$recordid=post("recordid");
	$data=array();
	$course_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE id=$recordid LIMIT 1");
	while($course_row = mysqli_fetch_assoc($course_qry)){
		$subjectItem = ''; $subject_id_arr = array();
		$subjectData = json_decode($course_row['subject_id'],true);
		foreach($subjectData as $key => $value){$subject_id_arr[] = $key;}
		$select_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id IN (".implode(",",$subject_id_arr).") and active=1 ORDER BY publishedDate DESC");
		while($select_row = mysqli_fetch_assoc($select_qry)){
			$subjectid = $select_row['id'];
			$get_subjectData=$subjectData[$subjectid];
			$subjectItem .= '<tr id="'.$subjectid.'"><td>'.$select_row['title'].'</td><td><div class="input-append input-group dtpicker" style="width: 85px;"><input class="oeDate" style="width: 85px;" data-format="yyyy-MM-dd" type="text" placeholder="កាលបរិច្ឆេទ" class="form-control" value="'.$get_subjectData['date'].'"><span class="input-group-addon add-on"><i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar" class="fa fa-calendar"></i></span></div></td><td><div class="input-append input-group"><input class="extend_time" style="width: 50px;" type="text" placeholder="ចំនួន" class="form-control" value="'.$get_subjectData['extend'].'"><span class="input-group-addon add-on">ថ្ងៃ</span></div></td><td><button type="button" class="btn btn-danger btn-xs removeSubject" data-subjectid="'.$subjectid.'"><i class="fa fa-times"></i></button></td></tr>';
		}	
				
		$data = array('year'=>$course_row['year'],'area'=>$course_row['area_id'],'course_num'=>$course_row['course_num'],'register_date'=>$course_row['register_date'],'course_end_date'=>$course_row['course_end_date'],'total_hours'=>$course_row['total_hours'],'max_participant'=>$course_row['max_participant'],'subjectItem'=>$subjectItem,'start_online_exam'=>$course_row['start_online_exam'],'active'=>$course_row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='nextCourseNumber'){
	$year=post("year");
	$area=post("area");
	
	if($year>0 and $area>0){
		$currentCourseNum = singleCell_qry("course_num","tblcourseschedule","year=$year AND area_id=$area ORDER BY course_num DESC LIMIT 1");
		echo $currentCourseNum+1;
	}
}elseif($cmd=='examDateList'){
	$examDate_year = post("examDate_year");
	$examDate_area=post("examDate_area");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");

	$sql_condition = 'WHERE';
	if($examDate_year>0){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." year=$examDate_year";}
	if($examDate_area>0){$sql_condition.= ($sql_condition=='WHERE'?'':' AND')." area=$examDate_area";}
	
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblexamschedule $sql_condition ORDER BY year,area,exam_date ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}

	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$examDateListString = '';$i=$startIndex+1;
	$examDate_qry = exec_query_utf8("SELECT * FROM tblexamschedule $sql_condition ORDER BY year,area,exam_date ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($examDate_row = mysqli_fetch_assoc($examDate_qry)){	
		$areaName = singleCell_qry("displayTitle","tblsubcategory","id=".$examDate_row['area']." LIMIT 1");
		$courseid_arr = explode(',',$examDate_row['course_id']);
		$courseNum = '';
		foreach($courseid_arr as $key=>$value){
			$eachCourseNum = singleCell_qry("course_num","tblcourseschedule","id=$value LIMIT 1");
			$courseNum .= '<div class="courseLabel">វគ្គទី '.$eachCourseNum.'</div>';
		}
		
		$examDateListString .= '<tr style="'.($examDate_row['active']==0?'color:red':'').'">
                                            	<td style="width:70px;'.($examDate_row['result_released']==1?'background:#8dc664;':'').'" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td>'.$examDate_row['year'].'</td>
												<td>'.$areaName.'</td>
												<td>'.$courseNum.'</td>
												<td>'.date("d/m/Y",strtotime($examDate_row['exam_date'])).'</td>
												<td class="tableCellCenter" style="width:50px;">
													<div class="btn_padding"><button class="btn btn-primary btn-xs editExamDate_btn" data-recordid="'.$examDate_row['id'].'"><i class="fa fa-pencil-square-o"></i> កែប្រែ</button></div>
												</td>
                                            </tr>                ';
		$i++;
	}

	if($examDateListString == ''){$examDateListString = '<tr><td colspan="6" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$examDateListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	echo json_encode($data);
}elseif($cmd=='newExamDate'){
	$recordid = $_POST["recordid"];
	$newCourse_year = $_POST["newCourse_year"];
	$newCourse_area = $_POST["newCourse_area"];
	$newCourse_id = $_POST["newCourse_id"];
	$newExam_date = $_POST["newExam_date"];	
	$result_released = isset($_POST['result_released'])?1:0;
	$active = isset($_POST['active'])?1:0;
	$datetime = date('Y-m-d H:i:s');	
	
	$result = 0; $msg='';
	if($newCourse_year<>'' and $newCourse_area<>'' and count($newCourse_id)>0 and $newExam_date<>''){
				if($recordid=='' or $recordid==0){
					$exist = singleCell_qry("id","tblexamschedule","year=$newCourse_year AND area=$newCourse_area AND exam_date='$newExam_date' LIMIT 1");				
					if($exist=='' or $exist<=0){
						$inserted = exec_query_utf8("INSERT INTO tblexamschedule SET year=$newCourse_year,area=$newCourse_area,course_id='".implode(',',$newCourse_id)."',exam_date='$newExam_date',created_date='$datetime',result_released=$result_released,active=$active");
						if($inserted){$result = 1; $msg='inserted';}
					}else{
						$msg='exist';					
					}
				}else{
					if(is_numeric($recordid)){
						$exist = singleCell_qry("id","tblexamschedule","year=$newCourse_year AND area=$newCourse_area AND exam_date='$newExam_date' AND id<>$recordid LIMIT 1");				
						if($exist=='' or $exist<=0){
							$updated = exec_query_utf8("UPDATE tblexamschedule SET year=$newCourse_year,area=$newCourse_area,course_id='".implode(',',$newCourse_id)."',exam_date='$newExam_date',result_released=$result_released,active=$active WHERE id=$recordid LIMIT 1");
							if($updated){$result = 1; $msg='updated';}
						}else{
							$msg='exist';		
						}
						
					}	
				}	
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);
	
}elseif($cmd=='iniEditExamDate'){
	$recordid=post("recordid");
	$data=array();
	$exam_qry = exec_query_utf8("SELECT * FROM tblexamschedule WHERE id=$recordid LIMIT 1");
	while($exam_row = mysqli_fetch_assoc($exam_qry)){
		
		$data = array('year'=>$exam_row['year'],'area'=>$exam_row['area'],'course_id'=>$exam_row['course_id'],'exam_date'=>$exam_row['exam_date'],'result_released'=>$exam_row['result_released'],'active'=>$exam_row['active']);
	}
	echo json_encode($data);
}elseif($cmd=='newguide'){
	$recordid = $_POST['recordid'];
	$getData = array('guide_code'=>$_POST['guide_code'],
						'inputCourse'=>$_POST['inputCourse'],
						'guide_lastName'=>$_POST['guide_lastName'],
						'guide_firstName'=>$_POST['guide_firstName'],
						'guide_lastNameEn'=>$_POST['guide_lastNameEn'],
						'guide_firstNameEn'=>$_POST['guide_firstNameEn'],
						'guide_dob'=>($_POST['guide_dob']==''?"NULL":"'".$_POST['guide_dob']."'"),
						'guide_gender'=>$_POST['guide_gender'],
						'guide_language_id'=>$_POST['guide_language_id'],
						'guide_idCard'=>$_POST['guide_idCard'],
						'guide_licenseExpiredDate'=>($_POST['guide_licenseExpiredDate']==''?"NULL":"'".$_POST['guide_licenseExpiredDate']."'"),
						'guide_mobile'=>$_POST['guide_mobile'],
						'guide_email'=>$_POST['guide_email'],
						'origin_id'=>$_POST['guide_origin_id']);	
	$data=array();	
	$traineeType = singleCell_qry("id","user_role","code='guide' limit 1");
	$inputPassword = '123456';
	$datetime=date("Y-m-d H:i:s");
	$update = false;
	if(is_numeric($recordid) and $recordid>0){$update=true;}
	
	$optional_input = array('guide_code'=>$getData['guide_code'],'guide_email'=>$getData['guide_email'],'guide_dob'=>$getData['guide_dob'],'guide_licenseExpiredDate'=>$getData['guide_licenseExpiredDate']);	
	$required_input = array_diff($getData,$optional_input);
	
	extract($getData);
	
	$isPending = false;$getuserid = 0;
	$result=0; $msg=""; $error = "មានបញ្ហាក្នុងការក្សាទិន្នន័យ៖<br />";
	
	//if($guide_email<>'' and $guide_mobile<>''){
		if($guide_code<>''){
			$guides_data = json_decode(file_get_contents("http://cambodia-touristguide.com/otgbs/public/api/guide/$guide_code"),true);
			if($guide_code<>$guides_data['code']){
				$data[] = array('result'=>'invalid','msg'=>'មិនមាន License ID: <span style="font-weight:bold;">'.$guide_code.'</span> ក្នុងប្រព័ន្ធទេ (បើអ្នកមិនចាំ សូមកុំបំពេញ)');
			}
		}
		$guide_mobile = str_replace(array(' ','-'),"",$guide_mobile);
		$sql_check = exec_query_utf8("SELECT * FROM tblusers WHERE (".($guide_code<>''?"code='$guide_code' OR ":" ").($guide_email<>''?"email='$guide_email' OR ":" ")."mobile='$guide_mobile') ".($update?"and id<>$recordid":"")." LIMIT 1");
		$userData = mysqli_fetch_array($sql_check,MYSQLI_ASSOC);
		$checkUser = mysqli_num_rows($sql_check);
		if($checkUser>0){$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េល ឬ License ID ឬលេខទូរស័ព្ទខាងលើ បានចុះឈ្មោះរួចហើយ'.$checkUser);}		
		if($checkUser==0 or $isPending){
			if(in_array('',$required_input)){
				$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញគ្រប់ប្រអប់ទាំងអស់'.array_search('', $required_input));
			}elseif(!is_numeric($inputCourse) and !$update){$data[] = array('result'=>'invalid','msg'=>'វគ្គសិក្សាមិនត្រឹមត្រូវ');
			}elseif(!$update){
				//CHECK IF COURSE IS AVAILABLE
				$course_available = isCourseAvailable($inputCourse);
				if(!$course_available){$data[] = array('result'=>'invalid','msg'=>'វគ្គសិក្សាមិនត្រឹមត្រូវ');}						
			}						
			if($guide_email<>''){if(!filter_var($guide_email, FILTER_VALIDATE_EMAIL)){$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េលមិនត្រឹមត្រូវ');}}
		}
	//}else{$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញលេខ License ID'.$guide_email.'t');}
	
	if(count($data)==0){		
		$dbAction = "INSERT INTO";
		$condAction = ",registerDate='$datetime',registerBy=".$_SESSION['adminid'].",pending=0";
		/*if($isPending){
			$dbAction = "UPDATE";
			$condAction = "WHERE code='$inputCode'";
			$code_con = "";
		}*/	
		if($update){
			$dbAction = "UPDATE";
			$condAction = ",lastUpdateDate='$datetime',lastUpdateBy='".$_SESSION['adminid']."' WHERE id=$recordid LIMIT 1";
		}	
		$encrypePassword = encodeString($inputPassword,$encryptKey);
		if(!$isPending){
			$sql_set = exec_insert($dbAction." tblusers SET
												type=$traineeType,
												code='$guide_code',
												firstName='$guide_firstName',
												lastName='$guide_lastName',
												firstNameEn='$guide_firstNameEn',
												lastNameEn='$guide_lastNameEn',
												dob=$guide_dob,
												gender='$guide_gender',
												language_id='$guide_language_id',
												idCard='$guide_idCard',
												licenseExpiredDate=$guide_licenseExpiredDate,
												mobile='$guide_mobile',
												email='$guide_email',
												origin_id='$origin_id',
												loginPassword='$encrypePassword'".$condAction);
			$result=1; $msg="inserted";
			//insert course		
			if(!$update){
				$getuserid = $sql_set;
				$course_set = exec_query_utf8("INSERT INTO tblcourseregister SET
															user_id=$getuserid,
															course_id=$inputCourse,
															online_reg=0,
															register_date='$datetime',
															created_date='$datetime'");		
			}
			
		} 
		
	}else{
		$msg = 'custom';
		foreach($data as $key=>$value){
			$error.= ($error==''?'':'<br /><i class="fa fa-times"></i> ').$value['msg'];
		}
	}
	
	$dt = array('result'=>$result,'msg'=>$msg,'error'=>$error);
	echo json_encode($dt);	
}elseif($cmd=='reg_course'){	
	//insert course		
	$guideid = $_POST['guideid'];
	$courseid = $_POST['courseid'];
	$datetime=date("Y-m-d H:i:s");
	$result=false;$msg='';
	$isIncourse = isIncourse($guideid,$courseid);
	if($isIncourse['result']){
		$msg='គណនីនេះមានក្នុងបញ្ជីវគ្គសិក្សារួចហើយ';				
	}elseif(!isCourseAvailable($courseid)){
		$msg='វគ្គសិក្សាមិនត្រឹមត្រូវ';		
	}else{		
		exec_query_utf8("INSERT INTO tblcourseregister SET
										user_id=$guideid,
										course_id=$courseid,
										online_reg=0,
										register_date='$datetime',
										created_date='$datetime'");	
		$result=true;$msg='ទិន្នន័យត្រូវបានបញ្ចូលដោយជោគជ័យ';
	}
	
	echo json_encode(array('result'=>$result,'msg'=>$msg));
}elseif($cmd=='rt_log'){
	$notifs = array();
	$log_qry = exec_query_utf8("SELECT * FROM user_activity where seen=0 ORDER BY datetime ASC");
	while($log_row = mysqli_fetch_assoc($log_qry)){	
		$userData = qry_arr("firstName,lastName","tblusers","id=".$log_row['user_id']." LIMIT 1");
		$activityTypeInfo = qry_arr("title","user_activity_type","id=".$log_row['type_id']." LIMIT 1");
		$fullname = $userData['lastName'].' '.$userData['firstName'];
		$notifs[$log_row['id']] = '<li class="newNotif" id="notif_'.$log_row['id'].'">                        	
						<a href="/admin/activity"><i class="fa fa-caret-right"></i> '.$fullname.' '.$activityTypeInfo['title'].' <span class="notif_date">'.date("d/m/Y H:i",strtotime($log_row['datetime'])).'</span></a>
					</li>';
	}	
	echo json_encode($notifs);
}elseif($cmd=='rt_log_seen'){
	$log_ids = $_POST['log_ids'];
	$datetime=date("Y-m-d H:i:s");
	exec_query_utf8("update user_activity set seen=1,seen_date='$datetime',seen_by=$sessonid where id IN (".implode(',',$log_ids).")");
}
?>