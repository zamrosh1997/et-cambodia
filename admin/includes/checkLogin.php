<?php 
include("../../includes/connect_db.php");

$username = post('username');
$password = post('password');

if($username=='' or $password==''){
echo 0;
exit;	
}

$encriptedpass = encodeString($password,$encryptKey);

if ($stmt = mysqli_prepare($conn, "SELECT id,email FROM tblusers WHERE BINARY code=? AND loginPassword=? AND active=? AND (type=? or type=? or type=? or type=?) LIMIT 1")) { //create a prepared statement
	$active = 1;
	//$type_arr = array();
	$type1 = singleCell_qry("id","user_role","code='superuser' LIMIT 1");
	$type2 = singleCell_qry("id","user_role","code='admin' LIMIT 1");
	$type3 = singleCell_qry("id","user_role","code='school' LIMIT 1");
	$type4 = singleCell_qry("id","user_role","code='professor' LIMIT 1");
	//$types = "$type1,$type2,$type3";
	
	mysqli_stmt_bind_param($stmt, "ssiiiii", $username,$encriptedpass,$active,$type1,$type2,$type3,$type4);
	mysqli_stmt_execute($stmt);			
	mysqli_stmt_store_result($stmt);
	$count = mysqli_stmt_num_rows($stmt);
				
	if($count==1){
		session_start(); 
		mysqli_stmt_bind_result($stmt, $get_userid, $get_username);      
		while(mysqli_stmt_fetch($stmt)){
			$_SESSION['adminid']=$get_userid;
		}
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
		echo 1;
	}
	else {
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
		echo 0;
	}
}	

?>