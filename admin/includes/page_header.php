<?php 
include("../includes/connect_db.php");
include("includes/checkSession.php");

	$userInfo=qry_arr("firstName,type,area_id","tblusers","id=".$_SESSION['adminid']." AND active=1 LIMIT 1");
	$firstName = $userInfo['firstName'];
	$roleInfo =qry_arr("code,menu_id","user_role","id=".$userInfo['type']." LIMIT 1");	
	$auth_menuid =$roleInfo['menu_id'];
	
	//first menu to be the redirect url
	$menuArr = explode(",",$auth_menuid);
	$menuData = qry_arr("code","backend_menu","id=".$menuArr[0]." LIMIT 1");
	$redirectUrl = "/admin/".$menuData['code'];	
	//$redirectUrl="/admin"; // when access to denied page
	$pageTitle = 'Guide Refreshment Admin';
	$logoCaption = 'Guide Refreshment Admin';
	
	//user menu
	$userMenu = ''; $auth_menu = array();
	$menu_qry = exec_query_utf8("SELECT * FROM backend_menu WHERE id IN ($auth_menuid) AND active=1 ORDER BY id ASC");
	while($menu_row = mysqli_fetch_assoc($menu_qry)){
		$userMenu .= $menu_row['btn'];
		$auth_menu[] = $menu_row['code'];
	}
	


/*$user_qry = exec_query_utf8("SELECT * FROM tblportaladmin WHERE id=".$_SESSION['adminid']." AND active=1 LIMIT 1");
while($user_row = mysqli_fetch_assoc($user_qry)){
	$firstName = ucfirst($user_row['fullname']);
}*/


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $pageTitle; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Bootstrap Core CSS -->
    <link href="/admin/css/jquery-ui.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/admin/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/admin/css/plugins/timeline.css" rel="stylesheet">
    
    <!-- datetime picker CSS -->
    <link href="/admin/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    
    <!-- chosen style -->
  	<link rel="stylesheet" href="/chosen/chosen.css">

    <!-- Custom CSS -->
    <link href="/admin/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/admin/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/admin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- DC Comment Boxes CSS -->
	<link type="text/css" rel="stylesheet" href="/admin/css/tsc_comment_boxes.css" />
    
       
     <!-- Toggle Switch -->
  	<link rel="stylesheet" href="/admin/css/toggle-switch.css">
    
    <!-- jQuery Version 1.11.0 -->
    <script src="/admin/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/admin/js/bootstrap.min.js"></script>
    
    <!-- jQuery Version 1.11.0 -->
    <script src="/admin/js/jquery-ui.min.js"></script>
    
    <!-- chosen -->
    <script src="/chosen/chosen.jquery.js" type="text/javascript"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/admin/js/plugins/metisMenu/metisMenu.min.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="/admin/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/admin/js/sb-admin-2.js"></script>
    <script src="/admin/js/functions.js"></script>
    
    <script type="text/javascript">
			$(document).ready(function(e) {			
				$("#login_frm").submit(function(){checkLogin(); return false;});
				
				 $('.dtpicker').datetimepicker({
				  pickTime: false
				});
				
				/*var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				  
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}*/
				
				$(".chosen-select").chosen({
					disable_search_threshold: 10,
					no_results_text: "រកមិនឃើញ!",
					width: "100%"
				  });
			});	
	</script>

</head>