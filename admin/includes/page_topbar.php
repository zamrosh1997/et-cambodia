<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin/"><?php echo $logoCaption; ?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            	<li class="dropdown isHiden" id="pageLoadingStatus">
                    <img src="/admin/images/loading.gif" width="16" />
                </li>
                <li class="dropdown" id="notif_box">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-globe fa-fw"></i>សកម្មភាពថ្មីៗ <span id="total_unseen"></span> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-notif notif_dropdown">
                    	<li id="nodata">                        	
                            <a href="/admin/activity"><i class="fa fa-clock-o"></i> គ្មានសកម្មភាពថ្មី</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>គណនី  <?php echo $firstName; ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">    
                    	<li>
                        	<a href="/admin/myaccount/"><i class="fa fa-user fa-fw"></i> ព័ត៌មានគណនី</a>
                        </li>
                        <li>
                        	<a href="/admin/logout/"><i class="fa fa-sign-out fa-fw"></i> ចាកចេញ</a>
                        </li>                        
                    </ul>
                </li>
            </ul>