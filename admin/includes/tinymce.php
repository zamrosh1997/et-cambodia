<script type="text/javascript" src="/admin/tinymce/tinymce.min.js"></script>
<script>					
					//tinymce.PluginManager.load('moxiemanager', '/admin/tinymce/plugins/moxiemanager/plugin.min.js');
					tinymce.init({
						selector: "textarea",theme: "modern",
						plugins: [
							"advlist autolink lists link image charmap print preview anchor",
							"searchreplace visualblocks code fullscreen",
							"insertdatetime media table contextmenu paste textcolor imagetools colorpicker emoticons"
						],
						toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | emoticons"
					});
					
</script>  