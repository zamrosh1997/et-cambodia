<?php 
include("includes/connect_db.php");
include("includes/checkSession.php");


/*$firstName='';
$user_qry = exec_query_utf8("SELECT * FROM tblportaladmin WHERE id=".$_SESSION['adminid']." AND active=1 LIMIT 1");
while($user_row = mysqli_fetch_assoc($user_qry)){
	$firstName = ucfirst($user_row['fullname']);
}
$userDenyCriteria = userDenyCriteria($_SESSION['adminid']);*/

if(isset($_SESSION['adminid'])){
	$firstName=singleCell_qry("fullname","tblportaladmin","id=".$_SESSION['adminid']." AND active=1 LIMIT 1");
	$userDenyCriteria = userDenyCriteria($_SESSION['adminid']);
	$redirectUrl="/admin"; // when access to denied page
	$pageTitle = 'SDBS Glamping Admin';
	$logoCaption = 'SDBS Glamping Admin';
}elseif(isset($_SESSION['userid'])){
	$firstName=singleCell_qry("companyName","tbluser","id=".$_SESSION['userid']." AND active=1 LIMIT 1");
	$userDenyCriteria = array();
	$redirectUrl="/"; // when access to denied page
	$pageTitle = 'Camping Management';
	$logoCaption = 'Camping Management';
}

/*$user_qry = exec_query_utf8("SELECT * FROM tblportaladmin WHERE id=".$_SESSION['adminid']." AND active=1 LIMIT 1");
while($user_row = mysqli_fetch_assoc($user_qry)){
	$firstName = ucfirst($user_row['fullname']);
}*/


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SDBS Glamping Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- start file upload style -->
        <!-- blueimp Gallery styles -->
        <link rel="stylesheet" href="css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="css/jquery.fileupload.css">
        <link rel="stylesheet" href="css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript><link rel="stylesheet" href="css/jquery.fileupload-noscript.css"></noscript>
        <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
    <!-- end file upload style -->
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Toggle Switch -->
  	<link rel="stylesheet" href="css/toggle-switch.css">
    
        <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>
    <script src="js/functions.js"></script>

</head>