<?php
date_default_timezone_set('Asia/Phnom_Penh'); 

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "sdbs";
$dbname = "glampingportal";

$conn = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname) or die("Error " . mysqli_error($conn));

function exec_query($query){	
	    global $conn;
		$result = mysqli_query($conn, $query) or
		//$result = $db->query($query) or
		die("could not execute query $query");
		//die('<span style="color:red;">Error: The request not sent.</span>');
		return $result;
}

function exec_query_utf8($query){
		exec_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = exec_query($query);
		return $result;
	}

function post($var) {
	if(isset($_POST[$var])){
		return (trim($_POST[$var]));
	}else{
		return '';
	}
}
function get($var){
	if (isset($_GET[$var])) {
		return (trim($_GET[$var]));
	}else{
		return '';
	}
}

function encodeString($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash ="";
		for ($i = 0; $i < $strLen; $i++) {
			$ordStr = ord(substr($string,$i,1));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
		}
		return $hash;
	}

function decodeString($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash = "";
		for ($i = 0; $i < $strLen; $i+=2) {
			$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= chr($ordStr - $ordKey);
		}
		return $hash;
	}


function filesize_formatted($path)
{
    $size = filesize($path);
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function format_filesize($size){
	$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$power = $size > 0 ? floor(log($size, 1024)) : 0;
	return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function singleCell_qry($field,$tbl,$cond){
	$resultInArray = mysqli_fetch_assoc(exec_query_utf8("SELECT ".$field." FROM ".$tbl.($cond==''?'':' WHERE ').$cond));
	return $resultInArray[$field];
}

?>