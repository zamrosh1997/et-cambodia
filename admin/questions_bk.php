<?php 
$pageInfo = array('code'=>'questions','name'=>'Questions and Test');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");
$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-question-circle fa-fw"></i> គ្រប់គ្រងសំណួរ<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-7">                    	
                        <div class="panel panel-default" id="questionList">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីសំណួរ</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="questionList_frm">
                                    <div class="form-group">                        
                                            <div class="input-group custom-search-form">
                                                <input type="text" id="questionList_search_txt" class="form-control" placeholder="វាយសំណួរ">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" id="questionList_search_btn">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                    </div>
                                </form>
                                <div>
                                
                                <div>
                                	<div class="form-group" style="display:inline-block">
                                        <label>មុខវិជ្ជា</label>
                                        <select class="form-control chosen-select input-sm" id="lessonid">
                                               <option value="0">--- ជ្រើសរើស ---</option>
                                               <?php
                                                        $lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY id DESC");
                                                        while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
                                                                echo '<option value="'.$lesson_row['id'].'">'.$lesson_row['title'].'</option>';
                                                        }												
                                                        ?>  
                                         </select>
                                    </div> 
                                    <div class="form-group" style="display:inline-block">
                                        <label>សំណួរសម្រាប់</label>
                                        <select class="form-control chosen-select input-sm" id="examType">
                                               <option value="">--- ជ្រើសរើស ---</option>
                                               <option value="1">ធ្វើតេសត៏សាកល្បង</option>
                                               <option value="0">ការប្រលង</option>         
                                         </select>
                                    </div>  
                                </div>
                                
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control chosen-select input-sm" id="nav_rowsPerPage">
                                                    <option value="5">០៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="questionList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="questionList_tbl">
                                        <thead>
                                            <tr>
                                                <th>សំណួរ</th>
                                            </tr>
                                        </thead>
                                        <tbody>  
                                        	                              
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                    
                    <div class="col-lg-5">
                        <div class="panel panel-default" id="newQuestion">
                            <div class="panel-heading">
                                <h3 class="panel-title"​ id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</h3>
                            </div>
                            <div class="panel-body">
                                <form role="form" id="newQuestion_frm">	  
                                	<input type="hidden" id="recordid" />
                                    <div class="form-group">
                                        <label>មុខវិជ្ជា</label>
                                        <select class="form-control chosen-select input-sm" id="newLessonid" required>
                                               <option value="0">--- ជ្រើសរើស ---</option>
                                                        <?php
                                                        $lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY id DESC");
                                                        while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
                                                                echo '<option value="'.$lesson_row['id'].'">'.$lesson_row['title'].'</option>';
                                                        }												
                                                        ?>  
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>សំណួរសម្រាប់</label>
                                        <select class="form-control chosen-select input-sm" id="newExamType" required>
                                               <option value="">--- ជ្រើសរើស ---</option>
                                               <option value="1">ធ្វើតេសត៏សាកល្បង</option>
                                               <option value="0">ការប្រលង</option>         
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>សំណួរ</label>
                                        <input type="text" id="newTitle" class="form-control" placeholder="សំណួរ" required>
                                    </div>
                                    <div class="form-group">
                                    	<label>កំនត់ចម្លើយជ្រើសរើស</label>
                                        <div class="input-group">
                                            <input type="text" id="newOption_txt" class="form-control" placeholder="ជម្រើស">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="addOption_btn">
                                                    <i class="fa fa-plus-circle"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row" id="options_box"></div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="active" name="active" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="active"></label>
                                                </div>
                                                <div class="switch_label"> ដាក់ដំណើរការ</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបើក ដើម្បីដាក់ដំណើរការ"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                    </div> 
                                    
                                	<div class="form-group">
                                        <button type="submit" id="newQuestion_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                        <button type="button" id="cancelQuestion_btn" name="cancelQuestion_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                    </div>
                                </form>                                
                                <div id="newQuestion_msg" class=""></div>
                            </div>
                        </div>
                    </div>
                    
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_questions").addClass('active');
						//--- end set active menu	
														
						$("#questionList_search_btn").click(function(){questionList('');});	
						$("#lessonid,#examType").change(function(){questionList('');});							
						
						//--- start navigation btn
						$("#nav_first").click(function(e){questionList('first');});
						$("#nav_prev").click(function(e){questionList('prev');});
						$("#nav_next").click(function(e){questionList('next');});
						$("#nav_last").click(function(e){questionList('last');});
						$("#nav_rowsPerPage").change(function(e){questionList('');});
						$("#nav_currentPage").change(function(e){questionList('goto');});
						//--- end navigation btn
						
						$("#addOption_btn").click(function(){addOptions();});	
						$(".q_options").click(function(){selectOption(this);alert('');});	
						
						$("#newQuestion_frm").submit(function(){newQuestion(); return false;});	
						$("#cancelQuestion_btn").on('click',(function(e) {closeEditForm('newQuestion');$("#options_box").html('');}));	
						
						questionList('');
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!--<div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>-->
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-heading">
                                                        <a name="a_editCateItem" class="anchorLocation"><div></div></a>
                                                        <h3 class="panel-title"><i class="fa fa-plus-circle fa-fw"></i> Edit Category Item</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/page_footer.php"); ?>