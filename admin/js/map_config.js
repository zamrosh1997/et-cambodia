// JavaScript Document
					function initialize(lat,lng,zoom,marker) {
						var myLatlng = new google.maps.LatLng(lat,lng);
						var map_canvas = document.getElementById('map_canvas');											
						var map_options = {
						  center: myLatlng,
						  zoom: parseInt(zoom),
						  mapTypeId: google.maps.MapTypeId.ROADMAP
						}
						
						var map = new google.maps.Map(map_canvas, map_options);
						
						if(marker==1){
							/*var circle = new google.maps.Circle({
								center: myLatlng,
								map: map,
								radius: 10000,          // IN METERS.
								fillColor: '#FF6600',
								fillOpacity: 0.3,
								strokeColor: "#FFF",
								strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
							});*/
							var marker = new google.maps.Marker({
								map:map,
								draggable:true,
								animation: google.maps.Animation.DROP,
								position : myLatlng							
							});
							google.maps.event.addListener(marker, 'click', toggleBounce);
							google.maps.event.addListener(marker, 'mouseup', function(event){
								getNewCoordinate('mouseup',event.latLng.lat(),event.latLng.lng(),map.getZoom());
								/*circle.setMap(null);
								circle = new google.maps.Circle({
									center: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng()),
									map: map,
									radius: 10000,          // IN METERS.
									fillColor: '#FF6600',
									fillOpacity: 0.3,
									strokeColor: "#FFF",
									strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
								});*/
							});
							google.maps.event.addListener(map, 'zoom_changed', function(event){
								getNewCoordinate('zoom_changed',0,0,map.getZoom());
							});
						}else{	
							
							var marker = null,circle=null;						
							google.maps.event.addListener(map, 'click', function(event){
								if(marker !== null){
									marker.setMap(null);
									//circle.setMap(null);							
								}
								
								marker = new google.maps.Marker({
									map:map,
									draggable:true,
									animation: google.maps.Animation.DROP,
									position : new google.maps.LatLng(event.latLng.lat(),event.latLng.lng())
								});
								/*circle = new google.maps.Circle({
									center: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng()),
									map: map,
									radius: 10000,          // IN METERS.
									fillColor: '#FF6600',
									fillOpacity: 0.3,
									strokeColor: "#FFF",
									strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
								});*/
								getNewCoordinate('mouseup',event.latLng.lat(),event.latLng.lng(),map.getZoom());
								google.maps.event.addListener(marker, 'mouseup', function(event){
									getNewCoordinate('mouseup',event.latLng.lat(),event.latLng.lng(),map.getZoom());
									/*circle.setMap(null);	
									circle = new google.maps.Circle({
										center: new google.maps.LatLng(event.latLng.lat(),event.latLng.lng()),
										map: map,
										radius: 10000,          // IN METERS.
										fillColor: '#FF6600',
										fillOpacity: 0.3,
										strokeColor: "#FFF",
										strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
									});*/
								});
								google.maps.event.addListener(map, 'zoom_changed', function(event){
									getNewCoordinate('zoom_changed',0,0,map.getZoom());
								});
							});
						}
					  }
					  
					  function toggleBounce() {
							  if (marker.getAnimation() != null) {
								marker.setAnimation(null);
							  } else {
								marker.setAnimation(google.maps.Animation.BOUNCE);
							  }
						}
					  
					  function getNewCoordinate(eventType,lat,lng,zoom){
							var cur_coordinate = $("#mapCoordinate").val();
							cur_coordinate = cur_coordinate.split(',');
							if(eventType=='zoom_changed'){
								$("#mapCoordinate,#showCoordinate").val(cur_coordinate[0]+','+cur_coordinate[1]+','+zoom+'z');
							}else if(eventType=='mouseup'){
								$("#mapCoordinate,#showCoordinate").val(lat+','+lng+','+zoom+'z');
							}
						}
					  //google.maps.event.addDomListener(window, 'load', function(){initialize(35.8615204, 127.096405,6,0)});