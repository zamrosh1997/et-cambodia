// JavaScript Document
var noAuthenMsg = '<span style="color:red;"><i class="fa fa-exclamation-triangle fa-fw"></i> Access denied! User restriction applied!</span>';
var noAuthenTitle  ='Authentication';
var modalType = 'yesno';
var noAuthenBtnName  ='Logout';
var noAuthenFuncName = 'logout';

$(function() {
	$(".page-header").prepend('<span id="resize_page_wrapper" title="ចុចដើម្បីពង្រីក ឬពង្រួម"><i class="fa fa-caret-left fa-fw"></i></span>');
	$("#resize_page_wrapper").css({'cursor':'pointer','color':'#428bca'});
    $("#resize_page_wrapper").click(function(){
		if($(".sidebar").is(':visible')){			
			$(".sidebar").fadeOut(300,function(){$("#page-wrapper").animate({margin:0},500,function(){$("#resize_page_wrapper").html('<i class="fa fa-caret-right fa-fw"></i>');});});			
		}else{			
			$("#page-wrapper").animate({margin:'0 0 0 250px'},300,function(){$(".sidebar").fadeIn();$("#resize_page_wrapper").html('<i class="fa fa-caret-left fa-fw"></i>');});
		}
	});
});

function showHideDiv(element){
	var box_btn = element,box_body = element.next();
	if(box_body.is(":visible")){
		box_body.animate({'height':0,opacity:0},200,function(){box_body.css({'display':'none'});});
		//box_body.css({'display':'none'});
	}else{
		box_body.css({'display':'block'});
		box_body.animate({'height':"auto",opacity:1},200);
		//box_body.css({'display':'block'});
	}
}

function logout(){
	window.location.href="/admin/logout/";
}
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function detectTextChanged(elementName,dataName){
	if($.type(elementName)!='string' || $.type(dataName)!='string'){return '';}
	var elementType_sign = elementName.substring(0,1);
	var result = false;			
	if(elementType_sign=='.'){
		$(elementName).each(function() {
			if($( this ).attr('type') == 'checkbox'){var inputVal = $( this ).prop('checked');}else{var inputVal = $( this ).val();}
			if($.trim(inputVal) != $.trim($( this ).data(dataName))){result = true;}
		});	
	}else if(elementType_sign=='#'){
		if($(elementName).attr('type') == 'checkbox'){var inputVal = $(elementName).prop('checked');}else{var inputVal = $(elementName).val();}
		if($.trim(inputVal) != $.trim($(elementName).data(dataName))){result = true;}
	}
	return result;
}

function invalidMsg(textbox,type) {
     if(textbox.validity.patternMismatch){
		if(type=='number'){
			textbox.setCustomValidity('Number Only');
		}else if(type=='email'){
			textbox.setCustomValidity('Wrong Email Format');
		}else{
        	textbox.setCustomValidity('Please use the correct format');
		}
    }    
    else {
        textbox.setCustomValidity('');
    }
    return true;
}		

function checkLogin(){
	$("#login_status_msg_cover").html('<div id="msg_loading"><div>ផ្ទៀងផ្ទាត់គណនី...</div></div>');
	$.post("/service/checklogin",{username: $("#username").val(),password:$("#password").val()} ,function(data){
		//var data = JSON.parse(data);
		if(data==1){			
			var gotoNext = getParameterByName('next');
			$("#login_status_msg_cover").html('<div id="msg_success"><div>ផ្ទៀងផ្ទាត់ជោគជ័យ</div></div>');
			setTimeout(function(){
				$("#login_status_msg_cover").html('<div id="msg_success"><div>កំពុងចូលប្រព័ន្ធ...</div></div>');
				if(gotoNext==''){window.location.href="/admin"}else{window.location.href=gotoNext;}
			},1000);
		}else{
			$("#login_status_msg_cover").html('<div id="msg_error"><div>ឈ្មោះគណនី ឬពាក្យសម្ងាត់មិនត្រឹមត្រូវ</div></div>');
		}
	});
}


		function setFocusBlock(id){		
			var eleid ='';			
			if(id != undefined && id!=''){
				eleid = id;
			}else{
				var getHash = window.location.hash;
				var splitHash = getHash.split('_');
				if(splitHash.length=2){eleid = splitHash[1];}
			}
			$(".panel-default").css({'box-shadow':0});
			$("#"+eleid).css({'box-shadow':'0 0 10px 0px #3ac5a6'});
			
			//--- start set active menu
			$("#side-menu li a").removeClass('active');		
			$("#m_"+eleid).addClass('active');
			//--- end set active menu	
		}
		
function hideSaveMsg(id){
	$("#"+id).animate({'opacity':0},500,function(){
		$("#"+id).animate({'height':0,'margin-bottom':0,'padding':0},500,function(){
			$("#"+id).removeClass('alert');
			$("#"+id).removeClass('alert-warning');
			$("#"+id).removeClass('alert-success');	
			$("#"+id).removeClass('alert-danger');
			$("#"+id).html('');	
		});
	});	
}

function iniDisplaySaveMsg(id){	
	$("#pageLoadingStatus").removeClass('isHiden');	

	var eleName = $("#"+id+"_msg");
	eleName.removeClass('alert-warning');
	eleName.removeClass('alert-success');	
	eleName.removeClass('alert-danger');
	
	eleName.css({'height':'auto','margin-bottom':20,'padding':'15px 35px 15px 15px'});
	eleName.animate({'opacity':1},500);
	eleName.addClass('alert alert-info');
	eleName.html('<span><img src="/admin/images/loading.gif" width="25" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
}

function displaySaveMsg(id,msgType,text){
	$("#pageLoadingStatus").addClass('isHiden');	

	var eleName = $("#"+id+"_msg");
	var msgImg = '';
	if(msgType=='info' || msgType=='warning'){msgImg = 'notice-info.png';}else if(msgType=='success'){msgImg = 'notice-success.png';}else if(msgType=='danger'){msgImg = 'notice-error.png';}
	eleName.removeClass('alert-info');
	eleName.addClass('alert-'+msgType+' alert-dismissable');
	eleName.html('<button type="button" class="close" onclick="hideSaveMsg(\''+id+'_msg\');">&times;</button><span><img src="/admin/images/'+msgImg+'" width="25" />&nbsp;&nbsp;&nbsp;'+text+'</span>');
}

var confirmDialog_timeout = 0;
function confirmDialog(modal,headerTxt,bodyTxt,mainBtnName,data,funcName){
	clearTimeout(confirmDialog_timeout);	
	confirmDialog_timeout = setTimeout(function(){
		$("#"+modal+"_modalLabel").html(headerTxt);
		$("#"+modal+"_modalLabelBodyText").html(bodyTxt);
		$("#"+modal+"_actionBtn").html(mainBtnName);
		$("#"+modal+"_confirmData").val(data);	
		$( "#"+modal+"_btn" ).trigger( "click" );
	},0);

	$("#"+modal+"_actionBtn").unbind('click');
	$("#"+modal+"_actionBtn").click(function(){window[funcName](data);});
}

function popupMsg(modal,headerTxt,bodyTxt){
	$("#"+modal+"_modalLabel").html(headerTxt);
	$("#"+modal+"_modalLabelBodyText").html(bodyTxt);
	$( "#"+modal+"_btn" ).trigger( "click" );
}

function popupSearchguide(){
	$( "#searchguide_btn" ).trigger( "click" );
}

function searchguide(){
	$("#searchguide_tbl tbody").html('<tr><td colspan="6" style="text-align:center; color:#c0434d;"><span><img src="/admin/images/loading.gif" width="15" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span></td></tr>');
	var search_txt = $("#search_txt").val();
	if($.trim(search_txt)==''){return false;}
	//iniDisplaySaveMsg(formName);
	$.post("/client/request",{cmd:'searchguide',search_txt:search_txt} ,function(data){
		//var data = JSON.parse(data);
		
		$("#searchguide_tbl tbody").html(data);
	});
}

function lessonList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#lessonList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'lessonList',keyword:searchKeyword,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#lessonList_tbl tbody").html(data.list);
				$(".editLesson_btn").unbind('click');
				$(".editLesson_btn").on('click',(function(e){iniEditLesson($(this).data('recordid'));}));		
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function questionList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#questionList_search_txt").val(),lessonid=$("#lessonid").val(),examType=$("#examType").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'questionList',keyword:searchKeyword,lessonid:lessonid,examType:examType,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#questionList_tbl tbody").html(data.list);				
				$(".iniEditQuestion").unbind('click');
				$(".iniEditQuestion").on('click',(function(e){iniEditQuestion($(this).data('recordid'));}));		
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {  
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function iniEditQuestion(id){
		var formName = 'newQuestion';
		iniDisplaySaveMsg(formName);
		$("#"+formName+"_btn").prop('disabled',true);
		$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែសំណួរ');
		$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
		$("#recordid").val(id);
		$.post("/service/request",{cmd:'iniEditQuestion',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#newLessonid").val(data.lessonid);
				$("#newExamType").val(data.type);
				$("#newTitle").val(data.title);
				
				showOptions(data.q_options,data.answer);
				$("#active").prop("checked",(data.active==0?false:true));				
				
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
				$('.chosen-select').trigger('chosen:updated');
			}
		});
}

function addOptions(){
	var newOption = $.trim($("#newOption_txt").val());
	$("#newOption_txt").val('').focus();
	if(newOption != ''){		
		var totalOptions = 0,allOptions = [];
		$(".option_item .options_del").remove();
		$(".option_item").each(function() {totalOptions++;allOptions.push($(this).html())});
		var newTotalCol = parseInt(12/(totalOptions+1));
		$("#options_box").html('');		

		$.each(allOptions,function(key,value){
			$("#options_box").append('<div id="opt_'+key+'" class="col-lg-'+newTotalCol+' col-md-'+newTotalCol+' col-sm-'+(newTotalCol*2)+' option_item">'+value+'<div class="options_del" data-optid="opt_'+key+'"><i class="fa fa-times"></i></div></div>');			
		});		

		$("#options_box").append('<div id="opt_'+totalOptions+'" class="col-lg-'+newTotalCol+' col-md-'+newTotalCol+' col-sm-'+(newTotalCol*2)+' option_item"><div class="well q_options">'+newOption+'</div><div class="options_del" data-optid="opt_'+totalOptions+'"><i class="fa fa-times"></i></div></div>');
		$("#options_box .well").unbind('click');
		$("#options_box .well").click(function(){selectOption(this);});	
		
		$(".options_del").unbind('click');
		$(".options_del").click(function(){removeOption(this);});	
	}else{
		$("#addOption_btn").css({backgroundColor:'red',color:'#fff'});
		$("#addOption_btn").stop().animate({backgroundColor:'#fff',color:'#333333'},1000);
	}
}

function removeOption(e){	
		var optid = $(e).data('optid');
		$("#"+optid).remove();
		var totalOptions = 0,allOptions = [];
		$(".option_item .options_del").remove();
		$(".option_item").each(function() {totalOptions++;allOptions.push($(this).html())});
		var newTotalCol = parseInt(12/(totalOptions));
		$("#options_box").html('');		

		$.each(allOptions,function(key,value){
			$("#options_box").append('<div id="opt_'+key+'" class="col-lg-'+newTotalCol+' col-md-'+newTotalCol+' col-sm-'+(newTotalCol*2)+' option_item">'+value+'<div class="options_del" data-optid="opt_'+key+'"><i class="fa fa-times"></i></div></div>');
		});		
		
		$("#options_box .well").unbind('click');
		$("#options_box .well").click(function(){selectOption(this);});	
		
		$(".options_del").unbind('click');
		$(".options_del").click(function(){removeOption(this);});	
}

function showOptions(options_,selected){
	//var newOption = $.trim($("#newOption_txt").val());
	$("#options_box").html('');	
	if(options_ != ''){	
		var allOptions = options_.split("|");	
		var totalOptions = allOptions.length;
		//$(".option_item").each(function() {totalOptions++;allOptions.push($(this).html())});
		var newTotalCol = parseInt(12/(totalOptions));			

		$.each(allOptions,function(key,value){
			//$("#options_box").append('<div class="col-lg-'+newTotalCol+' option_item">'+value+'</div>');
			if(key==(selected-1)){
				$("#options_box").append('<div id="opt_'+key+'" class="col-lg-'+newTotalCol+' col-md-'+newTotalCol+' col-sm-'+(newTotalCol*2)+' option_item"><div class="well q_option_s">'+value+'</div><div class="options_del" data-optid="opt_'+key+'"><i class="fa fa-times"></i></div></div>');
			}else{
				$("#options_box").append('<div id="opt_'+key+'" class="col-lg-'+newTotalCol+' col-md-'+newTotalCol+' col-sm-'+(newTotalCol*2)+' option_item"><div class="well q_options">'+value+'</div><div class="options_del" data-optid="opt_'+key+'"><i class="fa fa-times"></i></div></div>');
			}
		});		
		
		$("#options_box .well").unbind('click');
		$("#options_box .well").click(function(){selectOption(this);});	
		
		$(".options_del").unbind('click');
		$(".options_del").click(function(){removeOption(this);});	
	}
}

function selectOption(e){
	$(".q_option_s").addClass('q_options');	
	$("#options_box .well").removeClass('q_option_s');	
	$(e).removeClass('q_options');
	$(e).addClass('q_option_s');	
}

function optionData(type){
	var i=0,data = [],classNames='',answer=0;
	$("#options_box .well").each(function() {
		i++;
		classNames=$(this).attr('class').split(" ");
		if($.inArray('q_option_s',classNames)>0){
			answer=i;
		}
		data += (data==''?'':'|') + $(this).html();	
	});	
	if(type=='answer'){return answer;}else if(type=='total'){return (i);}else{return data;}	
}

function newQuestion(){
	iniDisplaySaveMsg('newQuestion');
	if($("#newTitle").val()=='' || $("#newLessonid").val()<=0 || $("#newExamType").val()==''){displaySaveMsg('newQuestion','danger','ទិន្នន័យមិនគ្រប់គ្រាន់'); return false;}
	if(optionData('answer')==0 || optionData('total')<=1){displaySaveMsg('newQuestion','danger','ត្រូវមានចម្លើយអោយជ្រើសរើស យ៉ាងតិច២ ដោយត្រូវកំណត់ចំលើយដែលត្រឹមត្រូវ');return false;}
	var getData = {recordid:$("#recordid").val(),
					newTitle:$("#newTitle").val(),
					newLessonid:$("#newLessonid").val(),
					newExamType:$("#newExamType").val(),
					optionData:optionData(''),
					answer:optionData('answer'),
					active:$("#active").prop('checked')
	}
	$("#newTitle,#newOption_txt").val('');$("#newLessonid,#newDifficultyid").val(0);$("#options_box").html('');
	$.post("/service/request",{cmd:'newQuestion',getData:getData} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg('newQuestion','danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				questionList('goto');
				if(data.result==1){
					if(data.msg=='inserted'){
						displaySaveMsg('newQuestion','success','សំនួរត្រូវបានបញ្ជូលសម្រាប់មេរៀន '+ $("#newLessonid option[value='"+getData.newLessonid+"']").text());
						$("#cancelQuestion_btn").trigger("click");
					}else if(data.msg=='updated'){
						displaySaveMsg('newQuestion','success','សំនួរត្រូវបានកែប្រែសម្រាប់មេរៀន '+ $("#newLessonid option[value='"+getData.newLessonid+"']").text());
						$("#cancelQuestion_btn").trigger("click");
					}else{
						displaySaveMsg('newQuestion','danger','Unexpected error!');
					}
				}else{
					displaySaveMsg('newQuestion','danger','Unexpected error!');
				}
			}
		});
}

function scoreList(){
		$("#scoreList_tbl tbody").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var lessonid=$("#lessonid").val();
		$.post("/service/request",{cmd:'scoreList',lessonid:lessonid} ,function(data){
			if(data=='no_authentication'){	
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				$("#scoreList_tbl tbody").html(data.list);	
				$("#scoreList_tbl tfoot").html(data.score);		
			}
		});
}

function setScore(){
	if($("#lessonid").val()<=0 || $("#txtScore").val()<=0 || $("#txtScore").val()==''){ return false;}
	var getData = {lessonid:$("#lessonid").val(),
					txtScore:$("#txtScore").val()
	}
	$("#txtScore").val('');
	$.post("/service/request",{cmd:'setScore',getData:getData} ,function(data){
			if(data=='no_authentication'){		
				//displaySaveMsg('newQuestion','danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);
				scoreList('');
				//displaySaveMsg('newQuestion','success','សំនួរកំរិត');
			}
		});
}

function updateSetting(){
	iniDisplaySaveMsg("questionSettings");	

	var allowQuiz=$( "#allowedQuiz" ).prop('checked'),totalQuestion=$("#totalQuestion").val(),showCorrectAnswer=$( "#showCorrectAnswer" ).prop('checked');
	if(allowQuiz){allowQuiz=1;}else{allowQuiz=0;}
	if(showCorrectAnswer){showCorrectAnswer=1;}else{showCorrectAnswer=0;}
	var updateData ={totalQuestion:totalQuestion,
					allowedQuiz:allowQuiz,
					showCorrectAnswer:showCorrectAnswer		
					}

		$.post("/service/request",{cmd:'updateSetting',updateData:updateData} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg("questionSettings","danger","ការបញ្ជារត្រូវបានច្រានចោល។")	
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);
				if(data==''){
					displaySaveMsg("questionSettings","success","ទិន្នន័យត្រូវបានកែប្រែដោយជោគជ័យ។");
				}else{
					displaySaveMsg("questionSettings","danger","មានបញ្ហា! ទិន្នន័យមិនបានកែប្រែទេ។");
				}			
			}
		});
}

function announcementList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#announcementList_search_txt").val(),typeid=$("#typeid").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'announcementList',keyword:searchKeyword,typeid:typeid,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#announcementList_tbl tbody").html(data.list);
				$(".editAnnouncement_btn").unbind('click');
				$(".editAnnouncement_btn").on('click',(function(e){iniEditAnnouncement($(this).data('recordid'));}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages);
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function guideList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#guideList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'guideList',keyword:searchKeyword,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){	
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#fullexport,#partexport").data('listname',data.listname);
				$("#fullexport").data('sql',data.fullsql);
				$("#partexport").data('sql',data.partsql);
				
				$("#guideList_tbl tbody").html(data.list);
				$(".editGuide_btn").unbind('click');
				$(".editGuide_btn").on('click',(function(e){loadProfile($(this).data('recordid'));}));	
				$(".statusGuide_btn").unbind('click');
				$(".statusGuide_btn").on('click',(function(e){
					var recordid = $(this).data('recordid'),status = $(this).data('status'),licenseid = $(this).data('licenseid');
					confirmDialog("yesno","ស្ថានភាពគណនី","តើអ្នកប្រាកដថានឹង"+(status==0?' ដាក់ដំណើរការ':' ផ្អាក')+"គណនី "+licenseid+" ឬ?",status==0?' ដាក់ដំណើរការ ':' ផ្អាក ',"guideStatus|"+recordid,"onOffData");
				}));
				$(".pendingGuide_btn").unbind('click');
				$(".pendingGuide_btn").on('click',(function(e){
					var recordid = $(this).data('recordid'),pending = $(this).data('status'),licenseid = $(this).data('licenseid');
					confirmDialog("yesno","ស្ថានភាពគណនី","តើអ្នកប្រាកដថានឹង"+(pending==1?' បើក':' ផ្អាក')+"គណនី "+licenseid+" ឬ?",pending==1?' បើក ':' ផ្អាក ',"guidePending|"+recordid,"onOffData");
				}));
				
				$(".addCourseGuide_btn").unbind('click');
				$(".addCourseGuide_btn").on('click',(function(e){
					var recordid = $(this).data('recordid'),licenseid = $(this).data('licenseid');
					confirmDialog("regCourse","ជ្រើសវគ្គសិក្សាសម្រាប់គណនី "+licenseid,"",'<i class="fa fa-plus-circle"></i> បញ្ចូលឈ្មោះ',recordid,"reg_course");
				}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function getCourse(){
	//var formName = 'resetPassword';
	//iniDisplaySaveMsg(formName);
	$("#course_id").html('<option value="">--- កំពុងដំណើរការ ---</option>');
	$.post("/service/request",{cmd:'getCourse',course_year:$("#course_year").val(),course_area:$("#course_area").val()} ,function(data){
		var data = JSON.parse(data);		
		if(data.result==1){
			$("#course_id").html('<option value="0">--- ជ្រើសរើស ---</option>');
			$("#course_id").append(data.data);
		}else{
			$("#course_id").html('<option value="0">--- គ្មានទិន្នន័យ ---</option>');
		}
	});
}

function reg_course(id){
	var formName = 'regCourse';
	iniDisplaySaveMsg(formName);
	//$("#course_id").html('<option value="">--- កំពុងដំណើរការ ---</option>');
	$.post("/service/request",{cmd:'reg_course',guideid:id,courseid:$("#selectCourse").val()} ,function(data){
		var data = JSON.parse(data);	
		if(data.result){
			displaySaveMsg(formName,'success',data.msg);
			guideList('');
		}else{
			displaySaveMsg(formName,'danger',data.msg);
		}
	});
}

function courseListMultiple(selected){
	//var formName = 'resetPassword';
	//iniDisplaySaveMsg(formName);
	$("#newCourse_id").html('');
	$('.chosen-select').trigger('chosen:updated');
	$.post("/service/request",{cmd:'courseListMultiple',course_year:$("#newCourse_year").val(),course_area:$("#newCourse_area").val(),selected:selected} ,function(data){
		var data = JSON.parse(data);		
		if(data.result==1){
			$("#newCourse_id").html(data.data);
		}
		$('.chosen-select').trigger('chosen:updated');
	});
}

function candidateList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#candidateList_search_txt").val(),course_id=$("#course_id").val(),pending_status=$("#pending_status").val(),reg_status=$("#reg_status").val(),reg_via=$("#reg_via").val();
		var currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'candidateList',keyword:searchKeyword,course_id:course_id,pending_status:pending_status,reg_status:reg_status,reg_via:reg_via,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){			
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var subjectData = data.subjectData;
				
				$("#fullexport,#partexport").data('listname',data.listname);
				$("#fullexport").data('sql',data.fullsql);
				$("#partexport").data('sql',data.partsql);
				
				
				$("#candidateList_tbl tbody").html(data.list);
				$(".courseRegStatus_btn").unbind('click');
				$(".courseRegStatus_btn").on('click',(function(e){
					var recordid = $(this).data('recordid'),status = $(this).data('status'),licenseid = $(this).data('licenseid');
					confirmDialog("yesno","បញ្ជីឈ្មោះ","តើអ្នកប្រាកដថានឹង"+(status==0?'បញ្ចូល':'លុប')+"គណនី "+licenseid+ (status==0?' ទៅក្នុង':' ចេញពី') + "បញ្ជីឬ?",status==0?' បញ្ចូល':' លុប ',"courseRegStatus|"+recordid,"onOffData");
				}));
				$(".paidStatus_btn").unbind('click');
				$(".paidStatus_btn").on('click',(function(e){
					var recordid = $(this).data('recordid'),status = $(this).data('status'),licenseid = $(this).data('licenseid');
					confirmDialog("yesno","បញ្ជាក់ការបងប្រាក់","គណនី "+licenseid+" នឹងក្លាយជាគណនីបង់ប្រាក់រួច។ តើអ្នកប្រាកដដែរឬទេ?",'បាទ/ចាស',"paidStatus|"+recordid,"onOffData");
				}));
				$(".score_btn").unbind('click');
				$(".score_btn").on('click',(function(e){
					var recordid = $(this).data('recordid'),guidename = $(this).data('guidename');
					popupMsg("popup","ពិន្ទុ សម្រាប់ <strong>"+guidename+"</strong>",subjectData[recordid]);
				}));				
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {  
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}				

				//active function button
				/*$(".updateScore").click(function(){
					if($("."+this.id).prop("disabled")){
						$("."+this.id).prop("disabled",false);	
					}else{
						$("."+this.id).prop("disabled",true);
						updateScore(this.id);	
					}
				});	*/			

				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function update_score(e){
		var formName = 'update_score';
		e.preventDefault();
		iniDisplaySaveMsg(formName);		
		$.ajax({
			url: "/service/request", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			dataType: 'json',
			data: new FormData($("#"+formName+"_frm")[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data.result==1){
					displaySaveMsg(formName,'success',data.msg);
					candidateList('goto');
					$("#popup_modalCloseBtn").trigger('click');
				}else{
					displaySaveMsg(formName,'danger',data.msg);
				}
			},
			error: function (responseData, textStatus, errorThrown) {
				displaySaveMsg(formName,'danger','Unexpected Error');
			}
		});
}


function userList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#userList_search_txt").val(),typeid=$("#typeid").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'userList',keyword:searchKeyword,typeid:typeid,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#userList_tbl tbody").html(data.list);
				$(".editUser_btn").unbind('click');
				$(".editUser_btn").on('click',(function(e){iniEditUser($(this).data('recordid'));}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function newguideList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#ng_guidelist_search_txt").val(),result_id=$("#result_id").val(),language_id=$("#languageid").val(),search_gender=$("#search_gender").val(),from_date=$("#from_date").val(),to_date=$("#to_date").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'newguideList',keyword:searchKeyword,result_id:result_id,language_id:language_id,search_gender:search_gender,from_date:from_date,to_date:to_date,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#ng_guidelist_tbl tbody").html(data.list);
				$(".editRow_btn").unbind('click');
				$(".editRow_btn").on('click',(function(e){iniEdit_ng_profile($(this).data('recordid'));}));	
				
				$(".qrcode_img").unbind('click');
				$(".qrcode_img").on('click',(function(e){popupMsg('popup','QR កូដ សម្រាប់ '+$(this).data('name'),'<div style="text-align:center; margin:50px 0;"><img src="'+$(this).data('src')+'" /></div>')}));	
				
				$(".save_ng_score_btn").unbind('click');
				$(".save_ng_score_btn").on('click',(function(e){save_ng_score($(this).data('recordid'),$(this));}));	
				
				
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function iniEdit_ng_profile(id){
	var formName = 'ng_profile';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែគណនី');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEdit_ng_profile',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				$("#fname_kh").val(data.fname_kh);	
				$("#lname_kh").val(data.lname_kh);		
				$("#fname_en").val(data.fname_en);		
				$("#lname_en").val(data.lname_en);		
				$("#guide_gender").val(data.gender);		
				$("#language_id").val(data.language_id);	
				$("#exam_id").val(data.exam_id);	
				$("#score_avg").val(data.score_avg);	
				$("#score_grade").val(data.score_grade);					
				$("#active").prop("checked",(data.active==0?false:true));
				
				//$("#newPassword,#confirmNewPassword").prop('required',false);
				
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function iniEdit_translator_profile(id){
	var formName = 'translator_profile';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែគណនី');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEdit_translator_profile',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var filenames_str = data.photo;
				var filenames = filenames_str.split("|");
				$("#course_id").val(data.course_id);
				$("#fname_kh").val(data.fname_kh);	
				$("#lname_kh").val(data.lname_kh);		
				$("#fname_en").val(data.fname_en);		
				$("#lname_en").val(data.lname_en);		
				$("#gender").val(data.gender);		
				$("#nationality_id").val(data.nationality_id);	
				$("#dob").val(data.dob);	
				$("#passport").val(data.passport);	
				$("#mobile").val(data.mobile);	
				$("#allfiles").val(filenames_str);		
				$("#selectedFile").html('').show();	
				if(filenames.length>0 && filenames_str!=''){						
					var fileid = $.now();
					$.each(filenames, function (key, value) {
						//$("#"+key).data('inidata',data);						
						$("#selectedFile").prepend('<div id="file_'+fileid+'" style="'+($("#selectedFile").html()==''?'':'padding-bottom:5px; margin-bottom:5px; border-bottom:1px solid #e9e9e9;')+'"><i class="fa fa-file-text-o fa-fw"></i> <a href="'+(data.filePath+value)+'" target="_blank">'+value+'</a> | <span data-eleid="file_'+fileid+'" class="delFile_txt" data-filename="'+value+'">លុប</span></div>');
						
						$(".delFile_txt").unbind('click');
						$(".delFile_txt").click(function(e){delFile($(this).data('eleid'),$(this).data('filename'))});	
					})					
				}			
				$("#active").prop("checked",(data.active==0?false:true));
				
				//$("#newPassword,#confirmNewPassword").prop('required',false);
				
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function save_ng_score(rowid,ele){
	var scorebyrow = [];
	$( ".row_"+rowid ).each(function() {
	  	scorebyrow.push(this.id+'_'+$(this).html())
	});
	
	ele.html('<img src="/admin/images/loading.gif" width="16" />').prop('disabled', true);
	$.post("/service/request",{cmd:'save_ng_score',scorebyrow:scorebyrow} ,function(data){
		if(data=='no_authentication'){			
			displaySaveMsg(formName,'danger',noAuthenMsg);
			confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
		}else{
			var data = JSON.parse(data);
			ele.removeClass("btn-primary");
			if(data.result){
				ele.addClass("btn-success");
				setTimeout(function(){ele.removeClass("btn-success");ele.addClass("btn-primary");},5000);
			}else{
				ele.addClass("btn-danger");
				setTimeout(function(){ele.removeClass("btn-danger");ele.addClass("btn-primary");},5000);
			}
			ele.html('<i class="fa fa-floppy-o fa-fw"></i> ពិន្ទុ').prop('disabled', false);
		}
	});
}

function translatorList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#ng_guidelist_search_txt").val(),search_course_id=$("#search_course_id").val(),typeid=$("#typeid").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'translatorList',keyword:searchKeyword,search_course_id:search_course_id,typeid:typeid,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#translator_list_tbl tbody").html(data.list);
				$(".editRow_btn").unbind('click');
				$(".editRow_btn").on('click',(function(e){iniEdit_translator_profile($(this).data('recordid'));}));	
				
				$(".qrcode_img").unbind('click');
				$(".qrcode_img").on('click',(function(e){popupMsg('popup','QR កូដ សម្រាប់ '+$(this).data('name'),'<div style="text-align:center; margin:50px 0;"><img src="'+$(this).data('src')+'" /></div>')}));					
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function courseList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var course_year=$("#course_year").val(),course_area=$("#course_area").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'courseList',course_year:course_year,course_area:course_area,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#courseList_tbl tbody").html(data.list);
				$(".editCourse_btn").unbind('click');
				$(".editCourse_btn").on('click',(function(e){iniEditCourse($(this).data('recordid'));}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function examDateList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var examDate_year=$("#examDate_year").val(),examDate_area=$("#examDate_area").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		$.post("/service/request",{cmd:'examDateList',examDate_year:examDate_year,examDate_area:examDate_area,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#examDateList_tbl tbody").html(data.list);
				$(".editExamDate_btn").unbind('click');
				$(".editExamDate_btn").on('click',(function(e){iniEditExamDate($(this).data('recordid'));}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}



function paymentList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#paymentList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		var searchDate_from = $("#payment_date_from").val(),searchDate_to = $("#payment_date_to").val(),payment_result = $("#payment_result").val();
		$.post("/service/request",{cmd:'paymentList',keyword:searchKeyword,searchDate_from:searchDate_from,searchDate_to:searchDate_to,payment_result:payment_result,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#paymentList_tbl tbody").html(data.list);
				//$(".editUser_btn").unbind('click');
				//$(".editUser_btn").on('click',(function(e){iniEditUser($(this).data('recordid'));}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}

function activityList(navAction){
		$("#nav_info").html('<span><img src="/admin/images/loading.gif" width="16" />&nbsp;&nbsp;&nbsp;កំពុងដំណើរការ...</span>');
		var searchKeyword=$("#activityList_search_txt").val(),currentPage = $("#nav_currentPage").val(),rowsPerPage = $("#nav_rowsPerPage").val();
		var searchDate_from = $("#activity_date_from").val(),searchDate_to = $("#activity_date_to").val(),activity_type = $("#activity_type").val();
		$.post("/service/request",{cmd:'activityList',keyword:searchKeyword,searchDate_from:searchDate_from,searchDate_to:searchDate_to,activity_type:activity_type,currentPage:currentPage,rowsPerPage:rowsPerPage,navAction:navAction} ,function(data){
			if(data=='no_authentication'){		
				$("#nav_info").html(noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				
				$("#activityList_tbl tbody").html(data.list);
				//$(".editUser_btn").unbind('click');
				//$(".editUser_btn").on('click',(function(e){iniEditUser($(this).data('recordid'));}));	
				
				$("#nav_currentPage").val(data.targetPage);
				if(navAction=='refresh' || navAction==''){
					$('#nav_currentPage').empty();
					$.each(data.gotoSelectNum, function(key, value) {   
						 $('#nav_currentPage')
							.append($("<option></option>")
							.attr("value",value)
							.text(value)); 
					});
				}
				if(data.totalPages==0){
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;គ្មានទិន្នន័យ');
				}else{
					$("#nav_info").html('<i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp;ទំព័រទី '+data.targetPage+' នៃ '+data.totalPages+' (សរុប '+data.totalRow+')');
				}
				$.each(data.nav_btn_disable, function (key, jdata) {if(jdata==1){$("#"+key).removeClass('disabled');}else{$("#"+key).addClass('disabled');}})
			}
		});
}


/*function updateScore(id){
	$("#total_"+id).html('<img src="/admin/images/loading.gif" width="16" />');
	var cateid = {};
	$("."+id).each(function() {var id_pars = this.id.split("_")[1];cateid[id_pars] = this.value;});	

	var getData = {registration_id:id,
					cateid:cateid
	}	
	$.post("/service/request",{cmd:'updateScore',getData:getData} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg('newQuestion','danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				if(data.msg==1){
					$("#total_"+id).html(data.data);
				}else{
					$("#total_"+id).html("--");
				}
			}
		});
}*/

var totalfiles=0,filesuploaded=0;
function uploadfile(submitbtn,e,frmData){
		e.preventDefault();
		var fileid = $.now();		
		var filepath = $("#file").val();
		$("#selectedFile").show();
		$("#selectedFile").prepend('<div id="file_'+fileid+'" style="'+($("#selectedFile").html()==''?'':'padding-bottom:5px; margin-bottom:5px; border-bottom:1px solid #e9e9e9;')+'"><i class="fa fa-file-text-o fa-fw"></i> <a id="link_'+fileid+'" href="#" target="_blank">'+(filepath.split('\\').pop())+'</a> <img id="load_'+fileid+'" src="/admin/images/sm_loading.gif" /></div>');
		$("#file").val('');		
		totalfiles++;
		$.ajax({
			url: "/admin/includes/uploadfile.php", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			dataType: 'json',
			data: frmData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data.result==1){
					$("#allfiles").val($("#allfiles").val()+($("#allfiles").val()==''?'':'|')+data.newfilename);
					$("#link_"+fileid).attr('href',data.filePath+data.newfilename);
					$("#file_"+fileid).append(' | '+ data.filesize + ' <i class="fa fa-check fa-fw"></i> | <span data-eleid="file_'+fileid+'" class="delFile_txt" data-filename="'+data.newfilename+'">លុប</span>').css('color','green');
					//register click event for delete file button
					$(".delFile_txt").unbind('click');
					$(".delFile_txt").click(function(e){delFile($(this).data('eleid'),$(this).data('filename'))});	
				}else{
					$("#file_"+fileid).append(' | '+ data.filesize + ' <i class="fa fa-times fa-fw"></i> : '+data.msg).css('color','red');
				}
				$("#load_"+fileid).remove();
				
				filesuploaded++;
				if(filesuploaded==totalfiles){$("#"+submitbtn).prop('disabled',false);totalfiles=0;filesuploaded=0;}
			},
			error: function (responseData, textStatus, errorThrown) {
				//alert('POST failed.');
				$("#file_"+fileid).append('<i class="fa fa-times fa-fw"></i> : Error').css('color','red');
				$("#load_"+fileid).remove();
			}
		});
}

function delFile(eleid,filename){	
	$("#"+eleid).remove();
	if($("#selectedFile").html()==''){$("#selectedFile").hide();}
	//remove filename from input
	var curFile = $("#allfiles").val().split("|");
	/*curFile = jQuery.grep(curFile, function(value) {
	  return value != filename;
	});*/
	curFile.splice($.inArray(filename, curFile),1);

	$("#allfiles").val(curFile.join("|"));	
	
	$.post("/service/request",{cmd:'delFile',filename:filename} ,function(data){

	});
}

function closeEditForm(formName){	
	$('#'+formName+'_frm').each(function(){this.reset();});
	$('#'+formName+'_frm select').each(function(){$(this).find('option:selected').removeAttr("selected");});
	$("#recordid").val(0);
	$("#allfiles").val('');
	$("#selectedFile").html('').hide();
	tinyMCE.triggerSave();
	
	$("#form_label").html('<i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី');
	$("#"+formName+"_btn").html('<i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល');
	$('.chosen-select').trigger('chosen:updated');
	if($("#selectCourse_div").length){$("#selectCourse_div select").prop('disabled',false);}
	if($("#subjectData").length){$("#subject_list tbody").html('');setSubject_course();}
}

/*function newLesson(e){
		var formName = 'newLesson';
		iniDisplaySaveMsg(formName);
		tinyMCE.triggerSave();
		e.preventDefault();
		$.ajax({
			url: "/service/request", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			dataType: 'json',
			data: new FormData($("#"+formName+"_frm")[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data.result==1){
					if(data.msg=='inserted'){
						displaySaveMsg(formName,'success','មេរៀនត្រូវបាន​បញ្ចូល​ដោយជោគជ័យ');
					}else if(data.msg=='updated'){
						displaySaveMsg(formName,'success','មេរៀនត្រូវបាន​កែប្រែ​ដោយជោគជ័យ');
					}					
					closeEditForm(formName);
					lessonList('');
				}else{
					displaySaveMsg(formName,'danger','ទិន្នន័យមិនគ្រប់គ្រាន់');
				}
			},
			error: function (responseData, textStatus, errorThrown) {
				displaySaveMsg(formName,'danger','Unexpected Error');
			}
		});
}*/

function iniEditLesson(id){
	var formName = 'newLesson';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEditLesson',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var filenames = data.filenames;
				$("#newTitle").val(data.title);
				$("#newDes").val(data.des);
				tinyMCE.get('newDes').setContent(data.des);
				
				//$("#currentDoc ul").html(data.filenames);	
				$("#allfiles").val(data.filenames_str);	
				$("#newTotalQuestion").val(data.totalQuestion);	
				$("#online_exam_date").val(data.online_exam_date);	
				$("#selectedFile").html('').show();	
				if(filenames.length>0){						
					var fileid = $.now();
					$.each(filenames, function (key, value) {
						//$("#"+key).data('inidata',data);						
						$("#selectedFile").prepend('<div id="file_'+fileid+'" style="'+($("#selectedFile").html()==''?'':'padding-bottom:5px; margin-bottom:5px; border-bottom:1px solid #e9e9e9;')+'"><i class="fa fa-file-text-o fa-fw"></i> <a href="'+(data.filePath+value)+'" target="_blank">'+value+'</a> | <span data-eleid="file_'+fileid+'" class="delFile_txt" data-filename="'+value+'">លុប</span></div>');
						
						$(".delFile_txt").unbind('click');
						$(".delFile_txt").click(function(e){delFile($(this).data('eleid'),$(this).data('filename'))});	
					})					
				}
				
				$("#allow_quiz").prop("checked",(data.allowQuiz==0?false:true));
				$("#publishNow").prop("checked",(data.publishNow==0?false:true));
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

/*function newAnnouncement(){
	tinyMCE.triggerSave();
	iniDisplaySaveMsg('newAnnouncement');
	if($("#newTitle").val()=='' || $("#newTypeid").val()<=0 || $("#newDes").val() == ''){displaySaveMsg('newAnnouncement','danger','ទិន្នន័យមិនគ្រប់គ្រាន់'); return false;}

	var getData = {newTitle:$("#newTitle").val(),
					newTypeid:$("#newTypeid").val(),
					data:$("#examid").val(),
					newDes:$("#newDes").val()
	}

	$("#newTitle,#newDes").val('');$("#newTypeid").val(0);
	$.post("/service/request",{cmd:'newAnnouncement',getData:getData} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg('newQuestion','danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);
				if(data==''){
					announcementList('');
					displaySaveMsg('newAnnouncement','success','ការប្រកាសត្រូវបានបោះផ្សាយដោយជោគជ័យ។');
				}else{
					displaySaveMsg('newAnnouncement','danger','មានបញ្ហា! ទិន្នន័យមិនបានបញ្ចូលទេ។');
				}
			}
		});
}*/

/*function newAnnouncement(e){
		var formName = 'newAnnouncement';
		iniDisplaySaveMsg(formName);
		tinyMCE.triggerSave();
		e.preventDefault();
		$.ajax({
			url: "/service/request", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			dataType: 'json',
			data: new FormData($("#"+formName+"_frm")[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data.result==1){
					if(data.msg=='inserted'){
						displaySaveMsg(formName,'success','សេចក្តីប្រកាសត្រូវបាន​បញ្ចូល​ដោយជោគជ័យ');
					}else if(data.msg=='updated'){
						displaySaveMsg(formName,'success','សេចក្តីប្រកាសត្រូវបាន​កែប្រែ​ដោយជោគជ័យ');
					}					
					closeEditForm(formName);
					announcementList('');
				}else{
					displaySaveMsg(formName,'danger','ទិន្នន័យមិនគ្រប់គ្រាន់');
				}
			},
			error: function (responseData, textStatus, errorThrown) {
				displaySaveMsg(formName,'danger','Unexpected Error');
			}
		});
}
*/
function addData(e,formName,funcName){
		//var formName = 'newAnnouncement';
		iniDisplaySaveMsg(formName);
		tinyMCE.triggerSave();
		e.preventDefault();
		if($("#subjectData").length){setSubject_course();}
		$.ajax({
			url: "/service/request", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			dataType: 'json',
			data: new FormData($("#"+formName+"_frm")[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data.result==1){
					if(data.msg=='inserted'){
						displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបាន​បញ្ចូល​ដោយជោគជ័យ');
					}else if(data.msg=='updated'){
						displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបាន​កែប្រែ​ដោយជោគជ័យ');
					}					
					
					if(funcName!=''){
						closeEditForm(formName);
						window[funcName]('goto');
					}
				}else{
					if(data.msg=='exist'){
						displaySaveMsg(formName,'danger','ទិន្នន័យមានរូចហើយ');
					}else if(data.msg=='not_match'){
						displaySaveMsg(formName,'danger','បញ្ជាក់ពាក្យសម្ងាត់មិនត្រឹមត្រូវ');
					}else if(data.msg=='wrong_password'){
						displaySaveMsg(formName,'danger','ពាក្យសម្ងាត់បច្ចុប្បន្នមិនត្រឹមត្រូវ');
					}else if(data.msg=='custom'){
						displaySaveMsg(formName,'danger',data.error);
					}else{
						displaySaveMsg(formName,'danger','ទិន្នន័យមិនគ្រប់គ្រាន់');
					}					
				}
			},
			error: function (responseData, textStatus, errorThrown) {
				displaySaveMsg(formName,'danger','Unexpected Error');
			}
		});
}

function iniEditAnnouncement(id){
	var formName = 'newAnnouncement';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែសេចក្តីប្រកាស');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEditAnnouncement',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var filenames = data.filenames;
				
				$("#newTypeid").val(data.type);
				$("#newTitle").val(data.title);
				$("#newDes").val(data.des);
				tinyMCE.get('newDes').setContent(data.des);
				$("#newDes").val(data.des);				
				$("#newTypeid").trigger('change');			
				
				//$("#currentDoc ul").html(data.filenames);	
				$("#allfiles").val(data.filenames_str);	
				$("#selectedFile").html('').hide();	
				if(filenames.length>0){		
					$("#selectedFile").show();									
					//var fileid = $.now();
					$.each(filenames, function (key, value) {
						//$("#"+key).data('inidata',data);						
						$("#selectedFile").prepend('<div id="file_'+key+'" style="'+($("#selectedFile").html()==''?'':'padding-bottom:5px; margin-bottom:5px; border-bottom:1px solid #e9e9e9;')+'"><i class="fa fa-file-text-o fa-fw"></i> <a href="'+(data.filePath+value)+'" target="_blank">'+value+'</a> | <span data-eleid="file_'+key+'" class="delFile_txt" data-filename="'+value+'">លុប</span></div>');
						
						$(".delFile_txt").unbind('click');
						$(".delFile_txt").click(function(e){delFile($(this).data('eleid'),$(this).data('filename'))});	
					})					
				}
				
				$("#publishNow").prop("checked",(data.active==0?false:true));
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function loadProfile(id){
	var formName = 'guideProfile';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#selectCourse_div select").prop('disabled',true).val("");
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែព័ត៌មាន');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'loadProfile',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);				
				$.each(data, function(key, value) { 
					div_id = "#guide_"+key;
					if ($(div_id).length > 0){ 
						$(div_id).val(value);
					}
					
				});				
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function reg_selectcourse(areaEleid,courseEleid){
	//var formName = 'resetPassword';
	//iniDisplaySaveMsg(formName);
	var eleid = $("#"+courseEleid),area_eleid = $("#"+areaEleid);
	eleid.html('<option value="">--- កំពុងដំណើរការ ---</option>');
	$.post("/client/request",{cmd:'getCourse',area:area_eleid.val()} ,function(data){
		var data = JSON.parse(data);		
		if(data.result==1){
			eleid.html('<option value="">--- ជ្រើសរើស ---</option>');
			eleid.append(data.data);
		}else{
			if(area_eleid.val()==''){
				eleid.html('<option value="">--- សូមជ្រើសរើសតំបន់ ---</option>');
			}else{
				eleid.html('<option value="">--- គ្មានទិន្នន័យ ---</option>');
			}
		}
	});
}

function updateGuide(){
	var btn = $("#update_licenseid_btn");
	btn.attr("disabled", true);
	btn.html('<img src="/admin/images/loading.gif" width="15" />');
	$.post("/service/request",{cmd:'updateGuide',recordid:$("#recordid").val(),guide_code:$("#guide_code").val()} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);				
				if(data==''){
					btn.html('<i class="fa fa-check"></i>');
					guideList('');
				}else{
					btn.html('<i class="fa fa-times"></i>');
				}
				setTimeout(function(){btn.html('<i class="fa fa-floppy-o"></i>').attr("disabled", false);},3000)
			}
		});
}

function iniEditUser(id){
	var formName = 'newUser';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែគណនី');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEditUser',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var filenames = data.filenames;
				$("#newTypeid").val(data.type);	
				$("#newUsername").val(data.username);		
				$("#newLName").val(data.lname);		
				$("#newFName").val(data.fname);		
				$("#newPassword").val(data.password);		
				$("#confirmNewPassword").val(data.password);					
				$("#active").prop("checked",(data.active==0?false:true));
				
				//$("#newPassword,#confirmNewPassword").prop('required',false);
				
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function onOffData(d){
	$("#yesno_modalCloseBtn").trigger('click');
	var data = $("#yesno_confirmData").val().split("|");
	$.post("/service/request",{cmd:data[0],recordid:data[1]} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);
				
				window[data]('');
			}
		});
}

function iniEditCourse(id){
	var formName = 'newCourse';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែគណនី');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEditCourse',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var filenames = data.filenames;
				$("#newCourse_year").val(data.year);	
				$("#newCourse_area").val(data.area);		
				$("#newCourse_num").val(data.course_num);		
				$("#newRegister_date").val(data.register_date);	
				$("#course_end_date").val(data.course_end_date);	
				$("#total_hours").val(data.total_hours);		
				$(".dtpicker").datetimepicker('update');
				$("#newMax_participant").val(data.max_participant);	
				$("#subject_list tbody").html(data.subjectItem);
				$("#start_online_exam").prop("checked",(data.start_online_exam==0?false:true));		
				$("#active").prop("checked",(data.active==0?false:true));
				
				$('.chosen-select').trigger('chosen:updated');				
				//$("#newPassword,#confirmNewPassword").prop('required',false);
				
				//set event click to button
				$(".removeSubject").unbind('click');
				$(".removeSubject").click(function(){removeSubject_course($(this).data('subjectid'));});
				
				//re-run date picker
				$('.dtpicker').datetimepicker({pickTime: false});
				
				//assign subject data
				setSubject_course();
				
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function addSubject_course(){
	var subjectid = $("#newSubject_id").val();
	var subjectTitle = $('#newSubject_id option:selected').text();
	
	if(subjectid==0){alert('Please select subject');return false;}
	var tbl_tr = '<tr id="'+subjectid+'"><td>'+subjectTitle+'</td><td><div class="input-append input-group dtpicker" style="width: 85px;"><input class="oeDate" style="width: 85px;" data-format="yyyy-MM-dd" type="text" placeholder="កាលបរិច្ឆេទ" class="form-control"><span class="input-group-addon add-on"><i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar" class="fa fa-calendar"></i></span></div></td><td><div class="input-append input-group"><input class="extend_time" style="width: 50px;" type="text" placeholder="ចំនួន" class="form-control" value="0"><span class="input-group-addon add-on">ថ្ងៃ</span></div></td><td><button type="button" class="btn btn-danger btn-xs removeSubject" data-subjectid="'+subjectid+'"><i class="fa fa-times"></i></button></td></tr>';
	
	if(!$(".removeSubject").length){
		$("#subject_list tbody").html(tbl_tr);
	}else{
		$("#subject_list tbody").append(tbl_tr);
	}
	
	//set event click to button
	$(".removeSubject").unbind('click');
	$(".removeSubject").click(function(){removeSubject_course($(this).data('subjectid'));});
	
	//re-run date picker
	$('.dtpicker').datetimepicker({pickTime: false});
	
	//reassign data
	setSubject_course();
}

function removeSubject_course(id){
	$("#"+id).remove();
	//reassign data
	setSubject_course();
	
	if(!$(".removeSubject").length){$("#subject_list tbody").html('<tr><td colspan="4" class="tableCellCenter">គ្មានមុខវិជ្ជា</td></tr>');}
}

function setSubject_course(){
	$("#newSubject_id option").each(function(index, element) {
		$(this).prop('disabled', false);
    });	
	
	var subjectData = {};
	$("#subject_list tbody tr").each(function(index, element) {
		var oeDate = $(this).find("input.oeDate").val(),extendtime = $(this).find("input.extend_time").val();
        subjectData[$(this).attr('id')] = {date: oeDate,extend:extendtime==''?0:extendtime};
		//disable the selected option	
		$("#newSubject_id option[value="+$(this).attr('id')+"]").prop('disabled', true);
		$("#newSubject_id").val(0);	
    });	
	
	//console.log(JSON.stringify(subjectData));
	$('.chosen-select').trigger('chosen:updated');	
		
	//set data to hide input
	$("#subjectData").val(JSON.stringify(subjectData));
}

function nextCourseNumber(){	
	var year=$("#newCourse_year").val(),area=$("#newCourse_area").val();
	if(year>0 && area>0){
		$.post("/service/request",{cmd:"nextCourseNumber",year:year,area:area} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);
				if($.isNumeric(data)){
					$("#newCourse_num").val(data);
				}
			}
		});
	}
}

function iniEditExamDate(id){
	var formName = 'newExamDate';
	iniDisplaySaveMsg(formName);
	$("#"+formName+"_btn").prop('disabled',true);
	$("#form_label").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែគណនី');
	$("#"+formName+"_btn").html('<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែ');
	$("#recordid").val(id);
	$.post("/service/request",{cmd:'iniEditExamDate',recordid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg(formName,'danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var filenames = data.filenames;
				$("#newCourse_year").val(data.year);	
				$("#newCourse_area").val(data.area);
				courseListMultiple(data.course_id);			
				$("#newExam_date").val(data.exam_date);	
				$(".dtpicker").datetimepicker('update');		
				$("#result_released").prop("checked",(data.result_released==0?false:true));		
				$("#active").prop("checked",(data.active==0?false:true));
				
				//$("#newPassword,#confirmNewPassword").prop('required',false);
				
				//confirmDialog("edit",'<i class="fa fa-pencil-square-o fa-fw"></i> កែប្រែមុខវិជ្ជា',"","",id,"");	
				displaySaveMsg(formName,'success','ទិន្នន័យត្រូវបានទាញដោយជោគជ័យ');
				$("#"+formName+"_btn").prop('disabled',false);
			}
		});
}

function rt_log(){
	var totalunseen = 0;
	$.post("/service/request",{cmd:'rt_log'} ,function(data){
		var data = JSON.parse(data);		
		$.each(data, function(key, value) {   
			 if(!$(".dropdown-notif #notif_"+key).length){
				 $(".dropdown-notif").prepend(value);
			 }
		});		
		
		if(Object.keys(data).length){
			if($(".dropdown-notif #nodata").length){$(".dropdown-notif #nodata").remove();}
		}
		$(".dropdown-notif li").each(function() {
			if($(this).attr('class')=='newNotif'){
				totalunseen++;
			}
		});
		if(totalunseen>0){
			$("#total_unseen").html(totalunseen).addClass('unseen_num');
		}else{
			$("#total_unseen").html('').removeClass('unseen_num');
		}
		
		setTimeout(rt_log,10000);
	});
}
function rt_log_seen(){
	var log_ids = [];
	$(".dropdown-notif li").each(function() {
		if($(this).attr('class')=='newNotif'){
			var id_parts = $(this).attr('id').split('_');
			log_ids.push(id_parts[1]);
		}
	});
	if(log_ids.length){
	$.post("/service/request",{cmd:'rt_log_seen',log_ids:log_ids} ,function(data){
		$(".dropdown-notif li").each(function() {
			if($(this).attr('class')=='newNotif'){$(this).removeClass('newNotif');}
        });
		$("#total_unseen").html('').removeClass('unseen_num');
	});
	}
}

function exportData(listName,sql){
	var moreData = {};
	if($("#course_id").length){moreData.course_id = $("#course_id").val();}
	$("#btn_print_excel").html('<img src="/admin/images/loading.gif" height="15" />');
	$.post("/phpExcel/standardReport.php",{listName:listName,sql:sql,moreData:moreData} ,function(data){
		window.location.href=data;
		$("#btn_print_excel").html('<i class="fa fa-file-excel-o fa-fw"></i');
	});			
}

$(document).ready(function(e) {	
    $("#fullexport,#partexport").click(function(){exportData($(this).data('listname'),$(this).data('sql'))});
	$("#notif_box").click(rt_log_seen);
	rt_log();
});













		

		

		

		

		

		