
function init() {
    $(".dragzones").draggable({
        start: handleDragStart,
        cursor: 'move',
        revert: 'invalid',
        opacity: .5,
        drag: function(){
            //var offset = $(this).position();
            //var xPos = offset.left;
           // var yPos = offset.top;
            //$(this).html(offset.left+" "+offset.top);
        }
    });
    $(".dropzones").droppable({
        drop: handleDropEvent,
        tolerance: "touch",
        out: handleDropRemove
    });
    
   
}

function handleDragStart (event, ui) {} 
function handleDropRemove(event, ui) {
        //allows drop after removal
       //$(this).removeClass('taken');   
}      
function handleDropEvent (event, ui) {
	if($("#allowMove").prop('checked')==false){		
		ui.draggable.draggable('option', 'revert', true);
	}else{
		ui.draggable.draggable('option', 'revert', false);
	}
	
	
   /*if ($(this).hasClass('taken')) {
       //rejects drop if full
        ui.draggable.draggable('option', 'revert', true);
    } else {
        //accepts drop if enpty
        ui.draggable.position({of: $(this), my: 'left top', at: 'left top'});
        $(this).addClass('taken');
   }*/
}

function highlightRoom(id){
	var hBtn = $("#highlight_btn_"+id),hightLightColor = 'none',borderRadius=0,boxShadow='0 0 0 #888888';
	if(hBtn.html()=='<i class="fa fa-eye-slash"></i>'){
		hightLightColor = 'transparent';borderRadius=0;boxShadow='0 0 0 #888888';
		hBtn.html('<i class="fa fa-eye"></i>').css({'color':'#2D91B5'});
	}else{
		hightLightColor = 'rgba(255,255,255,0.5)';borderRadius=5;boxShadow='0px 0px 5px #888888';
		hBtn.html('<i class="fa fa-eye-slash"></i>').css({'color':'red'});
	}
	
	$(".dragzones").each(function() {
		var id_parts = $(this).attr('id').split('_');
		if(id_parts[1]==id){$(this).css({'background-color':hightLightColor,'border-radius':borderRadius,'box-shadow':boxShadow});}
	});	
}

function saveRoomMap(){
	iniDisplaySaveMsg('saveMap');
	var locationData = {};
	$(".dragzones").each(function() {
		locationData[$(this).attr('id')] = {'top':$(this).css('top'),'left':$(this).css('left')}//$("#"+$(this).attr('id')+":first").offset();		
	});	
	$.post("/service/request",{cmd:'saveRoomMap_inup',locationData:locationData} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg('saveMap','danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				//var data = JSON.parse(data);
				displaySaveMsg('saveMap','success','Position data saved.');
			}
		});
}

function locateRoom(id){
	if(id<1){return false;}
	iniDisplaySaveMsg('saveMap');
	$.post("/service/request",{cmd:'locateRoomPosition',campingid:id} ,function(data){
			if(data=='no_authentication'){			
				displaySaveMsg('saveMap','danger',noAuthenMsg);
				confirmDialog(modalType,noAuthenTitle,noAuthenMsg,noAuthenBtnName,0,noAuthenFuncName);
			}else{
				var data = JSON.parse(data);
				var rooms = data.rooms;
				$("#roomMap").css({'background':'url('+data.mapImg+') center center no-repeat','background-size':'cover'});
				$("#roomMap").html('');
				
				$.each(rooms, function (key, value) {
					var typeid = value.typeid,position = value.position,roomLabel = value.roomLabel,active = value.active;
					//$("#room_"+key).animate({'top':position[0],'left':position[1],'opacity':1},1000);
					
					var d=document.createElement('div');
					$(d).addClass("dragzones pulse-grow")
						.css({'top':position[0],'left':position[1],'opacity':active,'cursor':(active==1?'move':'default')})
						//.animate({'top':position[0],'left':position[1],'opacity':active,'cursor':(active==1?'move':'default')},500)
						.html(roomLabel)
						.attr('id','room_'+typeid+'_'+key)
						.appendTo($("#roomMap")) //main div
						/*.click(function(){
							$(this).remove();
						})
						.hide()
						.slideToggle(300)
						.delay(2500)
						.slideToggle(300)
						.queue(function() {
							$(this).remove();
						});*/
				});
				init();//start drag drop config
				displaySaveMsg('saveMap','success','Position data loaded.');
			}
		});
}

