<?php 
$pageInfo = array('code'=>'lessons','name'=>'Training Lessons');
include("includes/page_header.php"); 
include("includes/page_topbar.php"); 
include("includes/page_leftmenu.php");


$cateid = get('c');
if (!isset($_SESSION['adminid'])) { header("location: ".$redirectUrl);}else{
	if(!in_array($pageInfo['code'],$auth_menu)){header("location: ".$redirectUrl);}
}
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            <i class="fa fa-file-text-o fa-fw"></i> គ្រប់គ្រងមុខវិជ្ជា<!--<small> <i class="fa fa-angle-right"></i> Authentication</small>-->
                        </h3>
                    </div>
                </div>
                
                <!-- /.row -->
                <div class="row">                    
                    <div class="col-lg-7">                    	
                        <div class="panel panel-default" id="lessonList">
                            <div class="panel-heading">
                            	<a name="a_lessonList" class="lessonList"><div></div></a>
                                <h3 class="panel-title"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីមុខវិជ្ចា</h3>                            
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                            	<form role="form" id="lessonList_frm">
                                    <div class="form-group">                        
                                            <div class="input-group custom-search-form">
                                                <input type="text" id="lessonList_search_txt" class="form-control" placeholder="វាយពាក្យ...">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" id="lessonList_search_btn">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                    </div>
                                </form>
                                <div>
                                <div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                    <option value="5">០៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                                <div style="clear:both; padding:2px 0;"></div>
                                
                                <div class="table-responsive" id="lessonList_tbl_cover">
                                    <table class="table table-striped table-bordered table-hover" id="lessonList_tbl">
                                        <thead>
                                            <tr>
                                                <th class="tableCellCenter">ល.រ.</th>
                                                <th>ចំនងជើងមុខវិជ្ជា</th>
                                                <th style=" min-width:90px;width:90px;" class="tableCellCenter"></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        </div>
                        
                    </div>
                    
                    
                    <div class="col-lg-5">
                        <div class="panel panel-default" id="newLesson">
                            <div class="panel-heading">
                                <h3 class="panel-title" id="form_label"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូលថ្មី</h3>
                            </div>
                            <div class="panel-body">
                                <form role="form" id="newLesson_frm" action="" method="post" enctype="multipart/form-data">	
                                	<input type="hidden" id="recordid" name="recordid" value="0" />
                                    <div class="form-group">
                                        <label for="newTitle">ចំនងជើងមុខវិជ្ជា</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចំនងជើងមុខវិជ្ចា"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="text" id="newTitle" name="newTitle" class="form-control" placeholder="ចំនងជើង" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="newDes">ខ្លឹមសារ</label> <span class="redStar">*</span>
                                        <textarea id="newDes" name="newDes"></textarea>
                                    </div> 
                                    <div class="form-group">
                                        <label for="file">ភ្ទាប់ឯកសារមេរៀន</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="សូមភ្ជាប់ឯកសារ ដែលមានប្រភេទជា jpg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, mp3, wav, mp4"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="file" id="file" name="file" class="form-control">
                                        <input type="hidden" id="allfiles" name="allfiles" />
                                        <div id="selectedFile" class="thumbnail" style="display:none; margin-top:10px;"></div>
                                    </div> 
                                    <div class="form-group">
                                        <label for="newTotalQuestion">ចំនួនសំណួរចេញប្រលងអនឡាញ</label> <span class="redStar">*</span>
                                        <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="សម្រាប់ទាំងការធ្វើតេស្ត និងការប្រលងអនឡាញ។ ប្រសិនបើចំនួនស្មើ ០ នោះនឹងចាប់យកចំនួនដែលប្រព័ន្ធបានកំនត់រួច។"><i class="fa fa-info-circle"></i></span></div>
                                        <input type="number" id="newTotalQuestion" name="newTotalQuestion" class="form-control" placeholder="ចំនួន" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="allow_quiz" name="allow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="allow_quiz"></label>
                                                </div>
                                                <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបិទ ឬបើក ដើម្បីអនុញ្ញាតអោយមានការប្រលងសាកល្បង ឬមិនអនុញ្ញាត"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                     </div>      
                                    <div class="form-group">
                                        <div class="switch">
                                                <div class="switch_input">
                                                <input id="publishNow" name="publishNow" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                <label for="publishNow"></label>
                                                </div>
                                                <div class="switch_label"> ដាក់អោយដំណើរការពេលនេះ</div>
                                                <div class="switch_lable_tooltip">
                                                <div class="tooltip-des inline_paddingLeft"><span data-toggle="tooltip" data-placement="right" title="ចុចបិទ ឬបើក ដើម្បីដាក់អោយដំណើរការពេលនេះ ឬចាំដាក់ដំណើរការពេលក្រោយ"><i class="fa fa-info-circle"></i></span></div>
                                                </div>
                                        </div>
                                     </div>      
                                	<div class="form-group">
                                    	<input type="hidden" name="cmd" value="newLesson" />
                                        <button type="submit" id="newLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> បញ្ចូល</button>
                                        <button type="button" id="cancelLesson_btn" name="cancelLesson_btn" class="btn btn-primary"><i class="fa fa-times fa-fw"></i> បោះបង់</button>
                                    </div>
                                </form>                                
                                <div id="newLesson_msg" class=""></div>
                            </div>
                        </div>
                    </div>
                    
                </div>                
                <!-- /.row -->
                <script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_lessons").addClass('active');
						//--- end set active menu	
														
						$("#lessonList_search_btn").click(function(){lessonList('');});							
						//--- start navigation btn
						$("#nav_first").click(function(e){lessonList('first');});
						$("#nav_prev").click(function(e){lessonList('prev');});
						$("#nav_next").click(function(e){lessonList('next');});
						$("#nav_last").click(function(e){lessonList('last');});
						$("#nav_rowsPerPage").change(function(e){lessonList('');});
						$("#nav_currentPage").change(function(e){lessonList('goto');});
						//--- end navigation btn
						
						
						lessonList('');	
						
						//$("#newLesson_frm").on('submit',(function(e) {newLesson(e);}));		
						$("#newLesson_frm").on('submit',(function(e) {addData(e,'newLesson','lessonList');}));		
						$("#file").on('change',(function(e) {
							$("#newLesson_btn").prop('disabled',true);
							uploadfile("newLesson_btn",e,new FormData($("#newLesson_frm")[0]));
						}));
						$("#cancelLesson_btn").on('click',(function(e) {closeEditForm('newLesson');}));	
			
                    });
                </script>

            </div>            
            
            <!-- /#page-wrapper -->
            				<div>
								<!-- Button trigger modal -->
                                <div id="edit_btn" data-toggle="modal" data-target="#edit_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="edit_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="edit_modalLabelBodyText1">
                                                <div class="panel panel-default" id="editCateItem">
                                                    <div class="panel-body">
                                                        <form role="form" id="editLesson_frm" action="/service/request" method="post" enctype="multipart/form-data">	
                                                            <div class="form-group">
                                                                <label>ចំនងជើងមុខវិជ្ជា</label>
                                                                <input type="text" id="editTitle" name="editTitle" class="form-control" placeholder="ចំនងជើង" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ឯកសារ៖</label>
                                                                <div id="currentDoc">
                                                                	<ul>
                                                                    	<li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                        <li><a href="#">អាចអោយមានការប្រលង</a></li>
                                                                    </ul>
                                                                	
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>ផ្លាស់ប្តូរឯកសារ</label>
                                                                <input type="file" id="editDoc" name="editDoc" class="form-control">
                                                            </div> 
                                                            <div class="form-group">
                                                                <div class="switch">
                                                                        <div class="switch_input">
                                                                        <input id="editallow_quiz" name="editallow_quiz" class="cmn-toggle cmn-toggle-round" type="checkbox" data-inidata="true" checked>
                                                                        <label for="editallow_quiz"></label>
                                                                        </div>
                                                                        <div class="switch_label"> អាចអោយមានការប្រលងសាកល្បង</div>
                                                                </div>
                                                             </div> 
                                                                       
                                                            <div class="form-group">
                                                                <input type="hidden" name="cmd" value="editLesson" />
                                                                <input type="hidden" id="edit_confirmData" name="edit_confirmData" value="" />
                                                                <button type="submit" id="editLesson_btn" name="newLesson_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                                            </div>
                                                        </form>                                
                                                        <div id="editLesson_msg" class=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="edit_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="edit_actionBtn"></button>
                                                <input type="hidden" id="edit_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                
                                <!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                           </div>  
<?php include("includes/page_footer.php");?>