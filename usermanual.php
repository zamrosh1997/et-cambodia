<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");


session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false));
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// ========= End Session ============ //

//if(isset($_SESSION['userid'])){header("location: /");}
$sec_code = isset($_GET['sec_code'])?get('sec_code'):'';

$pageName='User Manual | Tourist Guide Refreshment Course';
$pageCode='usermanual';
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>


  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-lightbulb-o fa-fw"></i> របៀបសិក្សាតាមអនឡាញ</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
      	<br />
        <div class="widget">                 	
            <div class="panel-group" id="accordion"> 
                  <?php             
				  	if($sec_code<>''){$sql_cond="and title='$sec_code'";}else{$sql_cond='';}     
                    $data_qry = exec_query_utf8("SELECT * FROM tblsubcategory WHERE mainCategoryid=(select id from tblmaincategory where title='user_manual' limit 1) $sql_cond order by priority ASC ");
                    while($data_row = mysqli_fetch_assoc($data_qry)){
                        echo '<div class="panel panel-default">	
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#'.$data_row['title'].'">
									  <i class="fa fa-arrow-circle-right fa-fw"></i> '.$data_row['displayTitle'].'
									</a>
								  </h4>
								</div>
								<div id="'.$data_row['title'].'" class="panel-collapse collapse">
								  <div class="panel-body">'.$data_row['description'].'</div>
								</div>
							  </div>
							  ';
                    }	                  
                  ?>
         	</div>
            <hr />
            <div>
            	<p><i class="fa fa-angle-double-right fa-fw"></i> លោកអ្នកអាច <a title="ទាញយក User Manual" href="http://cambodia-touristguide.com/download/p4z5a4l5j574v3b4j5v5e416p5l4n5n4t4f5r4i4u5h406t2f3v5c484y4i5v5y5j5h474o49404w2035484u254745313z2z3t5d4o5">ចុចទីនេះ</a>&nbsp;ដើម្បីទាញយកឯកសារនេះ​ក្នុងទំរង់ជា PDF។</p>
            </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

</body>
</html>