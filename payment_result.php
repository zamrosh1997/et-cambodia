<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$pageName='Payment Result | Tourist Guide Refreshment Course';
$pageCode='PaymentResult';

if(isset($_GET['dt'])){
	$data_get = unserialize(decodeString($_GET['dt'],$encryptKey));	
	//var_dump($data_get);
	$user_token = $data_get['token'];
	$get_userid = $data_get['get_userid'];
	$timeToExpired = 60*60;						 
	$valid_link=true;
	if(time() - $data_get['token'] > $timeToExpired){$valid_link = false;}		
	if(!$valid_link){	
		header("location: /");
	}
}else{
	//header("location: /");
}

?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-archive fa-fw"></i> ការចុះឈ្មោះជោគជ័យ</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
        <?php if($_GET['status']=='success'){ ?>
        	<div class="well">
        		<div class="alert alert-success" style="text-align:center;">
                	<div style="font-size:50px; color:#45c8f2">
                    	<i class="fa fa-check"></i>
                    </div>
                    <div>
                    	<?php
						$transactionInfo = '';
						if(isset($_GET['token'])){
							$user_data = qry_arr("id,email","tblusers","id='".$get_userid."' AND active=1 LIMIT 1");
							$user_email = $user_data['email'];
							$user_id = $user_data['id'];		
							$paymentData = qry_arr("userid,tid,account,amount","tblpayment","userid=$get_userid AND user_token='$user_token' AND active=1 ORDER BY id DESC LIMIT 1");
															
							$transactionInfo = '<div class="col-md-6 col-md-offset-3" style="text-align:left;">
													ព័ត៌មាននៃការបង់ប្រាក់៖<br />
													- Transaction ID: '.$paymentData['tid'].'<br />
													- លេខគណនី Wing: '.$paymentData['account'].'<br />
													- ទឹកប្រាក់: '.$paymentData['amount'].'<br />
													- Email: '.($user_email==''?'N/A':$user_email).'
													</div><div class="clearfix"></div>';
						}
							
						if(isset($_SESSION['userid'])){
							echo 'អ្នកបានបង់ប្រាក់ដោយជោគជ័យ! អរគុណ។<br /><br />'.$transactionInfo;
						}else{							
							$loginLink = 'http://et.cambodia-touristguide.com/login';							
							echo '
							អ្នកបានចុះឈ្មោះដោយជោគជ័យ!<br />
							សូមពិនិត្យអ៊ីម៉េលរបស់អ្នកទាំងប្រអប់ Inbox និង Spam ដើម្បីដាក់ដំណើរការគណនីរបស់អ្នក។<br /><br />		
							
							<span style="color:blue;"><i class="fa fa-hand-o-right"></i> បញ្ជាក់៖ អ្នកអាចបង់ប្រាក់តាមរយៈសាលានៃតំបន់ដែលអ្នកចុះឈ្នោះរៀន។ <br />ឬបង់តាម Online ពេលអ្នកចូលក្នុងគណនីរបស់អ្នក។</span>
							
							'.$transactionInfo.'							
							
							';
						}
						?>
                    </div>                
                </div>
             </div>
         <?php }else{ ?>
			 
			 <div class="form well">
             	<h5 class="alert alert-danger" style="text-align:center;">
                	<div style="font-size:50px;">
                    	<i class="fa fa-exclamation-triangle fa-fw"></i>
                    </div>
             		ការបង់ប្រាក់បានបរាជ័យ!<br /><br />
                    
                    បញ្ជាក់៖ អ្នកអាចចូលក្នុងគណនីរបស់អ្នក ដើម្បីធ្វើការបង់ប្រាក់ពេលក្រោយ។
                </h5>           
             </div>
			 
		 <?php }?>
         </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->

<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>
</body>
</html>