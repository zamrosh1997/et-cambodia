<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");

session_start(); //Start the session
ob_start();


if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);exit;}

$pageName='Subjects for Online Exam | Tourist Guide Refreshment Course';
$pageCode='examsubjects';

$examLessonTip = qry_arr("displayTitle,description","tblsubcategory"," title='exam_lesson_tip' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");

?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>


  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-list-alt fa-fw"></i> មេរៀនសម្រាប់ប្រលងអនឡាញ</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
      	<br />
        <div class="widget">                 	
            <div class="panel-group" id="accordion"> 
            		<div class="alert alert-info">
                        <i class="fa fa-info-circle fa-fw"></i>
                        
                        <?php
							$allowedExamTimes = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedExamTimes' AND active=1 LIMIT 1");  
							$examPeriod = singleCell_qry("settingValue","tblgeneralsetting","settingName='examPeriod' AND active=1 LIMIT 1");   
							$examSessionTime = singleCell_qry("settingValue","tblgeneralsetting","settingName='examSessionTime' AND active=1 LIMIT 1");    
							         
							
						?>
                        
                        <?=$examLessonTip['displayTitle']?>
                        <?=str_replace(array('[examPeriod]','[allowedExamTimes]','[examSessionTime]'),array($examPeriod,$allowedExamTimes,$examSessionTime),$examLessonTip['description'])?>
                    </div>
                    <hr />
                  <?php   
				  	
					//check page accessibility
					if(!$start_online_exam or !$course_reg_status or $course_unpaid_status or !$exam_date_set){header("location: /"); exit;}
				  
				  	$datetime =date("Y-m-d H:i:s");				  	
				  	   
					$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id IN ($subject_ids) and active=1 ORDER BY publishedDate DESC");
					while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
						$lessonid = $lesson_row['id'];
						$examStr = '';$i=1; $selected_score = '---';
						$selectedExamid = singleCell_qry("id","tblrandomquestion","userid=".$_SESSION['userid']." and lessonid=$lessonid and exam=1 and (finishedDate IS NOT NULL or time_expired=1) and active=1 ORDER BY score DESC limit 1");       
						$exam_qry = exec_query_utf8("SELECT * FROM tblrandomquestion WHERE userid=".$_SESSION['userid']." and lessonid=$lessonid and exam=1 and (finishedDate IS NOT NULL or time_expired=1) and active=1 ORDER BY takenDate ASC");
						while($exam_row = mysqli_fetch_assoc($exam_qry)){
							$testCompleted = false;$rowSelected = false;
							$time_expired = $exam_row['time_expired'];
							if($exam_row['finishedDate']<>NULL){$testCompleted = true;}
							if($selectedExamid==$exam_row['id']){$rowSelected = true;}
							
							$session_score = (number_format($exam_row['score'],(intval($exam_row['score'])==$exam_row['score']?0:1)).'/'.$exam_row['fullScore']);
							
							if($rowSelected){$selected_score = $session_score;}
							$examStr .= '
											<tr style="'.($rowSelected?'color:#239651;font-weight:bold;':'').'">
												<td><a href="/examResult/'.encodeString($exam_row['id'],$encryptKey).'">ប្រលងលើកទី '.$i.' '.($rowSelected?'<i class="fa fa-check"></i>':'').'</a></td>
												<td class="tableCellCenter">'.date("d/m/Y H:i",strtotime($exam_row['takenDate'])).'</td>
												<td class="tableCellCenter">'.($testCompleted?timePeriod($exam_row['takenDate'],$exam_row['finishedDate'],true):($time_expired?$exam_row['allowed_time'].'នាទី':'NA')).'</td>
												<td class="tableCellCenter">'.$session_score.'</td>
												<td class="tableCellCenter">'.($testCompleted?'បញ្ចប់':($time_expired?'អស់ពេល':'---')).'</td>
											</tr>
										';
							$i++;
						}
						$totalExamTimes = mysqli_num_rows($exam_qry);
						if($examStr==''){$examStr='<tr><td colspan="5" class="tableCellCenter">អ្នកមិនទាន់បានប្រលងទេ</td></tr>';}
						
						$get_subjectData = $subjectData[$lessonid];
						$exam_date_set = false;$exam_date = $exam_expired_date = '';$exam_expired=false;$extend_day_txt = '';
						if($get_subjectData['date']<>''){
							$exam_date_set = true;
							$exam_date = $get_subjectData['date'];
							if($get_subjectData['extend']>0){$extend_day=$get_subjectData['extend'];$extend_day_txt = '<span class="color bold">(បន្ថែម)</span>';}else{$extend_day=0;}
							$exam_expired_date = date("Y-m-d",strtotime($exam_date . ' + '.($examPeriod+$extend_day).' days'));
							if(strtotime($datetime)>=strtotime($exam_expired_date)){$exam_expired=true;}else{$exam_expired=false;}
						}
						
						if(strtotime($datetime)>=strtotime($exam_date)){$exam_started=true;}else{$exam_started=false;}
						$exam_date_kh = khmerDate($exam_date);
						$exam_expired_date_kh = khmerDate($exam_expired_date);
						//check exam availibility by subject to show button or msg
						$exam_btn=''; $showExamList = false;
						if($totalExamTimes>=$allowedExamTimes or !$exam_date_set){
							if(!$exam_date_set){
								$exam_btn='<div class="alert alert-info"><i class="fa fa-info-circle fa-fw"></i> កាលបរិច្ឆេទប្រលងមិនទាន់បានកំនត់ទេ</div>';
							}else{
								$exam_btn='<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-fw"></i> អ្នកបានប្រើអស់ចំនួននៃការប្រលងហើយ</div>';
								$showExamList = true;
							}
						}else{
							if(!$exam_started){
								$exam_btn='<div class="alert alert-info"><i class="fa fa-info-circle fa-fw"></i> ការប្រលងនឹងចាប់ផ្តើមនៅ៖ <strong>'.$exam_date_kh.'</strong></div>';
							}else{
								if($exam_expired){
									$exam_btn='<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-fw"></i> ការប្រលងបានផុតកំណត់</div>';
									$showExamList = true;
								}else{
									if($totalExamTimes==($allowedExamTimes-1)){
										$exam_btn='<a href="/exam/'.encodeString($lesson_row['id'],$encryptKey).'" class="btn btn-primary"><i class="fa fa-play-circle-o fa-fw"></i> ចាប់ផ្តើមប្រលង (ចុងក្រោយ)</a>';
										$showExamList = true;
									}else{
										if($totalExamTimes>0){
											$exam_btn='<a href="/exam/'.encodeString($lesson_row['id'],$encryptKey).'" class="btn btn-primary"><i class="fa fa-play-circle-o fa-fw"></i> ចាប់ផ្តើមប្រលង (ម្តងទៀត)</a>';
											$showExamList = true;
										}else{
											$exam_btn='<a href="/exam/'.encodeString($lesson_row['id'],$encryptKey).'" class="btn btn-primary"><i class="fa fa-play-circle-o fa-fw"></i> ចាប់ផ្តើមប្រលង</a>';
										}
									}	
								}
							}
						}
					
						
						if($exam_expired and $selected_score == '---'){$selected_score = 0;}						
                        echo '<div class="panel panel-default">	
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#lesson_'.$lessonid.'">
									  <i class="fa fa-file-text-o fa-fw"></i> '.$lesson_row['title'].'									  
									</a>
									<div class="fs-11"><span class="boldTxt">ចាប់ផ្តើម៖</span> '.(($exam_date=='')?'---':$exam_date_kh).' | <span class="boldTxt">ផុតកំណត់ (ថ្ងៃបិទការប្រលង)៖</span> <span class="'.(($exam_expired_date=='')?'':($exam_expired?'red-bold':'blink_me')).'">'.(($exam_expired_date=='')?'---':$exam_expired_date_kh).'</span> '.$extend_day_txt.' | <span class="boldTxt">ពិន្ទុ៖</span> '.$selected_score.'</div>
								  </h4>
								</div>
								<div id="lesson_'.$lessonid.'" class="panel-collapse collapse">
								  <div class="panel-body">								  		
								  		'.($showExamList?'<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>ប្រលង</th>
													<th class="tableCellCenter">ចាប់ផ្តើម</th>
													<th class="tableCellCenter">ប្រើរយៈពេល</th>
													<th class="tableCellCenter">ពិន្ទុ</th>
													<th class="tableCellCenter">ស្ថានភាព</th>
												</tr>
											</thead>
											<tbody>'.$examStr.'</tbody>
										</table>':'').'
								  		'.$exam_btn.'
								  </div>
								</div>
							  </div>
							  ';
                    }        
                  ?>
         	</div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

</body>
</html>