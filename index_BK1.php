<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$pageName='Homepage | Tourist Guide Refreshment Course';
$pageCode='Homepage';
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<?php include("includes/intro_progress.php"); ?>

<?php
$system_purpose = qry_arr("displayTitle,description","tblsubcategory"," title='system_purpose' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
$participation_policy = qry_arr("displayTitle,description","tblsubcategory"," title='participation_policy' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
$course_guideline = qry_arr("displayTitle,description","tblsubcategory"," title='course_guideline' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
$authorized_school = qry_arr("displayTitle,description","tblsubcategory"," title='authorized_school' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
// $testing = qry_arr("displayTitle,description","tblsubcategory"," title='Testing' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");

$allowedQuiz = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedQuiz' AND active=1 LIMIT 1");
?>
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-lg-8 col-md-8 col-sm-8">

      <!--  កម្មវិធីអនឡាញនៃវគ្គវិក្រឹត្យការមគ្គុទ្ទេសក៏ទេសចរណ៍ -->
  		<div class="widget">
        <h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> <?=$system_purpose['displayTitle']?></h4>
          	<?php			
  			$systemInfo = singleCell_qry("description","tblsubcategory"," title='system_description' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
  			echo $systemInfo;			
  			?>
  		</div>

       <!-- និតិវិធីនៃវគ្គវិក្រឹត្យការ -->
      <div class="widget">
        <h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> <?=$course_guideline['displayTitle']?></h4>
        <div class="widget-content">
        <?=$course_guideline['description']?>
        </div>
      </div> 


        <!-- គោលបំណង -->
        <!-- <div class="widget">
    			<h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> <?=$system_purpose['displayTitle']?></h4>
    			<div class="widget-content">
    			<?=$system_purpose['description']?>
    			</div>
        </div>
 -->
        <!--  លក្ខខណ្ឌនៃការទទួលបានវិញ្ញាបនបត្រ -->
       <!--  <div class="widget">
    			<h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> <?=$participation_policy['displayTitle']?></h4>
    			<div class="widget-content">
    			<?=$participation_policy['description']?>
    			</div>
        </div> -->

          <!-- testing -->
       <!--  <div class="widget">
          <h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> <?=$testing['displayTitle']?></h4>
          <div class="widget-content">
          <?=$testing['description']?>
          </div>
        </div>   -->

        <!-- មេរៀន-មុខវិជ្ជាសិក្សា -->
        <div class="widget">
                <h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> មេរៀន-មុខវិជ្ជាសិក្សា</h4>
                  <div class="widget-content">
                    <table class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th class="tableCellCenter" style="width:50px;">ល.រ.</th>
                          <th>មុខវិជ្ជា</th>
                          <!-- <th style="width:120px;"></th> -->
                        </tr>
                      </thead>
                      <tbody>    
          <?php
						$i=1;
						$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE active=1 ORDER BY priority ASC LIMIT 10");
						while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
							echo '<tr>
								  <td class="tableCellCenter">'.$i.'</td>
								  <td class="tableCellLeftCenter"><a href="/lesson/'.encodeString($lesson_row['id'],$encryptKey).'">'.$lesson_row['title'].'</a></td>
								 
								</tr>';	
							$i++;
						}						
						?>    
                </tbody>
              </table>
            </div>
        </div> 

     
     <!-- សាលាដែលទទួលបានការអនុញ្ញាត -->
         <!-- <div class="widget">
    			<h4 class="khmerTitle"> <i class="fa fa-file-text-o fa-fw"></i> <?=$authorized_school['displayTitle']?></h4>
    			<div class="widget-content">
    			<?=$authorized_school['description']?>
    			</div>
        </div> -->
        
               
      <!--   <div class="well">
        	<?php
			$schoolmap = qry_arr("displayTitle,description","tblsubcategory"," title='hoteltouristpp_school_map' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
			?>
        	<h5><?=$schoolmap['displayTitle']?></h5>
            <?=$schoolmap['description']?>
		</div> -->
        
        <!-- <div class="well">
        	<?php
			$schoolmap = qry_arr("displayTitle,description","tblsubcategory"," title='mekong_siemreap_school_map' AND mainCategoryid=(select id from tblmaincategory where title='content' limit 1) limit 1");
			?>
        	<h5><?=$schoolmap['displayTitle']?></h5>
            <?=$schoolmap['description']?>
		</div> -->
        
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

</body>
</html>