<?php	
	/** Error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	//date_default_timezone_set('Europe/London');	
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');	
	/** Include PHPExcel */
	require_once 'phpexcel/PHPExcel.php';
	include '../../includes/function.php';
	include '../../includes/connect_db.php';	
	include '../includes/sessionInfo.php';
	
	if(username==''){echo 'You do not have permission to use this feature!';exit;}
	
	
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();	
	$startRowIndex =7;
	$exportedDate = gmdate("d-M-Y H:i:s A");
	$letters = range('A', 'Z');
	$borderStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
	$cellHeadColor=array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'startcolor'=>array('rgb'=>'d4d4d4'));
	$bgRedColor=array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'startcolor'=>array('rgb'=>'fe96a6'));
	$bgOrangeColor=array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'startcolor'=>array('rgb'=>'fedfb1'));
	
	
	
		$type=post("type");
		$currentDate = gmdate("Y-m-d"); 
		$basedCurrency = ' $';
		
		if($type==0){
			
			$balanceSQL = 'WHERE';$searchCustomer='All';
			$customerid=post("customerid");if($customerid <> ''){$balanceSQL.=" user_id=$customerid";$searchCustomer=$customerid;}
			$accdate=post("accdate");
			if(!is_date($accdate)){$accdate=$currentDate;}
			
			//Set infor for Report
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Customer: '.$searchCustomer)->mergeCells('A2:L2');
			$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Report for: '.gmdate("d-M-Y",strtotime($accdate)).' (GTM 0)')->mergeCells('A3:L3');
			
			$columnName=array(
								array('ID','Name','Opening Balance','Deposit','Profit/Loss','Withdrawal','Bonus','Balance'),
								array('Net Balance','Gross Balance','Real','Credit','Net Balance','Gross Balance'),
								array('Cashabel','Noncashable')
							);		
			$listTitle='Daily Accounting Report';
			$sheetName='DailyAccountingReport';
			$fileName='DailyAccountingReport';	
			
			
			// merge cell
			$objPHPExcel->getActiveSheet()->setCellValue("A1",$listTitle)->mergeCells("A1:L1");
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(15)->getColor()->setRGB('6F6F6F');	
			
			//*** START list header
			//header row 1
			$objPHPExcel->getActiveSheet()->setCellValue("A4",$columnName[0][0])->mergeCells("A4:A6");
			$objPHPExcel->getActiveSheet()->setCellValue("B4",$columnName[0][1])->mergeCells("B4:B6");
			$objPHPExcel->getActiveSheet()->setCellValue("C4",$columnName[0][2])->mergeCells("C4:D4");
			$objPHPExcel->getActiveSheet()->setCellValue("E4",$columnName[0][3])->mergeCells("E4:E6");
			$objPHPExcel->getActiveSheet()->setCellValue("F4",$columnName[0][4])->mergeCells("F4:F6");
			$objPHPExcel->getActiveSheet()->setCellValue("G4",$columnName[0][5])->mergeCells("G4:G6");
			$objPHPExcel->getActiveSheet()->setCellValue("H4",$columnName[0][6])->mergeCells("H4:J4");
			$objPHPExcel->getActiveSheet()->setCellValue("K4",$columnName[0][7])->mergeCells("K4:L4");
			
			//header row 2
			$objPHPExcel->getActiveSheet()->setCellValue("C5",$columnName[1][0])->mergeCells("C5:C6");
			$objPHPExcel->getActiveSheet()->setCellValue("D5",$columnName[1][1])->mergeCells("D5:D6");
			$objPHPExcel->getActiveSheet()->setCellValue("H5",$columnName[1][2])->mergeCells("H5:I5");
			$objPHPExcel->getActiveSheet()->setCellValue("J5",$columnName[1][3])->mergeCells("J5:J6");
			$objPHPExcel->getActiveSheet()->setCellValue("K5",$columnName[1][4])->mergeCells("K5:K6");
			$objPHPExcel->getActiveSheet()->setCellValue("L5",$columnName[1][5])->mergeCells("L5:L6");
			
			//header row 3
			$objPHPExcel->getActiveSheet()->setCellValue("H6",$columnName[2][0]);
			$objPHPExcel->getActiveSheet()->setCellValue("I6",$columnName[2][1]);
			
			//$objPHPExcel->getActiveSheet()->getColumnDimension("C3")->setAutoSize(true); //now working
			$objPHPExcel->getActiveSheet()->getStyle("A4:L6")->applyFromArray($borderStyle);
			$objPHPExcel->getActiveSheet()->getStyle("A4:L6")->getFill()->applyFromArray($cellHeadColor);	
			$objPHPExcel->getActiveSheet()->getStyle("A1:L6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//*** END list header
			
			
			//get all demo/not-deposit account
			$demoArray = array();
			$customer_qry = exec_query_utf8("SELECT * FROM tblcustomer WHERE accountType<>2 ORDER BY id asc");
			while($row_customer=mysqli_fetch_assoc($customer_qry)){$demoArray[]=$row_customer['id'];}			
			$demoList = implode(",", $demoArray);				
			//exclude this demoList from query
			if(count($demoArray)>0){
				//$balanceSQL.=' AND user_id NOT IN ('.$demoList.')';
				if($balanceSQL == 'WHERE'){$balanceSQL.=' user_id NOT IN ('.$demoList.')';}else{$balanceSQL.=' AND user_id NOT IN ('.$demoList.')';}
			}else{if($balanceSQL == 'WHERE')$balanceSQL='';}

			$balance_qry = exec_query_utf8("SELECT * FROM tblbalance $balanceSQL ORDER BY datetime desc"); // order by expiredDate
			$totalRecords = mysqli_num_rows($balance_qry);
			$i=1;
			//declare for sum amount of each item
			$sumOpeningNetBalance=0;$sumOpeningGrossBalance=0;$sumDeposit=0;$sumPNL=0;$sumWithdraw=0;$sumBonusRC=0;$sumBonusRNC=0;$sumBonusRNCremain=0;$sumBonusCredit=0;$sumNetBalance=0;$sumGrossBalance=0;
			$positivSign='';
			while($row_balance=mysqli_fetch_assoc($balance_qry)){
				if($i%2 == 0){$tbgColor='background:#ebebeb;';}else{$tbgColor='background:#f0f9fa;';}
				$customerid = $row_balance['user_id'];
				//get customer info
				$customerName = '--';$currencyAbv='';$currencySign='';$exchangeRate=0;$currencyid=0;
				$customer_qry = exec_query_utf8("SELECT * FROM tblcustomer WHERE id=$customerid LIMIT 1");
				while($row_customer=mysqli_fetch_assoc($customer_qry)){$customerName=$row_customer['fullname'];$currencyAbv=$row_customer['currency'];}
				//get currency sign
				$currency_qry = exec_query_utf8("SELECT * FROM tblcurrency WHERE abv='$currencyAbv' LIMIT 1");//qry to get currency symbol
				while($row_currency = mysqli_fetch_assoc($currency_qry)){$currencySign = ' '.$row_currency['sign'];$exchangeRate=$row_currency['exchangeRate'];$currencyid=$row_currency['id']; }
				
				//get exchange rate by date
				$historyDate=$accdate;
				$getLastRate_qry = exec_query_utf8("SELECT * FROM tblexchangeratehistory WHERE currencyid=$currencyid AND updatedDate='$historyDate' ORDER BY updatedDate DESC LIMIT 1");
				$lastrateCount = mysqli_num_rows($getLastRate_qry);
				if($lastrateCount==0){
					$getLastRate_qry = exec_query_utf8("SELECT * FROM tblexchangeratehistory WHERE currencyid=$currencyid AND updatedDate<'$historyDate' ORDER BY updatedDate DESC LIMIT 1");
					while($getLastRate_row=mysqli_fetch_array($getLastRate_qry)){$exchangeRate=$getLastRate_row['rate'];}				
				}else{
					while($getLastRate_row=mysqli_fetch_array($getLastRate_qry)){$exchangeRate = $getLastRate_row['rate'];}
				}
				
				//get balance by date
				$lastNetBalanceByDate=0;$lastGrossBalanceByDate=0;
				$openingBalance_qry = exec_query_utf8("SELECT * FROM tblbalancehistory WHERE user_id=$customerid AND datetime <= '$accdate"." 23:59:59' ORDER BY datetime DESC, id DESC LIMIT 1");//qry to get currency symbol
				while($row_openingBalance = mysqli_fetch_assoc($openingBalance_qry)){
					$lastNetBalanceByDate = $row_openingBalance['netBalance'];$lastGrossBalanceByDate = $row_openingBalance['grossBalance'];
				}
				//get opening balance
				$lastClosedDate= gmdate("Y-m-d 00:00:00",strtotime($accdate));$openingNetBalance=0;$openingGrossBalance=0;
				$openingBalance_qry = exec_query_utf8("SELECT * FROM tblbalancehistory WHERE user_id=$customerid AND datetime < '$lastClosedDate' ORDER BY datetime DESC, id DESC LIMIT 1");
				while($row_openingBalance = mysqli_fetch_assoc($openingBalance_qry)){
					$openingNetBalance = $row_openingBalance['netBalance'];$openingGrossBalance = $row_openingBalance['grossBalance'];
				}
				//------ get new balance update amount for ByDate
				//get ByDate deposit		
				$totalByDateDeposit=0;
				$deposit_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDateDeposit FROM tbldeposit WHERE customerid=$customerid AND depositType=1 AND processedDate LIKE '%$accdate%' AND processingStatus=2");
				while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateDeposit = $row_deposit['totalByDateDeposit'];}
				if($totalByDateDeposit=='')$totalByDateDeposit=0;
				//get ByDate withdrawal
				$totalByDateWithdraw=0;
				$withdraw_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDatewithdraw FROM tblwithdrawal WHERE customerid=$customerid AND processedDate LIKE '%$accdate%' AND processingStatus=2");
				while($row_withdraw = mysqli_fetch_assoc($withdraw_qry)){$totalByDateWithdraw = $row_withdraw['totalByDatewithdraw'];}
				if($totalByDateWithdraw=='')$totalByDateWithdraw=0;
				//get ByDate PNL
				$totalWinAmount=0;$totalLostAmount=0;$totalProgressAmount=0;$totalInvest=0;
				$position_qry = exec_query_utf8("SELECT sum(payoutAmount) as totalPayout,sum(amount) as winInvest FROM tblposition WHERE userid=$customerid AND expireDate LIKE '%$accdate%' AND result='W' AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalWinAmount=$row_position['totalPayout']-$row_position['winInvest'];}
				$position_qry = exec_query_utf8("SELECT sum(payoutAmount) as totalPayout,sum(amount) as lossInvest FROM tblposition WHERE userid=$customerid AND expireDate LIKE '%$accdate%' AND result='L' AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalLostAmount=$row_position['totalPayout']-$row_position['lossInvest'];}
				
				$position_qry = exec_query_utf8("SELECT sum(amount) as totalInvest FROM tblposition WHERE userid=$customerid AND executionDate LIKE '%$accdate%' AND expireDate NOT LIKE '%$accdate%' AND expired=1 AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalInvest= - $row_position['totalInvest'];
				}				
				$position_qry = exec_query_utf8("SELECT sum(amount) as totalInvest FROM tblposition WHERE userid=$customerid AND executionDate NOT LIKE '%$accdate%' AND expireDate LIKE '%$accdate%' AND expired=1 AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalInvest += $row_position['totalInvest'];}
				
				$position_qry = exec_query_utf8("SELECT sum(payoutAmount) as totalPayout,sum(amount) as lossInvest FROM tblposition WHERE userid=$customerid AND executionDate LIKE '%$accdate%' AND expired=0 AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalProgressAmount=$row_position['totalPayout']-$row_position['lossInvest'];}
				//$totalInvest = - $totalInvest;
				$calcPNL = $totalWinAmount + $totalLostAmount + $totalInvest + $totalProgressAmount;if($calcPNL>0){$positivSign='+ ';}else{$positivSign='';}
				//get ByDate Real Bonus	- Cashable
				$totalByDateRealBonusC=0;
				$deposit_qry = exec_query_utf8("SELECT sum(remainingBonus) as totalByDateRealBonusC FROM tblbonus WHERE customerid=$customerid AND completedDate LIKE '%$accdate%' AND status=2");
				while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateRealBonusC = $row_deposit['totalByDateRealBonusC'];}
				if($totalByDateRealBonusC=='')$totalByDateRealBonusC=0;	
				//get ByDate Real Bonus	- NonCashable
				$totalByDateRealBonusNC=0;
				$deposit_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDateRealBonusNC FROM tbldeposit WHERE customerid=$customerid AND depositType=0 AND memo<>'credit' AND processedDate LIKE '%$accdate%' AND processingStatus=2");
				while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateRealBonusNC = $row_deposit['totalByDateRealBonusNC'];}
				if($totalByDateRealBonusNC=='')$totalByDateRealBonusNC=0;
				$totalByDateRealBonusNCremaining = $totalByDateRealBonusNC - $totalByDateRealBonusC;
				//get ByDate Credit Bonus		
				$totalByDateCreditBonus=0;
				$deposit_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDateCreditBonus FROM tbldeposit WHERE customerid=$customerid AND depositType=0 AND memo='credit' AND processedDate LIKE '%$accdate%' AND processingStatus=2");
				while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateCreditBonus = $row_deposit['totalByDateCreditBonus'];}
				if($totalByDateCreditBonus=='')$totalByDateCreditBonus=0;

				$dataRow=array($row_balance['user_id'],$customerName,cf($openingNetBalance).$currencySign,cf($openingGrossBalance).$currencySign,cf($totalByDateDeposit).$currencySign,$positivSign.cf($calcPNL).$currencySign,
				cf($totalByDateWithdraw).$currencySign,cf($totalByDateRealBonusC).$currencySign,cf($totalByDateRealBonusNCremaining).$currencySign,cf($totalByDateCreditBonus).$currencySign,cf($lastNetBalanceByDate).$currencySign,
				cf($lastGrossBalanceByDate).$currencySign);
				
				for($a=0;$a<=count($dataRow)-1;$a++){
					$objPHPExcel->getActiveSheet()->setCellValue($letters[$a].$startRowIndex, $dataRow[$a]);
					$objPHPExcel->getActiveSheet()->getStyle($letters[$a].$startRowIndex)->applyFromArray($borderStyle);
				}
				
				$sumOpeningNetBalance += $openingNetBalance/$exchangeRate;$sumOpeningGrossBalance+=$openingGrossBalance/$exchangeRate;$sumDeposit+=$totalByDateDeposit/$exchangeRate;$sumPNL+=$calcPNL/$exchangeRate;
				$sumWithdraw+=$totalByDateWithdraw/$exchangeRate;$sumBonusRC+=$totalByDateRealBonusC/$exchangeRate;$sumBonusRNC+=$totalByDateRealBonusNC/$exchangeRate;$sumBonusRNCremain+=$totalByDateRealBonusNCremaining/$exchangeRate;
				$sumBonusCredit+=$totalByDateCreditBonus/$exchangeRate;$sumNetBalance+=$lastNetBalanceByDate/$exchangeRate;$sumGrossBalance+=$lastGrossBalanceByDate/$exchangeRate;
				if($sumPNL>0){$positivSign='+ ';}else{$positivSign='';}
				$i+=1;
				$startRowIndex++;
			}
			
			$dataTotal=array('Total',$totalRecords,cf($sumOpeningNetBalance).$basedCurrency,cf($sumOpeningGrossBalance).$basedCurrency,cf($sumDeposit).$basedCurrency,$positivSign.cf($sumPNL).$basedCurrency,
			cf($sumWithdraw).$basedCurrency,cf($sumBonusRC).$basedCurrency,cf($sumBonusRNCremain).$basedCurrency,cf($sumBonusCredit).$basedCurrency,cf($sumNetBalance).$basedCurrency,
			cf($sumGrossBalance).$basedCurrency);
				
			for($a=0;$a<=count($dataTotal)-1;$a++){
				$objPHPExcel->getActiveSheet()->setCellValue($letters[$a].$startRowIndex, $dataTotal[$a]);
				$objPHPExcel->getActiveSheet()->getStyle($letters[$a].$startRowIndex)->applyFromArray($borderStyle);
			}
			$objPHPExcel->getActiveSheet()->getStyle("A".$startRowIndex.":".$letters[count($dataTotal)-1].$startRowIndex)->getFill()->applyFromArray($bgOrangeColor);	
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+2), 'Exported Date: '.$exportedDate.' (GTM 0)')->mergeCells('A'.($startRowIndex+2).':'.'L'.($startRowIndex+2));
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+3), 'Exported By: '.strtoupper(username))->mergeCells('A'.($startRowIndex+3).':'.'L'.($startRowIndex+3));
			
		}elseif($type==1){
			
			$balanceSQL = 'WHERE';
			$startDate = post("startDate");			
			$endDate = post("endDate");
			if(is_date($endDate) == false){
				$endDate=$currentDate;
			}
			
			if(is_date($startDate) == false){
				$startDate=gmdate("Y-m").'-01';
			}
			
			//get total number of days	
			 $datediff = strtotime($endDate) - strtotime($startDate);
			 $totalDay = floor($datediff/(60*60*24)) + 1;			 
			
			//Set infor for Report
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Report from: '.gmdate("d-M-Y",strtotime($startDate)).' to '.gmdate("d-M-Y",strtotime($endDate)).' (GTM 0)')->mergeCells('A2:M2'); 
			
			$columnName=array(
								array('Date','Opening Balance','Deposit','Profit/Loss','Withdrawal','Bonus','Balance','Company\'s PNL'),
								array('Net Balance','Gross Balance','Real','Credit','Net Balance','Gross Balance','Net PNL','Gross PNL'),
								array('Cashabel','Noncashable')
							);		
			$listTitle='Company Accounting Report';
			$sheetName='CompanyAccountingReport';
			$fileName='CompanyAccountingReport';	
			
			// merge cell
			$objPHPExcel->getActiveSheet()->setCellValue("A1",$listTitle)->mergeCells("A1:M1");
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(15)->getColor()->setRGB('6F6F6F');	
			
			//*** START list header
			//header row 1
			$objPHPExcel->getActiveSheet()->setCellValue("A4",$columnName[0][0])->mergeCells("A4:A6");
			$objPHPExcel->getActiveSheet()->setCellValue("B4",$columnName[0][1])->mergeCells("B4:C4");
			$objPHPExcel->getActiveSheet()->setCellValue("D4",$columnName[0][2])->mergeCells("D4:D6");
			$objPHPExcel->getActiveSheet()->setCellValue("E4",$columnName[0][3])->mergeCells("E4:E6");
			$objPHPExcel->getActiveSheet()->setCellValue("F4",$columnName[0][4])->mergeCells("F4:F6");
			$objPHPExcel->getActiveSheet()->setCellValue("G4",$columnName[0][5])->mergeCells("G4:I4");
			$objPHPExcel->getActiveSheet()->setCellValue("J4",$columnName[0][6])->mergeCells("J4:K4");
			$objPHPExcel->getActiveSheet()->setCellValue("L4",$columnName[0][7])->mergeCells("L4:M4");
			
			//header row 2
			$objPHPExcel->getActiveSheet()->setCellValue("B5",$columnName[1][0])->mergeCells("B5:B6");
			$objPHPExcel->getActiveSheet()->setCellValue("C5",$columnName[1][1])->mergeCells("C5:C6");
			$objPHPExcel->getActiveSheet()->setCellValue("G5",$columnName[1][2])->mergeCells("G5:H5");
			$objPHPExcel->getActiveSheet()->setCellValue("I5",$columnName[1][3])->mergeCells("I5:I6");
			$objPHPExcel->getActiveSheet()->setCellValue("J5",$columnName[1][4])->mergeCells("J5:J6");
			$objPHPExcel->getActiveSheet()->setCellValue("K5",$columnName[1][5])->mergeCells("K5:K6");
			$objPHPExcel->getActiveSheet()->setCellValue("L5",$columnName[1][6])->mergeCells("L5:L6");
			$objPHPExcel->getActiveSheet()->setCellValue("M5",$columnName[1][7])->mergeCells("M5:M6");
			
			//header row 3
			$objPHPExcel->getActiveSheet()->setCellValue("G6",$columnName[2][0]);
			$objPHPExcel->getActiveSheet()->setCellValue("H6",$columnName[2][1]);
			
			//$objPHPExcel->getActiveSheet()->getColumnDimension("C3")->setAutoSize(true); //now working
			$objPHPExcel->getActiveSheet()->getStyle("A4:M6")->applyFromArray($borderStyle);
			$objPHPExcel->getActiveSheet()->getStyle("A4:M6")->getFill()->applyFromArray($cellHeadColor);	
			$objPHPExcel->getActiveSheet()->getStyle("A1:M6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1:M6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//*** END list header
			
			
			//get all demo/not-deposit account
			$demoArray = array();
			$customer_qry = exec_query_utf8("SELECT * FROM tblcustomer WHERE accountType<>2 ORDER BY id asc");
			while($row_customer=mysqli_fetch_assoc($customer_qry)){$demoArray[]=$row_customer['id'];}			
			$demoList = implode(",", $demoArray);				
			//exclude this demoList from query
			if(count($demoArray)>0){
				if($balanceSQL == 'WHERE'){$balanceSQL.=' user_id NOT IN ('.$demoList.')';}else{$balanceSQL.=' AND user_id NOT IN ('.$demoList.')';}
			}else{if($balanceSQL == 'WHERE')$balanceSQL='';}
			
			$customerOpeningNetBalance=0;$customerOpeningGrossBalance=0;$customerDeposit=0;$customerPNL=0;$customerWithdraw=0;$customerBonusRC=0;$customerBonusRNC=0;$customerRNCremain=0;$customerBonusCredit=0;$customerNetBalance=0;$customerGrossBalance=0;
			$totalCompanyNetPNL=0;$totalCompanyGrossPNL=0;
			$positivSign='';
			for($j=1;$j<=$totalDay;$j++){
			if($j%2 == 0){$tbgColor='background:#ebebeb;';}else{$tbgColor='background:#f0f9fa;';}
				$datetime = new DateTime($startDate);
				$datetime->modify('+'.($j-1).' day');
				$eachDay = $datetime->format('Y-m-d');
				
				$balance_qry = exec_query_utf8("SELECT * FROM tblbalance $balanceSQL ORDER BY datetime desc"); // order by expiredDate
				$totalRecords = mysqli_num_rows($balance_qry);
				$i=1;
				//declare for sum amount of each item
				$sumOpeningNetBalance=0;$sumOpeningGrossBalance=0;$sumDeposit=0;$sumPNL=0;$sumWithdraw=0;$sumBonusRC=0;$sumBonusRNC=0;$sumBonusRNCremain=0;$sumBonusCredit=0;$sumNetBalance=0;$sumGrossBalance=0;				
				while($row_balance=mysqli_fetch_assoc($balance_qry)){
					
					$customerid = $row_balance['user_id'];
					//get customer info
					$customerName = '--';$currencyAbv='';$currencySign='';$exchangeRate=0;$currencyid=0;
					$customer_qry = exec_query_utf8("SELECT * FROM tblcustomer WHERE id=$customerid LIMIT 1");
					while($row_customer=mysqli_fetch_assoc($customer_qry)){$customerName=$row_customer['fullname'];$currencyAbv=$row_customer['currency'];}
					//get currency sign
					$currency_qry = exec_query_utf8("SELECT * FROM tblcurrency WHERE abv='$currencyAbv' LIMIT 1");//qry to get currency symbol
					while($row_currency = mysqli_fetch_assoc($currency_qry)){$currencySign = ' '.$row_currency['sign'];$exchangeRate=$row_currency['exchangeRate'];$currencyid=$row_currency['id'];}
					
					//get exchange rate by date
					$historyDate=$eachDay;
					$getLastRate_qry = exec_query_utf8("SELECT * FROM tblexchangeratehistory WHERE currencyid=$currencyid AND updatedDate='$historyDate' ORDER BY updatedDate DESC LIMIT 1");
					$lastrateCount = mysqli_num_rows($getLastRate_qry);
					if($lastrateCount==0){
						$getLastRate_qry = exec_query_utf8("SELECT * FROM tblexchangeratehistory WHERE currencyid=$currencyid AND updatedDate<'$historyDate' ORDER BY updatedDate DESC LIMIT 1");
						while($getLastRate_row=mysqli_fetch_array($getLastRate_qry)){$exchangeRate=$getLastRate_row['rate'];}				
					}else{
						while($getLastRate_row=mysqli_fetch_array($getLastRate_qry)){$exchangeRate = $getLastRate_row['rate'];}
					}
					
					//get balance by date
					$lastNetBalanceByDate=0;$lastGrossBalanceByDate=0;
					$openingBalance_qry = exec_query_utf8("SELECT * FROM tblbalancehistory WHERE user_id=$customerid AND datetime <= '$eachDay"." 23:59:59' ORDER BY datetime DESC, id DESC LIMIT 1");//qry to get currency symbol
					while($row_openingBalance = mysqli_fetch_assoc($openingBalance_qry)){
						$lastNetBalanceByDate = $row_openingBalance['netBalance'];$lastGrossBalanceByDate = $row_openingBalance['grossBalance'];
					}
					
					//get opening balance
					$lastClosedDate= gmdate("Y-m-d 00:00:00",strtotime($eachDay));$openingNetBalance=0;$openingGrossBalance=0;
					$openingBalance_qry = exec_query_utf8("SELECT * FROM tblbalancehistory WHERE user_id=$customerid AND datetime < '$lastClosedDate' ORDER BY datetime DESC, id DESC LIMIT 1");
					while($row_openingBalance = mysqli_fetch_assoc($openingBalance_qry)){
						$openingNetBalance = $row_openingBalance['netBalance'];$openingGrossBalance = $row_openingBalance['grossBalance'];
					}
					//------ get new balance update amount for ByDate
					//get ByDate deposit		
					$totalByDateDeposit=0;
					$deposit_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDateDeposit FROM tbldeposit WHERE customerid=$customerid AND depositType=1 AND processedDate LIKE '%$eachDay%' AND processingStatus=2");
					while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateDeposit = $row_deposit['totalByDateDeposit'];}
					if($totalByDateDeposit=='')$totalByDateDeposit=0;
					//get ByDate withdrawal
					$totalByDateWithdraw=0;
					$withdraw_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDatewithdraw FROM tblwithdrawal WHERE customerid=$customerid AND processedDate LIKE '%$eachDay%' AND processingStatus=2");
					while($row_withdraw = mysqli_fetch_assoc($withdraw_qry)){$totalByDateWithdraw = $row_withdraw['totalByDatewithdraw'];}
					if($totalByDateWithdraw=='')$totalByDateWithdraw=0;
					//get ByDate PNL
					$totalNetWinAmount=0;$totalNetLostAmount=0;$totalProgressAmount=0;$totalInvest=0;
					$position_qry = exec_query_utf8("SELECT sum(payoutAmount) as totalPayout,sum(amount) as winInvest FROM tblposition WHERE userid=$customerid AND expireDate LIKE '%$eachDay%' AND result='W' AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalNetWinAmount=$row_position['totalPayout']-$row_position['winInvest'];}
					$position_qry = exec_query_utf8("SELECT sum(payoutAmount) as totalPayout,sum(amount) as lossInvest FROM tblposition WHERE userid=$customerid AND expireDate LIKE '%$eachDay%' AND result='L' AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalNetLostAmount=$row_position['totalPayout']-$row_position['lossInvest'];}
					
					$position_qry = exec_query_utf8("SELECT sum(amount) as totalInvest FROM tblposition WHERE userid=$customerid AND executionDate LIKE '%$eachDay%' AND expireDate NOT LIKE '%$eachDay%' AND expired=1 AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalInvest= - $row_position['totalInvest'];
				}				
					$position_qry = exec_query_utf8("SELECT sum(amount) as totalInvest FROM tblposition WHERE userid=$customerid AND executionDate NOT LIKE '%$eachDay%' AND expireDate LIKE '%$eachDay%' AND expired=1 AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalInvest += $row_position['totalInvest'];}
					
					$position_qry = exec_query_utf8("SELECT sum(payoutAmount) as totalPayout,sum(amount) as lossInvest FROM tblposition WHERE userid=$customerid AND executionDate LIKE '%$eachDay%' AND expired=0 AND other <> 'Cancel'");while($row_position=mysqli_fetch_assoc($position_qry)){$totalProgressAmount=$row_position['totalPayout']-$row_position['lossInvest'];}
					$calcPNL = $totalNetWinAmount + $totalNetLostAmount + $totalInvest + $totalProgressAmount;if($calcPNL>0){$positivSign='+ ';}else{$positivSign='';}
					//get ByDate Real Bonus	- Cashable
					$totalByDateRealBonusC=0;
					$deposit_qry = exec_query_utf8("SELECT sum(remainingBonus) as totalByDateRealBonusC FROM tblbonus WHERE customerid=$customerid AND completedDate LIKE '%$eachDay%' AND status=2");
					while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateRealBonusC = $row_deposit['totalByDateRealBonusC'];}
					if($totalByDateRealBonusC=='')$totalByDateRealBonusC=0;	
					//get ByDate Real Bonus	- NonCashable
					$totalByDateRealBonusNC=0;
					$deposit_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDateRealBonusNC FROM tbldeposit WHERE customerid=$customerid AND depositType=0 AND memo<>'credit' AND processedDate LIKE '%$eachDay%' AND processingStatus=2");
					while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateRealBonusNC = $row_deposit['totalByDateRealBonusNC'];}
					if($totalByDateRealBonusNC=='')$totalByDateRealBonusNC=0;
					$totalByDateRealBonusNCremaining = $totalByDateRealBonusNC - $totalByDateRealBonusC;
					//get ByDate Credit Bonus		
					$totalByDateCreditBonus=0;
					$deposit_qry = exec_query_utf8("SELECT sum(requestedAmount) as totalByDateCreditBonus FROM tbldeposit WHERE customerid=$customerid AND depositType=0 AND memo='credit' AND processedDate LIKE '%$eachDay%' AND processingStatus=2");
					while($row_deposit = mysqli_fetch_assoc($deposit_qry)){$totalByDateCreditBonus = $row_deposit['totalByDateCreditBonus'];}
					if($totalByDateCreditBonus=='')$totalByDateCreditBonus=0;
					
					$sumOpeningNetBalance += $openingNetBalance/$exchangeRate;$sumOpeningGrossBalance+=$openingGrossBalance/$exchangeRate;$sumDeposit+=$totalByDateDeposit/$exchangeRate;$sumPNL+=$calcPNL/$exchangeRate;
					$sumWithdraw+=$totalByDateWithdraw/$exchangeRate;$sumBonusRC+=$totalByDateRealBonusC/$exchangeRate;$sumBonusRNC+=$totalByDateRealBonusNC/$exchangeRate;$sumBonusRNCremain+=$totalByDateRealBonusNCremaining/$exchangeRate;
					$sumBonusCredit+=$totalByDateCreditBonus/$exchangeRate;$sumNetBalance+=$lastNetBalanceByDate/$exchangeRate;$sumGrossBalance+=$lastGrossBalanceByDate/$exchangeRate;
					if($sumPNL>0){$positivSign='+ ';}else{$positivSign='';}
					$i+=1;
				}
				
				$companyNetPNL=-($sumPNL+$sumBonusRC);$companyGrossPNL=-($sumPNL+$sumBonusRC+$sumBonusRNC+$sumBonusCredit);
				if($companyNetPNL>0){$NetPositivSign='+ ';}else{$NetPositivSign='';}
				if($companyGrossPNL>0){$GrossPositivSign='+ ';}else{$GrossPositivSign='';}
					
				$dataRow=array(gmdate("d-M-Y",strtotime($eachDay)),cf($sumOpeningNetBalance).$basedCurrency,cf($sumOpeningGrossBalance).$basedCurrency,cf($sumDeposit).$basedCurrency,$positivSign.cf($sumPNL).$basedCurrency,
				cf($sumWithdraw).$basedCurrency,cf($sumBonusRC).$basedCurrency,cf($sumBonusRNCremain).$basedCurrency,cf($sumBonusCredit).$basedCurrency,cf($sumNetBalance).$basedCurrency,
				cf($sumGrossBalance).$basedCurrency,$NetPositivSign.cf($companyNetPNL).$basedCurrency,$GrossPositivSign.cf($companyGrossPNL).$basedCurrency);
					
				for($a=0;$a<=count($dataRow)-1;$a++){
					$objPHPExcel->getActiveSheet()->setCellValue($letters[$a].$startRowIndex, $dataRow[$a]);
					$objPHPExcel->getActiveSheet()->getStyle($letters[$a].$startRowIndex)->applyFromArray($borderStyle);
				}
				
				$customerOpeningNetBalance += $sumOpeningNetBalance;$customerOpeningGrossBalance+=$sumOpeningGrossBalance;$customerDeposit+=$sumDeposit;$customerPNL+=$sumPNL;$customerWithdraw+=$sumWithdraw;$customerBonusRC+=$sumBonusRC;$customerBonusRNC+=$sumBonusRNC;$customerRNCremain+=$sumBonusRNCremain;$customerBonusCredit+=$sumBonusCredit;$customerNetBalance+=$sumNetBalance;$customerGrossBalance+=$sumGrossBalance;
				$totalCompanyNetPNL+=$companyNetPNL;$totalCompanyGrossPNL+=$companyGrossPNL;
				
				$startRowIndex++;
				
			}
				if($customerPNL>0){$positivSign='+ ';}else{$positivSign='';}
				if($totalCompanyNetPNL>0){$comNetPositivSign='+ ';}else{$comNetPositivSign='';}
				if($totalCompanyGrossPNL>0){$comGrossPositivSign='+ ';}else{$comGrossPositivSign='';}


			$dataTotal=array($totalDay.' days',cf($customerOpeningNetBalance).$basedCurrency,cf($customerOpeningGrossBalance).$basedCurrency,cf($customerDeposit).$basedCurrency,$positivSign.cf($customerPNL).$basedCurrency,
			cf($customerWithdraw).$basedCurrency,cf($customerBonusRC).$basedCurrency,cf($customerRNCremain).$basedCurrency,cf($customerBonusCredit).$basedCurrency,cf($customerNetBalance).$basedCurrency,
			cf($customerGrossBalance).$basedCurrency,$comNetPositivSign.cf($totalCompanyNetPNL).$basedCurrency,$comGrossPositivSign.cf($totalCompanyGrossPNL).$basedCurrency);
					
			for($a=0;$a<=count($dataTotal)-1;$a++){
				$objPHPExcel->getActiveSheet()->setCellValue($letters[$a].$startRowIndex, $dataTotal[$a]);
				$objPHPExcel->getActiveSheet()->getStyle($letters[$a].$startRowIndex)->applyFromArray($borderStyle);
			}
			$objPHPExcel->getActiveSheet()->getStyle("A".$startRowIndex.":".$letters[count($dataTotal)-1].$startRowIndex)->getFill()->applyFromArray($bgOrangeColor);		
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+2), 'Exported Date: '.$exportedDate.' (GTM 0)')->mergeCells('A'.($startRowIndex+2).':'.'M'.($startRowIndex+2));
			$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+3), 'Exported By: '.strtoupper(username))->mergeCells('A'.($startRowIndex+3).':'.'M'.($startRowIndex+3));
		}else{
			echo 'Invalid Paramete!';exit;
		}
	
	
	
	
	$objPHPExcel->getActiveSheet()->setTitle($sheetName);
	$excelName = $fileName.".xlsx";
		
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($excelName);	
	//header("Location:". $excelName);
	echo 'phpExcel/'.$excelName;

		
