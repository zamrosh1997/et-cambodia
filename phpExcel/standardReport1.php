<?php	
	/** Error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	//date_default_timezone_set('Europe/London');	
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');	
	/** Include PHPExcel */
	require_once 'phpexcel/PHPExcel.php';
	include '../includes/connect_db.php';	
	
	//if(username==''){echo 'You do not have permission to use this feature!';exit;}
	
	$listName=$_POST["listName"];	
	$stringSQL=decodeString($_POST["sql"],$encryptKey);	
	$moreData=$_POST["moreData"];	
	$validRequest=false;
	
	if($listName=='guideList'){		
		$selectedFields='code,lastName,firstName,gender,mobile,registerDate';
		$qryString=str_replace('*',$selectedFields,$stringSQL);	
		//$tblHeader = $selectedFields;
		$columnName=explode(',',$selectedFields);
		$listTitle="បញ្ជីឈ្មោះមគ្គុទេសក៍ទេសចរណ៍";
		$sheetName="បញ្ជីឈ្មោះមគ្គុទេសក៍ទេសចរណ៍";
		$fileName=$listName;	
		$validRequest=true;
		$header_rowspan = 1;
	}elseif($listName=='candidateList'){
		$selectedFields='code,user_id,gender,dob';
		$qryString=str_replace('*',$selectedFields,$stringSQL);	
		//$tblHeader = 'License ID,ឈ្មោះ (ខ្មែរ),ឈ្មោះ (ឡាតាំង),ភេទ,ថ្ងៃកំណើត';
		//$columnName=explode(',',$tblHeader);	
		$columnName=array('licenseid'=>'License ID',
							'name_kh'=>'ឈ្មោះ (ខ្មែរ)',
							'name_en'=>'ឈ្មោះ (ឡាតាំង)',
							'gender'=>'ភេទ',
							'dob'=>'ថ្ងៃកំណើត');	
		$listTitle="បញ្ជីឈ្មោះតាមវគ្គសិក្សា";
		$sheetName="បញ្ជីឈ្មោះតាមវគ្គសិក្សា";
		$fileName=$listName;	
		$validRequest=true;
		$header_rowspan = 2;
		
		//------get more table headers
		//get subject by course
		$course_subject = json_decode(singleCell_qry("subject_id","tblcourseschedule","id=".$moreData['course_id']." LIMIT 1"),true);
		$subject_id_arr = array_keys($course_subject);
		$subject_id_str = implode(",",$subject_id_arr);
		
		//subject score
		$subjectData = array();$scoreCol=array('Paper','Online','Total','Grade');
		$subjectName = $subjectScore = '';
		foreach($subject_id_arr as $key => $value){
			$lessonInfo = qry_arr("title,lesson_code","tbllessons","id=$value LIMIT 1");			
			$columnName[$lessonInfo['lesson_code']]=$scoreCol;
		}
	}
	
	if(!$validRequest){echo 'Invalid Request!';exit;}
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	$startRowIndex =(1+$header_rowspan)+1;
	$exportedDate = date("d-M-Y H:i:s A");
	$letters = range('A', 'Z');
	$getSQLCondition = getStringBetween($stringSQL,'where','order');
	$borderStyle = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
	$cellHeadColor=array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'startcolor'=>array('rgb'=>'d4d4d4'));
	$bgRedColor=array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'startcolor'=>array('rgb'=>'fe96a6'));
	
	// merge cell
	$objPHPExcel->getActiveSheet()->setCellValue("A1",$listTitle)->mergeCells("A1:".$letters[count($columnName)]."1");	//not use count()-1 bcoz we add No. column which is not in fieldlist			
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(15)->getColor()->setRGB('6F6F6F');	
	$objPHPExcel->getActiveSheet()->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'No.');	
	$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($borderStyle);
	$objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->applyFromArray($cellHeadColor);	
	$i=0;$sub_head_cell = array();$next_col_index = 1;
	foreach($columnName as $key=>$value){						
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle($letters[$i+$next_col_index].'2')->applyFromArray($borderStyle);
		$objPHPExcel->getActiveSheet()->getStyle($letters[$i+$next_col_index].'2')->getFill()->applyFromArray($cellHeadColor);	
		$objPHPExcel->getActiveSheet()->getStyle($letters[$i+$next_col_index].'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
		if($header_rowspan>1){
			if(is_array($value)){
				$objPHPExcel->getActiveSheet()->setCellValue($letters[$i+$next_col_index].'2', $key);					
				if(count($value)>1){					
					$objPHPExcel->getActiveSheet()->mergeCells($letters[$i+$next_col_index].'2:'.$letters[$i+count($value)].'2');
					$next_col_index = $next_col_index + (count($value)-1);
					$k=0;
					foreach($value as $sub_key=>$sub_value){
						$sub_head_cell[] = $letters[$i+$next_col_index+$k].'3';
						$k++;
					}	
				}
			}else{
				$objPHPExcel->getActiveSheet()->setCellValue($letters[$i+$next_col_index].'2', $value);			
				$objPHPExcel->getActiveSheet()->mergeCells($letters[$i+$next_col_index].'2:'.$letters[$i+1].(1+$header_rowspan));
			}
					
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue($letters[$i+$next_col_index].'2', $value);
		} 
		$i++;
	}
	if($header_rowspan>1){
		for($m=2;$m<=$header_rowspan;$m++){
			foreach($columnName as $key=>$value){
				if(is_array($value)){
					foreach($value as $sub_key=>$sub_value){
						$objPHPExcel->getActiveSheet()->setCellValue($letters[$i+1].'2', $sub_value);
						$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
						$objPHPExcel->getActiveSheet()->getStyle($letters[$i+1].'2')->applyFromArray($borderStyle);
						$objPHPExcel->getActiveSheet()->getStyle($letters[$i+1].'2')->getFill()->applyFromArray($cellHeadColor);	
						$objPHPExcel->getActiveSheet()->getStyle($letters[$i+1].'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}
				$i++;
			}
		}
	}
	
	
	$result = exec_query_utf8($stringSQL);
	$autoNum=1;
	if($listName=='guideList'){
		while($result_row = mysqli_fetch_assoc($result)){	
			$rowdata = array($result_row['code'],
							$result_row['lastName'],$result_row['firstName'],
							$result_row['gender']=='f'?'ស្រី':'ប្រុស',
							'(855) '.$result_row['mobile'],
							date('d/m/Y',strtotime($result_row['registerDate'])));
										
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRowIndex, $autoNum);		
			for($i=0;$i<=count($columnName)-1;$i++){
				$newData = $rowdata[$i];
				$objPHPExcel->getActiveSheet()->setCellValue($letters[$i+1].$startRowIndex, strip_tags($newData));		
			}		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$startRowIndex.':'.$letters[count($columnName)].$startRowIndex)->applyFromArray($borderStyle);//not use count()-1 bcoz we add No. column which is not in fieldlist				
			$startRowIndex++;$autoNum++;
		}
	}elseif($listName=='candidateList'){
		while($result_row = mysqli_fetch_assoc($result)){	
			$rowid = $result_row['id'];
			$candidate_data = qry_arr("code,firstName,lastName,firstNameEn,lastNameEn,gender,dob,active","tblusers","id='".$result_row['user_id']."' LIMIT 1");
			$fullname = $candidate_data['lastName'].' '.$candidate_data['firstName'];
			$fullnameEn = $candidate_data['lastNameEn'].' '.$candidate_data['firstNameEn'];
		
			$rowdata = array($candidate_data['code'],
							$fullname,
							$fullnameEn,
							($candidate_data['gender']=='f'?'ស្រី':'ប្រុស'),
							$candidate_data['dob']==''?'N/A':date('d/m/Y',strtotime($candidate_data['dob'])));
										
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$startRowIndex, $autoNum);		
			for($i=0;$i<=count($columnName)-1;$i++){
				$newData = $rowdata[$i];
				$objPHPExcel->getActiveSheet()->setCellValue($letters[$i+1].$startRowIndex, strip_tags($newData));		
			}		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$startRowIndex.':'.$letters[count($columnName)].$startRowIndex)->applyFromArray($borderStyle);//not use count()-1 bcoz we add No. column which is not in fieldlist				
			$startRowIndex++;$autoNum++;
		}
	}
		
	//$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+1), 'Queried By: '.$getSQLCondition)->mergeCells('A'.($startRowIndex+1).':'.$letters[count($columnName)-1].($startRowIndex+1));
	$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+1), 'កាលបរិច្ឆេទបោះពុម្ភ៖ '.$exportedDate.' (UTC +07:00 ភ្នំពេញ)')->mergeCells('A'.($startRowIndex+1).':'.$letters[count($columnName)].($startRowIndex+1));
	//$objPHPExcel->getActiveSheet()->setCellValue('A'.($startRowIndex+3), 'Exported By: '.strtoupper(username))->mergeCells('A'.($startRowIndex+3).':'.$letters[count($columnName)-1].($startRowIndex+3));
	  
	$objPHPExcel->getActiveSheet()->setTitle($sheetName);
	$excelName = $fileName.".xlsx";
		
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($excelName);	
	//header("Location:". $excelName);
	echo '/phpExcel/'.$excelName;

		
