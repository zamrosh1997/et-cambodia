<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

if(isset($_SESSION['userid'])){header("location: /");}

$pageName='Reset Password | Tourist Guide Refreshment Course';
$pageCode='resetPassword';
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>

<?php 

$requestNewPass = false;
$expiredPeriod = 10*60;

if(isset($_GET['dt'])){
	if($_GET['dt']<>''){
		$data = unserialize(decodeString($_GET['dt'],$encryptKey));
		extract($data); //code,time
		if((time()-$time)<=$expiredPeriod and time()>$time){
			$checkUser = exec_query_utf8("SELECT * FROM tblusers WHERE id=$guideid AND pending=0 AND active=1 LIMIT 1");			
			if(mysqli_num_rows($checkUser)>0){	
				$userData = mysqli_fetch_array($checkUser,MYSQL_ASSOC);
				$to = $userData['email'];$toName = $userData['lastName'].''.$userData['firstName'];
			
				$rawPass = strtolower(rndStr(8));
				$encryptedpass = encodeString($rawPass,$encryptKey);
				$newPass = exec_query_utf8("UPDATE tblusers SET loginPassword='$encryptedpass' WHERE id=$guideid AND pending=0 AND active=1 LIMIT 1");
				if($newPass){
					
/*------ Start Sending to Email----------*/
			$self_resetLink = 'http://et.cambodia-touristguide.com/account';
			$formatDate = date("d M Y H:i A");
			
			$subject = "New Password | Cambodia Tourist Guide";
			$message = '
			
			<div>
				<div style="float:left;"><img src="http://'.$_SERVER['HTTP_HOST'].'/img/logo_xs.png" /></div>
				<div style="float:right; text-align:right;">'.$formatDate.'<br />ប្រព័ន្ធបណ្តុះបណ្តាលវិក្រិត្យការមគ្គុទេ្ទសក៍ទេសចរណ៍</div>
			</div>
			<div style="clear:both; padding-top:20px;"></div>
			
			សួស្តី '.$toName.'
			<p>
			អ្នកបានស្នើរអោយផ្លាស់ប្តួរពាក្យសម្ងាត់របស់អ្នក។<br />
			ខាងក្រោមនេះគឺជាពាក្យសម្ងាត់ថ្មីរបស់អ្នក បង្កើតដោយដោយប្រព័ន្ធស្វ័យប្រវត្តិ៖
			<br /><br />
			ពាក្យសម្ងាត់ថ្មី៖ <strong>'.$rawPass.'</strong>
			<br /><br />
			
			<strong>សំខាន់៖ សូមចូលគណនីរបស់អ្នក នៃប្រព័ន្ធបណ្តុះបណ្តាលវិក្រិត្យការមគ្គុទេ្ទសក៍ទេសចរណ៍ រួចផ្លាស់ប្តូរពាក្យសម្ងាត់ថ្មីដោយខ្លួនឯងម្តងទៀត។</strong><br />
			ដើម្បីផ្លាស់ប្តូរពាក្យសម្ងាត់ដោយខ្លួនឯង សូមចូលតាម Link ខាងក្រោម៖<br /><br />
			
			<a href="'.$self_resetLink.'">'.$self_resetLink.'</a>
			
			<br /><br />
			<span style="font-size:11px;">
				ផ្ញើរដោយ៖ ប្រព័ន្ធបណ្តុះបណ្តាលវិក្រិត្យការមគ្គុទេ្ទសក៍ទេសចរណ៍<br />
				វ៉េបសាយ៖ <a href="http://et.cambodia-touristguide.com">http://et.ambodia-touristguide.com</a>
			</span>
			</p>
				
			<hr />
			<div style="font-size:11px;color:#888888;">
			សូមកុំឆ្លើយតបនឹងអ៊ីម៉េលនេះ។ ប្រអប់សារនេះមិនត្រូវបានត្រួតពិនិត្យទេ ហើយអ្នកនឹងមិនទទួលបានការឆ្លើយតបវិញឡើយ។ សម្រាប់ព័ត៌មានផ្សេងៗ សូមទាក់ទងមកកាន់ <div style="display:inline-block;">Emai: refreshment@cambodia-touristguide.com</div> | <div style="display:inline-block;">ទូរស័ព្ទ: (855) 12 999 032</div> ។
			</div>
			'; 
											
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$headers .= 'From: Cambodia Tourist Guide <noreply@cambodia-touristguide.com> \r\n';
			
			$sent = mail($to, $subject, $message, $headers) ;
			
			if($sent){
				$result='success';$msg=$to;
			}else{
				$result='failed';$msg='mail failed';
			}
/*------ End Sending to Email----------*/
					
					
					$requestNewPass = true;
				}				
			}			
		}
	}
}

?>

  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-bolt fa-fw"></i> ផ្លាស់ប្តូរពាក្យសម្ងាត់</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                    <!-- Title -->                              
                                 	<div class="form" id="resetPassword_div">
                                    <?php 
										if($requestNewPass){
									?>
                                       <div class="green_gradient">
                                            <div><img src="/admin/images/notice-success.png" /> ពាក្យសម្ងាត់ថ្មីត្រូវផ្ញើរទៅកាន់អ៊ីម៉េលរបស់អ្នក។<br />សូមពិនិត្យប្រអប់ Inbox និង Spam នៃអ៊ីម៉េលរបស់អ្នក។ អរគុណ!</div>
                                            <div style="font-size:50px; padding-top:20px;"><i class="fa fa-envelope fa-fw"></i></div>
                                       </div>     
                                    <?php
										}else{
									?>
                                      <form id="resetPassword_frm" role="form">
                                         <div class="form-group">
                                           <label for="username" class="control-label">អ៊ីម៉េល ឬ License ID ឬទូរស័ព្ទ</label>
                                           <input type="text" class="form-control" id="getusername" placeholder="អ៊ីម៉េល ឬ License ID ឬទូរស័ព្ទ" required>
                                         </div>
                                         <div class="form-group">
                                             <button type="submit" id="resetPassword_btn" class="btn btn-default"><i class="fa fa-paper-plane fa-fw"></i> បញ្ជូន</button>
                                         </div>                                                                                  
                                       </form>   
                                       <div id="resetPassword_msg" class=""></div>  
                                    <?php
										}
									?>                                        
                                    </div> 
                </div>

         </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
    
	$("#resetPassword_frm").on('submit',(function(e) {resetPassword();e.preventDefault();}));	
	
	window.history.replaceState("", "", "/resetPassword");
	
});

</script>

</body>
</html>