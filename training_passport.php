<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");

session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false));
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// ========= End Session ============ //

if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

$pageName='Passport វិជ្ជាជីៈមគ្គុទេ្ទសក៍ទេសចរណ៍ | Tourist Guide Refreshment Course';
$pageCode='passport';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h3><i class="fa fa-tasks fa-fw"></i> Passport វិជ្ជាជីៈមគ្គុទេ្ទសក៍ទេសចរណ៍</h3>
        </div>
        
        <!--<div style="float:right; width:300px;"></div>-->
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                    <h4 class="khmerTitle">១. ការអប់រំ និងបណ្តុះបណ្តាល</h4>
                    				<table class="table table-striped table-bordered table-hover" id="education_tbl">
                                        <thead>
                                            <tr>
                                                <th class="tableCellCenter" colspan="2">កាលបរិច្ឆេទ</th>
                                                <th class="tableCellCenter" rowspan="2">រយៈពេល</th>
                                                <th class="tableCellCenter" rowspan="2">ឈ្មោះវិញ្ញបនបត្រ</th>
                                                <th class="tableCellCenter" rowspan="2">មុខវិជ្ជា/ជំនាញវិជ្ជាជីវៈ</th>
                                                <th class="tableCellCenter" rowspan="2">ស្ថាប័ន/អង្គភាពបណ្តុះបណ្តាល</th>
                                            </tr>
                                            <tr>
                                                <th class="tableCellCenter">ចាប់ផ្តើម</th>
                                                <th class="tableCellCenter">បញ្ចប់</th>
                                            </tr>
                                        </thead>
                                        <tbody>  
                                        	<tr>
                                                <td colspan="6" class="tableCellCenter"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យ</td>
                                            </tr>                                  
                                        </tbody>
                                    </table>
                      
                      <h4 class="khmerTitle">២. បទពិសោធន៍វិជ្ជាជីវៈ</h4>
                    				<table class="table table-striped table-bordered table-hover" id="experience_tbl">
                                        <thead>
                                            <tr>
                                                <th class="tableCellCenter" colspan="2">កាលបរិច្ឆេទ</th>
                                                <th class="tableCellCenter" rowspan="2">ឈ្មោះនិយោជក</th>
                                                <th class="tableCellCenter" rowspan="2">តួនាទី</th>
                                                <th class="tableCellCenter" rowspan="2">ការងារទទួលខុសត្រូវ</th>
                                            </tr>
                                            <tr>
                                                <th class="tableCellCenter">ចាប់ផ្តើម</th>
                                                <th class="tableCellCenter">បញ្ចប់</th>
                                            </tr>
                                        </thead>
                                        <tbody>  
                                        	<tr>
                                                <td colspan="5" class="tableCellCenter"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យ</td>
                                            </tr>                                  
                                        </tbody>
                                    </table>      
                 	
                 </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->

<!-- Newsletter starts -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						//$("#m_lessons").addClass('active');
						//--- end set active menu	
						
                    });
                </script>

</body>
</html>