<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$pageName='Account Activation | Tourist Guide Refreshment Course';
$pageCode='account_activation';

if(isset($_SESSION['userid'])){header("location: /");}

$getuserid = decodeString($_GET['id'],$encryptKey);
$validlink = false;
if(is_numeric($getuserid)){
	$userData = qry_arr("pending","tblusers","id=$getuserid LIMIT 1");
	$validlink = true;
}

?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-archive fa-fw"></i> ការចុះឈ្មោះជោគជ័យ</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
        	<div class="well">
            	<?php if($validlink){ ?>
        		<div class="alert alert-success" style="text-align:center;">
                	<div style="font-size:50px; color:#45c8f2">
                    	<i class="fa fa-check"></i>
                    </div>
                    <div>
                    	<?php						
							if($userData['pending']==1){
								exec_query_utf8("UPDATE tblusers SET pending=0 WHERE id=$getuserid LIMIT 1");
								echo 'គណនីរបស់ត្រូវបានដាក់អោយដំណើរដោយជោគជ័យ!';
							}else{
								echo 'គណនីរបស់អ្នកត្រូវបានដាក់អោយដំណើរការរួចហើយ!';
							}
							
							$loginLink = 'http://et.cambodia-touristguide.com/login';							
							echo '<br />សូមចុចប៊ូតុងខាងក្រោម ដើម្បីចូលប្រើប្រាស់ប្រព័ន្ទ៖<br /><br />
							
							<a href="'.$loginLink.'"><img src="/img/login-btn.png" /></a>		
							
							'.$transactionInfo.'							
							
							';
						?>
                    </div>                
                </div>
                <?php }else{?>
                <div class="alert alert-danger" style="text-align:center;">
                	<div style="font-size:50px; color:#A22729;">
                    	<i class="fa fa-exclamation-triangle"></i>
                    </div>
                    <div>
                    	ព័ត៌មានមិនត្រឹមត្រូវ!
                    </div>                
                </div>                
                <?php }?>
             </div>
         </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->

<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>
</body>
</html>