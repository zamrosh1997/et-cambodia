<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");

session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false)); 
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// =============== End Session ============ //

if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

$pageName='បញ្ជីឈ្មោះប្រលង | Tourist Guide Refreshment Course';
$pageCode='candidateList';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h3><i class="fa fa-file-text-o fa-fw"></i> បញ្ជីឈ្មោះ</h3>
        </div>
        
        <div style="float:right; width:300px;">
        	<form role="form" id="candidateList_search_frm">
        		<div class="input-group custom-search-form">
					<input type="text" id="candidateList_search_txt" class="form-control" placeholder="ឈ្មោះ ឬ License ID">
					<span class="input-group-btn">
						<button class="btn btn-default" id="candidateList_search_btn" type="button">
						<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
           </form>
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
        		 <div class="well">
                 					<div class="form-group" style="display:inline-block;">
                                        <label>ប្រចាំឆ្នាំ</label>
                                        <select class="form-control input-sm" id="course_year">
                                               <option value="0" data-expired="0">--- ជ្រើសរើស ---</option>
                                         <?php												
												$year_qry = exec_query_utf8("SELECT year FROM tblcourseschedule WHERE id IN (SELECT course_id FROM tblcourseregister WHERE user_id='".$userid."' AND active=1) AND active=1 GROUP BY year ORDER BY year ASC");
												while($year_row = mysqli_fetch_assoc($year_qry)){
													echo '<option value="'.$year_row['year'].'" data-expired="1">'.$year_row['year'].'</option>';
												}	
                                         ?>  
                                         </select>
                                    </div> 
                 					<div class="form-group" style="display:inline-block;">
                                        <label>វគ្គសិក្សាដែលបានចូមរួម</label>
                                        <select class="form-control input-sm" id="course_num">
                                               <option value="0">--- ជ្រើសរើស ---</option>
                                         </select>
                                    </div> 
                 
                 
                 </div>
                 <div class="form well">
                 	<div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                	<option value="5">៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                    <div style="clear:both; padding:2px 0;"></div>
                    				<table class="table table-striped table-bordered table-hover" id="candidateList_tbl">
                                        <thead>
                                            <tr>
                                                <th class="tableCellCenter" rowspan="2">ល.រ</th>
                                                <th class="tableCellCenter" rowspan="2">License ID</th>
                                                <th class="tableCellCenter" colspan="2">គោត្តនាម និងនាម</th>
                                                <th class="tableCellCenter" rowspan="2">ភេទ</th>
                                                <th class="tableCellCenter" rowspan="2">ថ្ងៃខែឆ្នាំកំណើត</th>
                                            </tr>
                                            <tr>
                                                <th class="tableCellCenter">ខ្មែរ</th>
                                                <th class="tableCellCenter">ឡាតាំង</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                
                 	
                 </div>
                 
                 <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                  </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->

<!-- Newsletter starts -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						//$("#m_lessons").addClass('active');
						//--- end set active menu	
														
						$("#candidateList_search_btn").click(function(){candidateList('');});	
						$("#candidateList_search_frm").submit(function(e){candidateList(''); e.preventDefault();});		
						
						$("#course_year").change(courseSelectList);
						$("#course_num").change(function(){candidateList('');});
						//--- start navigation btn
						$("#nav_first").click(function(e){candidateList('first');});
						$("#nav_prev").click(function(e){candidateList('prev');});
						$("#nav_next").click(function(e){candidateList('next');});
						$("#nav_last").click(function(e){candidateList('last');});
						$("#nav_rowsPerPage").change(function(e){candidateList('');});
						$("#nav_currentPage").change(function(e){candidateList('goto');});
						//--- end navigation btn						
						
						candidateList('');						
                    });
                </script>

</body>
</html>