<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

$pageName='Exam List | Tourist Guide Refreshment Course';
$pageCode='exam list';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h3><i class="fa fa-th-list fa-fw"></i> បញ្ជីនៃការប្រលង</h3>
        </div>
        
        <div style="float:right; width:300px;">
        	<form role="form" id="examList_search_frm">
        		<div class="input-group custom-search-form">
					<input type="text" id="examList_search_txt" class="form-control" placeholder="ការប្រលង">
					<span class="input-group-btn">
						<button class="btn btn-default" id="examList_search_btn" type="button">
						<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
           </form>
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                 	<div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                	<option value="5">៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                    <div style="clear:both; padding:2px 0;"></div>
                    
                    				<table class="table table-striped table-bordered table-hover" id="examList_tbl">
                                        <thead>
                                            <tr>
                                                <th>ការប្រលង</th>
                                                <th class="tableCellCenter" style="width:120px;">កាលបរិច្ឆេទប្រលង</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                
                 	
                 </div>
                 
                 <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                  </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->

<!-- Newsletter starts -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						//$("#m_lessons").addClass('active');
						//--- end set active menu	
														
						$("#examList_search_btn").click(function(){examList('');});	
						$("#examList_search_frm").submit(function(e){examList(''); e.preventDefault();});						
						//--- start navigation btn
						$("#nav_first").click(function(e){examList('first');});
						$("#nav_prev").click(function(e){examList('prev');});
						$("#nav_next").click(function(e){examList('next');});
						$("#nav_last").click(function(e){examList('last');});
						$("#nav_rowsPerPage").change(function(e){examList('');});
						$("#nav_currentPage").change(function(e){examList('goto');});
						//--- end navigation btn						
						
						examList('');						
                    });
                </script>

</body>
</html>