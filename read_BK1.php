<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$lessonid = decodeString(get('id'),$encryptKey);

$docPath = '/documents/';$downloadPath = '/download/'; 
$nodocFilename = 'nodoc.pdf';
$docFilename = array();
$nodocUrl = $docPath.$nodocFilename;
$lesson_title = '';$docUrl='';$description='';
$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id=$lessonid AND active=1 LIMIT 1");
while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
	$lesson_title = $lesson_row['title'];
	if($lesson_row['filename']<>''){$docFilename = explode("|",$lesson_row['filename']);$description=$lesson_row['description'];}
	//$docUrl= $docPath .$docFilename;
}

$pageName=$lesson_title.' | Tourist Guide Refreshment Course';
$pageCode='read';

// if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}
if(mysqli_num_rows($lesson_qry)==0){header("location:/lessons");}

// $userid = $_SESSION['userid'];
$datetime = date("Y-m-d H:i:s");

//set data to readlist
exec_query_utf8("INSERT INTO tblreadlist SET userid=$userid,lessonid=$lessonid,datetime='$datetime'");
//add views
exec_query_utf8("UPDATE tbllessons SET views=views+1 WHERE id=$lessonid AND active=1 LIMIT 1");
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">            
            	<div>
                 	<div style="float:left;">
                    	<!--<?php if($docFilename<>''){ ?>
                    	<a href="<?php echo $docUrl; ?>"><div class="btn btn-default btn-info"><i class="fa fa-download fa-fw"></i> ទាញឯកសារ</div></a>
                        <?php }else{echo '<span style="color:red;">គ្មានឯកសារ</span>';} ?>-->
                        <h4><i class="fa fa-book fa-fw"></i> <?php echo $lesson_title; ?></h4>
                    </div>
                    <div style="float:right;">
                    	<a href="/lessons/"><div class="btn btn-default btn-info"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីមេរៀន</div></a>
                        
                    	<a href="/test/<?php echo encodeString($lessonid,$encryptKey); ?>"><div class="btn btn-default btn-info"><i class="fa fa-hand-o-right fa-fw"></i> ធ្វើតេសត៏</div></a>
                    </div>
                    <div class="clearfix"></div>
                 </div>            
      </div>
    </div>
  </div>
</div>

<!-- Page Heading ends -->

<!-- CTA Starts -->

<div class="container">
    <div class="row">
      <?php 
      include("includes/sidebar.php");
      $attachedFile = array_reverse($docFilename); 
      ?>
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="widget">
        
        			<div class="formy well">
                    	<?=$description?>
                      <!-- pdf file view -->
                      <?php 
                      		if(count($docFilename)==0){
                      			echo '<div><i class="fa fa-file-text-o fa-fw"></i> គ្មានឯកសារភ្ជាប់</div>';
							}else{
							
								foreach($attachedFile as $value){

							
								echo '<iframe src="https://docs.google.com/gview?url=http://et.cambodia-touristguide.com/documents/'.$value.'&embedded=true" style="width:600px; height:700px;" frameborder="0"></iframe>';						
								}	
							}
                      ?>
						 
                        <?php if($description<>''){echo '<hr />';} ?>
                        <h4>ឯកសារភ្ជាប់៖</h4>
                        
                        <?php
						if(count($docFilename)==0){
							echo '<div><i class="fa fa-file-text-o fa-fw"></i> គ្មានឯកសារភ្ជាប់</div>';
						}else{
							/*$docFilename = array_reverse($docFilename);
							foreach($docFilename as $value){
								$fileurl = $downloadPath.encodeString($value,$encryptKey);
								
								echo '<div>'.file_icon($value).' <a href="'.$fileurl.'" target="_blank">'.$value.'</a> ('.filesize_formatted('documents/'.$value).')</div>';
							}*/
							
							$attchment_visible = false;
							foreach($attachedFile as $value){
								$fileurl = $downloadPath.encodeString($value,$encryptKey);
								$file_arr=explode(".", $value);
								$file_extension = strtolower(end($file_arr));
								$img_ext = array('jpg','jpeg','png','gif');
								$displayName = substr($value, 0, strrpos($value, '_'));
								if(strpos($description,$value) === false or !in_array($file_extension,$img_ext)){
									echo '<div>'.file_icon($value).' <a href="'.$fileurl.'" target="_blank">'.$displayName.'</a> ('.filesize_formatted('/documents/'.$value).')</div>';
									$attchment_visible = true;
								}								
							}
							if(!$attchment_visible){
								echo '<div><i class="fa fa-file-text-o fa-fw"></i> គ្មានឯកសារភ្ជាប់</div>';
							}
						}
						
						?>
                    
                    <!--<object data="<?php echo $docUrl==$docPath?$nodocUrl:$docUrl; ?>" type="application/pdf" width="100%" height="800">
                    Download : <?php if($docUrl==$docPath){echo 'គ្មានឯកសារ';}else{ ?><a href="<?php echo $docUrl; ?>">test.pdf</a><?php } ?>
                    </object>-->
                    </div>
        </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
    $(".module_title_steps:first-child").addClass('step_focus');
	$("#"+ $(".module_title_steps:first-child").data('target')).show();
	
	$(".module_title_steps").click(function(){	
		$(".module_text").hide();
		$("#"+$(this).data('target')).show();	
		$(".module_title_steps").removeClass('step_focus');
		$(this).addClass('step_focus');	
	});
	
	$("#btn_next").click(function(){	
		var cTarget = $(".step_focus").data('target');
		var data_parts = cTarget.split('_');
		var btn_module = $("#btn_module_"+((parseInt(data_parts[2]))+1));
		var next_btn_module = $("#btn_module_"+((parseInt(data_parts[2]))+2));
		if(next_btn_module.length==0){$("#btn_next").html('ចប់មេរៀន <i class="fa fa-check-square-o fa-fw"></i>');}	
		if(btn_module.length==1){btn_module.trigger('click');}			
	});
});
</script>
</body>
</html>