<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

$rowid = decodeString(get('id'),$encryptKey);
if(!is_numeric($rowid)){header("location: /");}
$guideData = qry_arr("id,fname_kh,lname_kh,fname_en,lname_en,gender,language_id,exam_id,score_avg,score_grade","ng_profile","id=$rowid LIMIT 1");
if(!isset($guideData['id'])){header("location: /");}
$fullname_kh = $guideData['lname_kh'].' '.$guideData['fname_kh'];
$fullname_en = $guideData['lname_en'].' '.$guideData['fname_en'];
$gender = $guideData['gender']=='m'?'ប្រុស':'ស្រី';
$language = singleCell_qry("name_kh","tbllanguages","id=".$guideData['language_id']." LIMIT 1");
$examData = qry_arr("exam_date,school_id,region_id","ng_exam_result","id=".$guideData['exam_id']." LIMIT 1");
$schoolname = singleCell_qry("name_kh","ng_school","id=".$examData['school_id']." LIMIT 1");
$regionname = singleCell_qry("name_kh","ng_region","id=".$examData['region_id']." LIMIT 1");

$pageName='Tourist Guide Training';
$pageCode='profile_training';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>

  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h4>ព័ត៌មានមគ្គុទ្ទេសក៍ទេសចរណ៍</h4>           
        </div>
        
        <div style="float:right;">
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">      
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <!--<div class="row">
                 	<div class="col-xs-12">
                 		<h4><i class="fa fa-file-text-o fa-fw"></i> Profile</h4>
                    </div>
                    <div class="col-xs-9 hidden" style="text-align:right;">                    
                    	<button id="btn_print" type="button" title="បោះពុម្ភទៅ Printer" class="btn btn-sm btn-default btn-primary margin_t_b textAlignLeft"><i class="fa fa-print fa-fw"></i></button>  
                    </div>
                 </div>-->
                 <div class="form well" id="print_block">
                    <div>
                    	<div class="info_row">
                    		ឈ្នោះ (ខ្មែរ): <strong><?=$fullname_kh?></strong>
                        </div>
                        <div class="info_row">
                 			ឈ្នោះ (ឡាតាំង): <strong><?=ucwords(strtolower($fullname_en))?></strong>
                        </div>
                        <div class="info_row">
                        	ភេទ: <strong><?=$gender?></strong>
                        </div>
                        <div class="info_row">
                       	 	ភាសា: <strong><?=$language?></strong>
                        </div>
                        <div class="info_row">
                       		តំបន់: <strong><?=$regionname?></strong>
                        </div>
                        <div class="info_row">
                       		 ពិន្ទុ: <strong><?=$guideData['score_avg']?></strong>
                        </div>
                        <div class="info_row">
                       		និទ្ទេស: <strong><?=$guideData['score_grade']?></strong>
                        </div>
                        <div class="info_row">
                        	ប្រកាស: <strong><?=khmerDate($examData['exam_date'])?></strong>
                        </div>
                        <div class="info_row">
                       		សាលា: <strong><?=$schoolname?></strong>
                        </div>
                    </div>
                 </div>
        </div>
     </div>
      <?php include("includes/sidebar.php"); ?>
   </div>
</div>

<!-- CTA Ends -->




<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>

$(function(){
$("#btn_print_excel").click(function(){exportData("<?=$annTypeName?>","<?=encodeString($mainQry,$encryptKey)?>");});
$("#btn_print").click(function(){PrintElem("#print_block","<?=$pageName?>");});	
});

</script>

</body>
</html>