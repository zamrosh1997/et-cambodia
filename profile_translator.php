<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

$rowid = decodeString(get('id'),$encryptKey);
if(!is_numeric($rowid)){header("location: /");}
$translatorData = qry_arr("id,course_id,fname_kh,lname_kh,fname_en,lname_en,gender,nationality_id,dob,passport,mobile,photo","translator_profile","id=$rowid LIMIT 1");
if(!isset($translatorData['id'])){header("location: /");}
$fullname_kh = $translatorData['lname_kh'].' '.$translatorData['fname_kh'];
$fullname_en = $translatorData['lname_en'].' '.$translatorData['fname_en'];
$gender = $translatorData['gender']=='m'?'ប្រុស':'ស្រី';
$nationality = singleCell_qry("nationality","translator_nationality","id=".$translatorData['nationality_id']." LIMIT 1");

//course info
$courseinfo = qry_arr("school_id,course_title,start_date,end_date","training_course","id=".$translatorData['course_id']." limit 1");
$schoolname = singleCell_qry("name_en","ng_school","id=".$courseinfo['school_id']." LIMIT 1");

$filePath = '/documents/';$pic = explode("|",$translatorData['photo']);
$photo_url = $translatorData['photo']<>''?$filePath.$pic[0]:'';
$qrcodeImg = '/qrcode/translator/'.encodeString($rowid,$encryptKey);

$pageName='Tourist Translator';
$pageCode='profile_translator';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>

  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h4><?=$pageName?></h4>           
        </div>
        
        <div style="float:right;">
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="row">
                 	<div class="col-lg-3 col-md-3 col-sm-3">
                 		<h4><i class="fa fa-file-text-o fa-fw"></i> Profile</h4>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9" style="text-align:right;">                    
                    	<button id="btn_print" type="button" title="បោះពុម្ភទៅ Printer" class="btn btn-sm btn-default btn-primary margin_t_b textAlignLeft"><i class="fa fa-print fa-fw"></i></button>  
                    </div>
                 </div>
                 <div class="form well" id="print_block">
                    <div>
                    	<div class="info_row">
                       		<img src="<?=$photo_url?>" width="150" />
                        </div>
                    	<div class="info_row">
                    		ឈ្នោះ (ខ្មែរ): <strong><?=$fullname_kh?></strong>
                        </div>
                        <div class="info_row">
                 			ឈ្នោះ (ឡាតាំង): <strong><?=$fullname_en?></strong>
                        </div>
                        <div class="info_row">
                        	ភេទ: <strong><?=$gender?></strong>
                        </div>
                        <div class="info_row">
                       	 	សញ្ជាតិ: <strong><?=$nationality?></strong>
                        </div>
                        <div class="info_row">
                       		 ថ្ងៃខែឆ្នាំកំណើត: <strong><?=khmerDate($translatorData['dob'])?></strong>
                        </div>
                        <div class="info_row">
                       		លេខលិខិតឆ្លងដែន: <strong><?=$translatorData['passport']?></strong>
                        </div>
                        <div class="info_row">
                        	ទូរស័ព្ទ: <strong><?=$translatorData['mobile']?></strong>
                        </div> 
                        <div class="info_row">
                        	វគ្គបណ្តុះបណ្តាល: <strong><?=$courseinfo['course_title']?></strong>
                        </div>
                        <div class="info_row">
                        	កាលបរិច្ឆេទបណ្តុះបណ្តាល: <strong><?=date("d/M/Y",strtotime($courseinfo['start_date'])).' - '.date("d/M/Y",strtotime($courseinfo['end_date']))?></strong>
                        </div>
                        <div class="info_row">
                        	ឈ្នោះសាលា: <strong><?=$schoolname?></strong>
                        </div>
                        <div class="info_row" style="margin:30px 0; border-bottom:0px;">
                            <img src="<?=$qrcodeImg?>">
                        </div>                       
                    </div>
                 </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->




<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>

$(function(){
$("#btn_print_excel").click(function(){exportData("<?=$annTypeName?>","<?=encodeString($mainQry,$encryptKey)?>");});
$("#btn_print").click(function(){PrintElem("#print_block","<?=$pageName?>");});	
});

</script>

</body>
</html>