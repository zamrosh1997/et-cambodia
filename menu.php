<?php 
$pageName=' Home Page | Tourist Guide';
$pageCode='menu';

include("includes/header.php");?>
<?php include("includes/css_script.php");?>

	<link href="hover-master/css/demo-page.css" rel="stylesheet" media="all">
	<link href="hover-master/css/hover.css" rel="stylesheet" media="all">
	<style type="text/css" media="all">
        .page_menu {
			-webkit-box-shadow: 0px 0px 19px -2px rgba(0,0,0,0.4);
-moz-box-shadow: 0px 0px 19px -2px rgba(0,0,0,0.4);
box-shadow: 0px 0px 19px -2px rgba(0,0,0,0.4);

color:white !important; font-size:18px; width:100%; border-radius:5px; font-family:khmerNormal,Constantia, "Lucida Bright", "DejaVu Serif", Georgia, serif;
			
			background: #87e0fd; /* Old browsers */
background: -moz-linear-gradient(top,  #87e0fd 0%, #53cbf1 0%, #05abe0 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#87e0fd), color-stop(0%,#53cbf1), color-stop(100%,#05abe0)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #87e0fd 0%,#53cbf1 0%,#05abe0 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #87e0fd 0%,#53cbf1 0%,#05abe0 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #87e0fd 0%,#53cbf1 0%,#05abe0 100%); /* IE10+ */
background: linear-gradient(to bottom,  #87e0fd 0%,#53cbf1 0%,#05abe0 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#87e0fd', endColorstr='#05abe0',GradientType=0 ); /* IE6-9 */

			
text-shadow: rgb(3, 3, 3) -1px 1px 4px;
	
			
		}
		.page_menu:hover { color:#feaf43 !important; background:#45c7f1 !important;}
		.container{ max-width:500px; text-align:center;}
		.row{ margin:0; line-height:28px; text-align:center;}
		header{ background:none;}
        </style>
			<div class="col-lg-12">
           		<div class="logo" style="text-align:center;">

</head>
<body>

<!-- Header starts -->
<header>
    <div class="container">
      <div class="row">
                    <img src="img/logo_md.png" style="width:100%; max-width:480px;" />
                </div>
            </div>
      </div>
    </div>
</header>

<!-- CTA Starts -->

<div class="container">
    <div class="row">
      		<div class="col-lg-12">
           		<a href="/" class="button outline-inward page_menu">វគ្គបណ្តុះបណ្តាលវិក្រិត្យការ មគ្គុទេសន៏ទេសចរណ៏</a>
           	</div>
            <div class="col-lg-12">
           		<a href="#" class="button outline-inward page_menu">ប្រព័ន្ធត្រួតពិនិត្យ មគ្គុទេសន៏ទេសចរណ៏</a>
           	</div>
            <div class="col-lg-12">
           		<a href="#" class="button outline-inward page_menu">ប្រព័ន្ធសេវា មគ្គុទេសន៏ទេសចរណ៏</a>
           	</div>
            <div class="col-lg-12">
           		<a href="#" class="button outline-inward page_menu">ស្មើសុំអាជ្ញាបណ្ណ មគ្គុទេសន៏ទេសចរណ៏</a>
           	</div>
    </div>
</div>

<!-- CTA Ends -->


<?php include("includes/script.php"); ?>
</body>
</html>