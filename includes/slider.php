<!-- Slider starts -->
  <div class="parallax-slider">
          <!-- Slider (Parallax Slider) -->            
            <div id="da-slider" class="da-slider">
              <!-- Each slide should be enclosed inside "da-slide" class -->
              <div class="da-slide">
                <div class="slide-details">
                  <!-- Heading -->
                  <h2><span class="fs-18 khmerTitle">គោលបំណង</span></h2>
                  <!-- Para -->
                  <p>ក្រមប្រតិបត្តិវិជ្ជាជីវៈ មគ្គុទ្ទេសក៏ទេសចរណ៏មានគោលបំណងការពារ នឹងពង្រឹងប្រសិទ្ធភាពនៃការប្រតិបត្តិវិជ្ជាជីវៈ មគ្គុទ្ទេសក៏ទេសចរណ៏ និងធាននូវសេចក្តីថ្ងៃថ្នូររបស់មគ្គុទ្ទេសក៏ទេសចរណ៏ ក្នុងការប្រកបរបរវិជ្ជាជីវៈ ដើម្បីបង្កើននូវជំនឿទុកចិត្តពីភ្ញៀវ។</p>
                  <!-- Read more link -->
                  <a href="#" class="da-link">អានបន្ត</a>
                  <!-- Image -->
                  <div class="da-img"><img src="/<?php echo $add_path; ?>img/parallax/1.png" alt="image01" /></div>
                </div>
              </div>
              <div class="da-slide">
                <h2><span class="fs-18 khmerTitle">អត្តប្រយោជន៏</span></h2>
                <p>មគ្គុទ្ទេសក៏ទេសចរណ៏ មានបេសកកម្មចូលរួមអត្ថាធិប្បាយ ពន្យល់ និងលើកស្ទួយអំពភូមិសាស្ត្រធម្មជាតិ ប្រវត្តិសាស្ត្រ វប្បធម៌ ប្រពៃណី សិល្បៈ ទំនៀមទំលាប់ និងសេដ្ឋកិច្ចសង្គមនៅតាមទីតាំងភូមិសាស្ត្រ ដែលខ្លួនទទួលអាជ្ញាបណ្ណទេសចរណ៏ក្នុងការបំពេញមុខងារជា មគ្គុទ្ទេសក៏ទេសចរណ៏។</p>
                <a href="#" class="da-link">អានបន្ទ</a>
                <div class="da-img"><img src="/<?php echo $add_path; ?>img/parallax/2.png" alt="image01" /></div>
              </div>
              <nav class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>
              </nav>
            </div>
  </div>
<!-- Slider ends -->
<div class="sep"></div>

