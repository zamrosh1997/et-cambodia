<!-- JS -->

<script src="/js/bootstrap.js"></script> <!-- Bootstrap -->
<script src="/js/jquery.isotope.js"></script> <!-- Isotope -->
<script src="/js/jquery.prettyPhoto.js"></script> <!-- Pretty Photo -->
<script src="/js/filter.js"></script> <!-- Filter for support page -->

<script src="/jquery-cookie-master/src/jquery.cookie.js"></script> <!-- cookie -->

<script src="/js/jquery.flexslider-min.js"></script> <!-- Flex slider -->
<script src="/js/jquery.cslider.js"></script> <!-- Parallax Slider -->
<script src="/js/modernizr.custom.28468.js"></script> <!-- Parallax slider extra -->

<script src="/js/jquery.carouFredSel-6.1.0-packed.js"></script> <!-- Carousel for recent posts -->
<script src="/js/jquery.refineslide.min.js"></script> <!-- Refind slider -->
<script src="/js/bootstrap-datetimepicker.min.js"></script> <!-- datepicker -->
<script src="/chosen/chosen.jquery.js" type="text/javascript"></script><!-- chosen -->
<script src="/js/custom.js"></script> <!-- Custom codes -->

<script src="/js/functions.js"></script>
<script type="text/javascript">
    	$(document).ready(function(e) {			
			$("#login_frm").submit(function(){checkLogin(); return false;});
			$("#newsbox_btn").click(function(){showHideDiv("newsbox");});
			if(findBootstrapEnvironment()=='xs'){showHideDiv("newsbox");}
			
			 $('.dtpicker').datetimepicker({
			  pickTime: false
			});
			
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}
						
			/*var isMobile = {
				Android: function() {return navigator.userAgent.match(/Android/i);},
				BlackBerry: function() {return navigator.userAgent.match(/BlackBerry/i);},
				iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},
				Opera: function() {return navigator.userAgent.match(/Opera Mini/i);},
				Windows: function() {return navigator.userAgent.match(/IEMobile/i);},
				any: function() {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());}
			};	
					
			if(isMobile.any()){$.cookie('isMobile', true);}else{$.cookie('isMobile', false);}					
			
			if($.cookie('isMobile')=='true'){$("#deviceView").show();}
			if($.cookie('toMobile')=='true'){$("#device_mobile").addClass('active_mode');}else{$("#device_desktop").addClass('active_mode');}
			
			$(".deviceMode").click(function(){switchMode($(this).data('cookie'))});*/
			
        });	
</script>