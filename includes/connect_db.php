<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

date_default_timezone_set('Asia/Phnom_Penh'); 
$encryptKey = 'etraining';

$dbhost = "localhost";
$dbuser = "cambodm5_etuser";
$dbpass = "bT0RrE$%Gy(P";
$dbname = "cambodm5_et";
$conn = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname) or die("Error " . mysqli_connect_error($conn));
function exec_query($query){	
	    global $conn;
		$result = mysqli_query($conn, $query) or
		die("could not execute query $query");
		$lastid = mysqli_insert_id($conn);
		return array($result,$lastid);
}
function exec_query_utf8($query){
		exec_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = exec_query($query);
		return $result[0];
	}
function exec_insert($query){
		std_qry_home("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = exec_query($query);
		return $result[1];
	}
	
//------- start home db connection --------
$dbhost_home = "localhost";
$dbuser_home = "cambodm5_huser";
$dbpass_home = "homeuser123";
$dbname_home = "cambodm5_home";
$conn_home = mysqli_connect($dbhost_home,$dbuser_home,$dbpass_home,$dbname_home) or die("Error " . mysqli_error($conn_home));
function std_qry_home($query){	
	    global $conn_home;
		$result = mysqli_query($conn_home, $query) or
		die("could not execute query $query");
		$lastid = mysqli_insert_id($conn_home);
		return array($result,$lastid);
}
function qry_assoc_home($query){
		std_qry_home("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = std_qry_home($query);
		return $result[0];
	}
function insert_home($query){
		std_qry_home("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = std_qry_home($query);
		return $result[1];
	}
//------ end other db connection --------	

//------- start booking db connection --------
$dbhost_booking = "localhost";
$dbuser_booking = "cambodm5_otgbs";
$dbpass_booking = "#T!pPx!A,Q35";
$dbname_booking = "cambodm5_otgbs";
$conn_booking = mysqli_connect($dbhost_booking,$dbuser_booking,$dbpass_booking,$dbname_booking) or die("Error " . mysqli_error($conn_booking));
function std_qry_booking($query){	
	    global $conn_booking;
		$result = mysqli_query($conn_booking, $query) or
		die("could not execute query $query");
		$lastid = mysqli_insert_id($conn_booking);
		return array($result,$lastid);
}
function qry_assoc_booking($query){
		std_qry_booking("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = std_qry_booking($query);
		return $result[0];
	}
function insert_booking($query){
		std_qry_booking("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = std_qry_booking($query);
		return $result[1];
	}
//------ end other db connection --------
	
	
function post($var) {
	if(isset($_POST[$var])){
		return (trim($_POST[$var]));
	}else{
		return '';
	}
}
function get($var){
	if (isset($_GET[$var])) {
		return (trim($_GET[$var]));
	}else{
		return '';
	}
}



function getStringBetween($string, $start, $end){// use stripos for case-insensitive, without 'i' will be case-sensitive
    $string = " ".$string;
    $ini = stripos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = stripos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
}

function encodeString($string,$key) {
		if(is_numeric($string)){$string = $string*120119898;}
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash ="";
		for ($i = 0; $i < $strLen; $i++) {
			$ordStr = ord(substr($string,$i,1));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
		}
		return $hash;
	}

function decodeString($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash = "";
		for ($i = 0; $i < $strLen; $i+=2) {
			$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= chr($ordStr - $ordKey);
		}
		if(is_numeric($hash)){$hash = $hash/120119898;}
		return $hash;
	}

function enNum_khNum($str){
	$khNum = array('០','១','២','៣','៤','៥','៦','៧','៨','៩');
	$newStr = '';
	for($i=0;$i<strlen($str);$i++){
		if(is_numeric(substr($str,$i,1))){
			$newStr .= $khNum[substr($str,$i,1)];
		}else{
			$newStr .= substr($str,$i,1);
		}		
	}
	return $newStr;
}

function sentMail($subject,$to,$msg){
	$result=false;
	if(filter_var($to, FILTER_VALIDATE_EMAIL)){		
		//$subject = "Reset Password | Cambodia Tourist Guide";
		$formatDate = date("d M Y H:i A");
		$message = '		
		<div>
			<div style="float:left;"><img src="http://'.$_SERVER['HTTP_HOST'].'/img/logo_xs.png" /></div>
			<div style="float:right; text-align:right;">'.$formatDate.'<br />ប្រព័ន្ធចុះបញ្ជី មគ្គុទ្ទេសក៍ទេសចរណ៍កម្ពុជា</div>
		</div>
		<div style="clear:both; padding-top:20px;"></div>		
		'.$msg.'
		<br /><br />
		<span style="font-size:11px;">
			ផ្ញើរដោយ៖ ប្រព័ន្ធបណ្តុះបណ្តាលវិក្រិត្យការមគ្គុទេ្ទសក៍ទេសចរណ៍កម្ពុជា<br />
			វ៉េបសាយ៖ <a href="http://'.$_SERVER['HTTP_HOST'].'">http://'.$_SERVER['HTTP_HOST'].'</a>
		</span>
		</p>
			
		<hr />
		<div style="font-size:11px;color:#888888;">
		សូមកុំឆ្លើយតបនឹងអ៊ីម៉េលនេះ។ ប្រអប់សារនេះមិនត្រូវបានត្រួតពិនិត្យទេ ហើយអ្នកនឹងមិនទទួលបានការឆ្លើយតបវិញឡើយ។ សម្រាប់ព័ត៌មានផ្សេងៗ សូមទាក់ទងមកកាន់ <div style="display:inline-block;">Emai: refreshment@cambodia-touristguide.com</div> | <div style="display:inline-block;">ទូរស័ព្ទ: (855) 12 999 032</div> ។
		</div>
		'; 
										
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers .= 'From: Cambodia Tourist Guide <noreply@cambodia-touristguide.com> \r\n';
		
		$sent = mail($to, $subject, $message, $headers) ;
		
		if($sent){
			$result=true;
		}
	}
	return $result;		
}

function isCourseAvailable($inputCourse){
	$course_available = false;
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE id=$inputCourse AND active=1 ORDER BY id ASC LIMIT 1");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$course_id  = $select_row['id'];
		$register_date  = $select_row['register_date'];
		$start_ws  = $select_row['workshop_start_date'];
		$end_ws  = $select_row['workshop_end_date'];
		
		$examData = qry_arr("exam_date,result_released","tblexamschedule","FIND_IN_SET($inputCourse, course_id) AND active=1 ORDER BY exam_date ASC LIMIT 1");					
		$exam_result_released=$examData['result_released'];
		//$exam_date = singleCell_qry("exam_date","tblexamschedule","exam_date>'$end_ws' AND active=1 ORDER BY exam_date ASC LIMIT 1");						
		$total_participant = mysqli_num_rows(exec_query_utf8("SELECT id FROM tblcourseregister WHERE course_id=$course_id AND active=1"));
							
		$class_full = true;$allow_register=true;$course_ended = true;
		if($total_participant<$select_row['max_participant']){$class_full = false;}
		//if(time()>=strtotime($select_row['register_date'])){$allow_register = true;}
		if($start_ws<>''){
			if(time()<strtotime($start_ws)){$course_ended = false;}
		}else{
			if(!$exam_result_released){$course_ended = false;}
		}
		
		if(!$class_full and $allow_register and !$course_ended){$course_available = true;}						
	}
	return $course_available;
}

function isIncourse($guideid,$courseid=''){
	$incourse = false;$active = false;
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseregister WHERE user_id=$guideid ".($courseid<>''?"and course_id=$courseid":"")." ORDER BY register_date DESC LIMIT 1");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$incourse = true;
		$active=$select_row['active'];
		$current_course_id=$select_row['course_id'];
		$released_qry = exec_query_utf8("SELECT * FROM tblexamschedule WHERE FIND_IN_SET($current_course_id, course_id) AND active=1 ORDER BY exam_date ASC LIMIT 1");
		while($released_row = mysqli_fetch_assoc($released_qry)){if($released_row['result_released']){$incourse=false;}}		
	}
	return array('result'=>$incourse,'active'=>$active);
}

function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') == $date;
}



function calculateScore($rndQuestionid){
	$lessonid = singleCell_qry("lessonid","tblrandomquestion","id=$rndQuestionid LIMIT 1");
	
	//get total question allowed
	//$totalQuestionAllowed = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion' LIMIT 1");
	
	//get all question to be answered
	$q_str = singleCell_qry("randomQuestionid","tblrandomquestion","id=$rndQuestionid LIMIT 1");
	$q_arr = explode(',',$q_str);
	$totalQ = count($q_arr);

	$totalScore = 0;
	$totalStdScore = 0;
	$totalCorrect = 0;
	
	//get all answers
	$answer_str = singleCell_qry("answer","tblrandomquestion","id=$rndQuestionid LIMIT 1");
	$answer_arr = explode(',',$answer_str);
	$chosedAnswer_arr = array();
	$result_arr = array();
	foreach($answer_arr as $value){
		$q_data = explode(':',$value);
		$chosedAnswer_arr[] = $q_data[0];
		$result_arr[] = $q_data[1];
		
		if($q_data[1]==1){$totalCorrect ++;}
	}
	
	$getStdScore = singleCell_qry("score","tblscoresetting","lessonid=$lessonid AND active=1 LIMIT 1");		
	$totalScore = number_format(($totalCorrect/$totalQ)*$getStdScore,3);
	
	//return $correctByLevel;
	
	return array('answered'=>$totalScore,'full'=>$getStdScore);
}

function isCorrect($questionid,$answer){
	$result = false;
	$correctAnswer = singleCell_qry("answer","tblquestions","id=$questionid ORDER BY id ASC LIMIT 1");	
	if($correctAnswer == $answer){$result = true;}
	return $result;
}

function getRandomQuestion($lessonid,$isExam){
	/*$allowedDif = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedDifficulty' LIMIT 1");
	$allowedDif_arr = explode(',',$allowedDif);*/
	
	if($isExam){
		//number question by lesson
		$totalQuestionAllowed = singleCell_qry("total_question","tbllessons","id=$lessonid LIMIT 1");	
		//default number of question for online exam
		$defaultQuestionAllowed = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion_OE' LIMIT 1");	
		//if number by lesson is invalid, then get default number
		if($totalQuestionAllowed<=0 or !is_numeric($totalQuestionAllowed)){$totalQuestionAllowed=$defaultQuestionAllowed;}
	}else{
		//number of question for test
		$totalQuestionAllowed = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion' LIMIT 1");	
	}
		
	$getQestion = array();
	$q_qry = exec_query_utf8("SELECT id FROM tblquestions WHERE lessonid=$lessonid AND active=1 ORDER BY id ASC");
	while($q_row = mysqli_fetch_assoc($q_qry)){	
		$getQestion[] = $q_row['id'];			
	}
	//return false when there is not enough question posted
	if(count($getQestion)<$totalQuestionAllowed){return false;}
		
	//random the existing array item
	$rnd_key = array_rand($getQestion,$totalQuestionAllowed);
	$rndQuestion = array();
	foreach($rnd_key as $key => $value){
		$rndQuestion[] = $getQestion[$value];
	}
	shuffle($rndQuestion);
	
	//random answer/options
	$rndOption=array();
	foreach($rndQuestion as $key => $value){
		$getOption = singleCell_qry("options","tblquestions","id=$value LIMIT 1");
		$totalOption = count(explode("|",$getOption));
		$option_arr = range(1,$totalOption);
		shuffle($option_arr);		
		$rndOption[] = implode("-",$option_arr);
	}
	$rndQuestion = implode(",",$rndQuestion);
	$rndOption = implode(",",$rndOption);
	return array("questions"=>$rndQuestion,"options"=>$rndOption);
}

function filesize_formatted($path)
{
	$size=0;
	if (file_exists($_SERVER['DOCUMENT_ROOT'].$path) and $path<>'') {
    	$size = filesize($_SERVER['DOCUMENT_ROOT'].$path);
	}
	
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function format_filesize($size){
	$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	$power = $size > 0 ? floor(log($size, 1024)) : 0;
	return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function singleCell_qry($field,$tbl,$cond){
	$resultInArray = mysqli_fetch_assoc(exec_query_utf8("SELECT ".$field." FROM ".$tbl.($cond==''?'':' WHERE ').$cond));
	return $resultInArray[$field];
}

function qry_arr($field,$tbl,$cond){
	$resultInArray = mysqli_fetch_array(exec_query_utf8("SELECT ".$field." FROM ".$tbl.($cond==''?'':' WHERE ').$cond),MYSQLI_ASSOC);
	return $resultInArray;
}

/*function getBetween($content,$start,$end){
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}*/

function file_icon($filename){
	$doc_icon = array('pdf'=>'<i class="fa fa-file-pdf-o fa-fw"></i>',
					'png'=>'<i class="fa fa-file-image-o fa-fw"></i>',
					'gif'=>'<i class="fa fa-file-image-o fa-fw"></i>',
					'jpg'=>'<i class="fa fa-file-image-o fa-fw"></i>',
					'jpeg'=>'<i class="fa fa-file-image-o fa-fw"></i>',
					'doc'=>'<i class="fa fa-file-word-o fa-fw"></i>',
					'docx'=>'<i class="fa fa-file-word-o fa-fw"></i>',
					'xls'=>'<i class="fa fa-file-excel-o fa-fw"></i>',
					'xlsx'=>'<i class="fa fa-file-excel-o fa-fw"></i>',
					'ppt'=>'<i class="fa fa-file-powerpoint-o fa-fw"></i>',
					'pptx'=>'<i class="fa fa-file-powerpoint-o fa-fw"></i>',
					'zip'=>'<i class="fa fa-file-archive-o fa-fw"></i>',
					'rar'=>'<i class="fa fa-file-archive-o fa-fw"></i>',
					'mp3'=>'<i class="fa fa-file-audio-o fa-fw"></i>',
					'wav'=>'<i class="fa fa-file-audio-o fa-fw"></i>',
					'wma'=>'<i class="fa fa-file-audio-o fa-fw"></i>',
					'avi'=>'<i class="fa fa-file-video-o fa-fw"></i>',
					'mp4'=>'<i class="fa fa-file-video-o fa-fw"></i>',
					'flv'=>'<i class="fa fa-file-video-o fa-fw"></i>',
				);

	$extension = explode(".",$filename);
	$extension = strtolower(end($extension));
	$ext_icon='<i class="fa fa-file-text-o fa-fw"></i>';
	if(array_key_exists($extension,$doc_icon)){$ext_icon=$doc_icon[$extension];}
	return $ext_icon;
}

function rndStr($length) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

function dateTimeAgo($datetimeString){
	$start_date = new DateTime($datetimeString);
	$now_datetime = new DateTime(date("Y-m-d H:i:s"));
											
	$recieved_date = date_format(date_create($datetimeString),'d/m/Y');	
											
	$interval = $start_date->diff($now_datetime);
	$year_ago = $interval->y;$month_ago = $interval->m;$day_ago = $interval->d;$hour_ago = $interval->h;$minute_ago = $interval->i;$second_ago = $interval->s;										
	$datetime_ago = '';
											
	if($year_ago==0 and $month_ago==0 and $day_ago==0 and $hour_ago==0 and $minute_ago==0){
	$datetime_ago = ($second_ago<=1?"":(($second_ago>1 and $second_ago<4)?" ២ ឬ ៣":$second_ago)).($second_ago<=1?" អម្បាញ់មិញ":" វិនាទីមុន");
	}elseif($year_ago==0 and $month_ago==0 and $day_ago==0 and $hour_ago==0){
	$datetime_ago = $minute_ago.($minute_ago>1?" នាទីមុន":" នាទីមុន");
	}elseif($year_ago==0 and $month_ago==0 and $day_ago==0){
	$datetime_ago = $hour_ago.($hour_ago>1?" ម៉ោងមុន":" ម៉ោងមុន");
	}elseif($year_ago==0 and $month_ago==0){
	$datetime_ago = $day_ago.($day_ago>1?" ថ្ងៃមុន":" ថ្ងៃមុន");
	}elseif($year_ago==0){
	$datetime_ago = $month_ago.($month_ago>1?" ខែមុន":" ខែមុន");
	}else{
	$datetime_ago = $recieved_date;
	}
	return $datetime_ago;		
}

function timePeriod($startDate,$endDate,$format){
	$start_date = new DateTime($startDate);
	$end_date = new DateTime($endDate);
											
	$recieved_date = date_format(date_create($startDate),'d/m/Y');	
											
	$interval = $start_date->diff($end_date);
	$year_ago = $interval->y;$month_ago = $interval->m;$day_ago = $interval->d;$hour_ago = $interval->h;$minute_ago = $interval->i;$second_ago = $interval->s;										
	$formatedPeriod = '';
	
	$totalHours = ($year_ago*365*24)+($month_ago*30*24)+$hour_ago;
	$totalMinutes = $minute_ago;
	$totalSeconds = $second_ago;
											
	if($totalHours>0){
		$formatedPeriod = $totalHours.' ម៉ោង '.($totalMinutes>0?$totalMinutes.' នាទី':'');
	}elseif($totalMinutes>0){
		$formatedPeriod = $totalMinutes.' នាទី '.($totalSeconds>0?$totalSeconds.' វិនាទី':'');
	}elseif($totalSeconds>0){
		$formatedPeriod = $totalSeconds.' វិនាទី';
	}
	$json = array('hours'=>$totalHours,'minutes'=>$totalMinutes,'seconds'=>$totalSeconds);
	
	return $format?$formatedPeriod:$json;		
}

function isRecent($datetimeString){
	$start_date = new DateTime($datetimeString);
	$now_datetime = new DateTime(date("Y-m-d H:i:s"));												
	$interval = $start_date->diff($now_datetime);
	$days = $interval->days;
	
	$result=false;
	if($days<=7){$result=true;}
	return $result;
}

function adduserlog($logType,$userid,$more_data){
	//add to user log
	$result=false;
	if($userid > 0){
		$log_qry = exec_query_utf8("SELECT id FROM user_activity_type where code='$logType' and active=1 limit 1");
		while($log_row = mysqli_fetch_assoc($log_qry)){		
			$log_typeid = $log_row['id'];
			$userIP = getClientIP();$datetime = date("Y-m-d H:i:s"); $browser_info = addslashes(json_encode(getBrowser()));	
			if($log_typeid>0){
				exec_query_utf8("insert into user_activity set user_id=$userid,type_id=$log_typeid,ip='$userIP',browser_info='$browser_info',".($more_data<>''?"more_data='$more_data',":'')."datetime='$datetime'");	
				$result=true;
			}
		}	
	}
	return $result;
}

function getClientIP(){
	// Get user IP address
	if ( isset($_SERVER['HTTP_CLIENT_IP']) && ! empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	}
	
	$ip = filter_var($ip, FILTER_VALIDATE_IP);
	$ip = ($ip === false) ? '0.0.0.0' : $ip;	
	return $ip;
}

function isTesting($userid){
	$test = false;
	$testip = '116.212.140.35'; //ip that can assess testing environment
	$allolwed_user = 3; // user that can access testing environment
	if(getClientIP()==$testip or $userid==$allolwed_user){$test = true;}
	return $test;
}

function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

function khmerDate($date,$format='full'){
	$d = date("d",strtotime($date));
	$m = date("m",strtotime($date));
	$y = date("Y",strtotime($date));
	
	$khmerMonth = array('មករា','កុម្ភៈ','មីនា','មេសា','ឧសភា','មិថុនា','កក្កដា','សីហា','កញ្ញា','តុលា','វិច្ឆិកា','ធ្នូ');
	$full = enNum_khNum($d).' '.$khmerMonth[$m-1].' '.enNum_khNum($y);
	$khdate = array('d'=>enNum_khNum($d),'m'=>$khmerMonth[$m-1],'y'=>enNum_khNum($y),'full'=>$full);
	return $khdate[$format];
}

function getGrade($score){
	$scorerange = array(
							'A'=>array(95,100),
							'B'=>array(85,94),
							'C'=>array(70,84),
							'D'=>array(50,69),
							'E'=>array(0,49),
						);
	$grade = 'N/A';
	foreach($scorerange as $key=>$value){
		if($value[0] <=$score and $score < ($value[1]+1)){$grade = $key;}
	}
	return $grade;
}

function isOEStart($course_id){
	$result = false;
	
	
	return $result;
}

function saveOnlineScore($reg_id,$lessonid){
	if(!is_numeric($reg_id) or !is_numeric($lessonid)){return false;}
	$datetime = date("Y-m-d H:i:s"); 
	$courseInfo = qry_arr("user_id","tblcourseregister","id=$reg_id LIMIT 1");
	$userid = $courseInfo['user_id'];
	//verify lessonid
	
	//get highest score
	$highestScore = singleCell_qry("score","tblrandomquestion","userid=$userid and lessonid=$lessonid and exam=1 and (finishedDate IS NOT NULL or time_expired=1) and active=1 ORDER BY score DESC limit 1"); 
	//check if row not yet created
	$exist = singleCell_qry("id","exam_score","reg_id=$reg_id AND subject_id=$lessonid and active=1 LIMIT 1");	
	if($exist){
		$update = exec_query_utf8("update exam_score set online_exam_score=$highestScore,datetime='$datetime' where id=$exist limit 1");
	}else{
		$inserted = exec_query_utf8("INSERT INTO exam_score set reg_id=$reg_id,subject_id=$lessonid,online_exam_score=$highestScore,datetime='$datetime'");
	}
		
}

function avgScore($course_id,$reg_id){
	//get subject by course
	$course_subject = json_decode(singleCell_qry("subject_id","tblcourseschedule","id=$course_id LIMIT 1"),true);
	$subject_id_arr = array_keys($course_subject);
	
	//get score each subject
	$totalScore = 0;$paper_perc=0.8;$online_perc=0.2;
	foreach($subject_id_arr as $key => $value){
		$examData = qry_arr("paper_exam_score,online_exam_score","exam_score","reg_id=$reg_id and subject_id=$value and active=1 LIMIT 1");
		$paper_exam_score = $examData['paper_exam_score'];
		$online_exam_score = $examData['online_exam_score'];
		$paper_score_calc = $paper_exam_score;			
		$totalScore_bySubject = $paper_score_calc + $online_exam_score;
		$totalScore += $totalScore_bySubject;
	}			
	
	
	$total_subject = count($subject_id_arr);
	$agv_score = number_format($totalScore/$total_subject,2);
	
	return array('avg'=>$agv_score,'grade'=>getGrade($agv_score));
	
}

/*function ipInfo($ip){
	$url=file_get_contents("http://whatismyipaddress.com/ip/$ip");	
	preg_match_all('/<th>(.*?)<\/th><td>(.*?)<\/td>/s',$url,$output,PREG_SET_ORDER);	
	$isp=$output[1][2];	
	$city=$output[9][2];	
	$state=$output[8][2];	
	$zipcode=$output[12][2];	
	$country=$output[7][2];
	return array('isp'=>$isp,'city'=>$city,'state'=>$state,'zipcode'=>$zipcode,'country'=>$country);
}*/

function ipInfo($ip) {
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
    return $details;
}

?>