</head>
<body>
<!-- Header starts -->​
<header>
    <div class="container">
      <!--<div class="row">
      		<div class="col-sm-4" id="deviceView">
            	<div>
                    View as: 
                    <span id="device_mobile" class="deviceMode" data-cookie="mobile"><i class="fa fa-mobile"></i> Mobile</span> | 
                    <span id="device_desktop" class="deviceMode"><i class="fa fa-desktop" data-cookie="desktop"></i> Desktop</span>
                </div>
            </div>
      </div>-->
      <div class="row">
        <div class="col-md-2 col-sm-2">
              <!-- Logo. Use class "color" to add color to the text. -->
              <div class="logo">

                <a href="http://cambodia-touristguide.com"><img src="/img/mot_log.png" width="100px" height="100px" /></a>
                
               
                <!--<h1><a href="#">Mac<span class="color bold">Beath</span></a></h1>
                <p class="meta">something goes in meta area</p>-->
              </div>
        </div> 
        <div class="col-md-2 col-sm-2">
          <div class="logo">
                 <a href="http://cambodia-touristguide.com"><img src="/img/logo_sm.png" /></a>
          </div>
        </div> 

        <div class="col-md-8 col-sm-8">
            <div class="logo">
                <h2><span class="color khmerTitle" style="line-height:45px;">វគ្គបណ្តុះបណ្តាលវិក្រិត្យការ<br />មគ្គទេសក៏ទេសចរណ៍</span></h2>
            </div>
        </div>

      </div> 
      <div class="row">
      	<div class="col-lg-12 col-md-12 col-sm-12">
            
            <!-- Navigation -->             
            <div class="navbar bs-docs-nav" role="banner">
              <div class="container" style="padding-right:0;">
               <div class="navbar-header">
                 <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
               </div>
               
                  <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation" style="padding-right:0;">
                    <ul class="nav navbar-nav myMenu">
                      <li><a href="/"><i class="fa fa-home"></i> ទំព័រដើម</a></li>    
                      <!-- <li><a href="/courses"><i class="fa fa-table fa-fw"></i> វគ្គសិក្សា</a></li> -->
                      <li><a href="/lessons"><i class="fa fa-book"></i> មេរៀន</a></li>
                      <li><a href="/announcements"><i class="fa fa-bullhorn"></i> ព័ត៌មាន</a></li>
                      <li><a href="/usermanual"><i class="fa fa-lightbulb-o fa-fw"></i> របៀបសិក្សាតាមអនឡាញ</a></li> 
                      <!-- <li><a href="/design_team/page123">ក្រុមការងាររៀបចំ</a></li> -->
                      <?php if(isset($_SESSION['userid'])){ ?>
                     <!--  <li class="dropdown">
									 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user fa-fw"></i> គណនី <b class="caret"></b></a>
									 
					           </li> -->
                      <!-- <li><a href="/logout"><i class="fa fa-sign-out"></i> ចាកចេញ</a></li>    -->
                      <?php } ?>           
                    </ul>
                  </nav>
                 </div>

              </div>  
         </div>
      </div>
    </div>
  </header>