<?php 
include("connect_db.php");

$username = post('username');
$password = post('password');

if($username=='' or $password==''){
echo 0;
exit;	
}

$encriptedpass = encodeString($password,$encryptKey);

if ($stmt = mysqli_prepare($conn, "SELECT id,email FROM tblusers WHERE BINARY (email=? OR code=? OR mobile=?) AND loginPassword=? AND pending=? AND active=? LIMIT 1")) { //create a prepared statement
	$active = 1;$pending = 0;
	mysqli_stmt_bind_param($stmt, "ssssii", $username,$username,$username,$encriptedpass,$pending,$active);
	mysqli_stmt_execute($stmt);			
	mysqli_stmt_store_result($stmt);
	$count = mysqli_stmt_num_rows($stmt);
				
	if($count==1){
		session_start(); 
		mysqli_stmt_bind_result($stmt, $get_userid, $get_username);      
		while(mysqli_stmt_fetch($stmt)){
			$_SESSION['userid']=$get_userid;
			$_SESSION['username']= $get_username;
			adduserlog('login',$get_userid,'');
		}
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
		echo 1;
	}
	else {
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
		echo 0;
	}
}	

?>