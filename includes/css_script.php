<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Stylesheets -->
  <link href="/style/bootstrap.css" rel="stylesheet">
  <link href="/style/jquery-ui.css" rel="stylesheet">
  <!-- Pretty Photo -->
  <link href="/style/prettyPhoto.css" rel="stylesheet">
  <!-- Flex slider -->
  <link href="/style/flexslider.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="/style/font-awesome.css">
  <!-- Custom Fonts font-awesome-4.1.0 -->
  <link href="/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Parallax slider -->
  <link rel="stylesheet" href="/style/slider.css">
  <!-- Refind slider -->
  <link rel="stylesheet" href="/style/refineslide.css">  
  <!-- datepicker -->
  <link rel="stylesheet" href="/style/bootstrap-datetimepicker.min.css"> 
  <!-- Main stylesheet -->
  <link href="/style/style.css" rel="stylesheet">
  <!-- Stylesheet for Color -->
  <link href="/style/blue.css" rel="stylesheet">
  <!-- chosen style -->
  <link rel="stylesheet" href="/chosen/chosen.css">
  
  <!-- custom style -->
  <link href="/style/custom.css" rel="stylesheet">

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="/img/favicon/favicon.png">
  
  <script src="/js/jquery.js"></script>
  <script src="/js/jquery-ui.js"></script>
   <!-- jQuery -->