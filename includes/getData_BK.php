<?php include("connect_db.php");
session_start(); //Start the session
//date_default_timezone_set('Europe/London'); 
$cmd = post("cmd");
if(!isset($_SESSION['userid'])){
	$userid = 0;
	//echo json_encode(array(false));
	//exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

if($cmd=='lessonList'){
	$keyword = post("keyword");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");
	$allowedQuiz = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedQuiz' AND active=1 LIMIT 1");
	
	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.=($sql_condition=='WHERE'?" ":" AND ")."title LIKE '%$keyword%'";}
	if($sql_condition<>'WHERE'){$sql_condition .= ' AND';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tbllessons $sql_condition active=1 ORDER BY priority ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$lessonListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons $sql_condition active=1 ORDER BY priority ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($lesson_row = mysqli_fetch_assoc($lesson_qry)){	
		$docLink = '<a href="'.$docPath.$lesson_row['filename'].'" target="_blank">'.$lesson_row['filename'].'</a>';
		
		$lessonListString .= '<tr>
								  <td class="tableCellLeftCenter"><a href="/lesson/'.encodeString($lesson_row['id'],$encryptKey).'">'.$lesson_row['title'].'</a></td>
								  <td class="tableCellCenter">
								  	<a class="btn btn-large" style="padding:0;" href="/lesson/'.encodeString($lesson_row['id'],$encryptKey).'"><span class="label label-info" title="ចុចដើម្បីចូលអាន">មើល</span></a>
								  	<a class="btn btn-large '.((!$allowedQuiz or !$lesson_row['allowQuiz'])?'disabled':'').'" style="padding:0;" href="/test/'.encodeString($lesson_row['id'],$encryptKey).'"><span class="label label-info" title="ចុចដើម្បីប្រលង">ធ្វើតេស្ត</span></a></td>
								</tr>';	
		
		
		$i++;
	}
	if($lessonListString == ''){$lessonListString = '<tr><td colspan="2" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$lessonListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='announcementList'){
	$keyword = post("keyword");
	$typeid = post("typeid");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");
	
	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.=($sql_condition=='WHERE'?" ":" AND ")."title LIKE '%$keyword%'";}
	if($typeid>0){$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."type=$typeid";}
	$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."active=1";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = qry_assoc_home("SELECT * FROM tblannouncement $sql_condition ORDER BY datetime ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$announcementListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$announcement_qry = qry_assoc_home("SELECT * FROM tblannouncement $sql_condition ORDER BY datetime DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($announcement_row = mysqli_fetch_assoc($announcement_qry)){	
			
		$announcementListString .= '<tr>
								  <td class="tableCellLeftCenter"><a href="/announcement/'.encodeString($announcement_row['id'],$encryptKey).'">'.$announcement_row['title'].'</a></td>
								  <td class="tableCellCenter">'.$announcement_row['view'].'</td>
								  <td class="tableCellCenter">'.date("d/m/Y",strtotime($announcement_row['datetime'])).'</td>
								  
								</tr>';	
		
		
		$i++;
	}
	if($announcementListString == ''){$announcementListString = '<tr><td colspan="3" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$announcementListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='examList'){
	$keyword = post("keyword");
	//$typeid = post("typeid");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");
	
	//get exam result cate id
	$examResult_id = singleCell_qry("id","tblsubcategory","mainCategoryid=(SELECT id FROM tblmaincategory WHERE title='announcement' LIMIT 1) AND title='exam result' AND active=1 LIMIT 1");
	
	$sql_condition = 'WHERE';
	if($keyword<>''){$sql_condition.=($sql_condition=='WHERE'?" ":" AND ")."title LIKE '%$keyword%'";}
	//if($typeid>0){$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."type=$typeid";}
	$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."type=$examResult_id AND active=1";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblannouncement $sql_condition ORDER BY datetime ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$announcementListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$announcement_qry = exec_query_utf8("SELECT * FROM tblannouncement $sql_condition ORDER BY datetime ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($announcement_row = mysqli_fetch_assoc($announcement_qry)){	
			
		$announcementListString .= '<tr>
								  <td class="tableCellLeftCenter"><a href="/announcement/'.encodeString($announcement_row['id'],$encryptKey).'">'.$announcement_row['title'].'</a></td>
								  <td class="tableCellCenter">'.date("d/m/Y",strtotime($announcement_row['datetime'])).'</td>
								  
								</tr>';	
		
		
		$i++;
	}
	if($announcementListString == ''){$announcementListString = '<tr><td colspan="2" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$announcementListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='candidateList'){
	$keyword = post("keyword");
	$courseid = post("course_num");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");	
	
	$sql_condition = '';
	if($keyword<>''){$sql_condition.="AND user_id IN (SELECT id FROM tblusers WHERE firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%' OR code LIKE '%$keyword%')";}
	$sql_condition.= " AND course_id=$courseid";
	
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblcourseregister WHERE pending=0 AND active=1 $sql_condition ORDER BY register_date ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$candidateListString = '';$i=$startIndex+1;
	$candidate_qry = exec_query_utf8("SELECT * FROM tblcourseregister WHERE pending=0 AND active=1 $sql_condition ORDER BY register_date ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($candidate_row = mysqli_fetch_assoc($candidate_qry)){			
		$canData = qry_arr("code,firstName,lastName,firstNameEn,lastNameEn,gender,dob","tblusers","id='".$candidate_row['user_id']."' LIMIT 1");			
		$candidateListString .= '<tr style="'.($candidate_row['user_id']==$userid?'color:#428bca; font-weight:bold;':'').'">
								  	<td class="tableCellCenter">'.enNum_khNum($i).'</td>
									<td class="tableCellCenter">'.$canData['code'].'</td>
									<td>'.$canData['lastName'].' '.$canData['firstName'].'</td>
									<td>'.$canData['lastNameEn'].' '.$canData['firstNameEn'].'</td>
									<td class="tableCellCenter">'.($canData['gender']=='m'?'ប្រុស':($canData['gender']=='f'?'ស្រី':'N/A')).'</td>
									<td class="tableCellCenter">'.date("d/m/Y",strtotime($canData['dob'])).'</td>
								</tr>';	
		
		
		$i++;
	}
	if($candidateListString == ''){$candidateListString = '<tr><td colspan="6" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$candidateListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'totalRow'=>$totalRow,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='courseList'){
	$keyword = post("keyword");
	$course_year = post("course_year");
	$course_area = post("course_area");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");	
	
	$sql_condition = 'WHERE';
	if($course_year>0){$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."year=$course_year";}
	if($course_area>0){$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."area_id=$course_area";}
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblcourseschedule $sql_condition ORDER BY year,area_id,course_num ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	$row_span_num = 0;
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$courseListString = '';$i=$startIndex+1;
	$course_qry = exec_query_utf8("SELECT * FROM tblcourseschedule $sql_condition ORDER BY year,area_id,course_num ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($course_row = mysqli_fetch_assoc($course_qry)){	
		$areaName = singleCell_qry("displayTitle","tblsubcategory","id=".$course_row['area_id']." LIMIT 1");
		
		$examData = qry_arr("course_id,exam_date","tblexamschedule","year=".$course_row['year']." AND area=".$course_row['area_id']." AND exam_date>'".$course_row['workshop_end_date']."' AND active=1 ORDER BY exam_date ASC LIMIT 1");
		$exam_date = $examData['exam_date'];		
		if($exam_date<>''){$exam_date=date("d/m/Y",strtotime($exam_date));}else{
			$exam_date='N/A';	
		}
		
		$examDateCell = '';
		if($row_span_num==0){
			$course_id = explode(",",$examData['course_id']);
			sort($course_id,SORT_NUMERIC);
			if(count($course_id)>0){
				if($course_id[0]==$course_row['id']){
					$row_span_num = count($course_id);
					$examDateCell = '<td rowspan="'.count($course_id).'" class="tableCellCenter">'.$exam_date.'</td>';
				}else{
					$examDateCell = '<td class="tableCellCenter">N/A</td>';
				}
			}
			
		}
		
		if($course_row['workshop_start_date']<>''){
		$workshop_date = date("d",strtotime($course_row['workshop_start_date'])).'-'.date("d/m/Y",strtotime($course_row['workshop_end_date']));
		if($course_row['workshop_start_date']==$course_row['workshop_end_date']){$workshop_date = date("d/m/Y",strtotime($course_row['workshop_end_date']));}
		}else{$workshop_date = 'N/A';}
		
		$courseListString .= '<tr style="'.($course_row['active']==0?'color:red':'').'">
                                            	<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
												<td>'.$course_row['year'].'</td>
												<td>'.$areaName.'</td>
												<td>វគ្គសិក្សាទី '.$course_row['course_num'].'</td>
												<td>'.date("d/m/Y",strtotime($course_row['register_date'])).'</td>
												<td>'.$workshop_date.'</td>
												'.$examDateCell.'											
                                            </tr>';
		
		
		$i++;
		if($row_span_num>0){$row_span_num--;}
	}
	if($courseListString == ''){$courseListString = '<tr><td colspan="7" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$courseListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='resultList'){
	$keyword = post("keyword");
	$course_id = post("course_num");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");	
	
	$sql_condition = '';
	if($keyword<>''){$sql_condition.="AND can.user_id IN (SELECT id FROM tblusers WHERE firstName LIKE '%$keyword%' OR lastName LIKE '%$keyword%' OR firstNameEn LIKE '%$keyword%' OR lastNameEn LIKE '%$keyword%' OR code LIKE '%$keyword%')";}
	//$sql_condition.= ($sql_condition=='WHERE'?" ":" AND ")."can.course_id=$course_id";
	if($sql_condition=='WHERE'){$sql_condition = '';}
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblcourseregister can WHERE can.course_id=$course_id $sql_condition ORDER BY can.register_date ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}
	
	$scoreCate_qry = exec_query_utf8("SELECT id,title FROM tblsubcategory WHERE mainCategoryid=(SELECT id FROM tblmaincategory WHERE title='score' LIMIT 1) AND active=1");
	$totalScoreCate = mysqli_num_rows($scoreCate_qry);
	$scoreCateid = array();
	while($scoreCate_row = mysqli_fetch_assoc($scoreCate_qry)){	
		$scoreCateid[] = $scoreCate_row['id'];
	}
	
	$startIndex = ($targetPage-1)*$rowsPerPage;
	$candidateListString = '';$i=$startIndex+1;
	
	$mainQry = "select can.id,can.user_id,can.register_date,group_concat(score.score separator ',') strScore,group_concat(score.cateid separator ',') strScoreid,SUBSTRING_INDEX(SUBSTRING_INDEX(group_concat(score.score separator ','),',',2),',',-1) as str_arr,sum(score.score) as totalscore from tblcourseregister can left join tblcandidatescore score on score.registration_id=can.id WHERE can.course_id=$course_id $sql_condition group by can.id order by totalscore DESC";
	$candidate_qry = exec_query_utf8($mainQry." LIMIT ".$startIndex.",$rowsPerPage");
	//$candidate_qry = exec_query_utf8("SELECT * FROM tblcourseregister $sql_condition ORDER BY register_date ASC LIMIT ".$startIndex.",$rowsPerPage");
	while($candidate_row = mysqli_fetch_assoc($candidate_qry)){			
		$canData = qry_arr("code,firstName,lastName,gender","tblusers","id='".$candidate_row['user_id']."' LIMIT 1");
		
		$strScore = $candidate_row['strScore'];$strScoreid = $candidate_row['strScoreid'];
		$score_arr = explode(",",$strScore);
		$scoreid_arr = explode(",",$strScoreid);		

		$getScore = array();
		foreach($scoreid_arr as $key=>$value){
			$getScore[$value] = $score_arr[$key];
		}	

		//show score data by score category
		$scoreList = '';
		foreach($scoreCateid as $key=>$value){
			$scoreList .= '<td class="tableCellCenter">'.(in_array($value,$scoreid_arr)?$getScore[$value]:'--').'</td>';
		}		
					
		$candidateListString .= '<tr style="'.($candidate_row['user_id']==$userid?'color:#428bca;':'').'">
								  	<td>'.enNum_khNum($i).'</td>
									<td>'.$canData['code'].'</td>
									<td>'.$canData['lastName'].' '.$canData['firstName'].'</td>
									<td>'.($canData['gender']=='m'?'ប្រុស':($canData['gender']=='f'?'ស្រី':'N/A')).'</td>
									'.$scoreList.'
									<td>'.($strScore==''?'--':$candidate_row['totalscore']).'</td>
								</tr>';	
		
		
		$i++;
	}
	if($candidateListString == ''){$candidateListString = '<tr><td colspan="'.(5+$totalScoreCate).'" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$candidateListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='readList'){
	$keyword = post("keyword");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");
	
	$sql_condition = '';
	if($keyword<>''){$sql_condition.=($sql_condition=='AND'?" ":" AND ")."lessonid IN (SELECT id FROM tbllessons WHERE title LIKE '%$keyword%')";}
	
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblreadlist WHERE userid=$userid $sql_condition GROUP BY lessonid ORDER BY datetime ASC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$readListListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$readList_qry = exec_query_utf8("SELECT count(*) as totalView,lessonid,max(datetime) as maxDatetime FROM tblreadlist WHERE userid=$userid $sql_condition GROUP BY lessonid ORDER BY datetime DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($readList_row = mysqli_fetch_assoc($readList_qry)){	
		$lessonTitle = singleCell_qry("title","tbllessons","id=".$readList_row['lessonid']." LIMIT 1");
		$readListListString .= '<tr>
								  <td class="tableCellLeftCenter">'.$lessonTitle.'</td>
								  <td class="tableCellCenter">'.$readList_row['totalView'].'</td>
								  <td class="tableCellCenter">'.date("d/m/Y H:i",strtotime($readList_row['maxDatetime'])).'</td>
								</tr>';	
		
		
		$i++;
	}
	if($readListListString == ''){$readListListString = '<tr><td colspan="3" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$readListListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='testList'){
	$keyword = post("keyword");
	$currentPage = intval(post("currentPage"));
	$rowsPerPage = post("rowsPerPage");
	$navAction = post("navAction");
	
	$sql_condition = '';
	if($keyword<>''){$sql_condition.=($sql_condition=='AND'?" ":" AND ")."lessonid IN (SELECT id FROM tbllessons WHERE title LIKE '%$keyword%')";}
	
	//work with total page
	$navRow_qry = exec_query_utf8("SELECT * FROM tblrandomquestion WHERE exam=0 and userid=$userid $sql_condition ORDER BY id DESC");
	$totalRow = mysqli_num_rows($navRow_qry);
	$totalPages = $totalRow/$rowsPerPage;
	if($totalRow%$rowsPerPage>0){$totalPages = intval($totalPages) + 1;}
	
	//get the target page number	
	$targetPage = 1;$nav_btn_disable = array();
	if($navAction=='first'){
		$targetPage = 1;
	}elseif($navAction=='prev'){
		$targetPage = $currentPage-1;
	}elseif($navAction=='next'){
		$targetPage = $currentPage+1;
	}elseif($navAction=='last'){
		$targetPage = $totalPages;
	}elseif($navAction=='goto'){
		$targetPage = $currentPage;
	}
	//get goto select list
	$gotoSelectNum = array();
	for($i=1;$i<=$totalPages;$i++){
		$gotoSelectNum[] = $i;
	}
	
	if($totalPages==1 or $totalPages==0){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>0,'nav_last'=>0);
	}elseif($targetPage==1){
		$nav_btn_disable = array('nav_first'=>0,'nav_prev'=>0,'nav_next'=>1,'nav_last'=>1);
	}elseif($targetPage==$totalPages){
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>0,'nav_last'=>0);
	}else{
		$nav_btn_disable = array('nav_first'=>1,'nav_prev'=>1,'nav_next'=>1,'nav_last'=>1);
	}

	$startIndex = ($targetPage-1)*$rowsPerPage;
	$testListListString = '';$i=$startIndex+1;
	$docPath = '/documents/';
	$testList_qry = exec_query_utf8("SELECT * FROM tblrandomquestion WHERE exam=0 and userid=$userid $sql_condition ORDER BY id DESC LIMIT ".$startIndex.",$rowsPerPage");
	while($testList_row = mysqli_fetch_assoc($testList_qry)){	
		$lessonid = $testList_row['lessonid'];
		$lessonInfo = qry_arr("title,active","tbllessons","id=$lessonid LIMIT 1");
		$lessonTitle=$lessonInfo['title'];
		$lessonActive=$lessonInfo['active'];
		
		$testCompleted = false;
		if($testList_row['finishedDate']<>NULL){$testCompleted = true;}
		$testListListString .= '<tr style="'.($lessonActive?'':'border-left:2px solid #ff3737;').'">
								  <td class="tableCellLeftCenter"><a href="/tests/'.encodeString($testList_row['id'],$encryptKey).'">'.$lessonTitle.'</a></td>
								  <td class="tableCellCenter">'.date("d/m/Y H:i",strtotime($testList_row['takenDate'])).'</td>
								  <td class="tableCellCenter">'.($testCompleted?timePeriod($testList_row['takenDate'],$testList_row['finishedDate'],true):'---').'</td>
								  <td class="tableCellCenter">'.($testList_row['finishedDate']==NULL?'---':number_format($testList_row['score'],(intval($testList_row['score'])==$testList_row['score']?0:1)).'/'.$testList_row['fullScore']).'</td>
								  <td class="tableCellCenter">
								  	'.(($testCompleted or !$lessonActive)?'':'<a href="/test/'.encodeString($lessonid,$encryptKey).'">').'<span class="label label-'.(($testCompleted or !$lessonActive)?'warning':'info').'" title="ចុចដើម្បីធ្វើតេស្តបន្ត">បន្ត</span>'.(($testCompleted or !$lessonActive)?'':'</a>').'
								  	'.(($testCompleted or !$lessonActive)?'':'<a href="/test/'.encodeString($lessonid,$encryptKey).'/renew">').'<span class="label label-'.(($testCompleted or !$lessonActive)?'warning':'info').'" title="ចុចដើម្បីធ្វើតេស្តម្តងទៀត">ម្តងទៀត</span>'.(($testCompleted or !$lessonActive)?'':'</a>').'
									</td>
								 
								</tr>';	
		
		
		$i++;
	}
	if($testListListString == ''){$testListListString = '<tr><td colspan="5" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	$data = array('list'=>$testListListString,'targetPage'=>$targetPage,'totalPages'=>$totalPages,'gotoSelectNum'=>$gotoSelectNum,'nav_btn_disable'=>$nav_btn_disable);
	
	echo json_encode($data);
	
}elseif($cmd=='register'){
	$data=array();
	
	$getData = $_POST['getData'];
	$traineeType = singleCell_qry("id","user_role","code='guide' limit 1");
	$datetime=date("Y-m-d H:i:s");
	
	//$optional_input = array('inputEmail'=>$getData['inputEmail']);	
	$optional_input = array('inputCode'=>$getData['inputCode']);	
	$required_input = array_diff($getData,$optional_input);
	
	extract($getData);
	
	$isPending = false;$getuserid = 0;
	
	if($inputEmail<>'' and $inputMobile<>''){
		if($inputCode<>''){
			$guides_data = json_decode(file_get_contents("http://cambodia-touristguide.com/otgbs/public/api/guide/$inputCode"),true);
			if($inputCode<>$guides_data['code']){
				$data[] = array('result'=>'invalid','msg'=>'មិនមាន License ID: <span style="font-weight:bold;">'.$inputCode.'</span> ក្នុងប្រព័ន្ធទេ (បើអ្នកមិនចាំ សូមកុំបំពេញ)');
			}
		}
		$inputMobile = str_replace(array(' ','-'),"",$inputMobile);
		//$sql_check = exec_query_utf8("SELECT * FROM tblusers WHERE ".($inputEmail<>''?"email='$inputEmail' OR ":" ")."code='$inputCode' LIMIT 1");
		$sql_check = exec_query_utf8("SELECT * FROM tblusers WHERE ".($inputCode<>''?"code='$inputCode' OR ":" ")."email='$inputEmail' OR mobile='$inputMobile' LIMIT 1");
		$userData = mysqli_fetch_array($sql_check,MYSQLI_ASSOC);
		$checkUser = mysqli_num_rows($sql_check);
		if($checkUser>0){
			/*if($userData['pending']==1){
				$getuserid = $userData['id'];
				$isPending = true;
			}else{
				$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េល ឬលេខកូដខាងលើ បានចុះឈ្មោះរួចហើយ');
			}*/
			$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េល ឬ License ID ឬលេខទូរស័ព្ទខាងលើ បានចុះឈ្មោះរួចហើយ');
		}
		
		if($checkUser==0 or $isPending){
			if(in_array('',$required_input)){
				$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញគ្រប់ប្រអប់ទាំងអស់');
			}elseif(!is_numeric($inputCourse)){$data[] = array('result'=>'invalid','msg'=>'វគ្គសិក្សាមិនត្រឹមត្រូវ');
			}else{
				//CHECK IF COURSE IS AVAILABLE
				$course_available = isCourseAvailable($inputCourse);
				if(!$course_available){$data[] = array('result'=>'invalid','msg'=>'វគ្គសិក្សាមិនត្រឹមត្រូវ');}						
			}	
			if($inputEmail<>''){if(!filter_var($inputEmail, FILTER_VALIDATE_EMAIL)){$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េលមិនត្រឹមត្រូវ');}}
			if(strlen($inputPassword)<6){$data[] = array('result'=>'invalid','msg'=>'ពាក្យសម្ងាត់ត្រូវមាន ៦ តួយ៉ាងតិច');}
			if($inputPassword <> $inputConfirmPassword){$data[] = array('result'=>'invalid','msg'=>'ពាក្យសម្ងាត់មិនផ្ទៀតផ្ទាត់គ្នា');}
			if($inputAgreePolicy <> 'true'){$data[] = array('result'=>'invalid','msg'=>'សូមអាន និងយល់ស្របជាមួយលក្ខន្តិករបស់យើងជាមុនសិន');}
		}
	}else{
		$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញលេខទូរស័ព្ទ និងអ៊ីម៉េល');
	}
	
	
	
	if(count($data)==0){
		
		$dbAction = "INSERT INTO";$condAction = "";$code_con = "code='$inputCode',";
		/*if($isPending){
			$dbAction = "UPDATE";
			$condAction = "WHERE code='$inputCode'";
			$code_con = "";
		}*/		
		$encrypePassword = encodeString($inputPassword,$encryptKey);
		$to=$inputEmail;$toName =$inputLName.' '.$inputFName;	
		$sql_set = false;
		if(!$isPending){
			$sql_set = exec_insert($dbAction." tblusers SET
												type=$traineeType,
												".$code_con."
												firstName='$inputFName',
												lastName='$inputLName',
												firstNameEn='$inputFNameEn',
												lastNameEn='$inputLNameEn',
												dob='$inputDOB',
												gender='$inputGender',
												idCard='$inputIDCard',
												licenseExpiredDate='$inpuExpirytLicenseDate',
												mobile='$inputMobile',
												email='$inputEmail',
												loginPassword='$encrypePassword',
												registerDate='$datetime' ".$condAction);			
			//insert course		
			$getuserid = $sql_set;
			$course_id = exec_insert("INSERT INTO tblcourseregister SET
												user_id=$getuserid,
												course_id=$inputCourse,
												register_date='$datetime',
												created_date='$datetime'");	
			
			adduserlog('register_account',$getuserid,'');
			adduserlog('register_course',$getuserid,'');											
			
		}
		
		if(!$sql_set){
			$data[] = array('result'=>'invalid','msg'=>'Error Database');
		}else{					
			//----- start email -------------		
			$activatonlink = 'http://et.cambodia-touristguide.com/activate/'.encodeString($getuserid,$encryptKey);					
			$subject = "Account Activation | Cambodia Tourist Guide Refreshment Course";
			$message = '
			សួស្តី '.$toName.'
			<p>
			អ្នកបានចុះឈ្មោះប្រើប្រាស់ប្រព័ន្ធបណ្តុះបណ្តាលវិក្រិត្យការមគ្គុទេ្ទសក៍ទេសចរណ៍។<br />
			សូមចុចលើ Link ខាងក្រោម ដើម្បីចាប់ផ្តើមដំណើរការគណនីរបស់អ្នក។
			</p>
			<a href="'.$activatonlink.'">'.$activatonlink.'</a>						
			'; 
			sentMail($subject,$to,$message);
			//----- end email -------------
		}
	}
	
	$codeData = array('get_userid'=>$getuserid,'inputCode'=>$inputCode,'inputCourse'=>$inputCourse,'token'=>time());
	
	$code = encodeString(serialize($codeData),$encryptKey);
	$dt = array('data'=>$data,'codeData'=>$code);
	echo json_encode($dt);	
}elseif($cmd=='reg_course'){	
	//insert course		
	$courseid = $_POST['courseid'];
	$datetime=date("Y-m-d H:i:s");
	$result=false;$msg='';
	
	$isIncourse = isIncourse($userid,$courseid);
	if($isIncourse['result']){
		$msg='អ្នកបានចុះឈ្នោះក្នុងវគ្គនេះរួចហើយ';				
	}elseif(!isCourseAvailable($courseid)){
		$msg='វគ្គសិក្សាមិនត្រឹមត្រូវ';		
	}else{		
		exec_query_utf8("INSERT INTO tblcourseregister SET
										user_id=$userid,
										course_id=$courseid,
										register_date='$datetime',
										created_date='$datetime'");	
		adduserlog('register_course',$userid,$courseid);
		$result=true;$msg='ទិន្នន័យត្រូវបានបញ្ចូលដោយជោគជ័យ';
	}
	
	echo json_encode(array('result'=>$result,'msg'=>$msg));
}elseif($cmd=='registerNewCourse'){
	$data=array();	
	$getData = $_POST['getData'];
	$datetime=date("Y-m-d H:i:s");
	
	//$optional_input = array();	
	//$required_input = array_diff($getData,$optional_input);
	
	extract($getData);	
	
	//CHECK IF COURSE IS AVAILABLE	
	$course_available = isCourseAvailable($inputCourse);
	//check if already register
	$reg_data = qry_arr("active","tblcourseregister","user_id=$userid and  ORDER BY register_date DESC LIMIT 1");
	
	if(!$course_available){$data[] = array('result'=>'invalid','msg'=>'វគ្គសិក្សាមិនត្រឹមត្រូវ');}						
	
	if(count($data)==0){
		//insert course
		$course_set = exec_query_utf8("INSERT INTO tblcourseregister SET
									user_id=$userid,
									course_id='$inputCourse',
									register_date='$datetime',
									created_date='$datetime'");
		//adduserlog('register_course',$inputCourse,'');
		if(!$course_set){$data[] = array('result'=>'invalid','msg'=>'Error Database');}
	}
	
	//$codeData = array('inputCode'=>$inputCode,'inputCourse'=>$inputCourse,'token'=>time());
	$codeData = array('get_userid'=>$userid,'inputCode'=>$inputCode,'inputCourse'=>$inputCourse,'token'=>time());
	
	$code = encodeString(serialize($codeData),$encryptKey);
	$dt = array('data'=>$data,'codeData'=>$code);
	echo json_encode($dt);	
}elseif($cmd=='updateProfile'){
	$data=array();
	
	$getData = $_POST['getData'];
	if($getData['inpuExpirytLicenseDate']==''){$getData['inpuExpirytLicenseDate']='NULL';}else{$getData['inpuExpirytLicenseDate']="'".$getData['inpuExpirytLicenseDate']."'";}
	$datetime=date("Y-m-d H:i:s");
	
	//make some data optional
	$optional_input = array('inputEmail'=>$getData['inputEmail'],'inpuExpirytLicenseDate'=>$getData['inpuExpirytLicenseDate']);	
	$required_input = array_diff($getData,$optional_input);
	
	if(in_array('',$required_input)){$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញគ្រប់ប្រអប់ទាំងអស់');}
	extract($getData);	
	
	if($inputEmail<>'' and $inputMobile<>''){
		/*if($inputCode<>''){
			$guides_data = json_decode(file_get_contents("http://cambodia-touristguide.com/otgbs/public/api/guide/$inputCode"),true);
			if($inputCode<>$guides_data['code']){
				$data[] = array('result'=>'invalid','msg'=>'មិនមាន License ID: <span style="font-weight:bold;">'.$inputCode.'</span> ក្នុងប្រព័ន្ធទេ (បើអ្នកមិនចាំ សូមកុំបំពេញ)');
			}
		}*/
		$inputMobile = str_replace(array(' ','-'),"",$inputMobile);
		$sql_check = exec_query_utf8("SELECT * FROM tblusers WHERE (email='$inputEmail' OR mobile='$inputMobile') and id<>$userid LIMIT 1");
		$userData = mysqli_fetch_array($sql_check,MYSQLI_ASSOC);
		$checkUser = mysqli_num_rows($sql_check);
		if($checkUser>0){
			$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េល ឬលេខទូរស័ព្ទខាងលើមានរួចហើយ');
		}
	}else{
		$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញលេខទូរស័ព្ទ និងអ៊ីម៉េល');
	}
	
	if(count($data)==0){	
		exec_query_utf8("UPDATE tblusers SET
										firstName='$inputFName',
										lastName='$inputLName',
										firstNameEn='$inputFNameEn',
										lastNameEn='$inputLNameEn',
										dob='$inputDOB',
										gender='$inputGender',
										idCard='$inputIDCard',
										licenseExpiredDate=$inpuExpirytLicenseDate,
										mobile='$inputMobile',
										email='$inputEmail',
										lastUpdateDate='$datetime'
										WHERE id=$userid LIMIT 1");
		adduserlog('update_profile',$userid,'');
	}
	
	echo json_encode($data);		
}elseif($cmd=='updateAccount'){
	$data=array();
	
	$getData = $_POST['getData'];
	$datetime=date("Y-m-d H:i:s");
	
	if(in_array('',$getData)){$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញគ្រប់ប្រអប់ទាំងអស់');}
	extract($getData);
	
	if($currentPassword==''){
		$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញពាក្យសម្ងាត់បច្ចុប្បន្ន');
	}else{
		$encrypeOldPassword = encodeString($currentPassword,$encryptKey);	
		$checkPass = mysqli_num_rows(exec_query_utf8("SELECT id FROM tblusers WHERE id=$userid AND loginPassword='$encrypeOldPassword' LIMIT 1"));
		if($checkPass==0){$data[] = array('result'=>'invalid','msg'=>'ពាក្យសម្ងាត់បច្ចុប្បន្នមិនត្រឹមត្រូវ');}
		else{
			if(strlen($newPassword)<6){$data[] = array('result'=>'invalid','msg'=>'ពាក្យសម្ងាត់ត្រូវមាន ៦ អក្សរយ៉ាងតិច');}
			if($newPassword <> $confirmNewPassword){$data[] = array('result'=>'invalid','msg'=>'ពាក្យសម្ងាត់មិនផ្ទៀតផ្ទាត់គ្នា');}
		}
	}
	
	if(count($data)==0){
		$encrypePassword = encodeString($newPassword,$encryptKey);		
		exec_query_utf8("UPDATE tblusers SET loginPassword='$encrypePassword' WHERE id=$userid LIMIT 1");
		adduserlog('changed_pwd',$userid,'');
	}
	
	echo json_encode($data);	
}elseif($cmd=='subscription'){
	$data=array();
	
	$getData = $_POST['getData'];
	$datetime=date("Y-m-d H:i:s");
	
	if(in_array('',$getData)){$data[] = array('result'=>'invalid','msg'=>'សូមបំពេញឈ្មោះ និងអ៊ីម៉េលរបស់អ្នក!');}
	else{
		extract($getData);
		
		if (filter_var($subscribe_email, FILTER_VALIDATE_EMAIL)) {
			$emailExist = mysqli_num_rows(qry_assoc_home("SELECT id FROM tblsubscription WHERE email='$subscribe_email' AND active=1 LIMIT 1"));
			if($emailExist>0){
				$data[] = array('result'=>'valid','msg'=>'អ្នកបានបញ្ចូលអ៊ីម៉េលម្តងរូចហើយ។ អរគុណ!');
			}
		}else{
			$data[] = array('result'=>'invalid','msg'=>'អ៊ីម៉េលមិនត្រឹមត្រូវ។ សូមពិនិត្យម្តងទៀត!');
		}
	}
	
	if(count($data)==0){	
		$emailExist = mysqli_num_rows(qry_assoc_home("SELECT id FROM tblsubscription WHERE email='$subscribe_email' AND active=0 LIMIT 1"));
		if($emailExist>0){
			$data[] = array('result'=>'valid','msg'=>'អ៊ីម៉េលរបស់អ្នកត្រូវដាក់ដំណើរការដោយជោគជ័យ។ អរគុណ!');
			qry_assoc_home("UPDATE tblsubscription SET active=1,datetime='$datetime' WHERE email='$subscribe_email' AND active=0 LIMIT 1");
		}else{
			qry_assoc_home("INSERT INTO tblsubscription SET name='$subscribe_name', email='$subscribe_email',datetime='$datetime'");
		}
		
		//----- start email -------------
		$to = $subscribe_email;
		$subject = 'Email Subscription on Cambodia-touristguide.com';
		//$to = 'sereyvuth_ung@yahoo.com';
		$msg = 'អ្នកបានបញ្ចូលអ៊ីម៉េលរបស់អ្នក​ទៅក្នុងប្រព័ន្ធចុះបញ្ជី​មគ្គុទ្ទេសក៍ទេសចរណ៍កម្ពុជា។ អ្នកនឹងទទួលបានព័ត៌មានថ្មីៗពីប្រព័ន្ធចុះបញ្ជី​មគ្គុទ្ទេសក៍ទេសចរណ៍កម្ពុជា តាមរយៈអ៊ីម៉េលនេះ។';
		//get subscriber email
		sentMail($subject,$to,$msg);
		//----- end email -------------
	}
	
	echo json_encode($data);	
}elseif($cmd=='registerExam'){
	$examid=$_POST['id'];
	$datetime=date("Y-m-d H:i:s");
	
	$msg='';$data='';
	if($userid>0){
		if(mysqli_num_rows(exec_query_utf8("SELECT * FROM tblcandidates WHERE examid=$examid AND userid=$userid"))==0 and $examid>0){
			exec_query_utf8("INSERT INTO tblcandidates(examid,userid,registerDate) VALUES($examid,$userid,'$datetime')");
		}else{
			$msg='invalid';
		}
	}else{
		$msg='notlogin';
	}
	echo json_encode(array('msg'=>$msg,'data'=>$data));	
}elseif($cmd=='getCourse'){
	$area=$_POST['area']==''?0:$_POST['area'];
	$year=date("Y");
	$today = date("Y-m-d");
	
	$result=1;$data='';
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE area_id=$area AND year=$year AND active=1 ORDER BY course_num ASC");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$course_id  = $select_row['id'];
		$register_date  = $select_row['register_date'];
		$start_ws  = $select_row['workshop_start_date'];
		$end_ws  = $select_row['workshop_end_date'];
		
		$examData = qry_arr("exam_date,result_released","tblexamschedule","year=$year AND area=$area AND exam_date>'$end_ws' AND active=1 ORDER BY exam_date ASC LIMIT 1");
		$exam_result_released=$examData['result_released'];		
		//if($exam_date<>''){$exam_date=date("d/m/Y",strtotime($exam_date));}else{$exam_date='N/A';	}		
		$total_participant = mysqli_num_rows(exec_query_utf8("SELECT id FROM tblcourseregister WHERE course_id=$course_id AND active=1"));
		
		$class_full = true;$allow_register=true;$course_ended = true;
		if($total_participant<$select_row['max_participant']){$class_full = false;}
		if(time()>=strtotime($select_row['register_date'])){$allow_register = false;}
		if($start_ws<>''){
			if(time()<strtotime($start_ws)){$course_ended = false;}
		}else{
			if(!$exam_result_released){$course_ended = false;}
		}
		
		$option_dt = 'disabled="disabled" style="color:#ff8c00;"';
		if(!$class_full and $allow_register and !$course_ended){$option_dt = 'value="'.$course_id.'"';}
		//$option_dt = 'value="'.$course_id.'"';
		
		$data.= '<option '.$option_dt.'>វគ្គទី '.$select_row['course_num'].' - ថ្ងៃទី '.date("d/m/Y",strtotime($register_date)).'</option>';
	}
	
	if(mysqli_num_rows($select_qry)==0){$result=0;}
	echo json_encode(array('result'=>$result,'data'=>$data));	
}elseif($cmd=='courseSelectList'){
	$year=$_POST['year'];
	
	$result=1;$data='';
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE id IN (SELECT course_id FROM tblcourseregister WHERE user_id='".$userid."' AND active=1) AND year=$year AND active=1 ORDER BY course_num ASC");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$course_id  = $select_row['id'];
		$area  = $select_row['area_id'];
		$register_date  = $select_row['register_date'];
		//$start_ws  = $select_row['workshop_start_date'];
		//$end_ws  = $select_row['workshop_end_date'];
		
		//$exam_date = singleCell_qry("exam_date","tblexamschedule","year=$year AND area=$area AND exam_date>'$end_ws' AND active=1 ORDER BY exam_date ASC LIMIT 1");
		$areaName = singleCell_qry("displayTitle","tblsubcategory","id=$area AND active=1 LIMIT 1");
		
		//if($exam_date<>''){$exam_date=date("d/m/Y",strtotime($exam_date));}else{$exam_date='N/A';	}	
		
		$data.= '<option value="'.$course_id.'">'.$areaName.' - វគ្គទី '.$select_row['course_num'].' (ចុះឈ្មោះ '.date("d/m/Y",strtotime($register_date)).')</option>';
	}
	
	if(mysqli_num_rows($select_qry)==0){$result=0;}
	echo json_encode(array('result'=>$result,'data'=>$data));	
}elseif($cmd=='courseResultList'){
	$year=$_POST['year'];
	
	$courseid_exam = '';
	$qry = exec_query_utf8("SELECT course_id FROM tblexamschedule WHERE result_released=1 AND active=1 ORDER BY exam_date ASC");
	while($row = mysqli_fetch_assoc($qry)){
		if($row['course_id']<>''){
			$courseid_exam .= ($courseid_exam<>''?',':'').$row['course_id'];
		}
	}
	if($courseid_exam == ''){$courseid_exam = '0';}
	
	$result=1;$data='';
	$select_qry = exec_query_utf8("SELECT * FROM tblcourseschedule WHERE id IN (SELECT course_id FROM tblcourseregister WHERE user_id='".$userid."' AND active=1) AND id IN ($courseid_exam) AND year=$year AND active=1 ORDER BY course_num ASC");
	while($select_row = mysqli_fetch_assoc($select_qry)){
		$course_id  = $select_row['id'];
		$area  = $select_row['area_id'];
		$register_date  = $select_row['register_date'];
		$start_ws  = $select_row['workshop_start_date'];
		$end_ws  = $select_row['workshop_end_date'];
		
		$exam_date = singleCell_qry("exam_date","tblexamschedule","year=$year AND area=$area AND exam_date>'$end_ws' AND active=1 ORDER BY exam_date ASC LIMIT 1");
		$areaName = singleCell_qry("displayTitle","tblsubcategory","id=$area AND active=1 LIMIT 1");
		
		if($exam_date<>''){$exam_date=date("d/m/Y",strtotime($exam_date));}else{$exam_date='N/A';	}	
		
		$data.= '<option value="'.$course_id.'">'.$areaName.' | វគ្គទី '.$select_row['course_num'].' (ចុះឈ្មោះ '.date("d/m/Y",strtotime($register_date)).' | សិក្ខាសាលា '.date("d",strtotime($start_ws)).'-'.date("d/m/Y",strtotime($end_ws)).' | ប្រលង '.$exam_date.')</option>';
	}
	
	if(mysqli_num_rows($select_qry)==0){$result=0;}
	echo json_encode(array('result'=>$result,'data'=>$data));	
}elseif($cmd=='resetPassword'){
	$username=$_POST['username'];
	$datetime=date("Y-m-d H:i:s");
	$formatDate = date("d M Y H:i A");
	
	$result='';$msg=''; 
	if($username<>''){
		$checkUser = exec_query_utf8("SELECT * FROM tblusers WHERE (code='$username' OR email='$username' OR mobile='$username') AND pending=0 AND active=1 LIMIT 1");		
		if(mysqli_num_rows($checkUser)>0){
			$userData = mysqli_fetch_array($checkUser,MYSQL_ASSOC);
			$guideid = $userData['id'];
			$to=$userData['email'];$toName = $userData['lastName'].''.$userData['firstName'];			
			if(filter_var($to, FILTER_VALIDATE_EMAIL)){
				//----- start email -------------
				$linkData = array('guideid'=>$guideid,'time'=>time());
				$resetLink = 'http://et.cambodia-touristguide.com/resetPassword/'.encodeString(serialize($linkData),$encryptKey);				
				$subject = "Reset Password | Cambodia Tourist Guide Refreshment Course";
				$message = '
					សួស្តី '.$toName.'
					<p>
					អ្នកបានស្នើរអោយផ្លាស់ប្តួរពាក្យសម្ងាត់របស់អ្នក។<br />
					សូមចុចលើ Link ខាងក្រោម ដើម្បីផ្លាស់ប្តួរពាក្យសម្ងាត់របស់អ្នកនៅក្នុងប្រព័ន្ធ បណ្តុះបណ្តាលវិក្រិត្យការ​មគ្គុទេ្ទសក៍ទេសចរណ៍។
					<br /><br />
					<a href="'.$resetLink.'">'.$resetLink.'</a>
					<br /><br />
					
					<strong>ចំណាំ៖ ប្រសិនបើអ្នកមិនបានស្នើរទេ សូមកុំអើពើនឹងសារនេះ ឬលុបសារនេះចោល។</strong>				
				'; 			
				$sent = sentMail($subject,$to,$message);
				//----- end email -------------
				
				if($sent){
					$result='success';$msg=$to;
				}else{
					$result='failed';$msg='មានបញ្ហាធ្ញើរទៅកាន់ប្រអប់សារ';
				}
			}else{
				$result='failed';$msg='Email Error!';
			}
		}else{
			$result='failed';$msg='អ៊ីម៉េល ឬ License ID មិនត្រឹមត្រូវ';
		}
	}else{
		$result='failed';$msg='ទិន្នន័យមិនគ្រប់គ្រាន់';
	}
	$data = array('result'=>$result,'msg'=>$msg);
	echo json_encode($data);	
}elseif($cmd=='searchguide'){
	$search_txt = post("search_txt"); if($search_txt==''){exit;}	
	$sql_condition = 'WHERE guides.status=1';
	$dateqry = '';
	if($search_txt<>''){
		if(validateDate($search_txt)){$dateqry = "OR guides.dob = '$search_txt'";}
		$sql_condition.= " AND (first_name LIKE '%$search_txt%' 
							OR users.first_name LIKE '%$search_txt%' 
							OR users.last_name LIKE '%$search_txt%' 
							OR CONCAT_WS(' ',users.last_name,users.first_name)  LIKE '%$search_txt%' 
							OR CONCAT_WS(' ',users.first_name,users.last_name)  LIKE '%$search_txt%' 
							OR users.first_name_khm LIKE '%$search_txt%' 
							OR users.last_name_khm LIKE '%$search_txt%' 
							OR CONCAT_WS(' ',users.last_name_khm,users.first_name_khm)  LIKE '%$search_txt%'
							OR CONCAT_WS(' ',users.first_name_khm,users.last_name_khm)  LIKE '%$search_txt%' 
							OR users.telephone LIKE '%$search_txt%' 
							$dateqry)";
	}
	if($sql_condition=='WHERE'){$sql_condition = '';}
	
	$guide_qry = qry_assoc_booking("SELECT * FROM users join guides on users.id=guides.user_id $sql_condition ORDER BY users.last_name,users.first_name ASC LIMIT 10");
	$i=1;$guideList = '';
	while($guide_row = mysqli_fetch_assoc($guide_qry)){	
		$dob = $guide_row['dob']==''?'គ្មាន':date("d/m/Y",strtotime($guide_row['dob']));
		$mobile = $guide_row['telephone']==''?'គ្មាន':$guide_row['telephone'];
		$guideList .= '<tr>
							<td style="width:70px;" class="tableCellCenter">'.enNum_khNum($i).'</td>
							<td>'.$guide_row['code'].'</td>							
							<td>'.$guide_row['last_name_khm'].' '.$guide_row['first_name_khm'].'</td>
							<td>'.$guide_row['last_name'].' '.$guide_row['first_name'].'</td>
							<td>'.$dob.'</td>
							<td>'.$mobile.'</td>														
						</tr>';
		
		
		$i++;
		
	}
	if($guideList==''){$guideList='<tr><td colspan="6" style="text-align:center; color:#c0434d;"><i class="fa fa-frown-o"></i> គ្មានទិន្នន័យទេ</td></tr>';}
	echo $guideList;
}



?>