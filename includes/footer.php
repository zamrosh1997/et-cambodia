<!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">            
            <!-- Copyright info -->
            <div style="float:left;">
            	<div style="display:inline-block;"><span class="engFont">&copy;</span> ២០១៥ <a href="\"><img src="/img/logo_xs.png" height="30" /></a></div>
                <div style="display:inline-block;"><i class="fa fa-phone"></i> (855) 12 999 032</div>
                <div style="display:inline-block;"><i class="fa fa-building-o"></i> ដីឡូតិ៍៣A ផ្លូវលេខ១៦៩ សង្កាត់វាលវង់ ខណ្ឌ៧មករា រាជធានីភ្នំពេញ</div>
            </div>
            <div style="float:right;">
            	<?php //echo 'Powered by <a href="http://gigbcambodia.com/"><img src="/img/gigb_logo.png" height="30"></a>'; ?>
            </div>             
      </div>
    </div>
  <div class="clearfix"></div>
  </div>
</footer> 	

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

						
                        <div>
								<!-- Button trigger modal -->
                                <div id="confirm_btn" data-toggle="modal" data-target="#confirm_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="confirm_modal" tabindex="-1" role="dialog" aria-labelledby="confirm_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="confirm_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="confirm_modalLabelBodyText">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <!--<button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">បិទ</button>-->
                                                <button type="button" class="btn btn-primary" id="confirm_actionBtn"></button>
                                                <input type="hidden" id="confirm_confirmData" value="" />
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                        </div>
                        
                        <div>
								<!-- Button trigger modal -->
                                <div id="yesno_btn" data-toggle="modal" data-target="#yesno_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="yesno_modal" tabindex="-1" role="dialog" aria-labelledby="yesno_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="yesno_modalLabel">
                                                
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="yesno_modalLabelBodyText">
                                               
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="yesno_modalCloseBtn">បិទ</button>
                                                <button type="button" class="btn btn-primary" id="yesno_actionBtn"></button>
                                                <input type="hidden" id="yesno_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                        </div>
                        
                        <div>
								<!-- Button trigger modal -->
                                <div id="searchguide_btn" data-toggle="modal" data-target="#searchguide_modal"></div>
                                <!-- Modal -->
                                <div class="modal fade" id="searchguide_modal" tabindex="-1" role="dialog" aria-labelledby="searchguide_modalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="searchguide_modalLabel">
                                                	<i class="fa fa-search"></i> ស្វែងរក
                                                </h4>
                                            </div>
                                            <div class="modal-body" id="searchguide_modalLabelBodyText">
                                               	 <form class="form-horizontal" role="form" id="searchguide_frm">
                                                 	<div class="form-group" style="margin-right: 0;margin-left: 0;">
                                                       <div class="input-group">        
                                                           <input type="text" class="form-control" id="search_txt" placeholder="ឈ្មោះពេញ (ខ្មែរឬឡាតាំង) ឬ ថ្ងៃកំណើត (1989-12-23) ឬ លេខទូរស័ព្ទ" required>
                                                           <span class="input-group-btn" >
                                                           		<button class="btn btn-default" id="searchguide_btn" type="submit">
                                                                	<i class="fa fa-search"></i>
                                                                </button>
                                                           </span>
                                                       </div>
                                                     </div>
                                                 </form>
                                                 <div>
                                                 	<table class="table table-striped table-bordered table-hover" id="searchguide_tbl">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                                <th>License ID</th>
                                                                <th>ឈ្មោះខ្មែរ</th>
                                                                <th>ឈ្មោះឡាតាំង</th>
                                                                <th>ថ្ងៃកំណើត</th>
                                                                <th>ទួរស័ព្ទ</th>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!--<div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="searchguide_modalCloseBtn">បិទ</button>
                                                <button type="button" class="btn btn-primary" id="searchguide_actionBtn"></button>
                                                <input type="hidden" id="searchguide_confirmData" value="" />
                                            </div>-->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                        </div>
                        <script>$("#searchguide_frm").submit(function(e){ searchguide(); e.preventDefault();});</script>