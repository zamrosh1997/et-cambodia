<?php
date_default_timezone_set('Asia/Phnom_Penh'); 
$encryptKey = 'etraining';

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "etraining";

$conn = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname) or die("Error " . mysqli_error($conn));

function exec_query($query){	
	    global $conn;
		$result = mysqli_query($conn, $query) or
		//$result = $db->query($query) or
		die("could not execute query $query");
		//die('<span style="color:red;">Error: The request not sent.</span>');
		return $result;
}

function exec_query_utf8($query){
		exec_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = exec_query($query);
		return $result;
	}

function post($var) {
	if(isset($_POST[$var])){
		return (trim($_POST[$var]));
	}else{
		return '';
	}
}
function get($var){
	if (isset($_GET[$var])) {
		return (trim($_GET[$var]));
	}else{
		return '';
	}
}

function encodeString($string,$key) {
		if(is_numeric($string)){$string = $string*120119898;}
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash ="";
		for ($i = 0; $i < $strLen; $i++) {
			$ordStr = ord(substr($string,$i,1));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
		}
		return $hash;
	}

function decodeString($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash = "";
		for ($i = 0; $i < $strLen; $i+=2) {
			$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= chr($ordStr - $ordKey);
		}
		if(is_numeric($hash)){$hash = $hash/120119898;}
		return $hash;
	}

function enNum_khNum($str){
	$khNum = array('០','១','២','៣','៤','៥','៦','៧','៨','៩');
	$newStr = '';
	for($i=0;$i<strlen($str);$i++){
		if(is_numeric(substr($str,$i,1))){
			$newStr .= $khNum[substr($str,$i,1)];
		}else{
			$newStr .= substr($str,$i,1);
		}		
	}
	return $newStr;
}

function calculateScore($rndQuestionid){
	$lessonid = singleCell_qry("lessonid","tblrandomquestion","id=$rndQuestionid LIMIT 1");
	
	//get total question allowed
	$totalQuestionAllowed = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion' LIMIT 1");
	
	//get all question to be answered
	$q_str = singleCell_qry("randomQuestionid","tblrandomquestion","id=$rndQuestionid LIMIT 1");
	$q_arr = explode(',',$q_str);
	$totalQ = count($q_arr);

	$totalScore = 0;
	$totalStdScore = 0;
	$totalCorrect = 0;
	
	//get all answers
	$answer_str = singleCell_qry("answer","tblrandomquestion","id=$rndQuestionid LIMIT 1");
	$answer_arr = explode(',',$answer_str);
	$chosedAnswer_arr = array();
	$result_arr = array();
	foreach($answer_arr as $value){
		$q_data = explode(':',$value);
		$chosedAnswer_arr[] = $q_data[0];
		$result_arr[] = $q_data[1];
		
		if($q_data[1]==1){$totalCorrect ++;}
	}
	
	$getStdScore = singleCell_qry("score","tblscoresetting","lessonid=$lessonid AND active=1 LIMIT 1");		
	$totalScore = number_format(($totalCorrect/$totalQ)*$getStdScore,3);
	
	//return $correctByLevel;
	
	return array('answered'=>$totalScore,'full'=>$getStdScore);
}

function isCorrect($questionid,$answer){
	$result = false;
	$correctAnswer = singleCell_qry("answer","tblquestions","id=$questionid ORDER BY id ASC LIMIT 1");	
	if($correctAnswer == $answer){$result = true;}
	return $result;
}

function getRandomQuestion($lessonid){
	$allowedDif = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedDifficulty' LIMIT 1");
	$allowedDif_arr = explode(',',$allowedDif);
	
	//get total question allowed
	$totalQuestionAllowed = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion' LIMIT 1");
		
	$getQestion = array();
	$q_qry = exec_query_utf8("SELECT id FROM tblquestions WHERE lessonid=$lessonid AND active=1 ORDER BY id ASC");
	while($q_row = mysqli_fetch_assoc($q_qry)){	
		$getQestion[] = $q_row['id'];			
	}
	//return false when there is not enough question posted
	if(count($getQestion)<$totalQuestionAllowed){return false;}
		
	//random the existing array item
	$rnd_key = array_rand($getQestion,$totalQuestionAllowed);
	$rndQuestion = array();
	foreach($rnd_key as $key => $value){
		$rndQuestion[] = $getQestion[$value];
	}
	return $rndQuestion;
}

function filesize_formatted($path)
{
    $size = filesize($path);
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function singleCell_qry($field,$tbl,$cond){
	$resultInArray = mysqli_fetch_assoc(exec_query_utf8("SELECT ".$field." FROM ".$tbl.($cond==''?'':' WHERE ').$cond));
	return $resultInArray[$field];
}

/*function getBetween($content,$start,$end){
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}*/


?>