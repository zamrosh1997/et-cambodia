
<div class="slider-features content process" style="margin-top:0;">
  <div class="container ">
    <div class="row">

    		<!--១  ចុះឈ្មោះ  វិញ្ញាប័នបត្រ -->
    		<div class="col-md-4 col-sm-4">
				<div class="onethree">
				  <div class="onethree-left">
					<div class="process-meta">
						<span>១</span>
                    </div>
				  </div>
                  <div class="onethree-right" onClick="javascript:window.location.href='/register';" style="cursor:pointer;">
                 <!-- <div class="onethree-right"> -->
					<!-- Title and meta -->
					<h5 class="color">ចុះឈ្មោះ / វិញ្ញាបនបត្រ</h5>
					<p class="meta">បង្កើតគណនី ដើម្បីទទួលបានវិញ្ញាបនបត្រពេលប្រឡងជាប់</p>
				  </div>
				  <div class="clearfix"></div>
				</div>
			  </div>
    		<!--២ សិក្សា ធ្វើតេស្ត -->
    		<div class="col-md-4 col-sm-4">
				<div class="onethree">
				  <div class="onethree-left">
					<div class="process-meta">
						<span>២</span>
                    </div>
				  </div>
				  <div class="onethree-right">
					<h5 class="color">សិក្សាមុខវិជ្ជា / ធ្វើតេស្ត</h5>
					<p class="meta">សិក្សាមុខវិជ្ជា ទាញយកឯកសារ និងធ្វើតេស្ត</p>
				  </div>
				  <div class="clearfix"></div>   
				</div>     
			 </div>

			 <!--៣ ប្រឡង​ -->
			  <div class="col-md-4 col-sm-4">
				<div class="onethree">
				  <div class="onethree-left">
					<div class="process-meta">
						<span>៣</span>
                    </div>
				  </div>
				  <div class="onethree-right">
					<h5 class="color">ប្រឡង</h5>
					<p class="meta">ប្រឡងលើមុខវិជ្ជា​ ដើម្បីតេស្តសមត្ថភាព</p>
				  </div>
				  <div class="clearfix"></div>  
				</div>      
			  </div>

			

			  
			
    </div>
  </div>
</div>

<!-- Below slider ends