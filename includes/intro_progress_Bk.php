<!-- Below slider starts -->
<div class="slider-features content process" style="margin-top:0;">
  <div class="container ">
    <div class="row">
    		<div class="col-md-4 col-sm-4">
				<div class="onethree">
				  <div class="onethree-left">
					<div class="process-meta">
						<span>១</span>
                    </div>
				  </div>
                  <div class="onethree-right" onClick="javascript:window.location.href='/register';" style="cursor:pointer;">
					<!-- Title and meta -->
					<h5 class="color">ចុះឈ្មោះ</h5>
					<p class="meta">បង្កើតគណនី ដើម្បីសិក្សាវគ្គវិក្រិត្យការ មគ្គុទ្ទេសក៍ទេសចរណ៍</p>
				  </div>
				  <div class="clearfix"></div>
				</div>
			  </div>
			  <div class="col-md-4 col-sm-4">
				<div class="onethree">
				  <div class="onethree-left">
					<div class="process-meta">
						<span>២</span>
                    </div>
				  </div>
				  <div class="onethree-right">
					<h5 class="color">សិក្សាមុខវិជ្ជា / សិក្ខាសាលា</h5>
					<p class="meta">សិក្សាមុខវិជ្ជា ទាញយកឯកសារ និងធ្វើតែស្ត</p>
				  </div>
				  <div class="clearfix"></div>   
				</div>     
			  </div>
			  <div class="col-md-4 col-sm-4">
				<div class="onethree">
				  <div class="onethree-left">
					<div class="process-meta">
						<span>៣</span>
                    </div>
				  </div>
				  <div class="onethree-right">
					<h5 class="color">ប្រលង / វិញ្ញាបនបត្រ</h5>
					<p class="meta">ប្រលងលើមុខវិជ្ជា និងទទួលវិញ្ញាបនបត្រ</p>
				  </div>
				  <div class="clearfix"></div>  
				</div>      
			  </div>
    </div>
  </div>
</div>

<!-- Below slider ends -->