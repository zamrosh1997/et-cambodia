<?php
$data = unserialize(decodeString($_GET['dt'],$encryptKey));
$server_host = $_SERVER['HTTP_HOST'];

$allowedPayment = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedPayment' LIMIT 1");
if($allowedPayment and count($data)==4){
	
$param = array(
			'loginId'=>singleCell_qry("settingValue","tblgeneralsetting","settingName='wingOnlinePayID' AND active=1 LIMIT 1"),
			'password' =>singleCell_qry("settingValue","tblgeneralsetting","settingName='wingOnlinePayPassword' AND active=1 LIMIT 1"),
			'item'=>singleCell_qry("settingValue","tblgeneralsetting","settingName='onlinePayItemName' AND active=1 LIMIT 1"),
			'amount'=>singleCell_qry("settingValue","tblgeneralsetting","settingName='registrationFee' AND active=1 LIMIT 1"),
			'merchant_name'=>singleCell_qry("settingValue","tblgeneralsetting","settingName='onlinePayCompanyName' AND active=1 LIMIT 1"),
			'notify_service'=>'http://'.$server_host.'/payment/wing_notif_service/'.$_GET['dt'],
			'notify_url'=>'http://'.$server_host.'/payment/result/?dt='.$_GET['dt'],
			'return_url'=>'http://'.$server_host.'/payment/summary/.'.$_GET['dt']
	);


$wingRedirectUrl = singleCell_qry("settingValue","tblgeneralsetting","settingName='wingRedirectUrl' AND active=1 LIMIT 1");
$url = singleCell_qry("settingValue","tblgeneralsetting","settingName='wingAuthenticateUrl' AND active=1 LIMIT 1");
$content = json_encode($param);
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($curl);
if ($response === FALSE)
echo "cURL Error: " . curl_error($curl);
else header("Location: $wingRedirectUrl?token=$response");

}else{
	header("Location: /payment/summary/".$_GET['dt']);	
}
?>