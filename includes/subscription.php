<!-- Newsletter starts -->

<div class="container newsletter"> 
  <div class="row">
    <div class="col-md-12">
      <div class="well">
               <h5><i class="fa fa-rss fa-fw"></i> តាមដានព័ត៌មានថ្មីៗ</h5>
               <p>សូមបំពេញទំរង់ខាងក្រោម ដើម្បីទទួលព័ត៌មានថ្មីតាមរយៈអ៊ីម៉េលរបស់អ្នក៖</p>
               <form id="subscribe_frm" class="form-inline" role="form">
                 <div class="form-group">
                   <input type="text" class="form-control subscribe_input" id="subscribe_name" placeholder="ឈ្មោះ" required>
                 </div>
                 <div class="form-group">
                   <input type="email" class="form-control subscribe_input" id="subscribe_email" placeholder="អ៊ីម៉េល" required>
                 </div>
                 <button type="submit" class="btn btn-default">ចូលរួម</button>
                 <span id="subscribe_msg"></span>
               </form>

              
        <!-- <p>ប្រសិនបើអ្នកមានចំងល់អ្វី សូមផ្ញើរអ៊ីម៉េលមកកាន់ <a href="mailto:info@et.cambodia-touristguide.com"> info@et.cambodia-touristguide.com</a></p> -->
      </div>
    </div> 
  </div>
</div>

<script>
	
	$(document).ready(function(e) {
        $("#subscribe_frm").submit(function(){subscription(); return false;});
    });	

</script>

<!-- Newsletter ends