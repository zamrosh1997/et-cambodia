<?php
date_default_timezone_set('Asia/Phnom_Penh'); 
$encryptKey = 'etraining';

$dbhost = "localhost";
$dbuser = "khshare_et";
$dbpass = "etraining";
$dbname = "khshare_etraining";

$conn = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname) or die("Error " . mysqli_error($conn));

function exec_query($query){	
	    global $conn;
		$result = mysqli_query($conn, $query) or
		//$result = $db->query($query) or
		die("could not execute query $query");
		//die('<span style="color:red;">Error: The request not sent.</span>');
		return $result;
}

function exec_query_utf8($query){
		exec_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");		
		$result = exec_query($query);
		return $result;
	}

function post($var) {
	if(isset($_POST[$var])){
		return (trim($_POST[$var]));
	}else{
		return '';
	}
}
function get($var){
	if (isset($_GET[$var])) {
		return (trim($_GET[$var]));
	}else{
		return '';
	}
}

function encodeString($string,$key) {
		if(is_numeric($string)){$string = $string*120119898;}
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash ="";
		for ($i = 0; $i < $strLen; $i++) {
			$ordStr = ord(substr($string,$i,1));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
		}
		return $hash;
	}

function decodeString($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		$j = 0;
		$hash = "";
		for ($i = 0; $i < $strLen; $i+=2) {
			$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= chr($ordStr - $ordKey);
		}
		if(is_numeric($hash)){$hash = $hash/120119898;}
		return $hash;
	}

function enNum_khNum($str){
	$khNum = array('០','១','២','៣','៤','៥','៦','៧','៨','៩');
	$newStr = '';
	for($i=0;$i<strlen($str);$i++){
		if(is_numeric(substr($str,$i,1))){
			$newStr .= $khNum[substr($str,$i,1)];
		}else{
			$newStr .= substr($str,$i,1);
		}		
	}
	return $newStr;
}

function getRandomQuestion($lessonid){
	$allowedDif = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedDifficulty' LIMIT 1");
	$allowedDif_arr = explode(',',$allowedDif);
	
	//get total question allowed
	$tq_str = singleCell_qry("settingValue","tblgeneralsetting","settingName='totalQuestion' LIMIT 1");
	$totalQuestion_value = explode(',',$tq_str);
	$totalQuestion_arr = array();
	foreach($totalQuestion_value as $value){
		$q_data = explode(':',$value);
		$totalQuestion_arr[$q_data[0]] = $q_data[1];
	}
	
	$rndQuestion = array();
	foreach($allowedDif_arr as $key => $value){		
		$q_byDif = array();
		$q_qry = exec_query_utf8("SELECT id FROM tblquestions WHERE lessonid=$lessonid AND difficultyLevel = $value AND active=1 ORDER BY id ASC");
		while($q_row = mysqli_fetch_assoc($q_qry)){	
			$q_byDif[] = $q_row['id'];			
		}
		//return false when there is not enough question posted
		if(count($q_byDif)<$totalQuestion_arr[$value]){return false;}
		
		//random the existing array item
		$rand_keys = array_rand($q_byDif, $totalQuestion_arr[$value]);
		foreach($rand_keys as $rnd_key => $rnd_value){
			// add to question array
			$rndQuestion[] = $q_byDif[$rnd_value];
		}
	}
	return $rndQuestion;
}

function filesize_formatted($path)
{
    $size = filesize($path);
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

function singleCell_qry($field,$tbl,$cond){
	$resultInArray = mysqli_fetch_assoc(exec_query_utf8("SELECT ".$field." FROM ".$tbl.($cond==''?'':' WHERE ').$cond));
	return $resultInArray[$field];
}

/*function getBetween($content,$start,$end){
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}*/


?>