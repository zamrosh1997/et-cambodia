<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");

session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false));
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// ========= End Session ============ //

if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

$pageName='Account | Tourist Guide Refreshment Course';
$pageCode='account';

$userid = $_SESSION['userid'];
$account_qry = exec_query_utf8("SELECT * FROM tblusers WHERE id=$userid AND active=1 LIMIT 1");
$userData = mysqli_fetch_array($account_qry,MYSQLI_ASSOC);

$marital=array('s'=>'នៅលីវ','m'=>'រៀបការ','d'=>'លែងលះ');
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h3><i class="fa fa-user fa-fw"></i> ព័ត៌មានគណនី</h3>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6">
                    
            </div>
        </div>		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                 		<fieldset>
                        		<legend>ព័ត៌មានផ្ទាល់ខ្លួន</legend>
                 				<form role="form" id="profile_frm">	                                    
                                    <div class="form-group">
                               <label for="inputCode" class="control-label">License ID:</label>
                               <div>                                 
                                 <div class="input-group">        
                                   <input type="text" class="form-control" id="inputCode" placeholder="License ID" value="<?=$userData['code']?>" disabled>
                                   <span class="input-group-addon" id="license_msg"></span>
                                 </div>
                               </div>
                             </div>
                             
                             <div class="form-group">
                               <label for="inputLName" class="control-label">គោត្តនាម</label> <span class="redStar">*</span></label>
                               <div>
                               		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control col-lg-6" id="inputLName" placeholder="គោត្តនាម" value="<?=$userData['lastName']?>" required disabled>                                    
                                            <span class="input-group-addon">ខ្មែរ</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control col-lg-6" id="inputLName_en" placeholder="Family Name" value="<?=$userData['lastNameEn']?>" required disabled>  
                                            <span class="input-group-addon">ឡាតាំង</span>
                                        </div>
                                    </div>
                                 
                                 
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputFName" class="control-label">នាម</label> <span class="redStar">*</span></label>
                               <div>
                               		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control" id="inputFName" placeholder="នាម" value="<?=$userData['firstName']?>" required disabled>   
                                            <span class="input-group-addon">ខ្មែរ</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control" id="inputFName_en" placeholder="First Name" value="<?=$userData['firstNameEn']?>" required disabled>
                                            <span class="input-group-addon">ឡាតាំង</span>
                                        </div>
                                    </div>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputDOB" class="control-label">ថ្ងៃខែឆ្នាំកំណើត</label> <span class="redStar">*</span></label>
                               <div>
                                 <div class="input-append input-group dtpicker">
                                    <input data-format="yyyy-MM-dd" type="text" id="inputDOB" class="form-control" value="<?=$userData['dob']?>" required disabled>
                                    <span class="input-group-addon add-on">
                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                    </span>
                                </div>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputGender" class="control-label">ភេទ</label> <span class="redStar">*</span></label>
                               <div>
                                 <select id="inputGender" class="form-control" required disabled>
                                 	<option value="m" <?=$userData['gender']=='m'?'selected':''?>>ប្រុស</option>
                                    <option value="f" <?=$userData['gender']=='f'?'selected':''?>>ស្រី</option>
                                 </select>
                               </div>
                             </div>   
                             <div class="form-group">
                               <label for="inputIDCard" class="control-label">អត្តសញ្ញាណប័ណ្ណសញ្ជាតិខ្មែរ / លេខលិខិតឆ្លងដែន</label> <span class="redStar">*</span></label>
                               <div>
                                 <input type="text" class="form-control" id="inputIDCard" placeholder="ID/Passport" value="<?=$userData['idCard']?>" required disabled>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inpuExpirytLicenseDate" class="control-label">ថ្ងៃខែផុតសុពលភាពអាជ្ញាបណ្ណ មគ្គុទ្ទេសក៍ទេសចរណ៍ៈ</label>
                               <div>
                                 <div class="input-append input-group dtpicker">
                                    <input data-format="yyyy-MM-dd" type="text" id="inpuExpirytLicenseDate" class="form-control" value="<?=$userData['licenseExpiredDate']?>" disabled>
                                    <span class="input-group-addon add-on">
                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                    </span>
                                </div>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputMobile" class="control-label">លេខទូរស័ព្ទផ្ទាល់ខ្លួន</label> <span class="redStar">*</span></label>
                               <div>
                                 <input type="text" class="form-control" id="inputMobile" placeholder="លេខទូរស័ព្ទ" value="<?=$userData['mobile']?>" required disabled>
                               </div>
                             </div>                                        
                             <div class="form-group">
                               <label for="inputEmail" class="control-label">អ៊ីម៉ែល</label>
                               <div>
                                 <input type="email" class="form-control" id="inputEmail" placeholder="អ៊ីម៉ែល" value="<?=$userData['email']?>" disabled>
                               </div>
                             </div>
                             <div class="form-group">
                                <button type="submit" name="profile_btn" class="btn btn-primary" disabled><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                            </div>
                           </form> 
                           <div id="profile_msg" class=""></div>                               
                        </fieldset>
                        <fieldset>
                        	<legend>ព័ត៌មានគណនី</legend>
                            	<form role="form" id="account_frm">	
                        			<div class="form-group">
                                        <label>ពាក្យសម្ងាត់បច្ចុប្បន្ន</label> <span class="redStar">*</span></label>
                                        <input type="password" id="currentPassword" class="form-control" placeholder="ពាក្យសម្ងាត់បច្ចុប្បន្ន" required>
                                    </div> 
                                    <div class="form-group">
                                        <label>ពាក្យសម្ងាត់ថ្មី</label> <span class="redStar">*</span></label>
                                        <input type="password" id="newPassword" class="form-control" placeholder="ពាក្យសម្ងាត់ថ្មី" required>
                                    </div>      
                                    <div class="form-group">
                                        <label>បញ្ជាក់ពាក្យសម្ងាត់ថ្មី</label> <span class="redStar">*</span></label>
                                        <input type="password" id="confirmNewPassword" class="form-control" placeholder="បញ្ជាក់ពាក្យសម្ងាត់ថ្មី" required>
                                    </div> 
                                	<div class="form-group">
                                        <button type="submit" name="account_btn" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> កែប្រែ</button>
                                    </div>
                                </form>                                
                                <div id="account_msg" class=""></div>
                        </fieldset>
                 </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>	
					
					$(document).ready(function(e) {	
						$("#account_frm").submit(function(){ updateAccount(); return false;});
						$("#profile_frm").submit(function(){ updateProfile(); return false;});
                    });
                </script>

</body>
</html>