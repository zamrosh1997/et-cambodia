<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$pageName='Register | Tourist Guide Refreshment Course';
$pageCode='Register';
$urlData = str_replace('.','',$_GET['dt']);

$allowedPayment = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedPayment' LIMIT 1");
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-archive fa-fw"></i> សង្ខេបព័ត៌មាននៃការបង់ប្រាក់ចុះឈ្មោះ</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
        	<?php					 
					$data = unserialize(decodeString($urlData,$encryptKey));
					if(!is_array($data)){
						 echo '<div class="form well"><h5 class="alert alert-danger" style="text-align:center;"><i class="fa fa-exclamation-triangle fa-fw"></i> ព័ត៌មានមិនត្រឹមត្រូវ</h5></div>';
					}else{
						 extract($data);
						 $timeToExpired = 30*60;						 
						 						 
						 $valid_link=true;
						 if(time() - $token > $timeToExpired){$valid_link = false;}		
						 
						 //echo time().' - '.$token.' = '.(time() - $token).' : '.($valid_link?1:0);
						 
						 //$valid_link=true;
						 if(!$valid_link){	
						 	echo '<div class="form well"><h5 class="alert alert-danger" style="text-align:center;"><i class="fa fa-clock-o fa-fw"></i> ព័ត៌មានបានផុតកំណត់</h5></div>';
						 }else{
							 //get price
							 $registrationFee = singleCell_qry("settingValue","tblgeneralsetting","settingName='registrationFee' AND active=1 LIMIT 1");	
							 if($registrationFee==''){$registrationFee=0;}
							 $currency = '$';	
							 
							 $systemFeatures = singleCell_qry("settingValue","tblgeneralsetting","settingName='systemFeatures' AND active=1 LIMIT 1");
							 $onlinePayItemName_kh = singleCell_qry("settingValue","tblgeneralsetting","settingName='onlinePayItemName_kh' AND active=1 LIMIT 1");
			?>
        		<div class="form well">
                	<h4 class="title khmerTitle"><i class="fa fa-circle-o fa-fw"></i> លក្ខណៈនៃប្រព័ន្ធ៖</h4>
                    <div>
                    	<?=$systemFeatures?>
                    </div>                
                </div>
                <div class="form well">
                    <!-- Title -->
                     <h4 class="title khmerTitle"><i class="fa fa-circle-o fa-fw"></i> បង់ប្រាក់</h4>
                                      <!-- Register form (not working)--> 
                             		<table class="table table-striped table-bordered table-hover" id="lessonList_tbl">
                                        <thead>
                                            <tr>
                                                <th>បរិយាយ</th>
                                                <th style="width:150px;" class="tableCellCenter">តម្លៃ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<tr>
                                            	<td><?=$onlinePayItemName_kh?></td>
                                                <td class="tableCellCenter"><?=$registrationFee.$currency?></td>
                                            </tr>                                          	                                   
                                        </tbody>
                                        <tfoot>
                                        	<tr style="background:#b6efef; font-weight:bold;">
                                            	<td style="text-align:right;">សរុប</td>
                                                <td class="tableCellCenter"><?=$registrationFee.$currency?></td>
                                            </tr> 
                                        </tfoot>
                                    </table>
                                      
                             <form class="form-horizontal" action="" method="post" role="form" id="payment_frm">                             
                                 <div class="form-group">
                                   <div class="col-lg-offset-3 col-lg-9" style="text-align:right;">
                                   <?php if(!isset($_SESSION['userid'])){?>
                                   	<button type="submit" name="skippay" class="btn btn-default"><span style="display:inline-block;height:30px;line-height: 30px; vertical-align:middle;">បង់ប្រាក់ពេលក្រោយ <i class="fa fa-angle-double-right fa-fw"></i></span></button>
                                   <?php }?>
                                   
                                   <?php if($allowedPayment){?>
                                     <button type="submit" name="paynow" class="btn btn-default"><i class="fa fa-shopping-cart fa-fw"></i> យល់ព្រម និងបង់ប្រាក់ <img src="/admin/images/wing_logo.png" height="30" /></button>
                                   <?php }else{ ?>
                                   	<button type="button" class="btn btn-default" disabled><i class="fa fa-shopping-cart fa-fw"></i> យល់ព្រម និងបង់ប្រាក់ <img src="/admin/images/wing_logo.png" height="30" /></button>
                                   <?php } ?>
                                   </div>
                                 </div>
                                 <div class="form-group">
                                    <div style="margin-left:20px;" id="register_msg"></div>
                                 </div>
                           </form>
                           
                    
               </div>
			<?php 
					}
				}
			?>
         </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->

<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
    //$("#payment_frm").submit(function(e){window.location.href="/payment/checkout"; e.preventDefault();});
	$("#skippay").click(function(){window.location.href="/payment/result/?status=success";});
});
</script>

</body>
</html>

<?php

if(isset($_REQUEST['paynow'])){
header("location: /payment/checkout/".$urlData);	
}

if(isset($_REQUEST['skippay'])){
header("location: /payment/result/?status=success");	
}

?>