<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$qData = decodeString(get('id'),$encryptKey);

if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

//check if continue exam
$datetime = date('Y-m-d H:i:s');
$examCont = false; $startTime = 0;
$data_parts = explode(",",$qData);
if(count($data_parts)==1){
	$lessonid=$data_parts[0];
	$startTime = strtotime($datetime);
}elseif(count($data_parts)==3){
	$rndQNumber = $data_parts[0];
	//$startTime = $data_parts[1];
	$startTime = strtotime($datetime); //if want to limit time for each question but not limit time for exam time. This's for php check expired
	$lessonid = singleCell_qry("lessonid","tblrandomquestion","id=$rndQNumber AND active=1");
	if($data_parts[2]=='cont'){$examCont = true;}
}
//check if id is integer
if(!is_numeric($lessonid) or $startTime == 0){header("location: /");exit;}
//check if exam session is expired or not
$examPeriod = singleCell_qry("settingValue","tblgeneralsetting","settingName='examPeriod' AND active=1 LIMIT 1");   
$examSessionTime = singleCell_qry("settingValue","tblgeneralsetting","settingName='examSessionTime' AND active=1 LIMIT 1");  
$endSessionTime =  $startTime+($examSessionTime*60);
if(strtotime($datetime) > $endSessionTime){
	//update exam status
	exec_query_utf8("UPDATE tblrandomquestion SET time_expired=1 WHERE id=$rndQNumber AND active=1 LIMIT 1");
	//header("location: /examResult/".encodeString($rndQNumber,$encryptKey));exit;
	header("location: /examlessons/");exit;
}

$lesson_title = 'N/A';
$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id=$lessonid AND active=1 LIMIT 1");
while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
	$lesson_title = $lesson_row['title'];
}

//check if id is valid
if(mysqli_num_rows($lesson_qry)==0){header("location: /");exit;}

$pageName=$lesson_title.' | Tourist Guide Refreshment Course';
$pageCode='exam';

//user course
$reg_data = qry_arr("id,course_id,pending,active","tblcourseregister","user_id=".$_SESSION['userid']." ORDER BY register_date DESC LIMIT 1");
$course_reg_status = $reg_data['active'];
$course_unpaid_status = $reg_data['pending'];
$reg_id = $reg_data['id'];
$course_id = $reg_data['course_id'];

//get start online exam status
$scheduleData = qry_arr("subject_id,start_online_exam","tblcourseschedule","id=$course_id and active=1 LIMIT 1");
$start_online_exam = $scheduleData['start_online_exam'];
$subjectData = json_decode($scheduleData['subject_id'],true);
$get_subjectData = $subjectData[$lessonid];
$subject_id_arr = array();$exam_date_set = false;//check if online exam date is set by any subject
$exam_date_set = $exam_expired = $exam_started = false;$exam_date = $exam_expired_date = '';$extend_day_txt = '';
if($get_subjectData['date']<>''){
	$exam_date_set = true;
	$exam_date = $get_subjectData['date'];
	if($get_subjectData['extend']>0){$extend_day=$get_subjectData['extend'];$extend_day_txt = '<span class="color bold">(បន្ថែម)</span>';}else{$extend_day=0;}
	$exam_expired_date = date("Y-m-d",strtotime($exam_date . ' + '.($examPeriod+$extend_day).' days'));
	if(strtotime($datetime)>=strtotime($exam_expired_date)){$exam_expired=true;}else{$exam_expired=false;}
	if(strtotime($datetime)>=strtotime($exam_date)){$exam_started=true;}else{$exam_started=false;}
}

$exam_date_kh = khmerDate($exam_date);
$exam_expired_date_kh = khmerDate($exam_expired_date);

//check page accessibility
if(!$start_online_exam or !$course_reg_status or $course_unpaid_status or !$exam_date_set or !$exam_started or $exam_expired){header("location: /examlessons"); exit;}

//creat exam record
$errMsg = 'សំណួរសម្រាប់មុខវិជ្ជានេះមិនទាន់ រៀបចំរួចរាល់ទេ!';

if(!$examCont){	
	//allowed exam times
	$allowedExamTimes = singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedExamTimes' AND active=1 LIMIT 1");   
	//check current exam
	$totalExamTimes = mysqli_num_rows(exec_query_utf8("SELECT id FROM tblrandomquestion WHERE userid=".$_SESSION['userid']." AND course_id=$course_id and lessonid=$lessonid AND exam=1 AND (finishedDate IS NOT NULL or time_expired=1) and active=1"));	
	$rndQNumber = 0;
	if($totalExamTimes<$allowedExamTimes){
		//random question for new test
		$rnd_qestion_arr = getRandomQuestion($lessonid,true);	
		if($rnd_qestion_arr <> false){
			$errMsg = '';
			$rnd_qestion_str = $rnd_qestion_arr['questions'];
			$rnd_option_str = $rnd_qestion_arr['options'];
			$inserted_id = exec_insert("INSERT INTO tblrandomquestion SET userid=".$_SESSION['userid'].",course_id=$course_id,lessonid=$lessonid,exam=1,allowed_time=$examSessionTime,randomQuestionid='$rnd_qestion_str',option_order='$rnd_option_str',takenDate='$datetime'");
			adduserlog('new_exam',$_SESSION['userid'],$inserted_id);
			$rndQNumber = $inserted_id;
			//change exam url
			header("location: /exam/".encodeString($rndQNumber.','.$startTime.',cont',$encryptKey));exit;
		}
	}else{header("location: /");exit;}
}else{
	$examDone = singleCell_qry("finishedDate","tblrandomquestion","id=$rndQNumber AND active=1");
	if($examDone<>''){header("location: /");exit;}
}
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <h3><i class="fa fa-book fa-fw"></i> ប្រលងមុខវិជ្ជា៖ <?php echo $lesson_title; ?></h3>
            <span class="boldTxt">ចាប់ផ្តើម៖</span> <?=$exam_date_kh?> | <span class="boldTxt">ផុតកំណត់៖</span> <?=$exam_expired_date_kh?> <?=$extend_day_txt?>
            	<div class="formy well">
                 	<h4>ការណែនាំ៖</h4>
                    <p>
                    	- សូមធ្វើការសម្រេចចិត្តអោយបានច្បាស់មុន នឹងជ្រើសចម្លើយត្រឹមត្រូវ <br />
                        - បន្ទាប់ពីចុចទៅកាន់សំណួរបន្ទាប់ អ្នកមិនអាចត្រលប់មកសំណួរដែលបានឆ្លើយរួចវិញបានទេ។ <br />
                        - សូមឆ្លើយបញ្ចប់គ្រប់សំណួរទាំងអស់ មុននឹងចាកចេញពីទំព័រសំណួរ ព្រោះអ្នកមិនអាចវិលត្រឡប់មកបន្តវិញបានទេ។
                    </p>
                </div>
            
      </div>
    </div>
  </div>
</div>

<!-- Page Heading ends -->

<!-- CTA Starts -->

<div class="container">
    <div class="row">
      <?php //include("includes/sidebar.php"); ?>
      <div class="col-md-12 col-sm-12">
        <div class="widget">
       	
        			<form  action="" method="post" role="form" enctype="multipart/form-data">
        			<div class="formy well">
                    <?php
					$questionid = 0;$lastQ=false;
					$q_cond = "id=$rndQNumber AND active=1";
					$rndQuestion = qry_arr("id,randomQuestionid,option_order","tblrandomquestion",$q_cond);
					$rndQ_str = $rndQuestion['randomQuestionid'];			
					$recordid = $rndQuestion['id'];		
					$rndOption_str = $rndQuestion['option_order'];	
					$rndOptionByQ_arr = explode(",",$rndOption_str);	
					if($rndQ_str<>''){
						//questions taking
						$rndQ_arr = explode(',',$rndQ_str);		
						$totalQ=count($rndQ_arr);
						//taken questions
						$answer_str = trim(singleCell_qry("answer","tblrandomquestion",$q_cond));	
						if($answer_str==''){$answer_arr = array();}else{$answer_arr = explode(',',$answer_str);}
						$totalTakenQ = count($answer_arr);
						//check if it's the last question
						if($totalQ-$totalTakenQ == 1){$lastQ=true;}
						
						//question to resume
						$q_no = $totalTakenQ+1;
						$q_resume=$rndQ_arr[$q_no-1];
						$questionid = $q_resume;						
						
						//$i=1;
						//foreach($rndQ_arr as $key => $value){
							$question_qry = exec_query_utf8("SELECT * FROM tblquestions WHERE id=$q_resume ORDER BY id ASC LIMIT 1");
							while($question_row = mysqli_fetch_assoc($question_qry)){
								
								$options = explode('|',trim($question_row['options']));
								$respondsive_class = intval(12/count($options));
								$options_str ='';
								$orderNum = array('ក','ខ','គ','ឃ','ង','ច');
								$answer = $question_row['answer'] - 1;
								
								$rndOption_arr = array();
								if($rndOption_str<>'' and isset($rndOptionByQ_arr[$q_no-1])){
									$rndOption_arr = explode("-",$rndOptionByQ_arr[$q_no-1]);
								}
								
								if(count($rndOption_arr)>1){
									//show the random options
									foreach($rndOption_arr as $key=>$value){									
										$options_str .= '<div style=" text-align:center;" class="col-lg-'.$respondsive_class.' col-md-'.$respondsive_class.'"><input type="radio" name="q_options" value="'.$value.'"> '.$orderNum[$key].'. ' .$options[$value-1] . '</div>';
									}
								}else{			
									//show the options in original order						
									foreach($options as $key=>$value){									
										$options_str .= '*<div style=" text-align:center;" class="col-lg-'.$respondsive_class.' col-md-'.$respondsive_class.'"><input type="radio" name="q_options" value="'.($key+1).'"> '.$orderNum[$key].'. ' .$value . '</div>';
									}
								}
								
								$progress = number_format((($q_no-1)/$totalQ)*100,(($q_no-1)==0?0:2));
								echo '
									<div class="progress progress-animated progress-striped active">
									  <div id="examProgressBar" class="progress-bar progress-bar-success" data-percentage="100" style="width: '.$progress.'%;"><span style="color:blue;">'.enNum_khNum($progress).'%</span></div>
									</div>
									<div>
										<div style="float:left;"><h4>ចូរជ្រើសរើសចម្លើយមួយ ដែលត្រឹមត្រូវ៖ (សំណួរទី '.enNum_khNum($q_no).' នៃសំណួរសរុប '.enNum_khNum($totalQ).')</h4></div>
										<div style="float:right;" id="test_timer_cover" class="label label-success"><h4><i class="fa fa-clock-o" style="font-size:16px; color:#ffffff;"></i> <span id="test_timer" style="color:#ffffff;"></span></h4></div>
										<div class="clearfix"></div>
									</div>
									<div style="padding:10px 0 10px 0;">
										<h5>'.enNum_khNum($q_no).' . '.$question_row['title'].'</h5>
										<div class="row" style="margin-top:15px;">'.$options_str.'</div>  
									</div>
								';
								//$i++;
							}
						//}
					}else{echo '<span style="color:red;"> <i class="fa fa-times fa-fw"></i> '.$errMsg.'</span>';}
					
					?>
                    </div>
                    
                    <div class="formy well">
                    	<div style="float:right;">
                        	<?php
							if($rndQ_str<>''){
								echo '<button type="submit" name="submitAnswer_btn" class="btn btn-primary">សំណួរបន្ទាប់ <i class="fa fa-angle-double-right fa-fw"></i></button>';
							}else{
								
								echo '<a href="/examlessons"><span class="btn btn-primary">ថយក្រោយ <i class="fa fa-arrow-circle-left fa-fw"></i></span></a>';
							}
							
							?>
                        	
                        </div>
                        <div style="float:left;​">
                        	<div id="submitAnswer_msg"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                   	</form>
        </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {

<?php
if($rndQ_str<>''){
	//$takenDate = singleCell_qry("takenDate","tblrandomquestion","id=$recordid AND active=1 limit 1");
	$takenDate = $datetime; //if want to limit time for each question but not limit time for exam time. This's for JavaScript check expired, then refresh.
	$endingDate = date("Y-m-d H:i:s",(strtotime($takenDate) + ($examSessionTime*60)));
	/*$newDateTime = new DateTime($takenDate);
	$newDateTime->add(new DateInterval('PT' . $examSessionTime . 'M'));
	$json = array(
		'year' => $newDateTime->format('Y'),
		'month' => $newDateTime->format('n') - 1,
		'day' => $newDateTime->format('j'),
		'hour' => $newDateTime->format('H'),
		'minute' => $newDateTime->format('i'),
		'second' => $newDateTime->format('s')
	);*/
	
	$totalPeriod = timePeriod(date("Y-m-d H:i:s"),$endingDate,false);

?>	
	var totalHours=<?=$totalPeriod['hours']?>,totalMinutes=<?=$totalPeriod['minutes']?>,totalSeconds=<?=$totalPeriod['seconds']?>;
	var examTimer = setInterval(function() {
		totalSeconds--;
		if(totalSeconds==-1 && totalMinutes>0){totalMinutes--;totalSeconds=59;}
		if(totalMinutes==-1 && totalHours>0){totalHours--;}
		$('#test_timer').text((totalHours>0?totalHours+' ម៉ោង ':'') + (totalMinutes>0?totalMinutes+' នាទី ':'') + (totalSeconds+' វិនាទី'));	
		//set bg color
		if(totalMinutes<=3 && totalSeconds%2==0){
			$('#test_timer_cover').removeClass("label-success").addClass("label-danger");
			$('#examProgressBar').removeClass("progress-bar-success").addClass("progress-bar-danger");
			
		}else{
			$('#test_timer_cover').removeClass("label-danger").addClass("label-success");
			$('#examProgressBar').removeClass("progress-bar-danger").addClass("progress-bar-success");
		}
		
		//stop countdown if time exceeded
		if(totalSeconds<=0 && totalMinutes==0 && totalHours==0){
			clearInterval(examTimer);
			popupMsg("yesno",'<i class="fa fa-clock-o fa-fw"></i> អស់ពេល','អ្នកបានប្រើប្រាស់ពេល​យូរពេក! សូមធ្វើការប្រលងម្តងទៀត!');
			setTimeout(function(){window.location.href="/examlessons";},5000);
		}
	}, 1000);
<?php } ?>
	
});
</script>
</body>
</html>

<?php
if(isset($_REQUEST['submitAnswer_btn'])){
	if(isset($_POST['q_options'])){
		$answer = trim($_POST['q_options']);
		exec_query_utf8("UPDATE tblrandomquestion SET answer=CONCAT(answer,if(answer='','',','),'$answer:".(isCorrect($questionid,$answer)?1:0)."') WHERE id=$recordid AND active=1 LIMIT 1");	
		$scores = calculateScore($recordid);
		exec_query_utf8("UPDATE tblrandomquestion SET score=".$scores['answered'].",fullScore=".$scores['full']." WHERE id=$recordid AND active=1 LIMIT 1");		
		//exec_query_utf8("UPDATE tblrandomquestion SET score=".$scores['answered'].",fullScore=".$scores['full']." WHERE id=$recordid AND active=1 LIMIT 1");	
		if($lastQ){	//if last quesiton, set done data and go to result.
			exec_query_utf8("UPDATE tblrandomquestion SET finishedDate='$datetime' WHERE id=$recordid AND active=1 LIMIT 1");	
			saveOnlineScore($reg_id,$lessonid);
			adduserlog('finish_exam',$_SESSION['userid'],$recordid);
			header("location: /examResult/".encodeString($recordid,$encryptKey));exit;
		}else{header("location: /exam/".encodeString($recordid.','.$startTime.',cont',$encryptKey));exit;}
	}else{
		echo '<script>$("#submitAnswer_msg").html(\'<span style="color:red;"><i class="fa fa-times fa-fw"></i> សូមជ្រើសចម្លើយ ដែលត្រឹមត្រូវ!\');</script></span>';
	}
	
}
?>