<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

if(!isset($_SESSION['userid'])){header("location: /");}else{
	$isIncourse = isIncourse($_SESSION['userid']);
	if($isIncourse['result'] and $isIncourse['active']){header("location: /");}	
}

$pageName='Register New Course | Tourist Guide Refreshment Course';
$pageCode='Register_new_course';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-pencil-square-o fa-fw"></i> ចុះឈ្មោះចូលរួមវគ្គសិក្សា</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                    <!-- Title -->
                     <h4 class="title">ជ្រើសរើសវគ្គសិក្សា៖</h4>
                                  <div class="form">
                                      <!-- Register form (not working)-->
                                      
                             <form class="form-horizontal" role="form" id="regCourse_frm">                             
                             <div class="form-group">
                               <label for="inputArea" class="col-lg-3 control-label">តំបន់ <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <select id="inputArea" class="form-control" required>
                                 	<option value="">--- ជ្រើសរើស ---</option>
                                    <?php
									
									$select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
									while($select_row = mysqli_fetch_assoc($select_qry)){
										echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
									}	
									
									?>
                                 </select>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputCourse" class="col-lg-3 control-label">វគ្គសិក្សា <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <select id="inputCourse" class="form-control" required>
                                 	<option value="">--- សូមជ្រើសរើសតំបន់ ---</option>
                                 </select>
                               </div>
                             </div>   
                             <div class="form-group">
                               <div class="col-lg-offset-3 col-lg-9">
                                 <button type="submit" class="btn btn-default"><i class="fa fa-check fa-fw"></i> ចុះឈ្មោះ</button>
                                 <button type="reset" class="btn btn-default"><i class="fa fa-times fa-fw"></i> លុបចោល</button>
                                 
                               </div>
                             </div>
                             <div class="form-group">
                             	<div style="margin-left:20px;" id="regCourse_msg"></div>
                           	 </div>
                           </form>
                          </div> 
                    </div>

                </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->

<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
    $("#regCourse_frm").submit(function(e){reg_course();e.preventDefault();});	
	$("#inputArea").change(getCourse);
});
</script>

</body>
</html>