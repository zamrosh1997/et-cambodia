<?php
include("includes/connect_db.php");

$profile_id = decodeString($_GET['id'],$encryptKey);
$profileData = qry_arr("lname_kh,fname_kh","translator_profile","id=$profile_id LIMIT 1");

$fullname_kh = $profileData['lname_kh'].' '.$profileData['fname_kh'];
?>
<!-- Page heading starts -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title><?=$fullname_kh?> | វិញ្ញាបនបត្រមគ្គុទ្ទេសក៍ទេសចរណ៍</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content=""><link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Favicon -->
  <link rel="shortcut icon" href="/img/favicon/favicon.png">
  <link href="/style/style.css" rel="stylesheet">
  
   <!-- jQuery -->
</head>
<body style="border:0; margin:0; padding:0;">  
<!--
            
    <span id="cert_fullname_kh" class="cert_txt">អ៊ុង សិរីវុធ</span>
    <span id="cert_fullname_en" class="cert_txt">Ung Sereyvuth</span>
    <span id="cert_gender" class="cert_txt">ប្រុស</span>
    <span id="cert_dob" class="cert_txt">១២ មករា ១៩៨៩</span>
    <span id="cert_pob_vil" class="cert_txt">រមន់</span>
    <span id="cert_pob_com" class="cert_txt">សំរោង</span>
    <span id="cert_pob_dis" class="cert_txt">សំរោង</span>
    <span id="cert_pob_pro" class="cert_txt">តាកែវ</span> -->
<!--<button id="btn_print">Print</button>-->
<div id="print_block" style="left:0; right:0; margin:auto;width:1133px; height:800px;">
    <div style="width:1133px; height:800px; background:url(http://et.cambodia-touristguide.com/documents/certificate_edited_sm_1440645064.jpg) no-repeat center center; background-size:contain;">  
    <div id="cert_qrcode_ng" class="cert_txt"><span><img src="http://et.cambodia-touristguide.com/qrcode/translator/<?=$_GET['id']?>" width="100" height="100" /></span></div>
    
    </div>
</div>
<script src="/js/jquery.js"></script>
<script src="/js/functions.js"></script>
<script>

$(document).ready(function(e) {
	$("#btn_print").click(function(){PrintElemCert("#print_block","Print Test");});	
});


</script>
</body>
</html>