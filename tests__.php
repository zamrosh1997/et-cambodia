<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

$pageName='Read List | Tourist Guide Refreshment Course';
$pageCode='read list';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h3><i class="fa fa-th-list fa-fw"></i> បញ្ជីមុខវិជ្ជាដែលបានធ្វើតេស្ត</h3>
        </div>
        
        <div style="float:right; width:300px;">
        		<div class="input-group custom-search-form">
					<input type="text" id="testList_search_txt" class="form-control" placeholder="ឈ្មោះមុខវិជ្ជា">
					<span class="input-group-btn">
						<button class="btn btn-default" id="testList_search_btn" type="button">
						<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
             <div class="formy well">
                    
                    <object data="http://et.cambodia-touristguide.com/download/25t5m4z5n5r4m45415v5a4w5l574o5i4r4z516z3j5l4h5e4c4g5o2e32616w5w5j5r4d464w304n2y2v3d5q29454z253v2541463b434o4m5f4" type="application/pdf" width="100%" height="800">
                    </object>      
                    
                    <img src="http://et.cambodia-touristguide.com/download/l4t5n4m5w574u7w5s6y8z5x729y5x6u706u629q5u6o767m6r77745u777v7293747q7y5f509s545t74757m777f6t7z5s5y837j53987h5h5y2137484w2440354x2w234i4p4o5" width="100%" />
                 	
             </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->

<!-- Newsletter starts -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>				
				
                </script>

</body>
</html>