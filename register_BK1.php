<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

if(isset($_SESSION['userid'])){header("location: /");}

$pageName='Register | Tourist Guide Refreshment Course';
$pageCode='Register';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-pencil-square-o fa-fw"></i> ចុះឈ្មោះចូលរៀនវគ្គវិក្រឹតការមគ្គុទ្ទេសក៍ទេសចរណ៍ ប្រចាំឆ្នាំ ២០១៥</h3>
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                    <!-- Title -->
                     <h4 class="title">ពាក្យសុំចុះឈ្មោះចូលរៀន វគ្គវិក្រឹត្យការមគ្គុទ្ទេសក៍ទេសចរណ៍</h4>
                                  <div class="form">
                                      <!-- Register form (not working)-->
                                      
                             <form class="form-horizontal" role="form" id="register_frm">
                             
                             <div class="form-group">
                               <label for="inputCode" class="col-lg-3 control-label">License ID:</label>
                               <div class="col-lg-9">
                                   <div class="input-group">        
                                       <input type="text" class="form-control" id="inputCode" placeholder="License ID">
                                       <span class="input-group-addon" id="license_msg"></span>
                                     </div>
                             	</div>
                             </div>
                             
                             <div class="form-group">
                               <label for="inputArea" class="col-lg-3 control-label">តំបន់ <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <select id="inputArea" class="form-control">
                                 	<option value="">--- ជ្រើសរើស ---</option>
                                    <?php
									
									$select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
									while($select_row = mysqli_fetch_assoc($select_qry)){
										echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
									}	
									
									?>
                                 </select>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputCourse" class="col-lg-3 control-label">វគ្គសិក្សា <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <select id="inputCourse" class="form-control">
                                 	<option value="">--- សូមជ្រើសរើសតំបន់ ---</option>
                                 </select>
                               </div>
                             </div>
                             
                             <div class="form-group">
                               <label for="inputLName" class="col-lg-3 control-label">គោត្តនាម <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                               		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control col-lg-6" id="inputLName" placeholder="គោត្តនាម" required>                                    
                                            <span class="input-group-addon">ខ្មែរ</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control col-lg-6" id="inputLName_en" placeholder="Family Name" required>  
                                            <span class="input-group-addon">ឡាតាំង</span>
                                        </div>
                                    </div>
                                 
                                 
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputFName" class="col-lg-3 control-label">នាម <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                               		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control" id="inputFName" placeholder="នាម" required>   
                                            <span class="input-group-addon">ខ្មែរ</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:0;">
                                        <div class="input-group">        
                                        	<input type="text" class="form-control" id="inputFName_en" placeholder="First Name" required>
                                            <span class="input-group-addon">ឡាតាំង</span>
                                        </div>
                                    </div>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputDOB" class="col-lg-3 control-label">ថ្ងៃខែឆ្នាំកំណើត <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <div class="input-append input-group dtpicker">
                                    <input data-format="yyyy-MM-dd" type="text" id="inputDOB" class="form-control" placeholder="ឆ្នាំ-ខែ-ថ្ងៃ (1989-12-23)" required>
                                    <span class="input-group-addon add-on">
                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                    </span>
                                </div>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputGender" class="col-lg-3 control-label">ភេទ <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <select id="inputGender" class="form-control">
                                 	<option value="">--- ជ្រើសរើស ---</option>
                                 	<option value="m">ប្រុស</option>
                                    <option value="f">ស្រី</option>
                                 </select>
                               </div>
                             </div>   
                             <div class="form-group">
                               <label for="inputIDCard" class="col-lg-3 control-label">អត្តសញ្ញាណប័ណ្ណសញ្ជាតិខ្មែរ / លេខលិខិតឆ្លងដែន <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <input type="text" class="form-control" id="inputIDCard" placeholder="ID/Passport" required>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inpuExpirytLicenseDate" class="col-lg-3 control-label">ថ្ងៃខែផុតសុពលភាពអាជ្ញាបណ្ណ មគ្គុទ្ទេសក៍ទេសចរណ៍ៈ <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <div class="input-append input-group dtpicker">
                                    <input data-format="yyyy-MM-dd" type="text" id="inpuExpirytLicenseDate" class="form-control"  placeholder="ឆ្នាំ-ខែ-ថ្ងៃ (1989-12-23)" required>
                                    <span class="input-group-addon add-on">
                                        <i data-time-icon="fa fa-times" data-date-icon="fa fa-calendar"></i>
                                    </span>
                                </div>
                               </div>
                             </div> 
                             <div class="form-group">
                               <label for="inputMobile" class="col-lg-3 control-label">លេខទូរស័ព្ទផ្ទាល់ខ្លួន <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <input type="text" class="form-control" id="inputMobile" placeholder="លេខទូរស័ព្ទ Ex: 012123456" required>
                               </div>
                             </div>                                         
                             <div class="form-group">
                               <label for="inputEmail" class="col-lg-3 control-label">អ៊ីម៉ែល <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <input type="email" class="form-control" id="inputEmail" placeholder="អ៊ីម៉ែល" required>
                               </div>
                             </div>
                             <div class="form-group">
                               <label for="inputPassword" class="col-lg-3 control-label">ពាក្យសម្ងាត់ <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <input type="password" class="form-control" id="inputPassword" placeholder="ពាក្យសម្ងាត់" required>
                               </div>
                             </div>
                             <div class="form-group">
                               <label for="inputConfirmPassword" class="col-lg-3 control-label">បញ្ជាក់ពាក្យសម្ងាត់ <span class="redStar">*</span></label>
                               <div class="col-lg-9">
                                 <input type="password" class="form-control" id="inputConfirmPassword" placeholder="វាយពាក្យសម្ងាត់ម្តងទៀត" required>
                               </div>
                             </div>
                                              
                             <div class="form-group">
                               <div class="col-lg-offset-3 col-lg-9">
                                 <div class="checkbox">
                                   <label>
                                     <input id="inputAgreePolicy" type="checkbox"> ខ្ញុំបាន/នាងខ្ញុំ សូមទទួលខុសត្រូវចំពោះរាល់ព័ត៌មានទាំងឡាយដែលបានផ្តល់ជូននេះ ថាពិតជាត្រឹមត្រូវតាមការពិត និងសូមយល់ព្រម និងសន្យាគោរពអនុវត្តឲ្យបានខ្ជាប់ខ្ជួនតាមលក្ខខណ្ឌ និងការណែនាំរបស់ Cambodia-touristguide.com។ 
                                   </label>
                                 </div>
                               </div>
                             </div>
                             <div class="form-group">
                               <div class="col-lg-offset-3 col-lg-9">
                                 <button type="submit" class="btn btn-default"><i class="fa fa-check fa-fw"></i> ចុះឈ្មោះ</button>
                                 <button type="reset" class="btn btn-default"><i class="fa fa-times fa-fw"></i> លុបចោល</button>
                                 
                               </div>
                             </div>
                             <div class="form-group">
                             	<div style="margin-left:20px;" id="register_msg"></div>
                           	 </div>
                           </form>
                          </div> 
                    </div>

                </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->

<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
    $("#register_frm").submit(function(){register();return false;});	
	$("#inputCode").blur(function(){retrieveGuideInfo($(this).val())});	
	$("#inputArea").change(getCourse);
	/*$("#popupSearchguide").click(function(e){popupSearchguide(); e.preventDefault();});*/
	
	/*$.ajax({
			url:"http://cambodia-touristguide.com/otgbs/public/json",
			dataType: 'jsonp',
			jsonp : "callback",
			type : 'POST',
			crossDomain: true,
			data:{},
			success:function(data, textStatus, jqXHR){			
				alert('success');
			},
			error:function(data){
				console.log(data);
			}
		});*/

});


</script>

</body>
</html>