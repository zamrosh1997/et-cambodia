<?php
include("includes/connect_db.php");
include("includes/checkSession.php");
$questionid = decodeString(get('id'),$encryptKey);

$docPath = '/documents/';
$nodocFilename = 'nodoc.pdf';
$nodocUrl = $docPath.$nodocFilename;
$lesson_title = '';$docUrl='';
$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id=(SELECT lessonid FROM tblrandomquestion WHERE id=$questionid AND active=1 LIMIT 1) AND active=1 LIMIT 1");
while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
	$lesson_title = $lesson_row['title'];
	$docUrl= $docPath .$lesson_row['filename'];
}

$pageName=$lesson_title.' | Tourist Guide Refreshment Course';
$pageCode='exam';

if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <h2><i class="fa fa-book fa-fw"></i> លទ្ធផលប្រលងមុខវិជ្ជា៖ <?php echo $lesson_title; ?></h2>
            	<div class="formy well">
                 	<h4>ការណែនាំ៖</h4>
                    <p>
                    	- សូមធ្វើការសម្រេចចិត្តអោយបានច្បាស់មុន នឹងជ្រើសចម្លើយត្រឹមត្រូវ <br />
                        - បន្ទាប់ពីចុចទៅកាន់សំណួរបន្ទាប់ អ្នកមិនអាចត្រលប់មកសំណួរដែលបានឆ្លើយរួចវិញបានទេ។
                    </p>
                </div>
            
      </div>
    </div>
  </div>
</div>

<!-- Page Heading ends -->

<!-- CTA Starts -->

<div class="container">
    <div class="row">
      <?php //include("includes/sidebar.php"); ?>
      <div class="col-md-12 col-sm-12">
        <div class="widget">
       	
        			<form  action="" method="post" role="form" enctype="multipart/form-data">
        			<div class="formy well">
                    <?php
					
					$q_cond = "id=$questionid AND active=1 LIMIT 1";
					
					//questions taking
					$rndQ_str = singleCell_qry("randomQuestionid","tblrandomquestion",$q_cond);	
					if($rndQ_str==''){$rndQ_arr = array();}else{$rndQ_arr = explode(',',$rndQ_str);}						
					$totalQ=count($rndQ_arr);
					
					//taken questions
					$answer_str = trim(singleCell_qry("answer","tblrandomquestion",$q_cond));	
					if($answer_str==''){$answer_arr = array();}else{$answer_arr = explode(',',$answer_str);}
					$totalTakenQ = count($answer_arr);
					
					if($totalQ==$totalTakenQ){
						$answered_score = singleCell_qry("score","tblrandomquestion",$q_cond);
						$full_score = singleCell_qry("fullScore","tblrandomquestion",$q_cond);
						$score_arr = explode('.',$answered_score);
						if(intval($score_arr[1])==0){$answered_score = intval($answered_score);}
						echo 'អ្នកបានបញ្ចប់ការប្រលង។ ពិន្ទុរបស់អ្នកគឺ៖ <span style="color:blue; font-weight:bold;">'.enNum_khNum($answered_score.'/'.$full_score).'</span>';
					}else{
						echo 'not yet finished';
					}
					
					?>
                    </div>
                    
                    <div class="formy well">
                    	<div style="float:right;">
                        	<button type="submit" name="submitAnswer_btn" class="btn btn-primary"><i class="fa fa-list-ul fa-fw"></i> បញ្ជីមុខវិជ្ជា</button>
                        </div>
                        <div style="float:left;​">
                        	<div id="submitAnswer_msg"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                   	</form>
        </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
	
});
</script>
</body>
</html>
<?php

if(isset($_REQUEST['submitAnswer_btn'])){
	header("location: /lessons");
}

?>