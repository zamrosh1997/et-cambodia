<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");
session_start(); //Start the session
ob_start();
//if(!isset($_SESSION['userid'])){ //If session not registered
  //header("location:/et/?next=".$_SERVER['REQUEST_URI']);
//}

if(!isset($_SESSION['userid'])){
  $userid = 0;
  //echo json_encode(array(false));
  //exit;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");

// ========= End Session ============ //


$ann_id = decodeString(get('id'),$encryptKey);
if(!is_numeric($ann_id)){header("location: /");}
$downloadPath = 'http://cambodia-touristguide.com/download/'; 
$ann_title = '';$ann_des = '';$datetime='';$attachedFile=array();$view=0;$annType=0;$annTypeName = '';$data='';
$announcement_qry = qry_assoc_home("SELECT * FROM tblannouncement WHERE id=$ann_id AND active=1 LIMIT 1");
while($announcement_row = mysqli_fetch_assoc($announcement_qry)){
	$ann_title = $announcement_row['title'];$ann_des = $announcement_row['description'];	
	$posted_date_kh = khmerDate($announcement_row['datetime']);
	$view=$announcement_row['view'];
	$annType=$announcement_row['type'];
	$annTypeName = singleCell_qry("title","tblsubcategory","id=$annType LIMIT 1");
	$data=$announcement_row['data'];
	if($announcement_row['filename']<>''){$attachedFile = explode("|",$announcement_row['filename']);}
}

$pageName=$ann_title.' | Tourist Guide Refreshment Course';
$pageCode='announcementDetail';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>

<?php
if(mysqli_num_rows($announcement_qry)==0){header("location: /");}
qry_assoc_home("UPDATE tblannouncement SET view=(view+1) WHERE id=$ann_id AND active=1 LIMIT 1");
?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h4><i class="fa fa-bullhorn fa-fw"></i> សេចក្តីប្រកាស៖ <?php echo $ann_title; ?></h4>           
        </div>
        
        <div style="float:right;">
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                 	<div style="float:left;">
                        <div>
                        	<!-- <i class="fa fa-clock-o fa-fw"></i> ផ្សាយ៖ <?php echo $posted_date_kh['full']; ?> -->
                        </div>
                    </div>
                    <div style="float:right;">
                    	<i class="fa fa-eye fa-fw"></i> បើកមើល៖ <?php echo number_format($view).' ដង'; ?>
                    </div>
                    <div class="clearfix"></div>                 	 
                 </div>
                 <div class="row">
                 	<div class="col-lg-3 col-md-3 col-sm-3">
                 		<h4><i class="fa fa-file-text-o fa-fw"></i> ខ្លឹមសារ</h4>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9" style="text-align:right;">                    
                    	<button id="btn_print" type="button" title="បោះពុម្ភទៅ Printer" class="btn btn-sm btn-default btn-primary margin_t_b textAlignLeft"><i class="fa fa-print fa-fw"></i></button>  
                    </div>
                 </div>
                 <div class="formy well" id="print_block">
                    <div>
                 		<?php echo $ann_des; ?>
                    </div>
                 </div>
                 <div class="formy well">
                 		<h4>ឯកសារភ្ជាប់៖</h4>                        
                        <?php	
						if(count($attachedFile)==0){
							echo '<div><i class="fa fa-file-text-o fa-fw"></i> គ្មានឯកសារភ្ជាប់</div>';
						}else{
							$attachedFile = array_reverse($attachedFile);
							$attchment_visible = false;
							foreach($attachedFile as $value){
								$fileurl = $downloadPath.encodeString($value,$encryptKey);
								$file_extension = strtolower(end(explode(".", $value)));
								$img_ext = array('jpg','jpeg','png','gif');
								$displayName = substr($value, 0, strrpos($value, '_'));
								if(strpos($ann_des,$value) === false or !in_array($file_extension,$img_ext)){
									echo '<div>'.file_icon($value).' <a href="'.$fileurl.'" target="_blank">'.$displayName.'</a></div>';
									$attchment_visible = true;
								}								
							}
							if(!$attchment_visible){
								echo '<div><i class="fa fa-file-text-o fa-fw"></i> គ្មានឯកសារភ្ជាប់</div>';
							}
						}
						?>
                 </div>
                 <div class="formy well">
                 	<h4><i class="fa fa-hand-o-right fa-fw"></i> ព័ត៌មានផ្សេងទៀត</h4>
                    <div>
                 		<?php
						
						$announcement_qry = qry_assoc_home("SELECT * FROM tblannouncement WHERE type = $annType AND id<>$ann_id AND active=1 ORDER BY datetime DESC LIMIT 5");
						while($announcement_row = mysqli_fetch_assoc($announcement_qry)){
							$ann_date_kh = khmerDate($announcement_row['datetime']);
							echo '<a href="/announcement/'.encodeString($announcement_row['id'],$encryptKey).'"><div class="article-similar"><i class="fa fa-files-o fa-fw"></i> '.$announcement_row['title'].'</span></div></a>';
						}	
						if(mysqli_num_rows($announcement_qry)==0){echo '<div>គ្មានទិន្នន័យ</div>';}					
						?>
                    </div>
                 </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->




<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>

$(function(){
$("#btn_print_excel").click(function(){exportData("<?=$annTypeName?>","<?=encodeString($mainQry,$encryptKey)?>");});
$("#btn_print").click(function(){PrintElem("#print_block","<?=$pageName?>");});	
});

</script>

</body>
</html>