<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

if(!isset($_SESSION['userid']) and !isset($_SESSION['adminid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

$tid = decodeString(get('tid'),$encryptKey);

//check if id is integer
if(!is_numeric($tid)){header("location: /");}

$lesson_title = '';$takenDate='';$finishedDate='';$score=0;$rndQid_arr=array();$userAnswer=array();$isExam=0;
$test_qry = exec_query_utf8("SELECT * FROM tblrandomquestion WHERE id=$tid AND active=1 LIMIT 1");
while($test_row = mysqli_fetch_assoc($test_qry)){
	$lesson_title = singleCell_qry("title","tbllessons","id=".$test_row['lessonid']." LIMIT 1");
	$takenDate=date("d/m/Y H:i",strtotime($test_row['takenDate']));
	$isExam=$test_row['exam'];
	
	$testCompleted = false;
	if($test_row['finishedDate']<>NULL){$testCompleted = true;}		
	$testPeriod= $testCompleted?timePeriod($test_row['takenDate'],$test_row['finishedDate'],true):'---';
	$score= $testCompleted?number_format($test_row['score'],(intval($test_row['score'])==$test_row['score']?0:1)).'/'.$test_row['fullScore']:'---';
	$userAnswer=($test_row['answer']==''?array():explode(',',$test_row['answer']));
	
	$rndQid_arr=explode(',',$test_row['randomQuestionid']);
	
	$rndOption_str = $test_row['option_order'];	
	$rndOptionByQ_arr = explode(",",$rndOption_str);	
}

//check if id is valid
if(mysqli_num_rows($test_qry)==0){header("location: /");}

//check allow show correct answer
$showCorrectAnswer = singleCell_qry("settingValue","tblgeneralsetting","settingName='".($isExam?'showCorrectAnswer_OE':'showCorrectAnswer')."' AND active=1 LIMIT 1");

$pageName='លទ្ធផល'.($isExam?'ប្រលងអនឡាញ':'តេស្ត').'សម្រាប់មុខវិជ្ជា '.$lesson_title.' | Tourist Guide Refreshment Course';
$pageCode='test view';

?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      		<div style="float:left;">
            <h3><i class="fa fa-book fa-fw"></i> លទ្ធផល<?=$isExam?'ប្រលងអនឡាញ':'តេស្ត'?>លើមុខវិជ្ជា៖ <?php echo $lesson_title; ?></h3>
            </div>
            <div style="float:right;">
            	<a href="/<?=$isExam?'examlessons':'tests'?>"><span class="btn btn-primary">ថយក្រោយ <i class="fa fa-arrow-circle-left fa-fw"></i></span></a>
            </div>
            <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Page Heading ends -->

<!-- CTA Starts -->

<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
        			<div class="row formy well">
                    	<div class="col-md-4 col-sm-4" style="text-align:center;">
                     		<?php echo 'ចាប់ផ្តើម៖ '.$takenDate; ?>
                        </div>
                        <div class="col-md-4 col-sm-4" style="text-align:center;">
                     		<?php echo 'ប្រើពេល៖ '.$testPeriod; ?>
                        </div>
                        <div class="col-md-4 col-sm-4" style="text-align:center;">
                     		<?php echo 'ពិន្ទុ៖ '.$score; ?>
                        </div>                    
                    </div>
                    
                    <div class="formy well">
                    	<?php
						$qList = ''; 
						 foreach($rndQid_arr as $key=>$value){
							 $q_data = qry_arr("title,options,answer","tblquestions","id=$value LIMIT 1");
							 $options = explode('|',$q_data['options']);
							 $answer = $q_data['answer'];
							 
							 //get user answer
							 $answered = explode(':',isset($userAnswer[$key])?$userAnswer[$key]:'');
							 
							 
							 
							 
							 $optList = '';
							 if(count($userAnswer)>=$key+1){
								 
							 
								 $totalrow = count($options);
								 $colnum = intval(12/$totalrow);
								 $orderNum = array('ក','ខ','គ','ឃ','ង','ច');
								 
								 $rndOption_arr = array();
								 if($rndOption_str<>'' and isset($rndOptionByQ_arr[$key])){
									$rndOption_arr = explode("-",$rndOptionByQ_arr[$key]);
								 }
								 
								 if(count($rndOption_arr)>1){
									 foreach($rndOption_arr as $optKey=>$optVal){
										 $optNo = $optVal;
										 $optList .= '<div style="'.((($optNo==$answered[0] and $answered[1]==1) or ($optNo==$answer and $showCorrectAnswer))?'color:green; font-weight:bold;':(($optNo==$answered[0] and $answered[1]==0)?'color:red; font-weight:bold;':'')).'" class="col-lg-'.$colnum.' col-md-'.$colnum.' col-sm-'.($colnum*2).'">
														'.((($optNo==$answered[0] and $answered[1]==1) or ($optNo==$answer and $showCorrectAnswer))?'<i class="fa fa-check fa-fw"></i> ':(($optNo==$answered[0] and $answered[1]==0)?'<i class="fa fa-times fa-fw"></i> ':'')).$orderNum[$optKey].'. '.$options[$optVal-1].'
													</div>';
										 
									 }
								 }else{								 
									 foreach($options as $optKey=>$optVal){
										 $optNo = $optKey+1;
										 $optList .= '<div style="'.((($optNo==$answered[0] and $answered[1]==1) or ($optNo==$answer and $showCorrectAnswer))?'color:green; font-weight:bold;':(($optNo==$answered[0] and $answered[1]==0)?'color:red; font-weight:bold;':'')).'" class="col-lg-'.$colnum.' col-md-'.$colnum.' col-sm-'.($colnum*2).'">
														'.((($optNo==$answered[0] and $answered[1]==1) or ($optNo==$answer and $showCorrectAnswer))?'<i class="fa fa-check fa-fw"></i> ':(($optNo==$answered[0] and $answered[1]==0)?'<i class="fa fa-times fa-fw"></i> ':'')).$orderNum[$optKey].'. '.$optVal.'
													</div>';
										 
									 }
								 }
								 $passed=true;
							 }else{
								 $passed=false;
								$optList = '<div class="col-lg-12 bold" style="color:red;">
													មិនទាន់បានឆ្លើយ
												</div>'; 
							 }
							 
							 
							 $qList .= ($qList==''?'':'<hr />').'
							 			<div class="'.((isset($answered[1]) and $answered[1]==1 and $passed)?'bdl-green':((isset($answered[1]) and $answered[1]==0 and $passed)?'bdl-red':'')).'">							 				
							 				<div class="bold" style="padding-bottom:10px;">'.((isset($answered[1]) and $answered[1]==1 and $passed)?'<span style="color:green;"><i class="fa fa-check-circle-o fa-fw"></i></span>':((isset($answered[1]) and $answered[1]==0 and $passed)?'<span style="color:red;"><i class="fa fa-times-circle-o fa-fw"></i></span>':'<span style="color:orange;"><i class="fa fa-spinner fa-fw"></i></span>')).enNum_khNum($key+1).'. '.$q_data['title'].'</div>
											<div class="row">'.$optList.'</div>
										</div>';
							 
						 }
						 echo $qList;
						 
						?>
                    
                    </div>
        </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {
	
});
</script>
</body>
</html>