<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");
session_start(); //Start the session
ob_start();

if(!isset($_SESSION['userid'])){
  $userid = 0;
}else{$userid = $_SESSION['userid'];}

$guideData = qry_arr("code","tblusers","id=$userid LIMIT 1");


// echo $_SESSION['userid']."ID";  // userid = 128

// =============== End Session ============ //

$pageName='Lessons Area | Tourist Guide Refreshment Course';
$pageCode='lessons'; 
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h3><i class="fa fa-th-list fa-fw"></i> បញ្ជីមុខវិជ្ជា</h3>
        </div>
        
        <div style="float:right; width:300px;">
        		<div class="input-group custom-search-form">
					<input type="text" id="lessonList_search_txt" class="form-control" placeholder="ឈ្មោះមុខវិជ្ជា">
					<span class="input-group-btn">
						<button class="btn btn-default" id="lessonList_search_btn" type="button">
						<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
        </div>
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>


      <div class="col-md-8 col-sm-8">
        <div class="widget">
                 <div class="formy well">
                 	<div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                	<option value="5">៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div> 
                    <div style="clear:both; padding:2px 0;"></div>

                        
                            <!-- <a href="#" class="btn btn-sm btn-primary pull-right">តេស្ត</a><br /><br /> -->

                    				<table class="table table-striped table-bordered table-hover" id="lessonList_tbl">
                                        <thead>
                                            <tr>
                                                <th>មុខវិជ្ជា</th>
                                                <th style=" min-width:120px;width:120px;" class="tableCellCenter"></th>
                                            </tr>
                                        </thead>
                                        <tbody>                                    
                                        </tbody>
                            </table>
                                
                 	
                 </div>
                 
                 <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                  </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->

<!-- Newsletter starts -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						$("#m_lessons").addClass('active');
						//--- end set active menu	
														
						$("#lessonList_search_btn").click(function(){lessonList('');});							
						//--- start navigation btn
						$("#nav_first").click(function(e){lessonList('first');});
						$("#nav_prev").click(function(e){lessonList('prev');});
						$("#nav_next").click(function(e){lessonList('next');});
						$("#nav_last").click(function(e){lessonList('last');});
						$("#nav_rowsPerPage").change(function(e){lessonList('');});
						$("#nav_currentPage").change(function(e){lessonList('goto');});
						//--- end navigation btn
						
						
						lessonList('');
						
                    });
                </script>

</body>
</html>