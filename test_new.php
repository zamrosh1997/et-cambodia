<?php
include("includes/connect_db.php");
// include("includes/checkSession.php");
session_start(); //Start the session
ob_start();

if(!isset($_SESSION['userid'])){
  $userid = 0;
}else{$userid = $_SESSION['userid'];} 

$lessonid = decodeString(get('id'),$encryptKey);
// echo $_SESSION['userid'].$lessonid;exit();
//check if quiz is allowed in default
$num_ch=singleCell_qry("settingValue","tblgeneralsetting","settingName='allowedQuiz' AND active=1 LIMIT 1");

if($num_ch==0){header("location: /lessons/");exit;}

//check if id is integer 
// if(!is_numeric($lessonid)){header("location: /");exit;}

$docPath = '/documents/';
$nodocFilename = 'nodoc.pdf';
$nodocUrl = $docPath.$nodocFilename;
$lesson_title = '';$docUrl='';$allowQuizByLesson=false;
$lesson_qry = exec_query_utf8("SELECT * FROM tbllessons WHERE id=$lessonid AND active=1 LIMIT 1");

while($lesson_row = mysqli_fetch_assoc($lesson_qry)){
	$lesson_title = $lesson_row['title'];
	$docUrl= $docPath .$lesson_row['filename'];
	$allowQuizByLesson=$lesson_row['allowQuiz']==1?true:false;
}

//check if quiz is allowed by lesson
// echo $allowQuizByLesson; exit();
if(!$allowQuizByLesson){header("location: /lessons/");exit;}

//check if id is valid

if(mysqli_num_rows($lesson_qry)==0){header("location: /");}

$pageName=$lesson_title.' | Tourist Guide Refreshment Course';
$pageCode='test';

/*** check if the users is not already logged in ***/
if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

//get test id
$test_id = singleCell_qry("id","tblrandomquestion","exam=0 and userid=".$_SESSION['userid']." AND lessonid=$lessonid AND finishedDate IS NULL AND active=1");

//user course
// $reg_data = qry_arr("course_id,pending,active","tblcourseregister","user_id=".$_SESSION['userid']." ORDER BY register_date DESC LIMIT 1");
// echo $reg_data['pending']."ssss"; exit();
// if($reg_data['pending'] or !$reg_data['active']){header("location: /");exit;}
// $course_id = $reg_data['course_id'];

//creat exam record
$errMsg = 'សំណួរសម្រាប់មុខវិជ្ជានេះមិនទាន់ រៀបចំរួចរាល់ទេ!';
$datetime = date('Y-m-d H:i:s');
if(mysqli_num_rows(exec_query_utf8("SELECT id FROM tblrandomquestion WHERE exam=0 and userid=".$_SESSION['userid']." AND lessonid=$lessonid AND finishedDate IS NULL AND active=1"))==0){
	//random question for new test
	$rnd_qestion_arr = getRandomQuestion($lessonid,false);	
	if($rnd_qestion_arr <> false){
		$errMsg = '';
		$rnd_qestion_str = $rnd_qestion_arr['questions']; 
		$rnd_option_str = $rnd_qestion_arr['options'];
		$inserted_id = exec_insert("INSERT INTO tblrandomquestion SET userid=".$_SESSION['userid'].",course_id=0,lessonid=$lessonid,randomQuestionid='$rnd_qestion_str',option_order='$rnd_option_str',takenDate='$datetime'");
		

		adduserlog('new_test',$_SESSION['userid'],$inserted_id);
	}
}elseif(isset($_GET['action']) and $_GET['action']=='renew' and !isset($_REQUEST['submitAnswer_btn'])){	
	//random question for renew an uncomplete test
	$rnd_qestion_arr = getRandomQuestion($lessonid,false);
	if($rnd_qestion_arr <> false){

		
		$errMsg = '';
		$rnd_qestion_str = $rnd_qestion_arr['questions'];
		$rnd_option_str = $rnd_qestion_arr['options'];
		$id = exec_query_utf8("UPDATE tblrandomquestion SET userid=".$_SESSION['userid'].",lessonid=$lessonid,randomQuestionid='$rnd_qestion_str',option_order='$rnd_option_str',takenDate='$datetime',answer='' WHERE id=$test_id");

		echo $id;exit();

		adduserlog('renew_test',$_SESSION['userid'],$test_id);


	}
}
?>
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<!-- Page heading starts -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <h3><i class="fa fa-book fa-fw"></i> ប្រលងមុខវិជ្ជា៖ <?php echo $lesson_title; ?></h3>
            	<div class="formy well">
                 	<h4>ការណែនាំ៖</h4>
                    <p>
                    	- សូមធ្វើការសម្រេចចិត្តអោយបានច្បាស់មុន នឹងជ្រើសចម្លើយត្រឹមត្រូវ <br />
                        - បន្ទាប់ពីចុចទៅកាន់សំណួរបន្ទាប់ អ្នកមិនអាចត្រលប់មកសំណួរដែលបានឆ្លើយរួចវិញបានទេ។
                    </p>
                    <h5>សំគាល់៖ ការធ្វើតេស្តជាការសាកល្បងចំណេះដឹងតែប៉ុណ្ណោះ។ ពិន្ទុមិនត្រូវបានយកជាផ្លូវការឡើយ ហើយក៏មិនមែនជាការប្រលងអនឡាញដែរ។</h5>
                </div>
            
      </div>
    </div>
  </div>
</div>

<!-- Page Heading ends -->

<!-- CTA Starts -->

<div class="container">
    <div class="row">
      <?php //include("includes/sidebar.php"); ?>
      <div class="col-md-12 col-sm-12">
        <div class="widget">
       	
        			<form  action="" method="post" role="form" enctype="multipart/form-data">
        			<div class="formy well">
                    <?php
					$questionid = 0;$lastQ=false;
					$q_cond = "exam=0 and userid=".$_SESSION['userid']." AND lessonid=$lessonid AND finishedDate IS NULL AND active=1";
					
					// print_r($q_cond);

					$rndQuestion = qry_arr("id,randomQuestionid,option_order","tblrandomquestion",$q_cond);
					// print_r($rndQuestion);
					$rndQ_str = $rndQuestion['randomQuestionid'];
					// echo $rndQ_str;

					$recordid = $rndQuestion['id'];
					echo $recordid;   // follow tam lesson  => id (tblreandomquestion)

					$rndOption_str = $rndQuestion['option_order'];
					// put it to array
					$rndOptionByQ_arr = explode(",",$rndOption_str);

				
					if($rndQ_str<>''){

						// echo "is working now";
						//questions taking
						$rndQ_arr = explode(',',$rndQ_str);
						// print_r($rndQ_arr);   // show in array element


						// 10
						$totalQ=count($rndQ_arr);
						// print_r($totalQ);
					
						//taken questions
						$answer_str = trim(singleCell_qry("answer","tblrandomquestion",$q_cond));
						// print_r($answer_str);
						// not working
						// echo "Showing :".$answer_str;exit();

						if($answer_str==''){$answer_arr = array();}else{$answer_arr = explode(',',$answer_str);}
						$totalTakenQ = count($answer_arr);

						// when clicked 1 vea dok (10-1,10-2,..)
						// echo $totalQ."-".$totalTakenQ;
						// 0
						// echo "Showing :".$totalTakenQ;exit();
						//check if it's the last question
						if($totalQ-$totalTakenQ == 1){$lastQ=true;}
						
						//question to resum
						$q_no = $totalTakenQ+1;
						$q_resume=$rndQ_arr[$q_no-1];
						$questionid = $q_resume;


						
						//$i=1;
						//foreach($rndQ_arr as $key => $value){
							$question_qry = exec_query_utf8("SELECT * FROM tblquestions WHERE id=$q_resume ORDER BY id ASC LIMIT 1");
							
							// clicked 1 show id of question ,...
							// echo "SELECT * FROM tblquestions WHERE id=$q_resume ORDER BY id ASC LIMIT 1";

							while($question_row = mysqli_fetch_assoc($question_qry)){
								
								$options = explode('|',trim($question_row['options']));
								$respondsive_class = intval(12/count($options));
								$options_str ='';
								$orderNum = array('ក','ខ','គ','ឃ','ង','ច');
								$answer = $question_row['answer'] - 1;
								
								$rndOption_arr = array();
								if($rndOption_str<>'' and isset($rndOptionByQ_arr[$q_no-1])){
									$rndOption_arr = explode("-",$rndOptionByQ_arr[$q_no-1]);
								}
								
								if(count($rndOption_arr)>1){
									//show the random options
									foreach($rndOption_arr as $key=>$value){									
										$options_str .= '<div style=" text-align:center;" class="col-lg-'.$respondsive_class.' col-md-'.$respondsive_class.'"><input type="radio" name="q_options" value="'.$value.'"> '.$orderNum[$key].'. ' .$options[$value-1] . '</div>';
									}
								}else{			
									//show the options in original order						
									foreach($options as $key=>$value){									
										$options_str .= '*<div style=" text-align:center;" class="col-lg-'.$respondsive_class.' col-md-'.$respondsive_class.'"><input type="radio" name="q_options" value="'.($key+1).'"> '.$orderNum[$key].'. ' .$value . '</div>';
									}
								}
								
								$progress = number_format((($q_no-1)/$totalQ)*100,(($q_no-1)==0?0:2));
								echo '
									<div class="progress progress-animated progress-striped active">
									  <div class="progress-bar progress-bar-success" data-percentage="100" style="width: '.$progress.'%;"><span style="color:blue;">'.enNum_khNum($progress).'%</span></div>
									</div>
									<div>
										<div style="float:left;"><h4>ចូរជ្រើសរើសចម្លើយមួយ ដែលត្រឹមត្រូវ៖ (សំណួរទឺ '.enNum_khNum($q_no).' នៃសំណួរសរុប '.enNum_khNum($totalQ).')</h4></div>
										<div style="float:right;" class="label label-success"><h4><i class="fa fa-clock-o" style="font-size:16px; color:#ffffff;"></i> <span id="test_timer" style="color:#ffffff;"></span></h4></div>
										<div class="clearfix"></div>
									</div>
									<div style="padding:10px 0 10px 0;">
										<h5>'.enNum_khNum($q_no).' . '.$question_row['title'].'</h5>
										<div class="row" style="margin-top:15px;">'.$options_str.'</div>  
									</div>
								';
								//$i++;
							}
						//}

					// error message : សំណួរសម្រាប់មុខវិជ្ជានេះមិនទាន់ រៀបចំរួចរាល់ទេ!
					}else{

						echo '<span style="color:red;"> <i class="fa fa-times fa-fw"></i> '.$errMsg.'</span>';}
					
					?>
                    </div>
                    
                    <div class="formy well">
                    	<div style="float:right;">
                        	<?php
                        	// rndQ_str  : randomquestionid

							if($rndQ_str<>''){
								echo '<button type="submit" name="submitAnswer_btn" class="btn btn-primary">សំណួរបន្ទាប់ <i class="fa fa-angle-double-right fa-fw"></i></button>';
							}else{
								
								echo '<a href="/lessons"><span class="btn btn-primary">ថយក្រោយ <i class="fa fa-arrow-circle-left fa-fw"></i></span></a>';
							}
							
							?>
                        	
                        </div>
                        <div style="float:left;​">
                        	<div id="submitAnswer_msg"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                   	</form>
        </div>
      </div>
      
    </div>
</div>

<!-- CTA Ends -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

<script>
$(document).ready(function(e) {

<?php
if($rndQ_str<>''){
	$takenDate = singleCell_qry("takenDate","tblrandomquestion","exam=0 and userid=".$_SESSION['userid']." AND lessonid=$lessonid AND finishedDate IS NULL AND active=1");
	$newDateTime = new DateTime($takenDate);
	$json = array(
		'year' => $newDateTime->format('Y'),
		'month' => $newDateTime->format('n') - 1,
		'day' => $newDateTime->format('j'),
		'hour' => $newDateTime->format('H'),
		'minute' => $newDateTime->format('i'),
		'second' => $newDateTime->format('s')
	);
	
	$totalPeriod = timePeriod($takenDate,date("Y-m-d H:i:s"),false);

?>	
	var start = new Date(<?=$json['year']?>,<?=$json['month']?>,<?=$json['day']?>,<?=$json['hour']?>,<?=$json['minute']?>,<?=$json['second']?>),totalHours=<?=$totalPeriod['hours']?>,totalMinutes=<?=$totalPeriod['minutes']?>,totalSeconds=<?=$totalPeriod['seconds']?>;
	setInterval(function() {
		totalSeconds++;
		if(totalSeconds>0 && totalSeconds%60==0){totalMinutes++;totalSeconds=0;}
		if(totalMinutes>0 && totalMinutes%60==0){totalHours++;}
		$('#test_timer').text((totalHours>0?totalHours+' ម៉ោង ':'') + (totalMinutes>0?totalMinutes+' នាទី ':'') + (totalSeconds+' វិនាទី'));		
	}, 1000);
<?php } ?>
	
});
</script>
</body>
</html>


<?php
if(isset($_POST['submitAnswer_btn'])){
	// echo "submitAnswer_btn";
	// id option click
	// echo $_POST['q_options']; exit();
	
	if(isset($_POST['q_options'])){

		// echo "ID option:".$_POST['q_options']."<br />";

		$answer = trim($_POST['q_options']);

		// echo "question_id:".$questionid."/answer:".$answer;exit();
		// echo isCorrect($questionid,$answer);exit();
		// echo "show answer ID :".$answer;

		if(isCorrect($questionid,$answer)){
			// echo "is correct";exit();
			exec_query_utf8("UPDATE tblrandomquestion SET answer=CONCAT(answer,if(answer='','',','),'$answer:1') WHERE id=$recordid AND active=1 LIMIT 1");
		}else{
			// echo "is wrong";exit();
			exec_query_utf8("UPDATE tblrandomquestion SET answer=CONCAT(answer,if(answer='','',','),'$answer:0') WHERE id=$recordid AND active=1 LIMIT 1");
		}
		
		// echo $lastQ;

		if($lastQ){	

			$scores = calculateScore($recordid);
			// print_r($scores);

			exec_query_utf8("UPDATE tblrandomquestion SET score=".$scores['answered'].",fullScore=".$scores['full'].",finishedDate='$datetime' WHERE id=$recordid AND active=1 LIMIT 1");	
			adduserlog('finish_test',$_SESSION['userid'],$test_id);
			header("location: /tests/".encodeString($recordid,$encryptKey));
		}else{
			header("location: ".str_replace("/renew","",$_SERVER['REQUEST_URI']));
		}	
	}else{
		echo '<script>$("#submitAnswer_msg").html(\'<span style="color:red;"><i class="fa fa-times fa-fw"></i> សូមជ្រើសចម្លើយ ដែលត្រឹមត្រូវ!\');</script></span>';
	}
	
}
?>