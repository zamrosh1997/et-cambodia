<?php
include("includes/connect_db.php");
include("includes/checkSession.php");

//if(!isset($_SESSION['userid'])){header("location:/login?next=".$_SERVER['REQUEST_URI']);}

$pageName='បញ្ជីវគ្គសិក្សា | Tourist Guide Refreshment Course';
$pageCode='courseList';
?>
<!-- Page heading starts -->
<?php include("includes/header.php");?>
<?php include("includes/css_script.php");?>
<?php include("includes/header_menu.php");?>
  <!-- Seperator -->
  <div class="sep"></div>
  <!-- Header ends -->
<div class="page-head">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      	<div style="float:left;">
        	<h3><i class="fa fa-table fa-fw"></i> បញ្ជីវគ្គសិក្សា</h3>
        </div>
        
        <!--<div style="float:right; width:300px;">
        </div>-->
        		
      </div>
    </div>
  </div>
</div>
<!-- Page Heading ends -->
<!-- CTA Starts -->
<div class="container">
    <div class="row">
      <?php include("includes/sidebar.php"); ?>
      <div class="col-md-8 col-sm-8">
        <div class="widget">
        		 <div class="well">
                 					<div class="form-group col-lg-4 col-md-4 col-sm-4">
                                        <label>ប្រចាំឆ្នាំ</label>
                                        <select class="form-control input-sm" id="course_year">
                                               <option value="0" data-expired="0">--- ជ្រើសរើស ---</option>
                                         <?php												
												$year_qry = exec_query_utf8("SELECT year FROM tblcourseschedule WHERE active=1 GROUP BY year ORDER BY year ASC");
												while($year_row = mysqli_fetch_assoc($year_qry)){
													echo '<option value="'.$year_row['year'].'">'.$year_row['year'].'</option>';
												}	
                                         ?>  
                                         </select>
                                    </div> 
                                    <div class="form-group col-lg-4 col-md-4 col-sm-4">                        
                                                <label>តំបន់</label>
                                                <select class="form-control input-sm" id="course_area">
                                                       <option value="0">--- ជ្រើសរើស ---</option>
                                                 <?php
                                                        $select_qry = exec_query_utf8("SELECT item.id id,item.displayTitle displayTitle FROM tblsubcategory item join tblmaincategory type on item.mainCategoryid=type.id WHERE type.title='registrationArea' AND item.active=1 ORDER BY item.id ASC");
                                                        while($select_row = mysqli_fetch_assoc($select_qry)){
                                                            echo '<option value="'.$select_row['id'].'">'.$select_row['displayTitle'].'</option>';
                                                        }	
                                                 ?>  
                                                 </select>
                                    </div>
                                    <div class="clearfix"></div>
                 
                 
                 </div>
                 
                 <div class="form well">
                 	<div>
                                	<div style="float:right;">
                                		<label>
                                        	<div style="display:inline-block">ចំនួយទិន្នន័យ ក្នុង១ទំព័រ: </div>
                                            <div style="display:inline-block">
                                                <select class="form-control input-sm" id="nav_rowsPerPage">
                                                	<option value="5">៥</option>
                                                    <option value="10" selected>១០</option>
                                                    <option value="20">២០</option>
                                                    <option value="30">៣០</option>
                                                </select>
                                            </div>
                                         </label>
                                    </div>
                                    <div style="float:left;">
                                		<label>
                                        	<span id="nav_info"></span>
                                         </label>
                                    </div>
                                </div>
                    <div style="clear:both; padding:2px 0;"></div>
                    				<table class="table table-striped table-bordered table-hover" id="courseList_tbl">
                                        <thead>
                                            <tr>
                                                <th style="width:70px;" class="tableCellCenter">ល.រ.</th>
                                                <th style="width:50px;">ឆ្នាំ</th>
                                                <th>តំបន់</th>
                                                <th>វគ្គ</th>
                                                <th>ចូលរៀន</th>
                                                <th>ប្រលងអនឡាញ</th>
                                                <th>ប្រលង Paper</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                
                 	
                 </div>
                 
                 <div class="form-group" style="width:100%; text-align:center;">
                                    <button type="submit" class="btn btn-default" id="nav_first"><i class="fa fa-fast-backward"></i> ដំបូង</button>
                                    <button type="submit" class="btn btn-default" id="nav_prev"><i class="fa fa-caret-left"></i> ថយ</button>
                                    <select class="nav_pageNum btn btn-default" id="nav_currentPage">
                                         <option value="1">1</option>
                                         <option value="2">2</option>
                                         <option value="3">3</option>
                                         <option value="4">4</option>
                                     </select>
                                    <button type="submit" class="btn btn-default" id="nav_next">បន្ទាប់ <i class="fa fa-caret-right"></i></button>
                                    <button type="submit" class="btn btn-default" id="nav_last">ចុងក្រោយ <i class="fa fa-fast-forward"></i></button>
                  </div>
        </div>
     </div>
      
   </div>
</div>

<!-- CTA Ends -->

<!-- Newsletter starts -->
<?php include("includes/subscription.php"); ?>
<?php include("includes/footer.php"); ?>
<?php include("includes/script.php"); ?>

				<script>					
					// tooltip demo
					$('.tooltip-des').tooltip({
						selector: "[data-toggle=tooltip]",
						container: "body"
					})
					
					$(document).ready(function(e) {	
						//--- start set active menu
						//$("#m_lessons").addClass('active');
						//--- end set active menu	
														
						$("#courseList_search_btn").click(function(){courseList('');});	
						$("#course_year,#course_area").change(function(){courseList('');});
						//--- start navigation btn
						$("#nav_first").click(function(e){courseList('first');});
						$("#nav_prev").click(function(e){courseList('prev');});
						$("#nav_next").click(function(e){courseList('next');});
						$("#nav_last").click(function(e){courseList('last');});
						$("#nav_rowsPerPage").change(function(e){courseList('');});
						$("#nav_currentPage").change(function(e){courseList('goto');});
						//--- end navigation btn						
						
						courseList('');						
                    });
                </script>

</body>
</html>